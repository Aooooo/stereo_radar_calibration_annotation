import os
import json
import numpy as np
from glob import glob
from tqdm import tqdm

import src.GMM_covariance_center as gmm
import src.calibrate_lstsq as registration

def readConfig():
    """
    Function:
        Read the registration configuration.
    """
    config_filename = "./registration_config.json"
    with open(config_filename) as json_file:
        registration_config = json.load(json_file)
    return registration_config

def checkoutDir(dir_name):
    """
    Function:
        Checkout if the directory is exists, if not then create one

    Args:
        dir_name            ->          directory name
    """
    if not os.path.exists(dir_name):
        os.mkdir(dir_name)
    else:
        file_list = glob(os.path.join(dir_name, '*'))
        if len(file_list) is not 0:
            for f in file_list:
                os.remove(f)

def checkoutFiles(file_dir, file_prefix):
    """
    Function:
        checkout if the target files exist in the target directory

    Args:
        file_dir                ->          target directory
        file_prefix             ->          target file prefix
    """
    files = os.path.join(file_dir, file_prefix) + "*"
    print(files)
    if len(glob(files)) == 0:
        return False
    else:
        return True

if __name__ == "__main__":
    # read registration config file
    registration_config = readConfig()
    # separate gmm config from registration configure
    gmm_config = registration_config["GMM_models"]
    # separate registration config
    registr_config = registration_config["registration"]

    # checkout directories and files existance for GMM
    if not checkoutFiles(gmm_config["stereo_reflector_dir"], gmm_config["stereo_reflector_prefix"])\
    or not checkoutFiles(gmm_config["radar_reflector_dir"], gmm_config["radar_reflector_prefix"]):
        raise ValueError("Something wrong with the stereo points cloud or radar points cloud.")
    else:
        checkoutDir(gmm_config["save_dir"])
        checkoutDir(gmm_config["fig_dir"])
        print("Starting GMM models simulation....")
    # excute gmm to find stereo and radar gmm models in order to calibrate them
    gmm.main(np.array(gmm_config["points_sequence"]), gmm_config["frame_delay"],\
            gmm_config["stereo_reflector_dir"], gmm_config["radar_reflector_dir"], \
            gmm_config["stereo_reflector_prefix"], gmm_config["radar_reflector_prefix"], \
            gmm_config["save_dir"], gmm_config["fig_dir"], gmm_config["x_threshold"], \
            gmm_config["z_threshold"], gmm_config["magnitude_threshold"],\
            gmm_config["if_save"], gmm_config["if_plot"])

    # checkout directories and files existance for registration
    if not checkoutFiles(gmm_config["save_dir"], "stereo_GM_mean_"):
        raise ValueError("Not getting results from GMM models, the registration holds up.")
    else:
        checkoutDir(registr_config["result_dir"])
        print("Starting Radar-Stereo calibration")
    registration.main(gmm_config["save_dir"], registr_config["result_dir"], gmm_config["fig_dir"],\
                    registr_config["if_plot"])
