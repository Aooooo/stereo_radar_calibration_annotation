# Registration of Radar and Stereo

## Configure the Parameters
First of all, parameters need to be filled/changed in
```
registration_config.json
```

**Note:** the most important parameter is `points_sequence`.

## Perform Registration 
To run registration,
```
registration_process.py
```

**Note:** `sensors_match_samples` stores all the figures. `registration_matrix` stores the final registration matrices.

## Other Functions
All other source codes are stored in directory `others`, which includes
```
compare_in_polar.py
```
and
```
verify_registr_mat.py
```

**Note:** These codes still need to be debugged.
