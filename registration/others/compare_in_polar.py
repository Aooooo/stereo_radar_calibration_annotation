import numpy as np
import cv2
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
from sklearn import mixture
from tqdm import tqdm
from glob import glob
import os
from sklearn.cluster import DBSCAN
from collections import Counter

def getTotoalNum(target_dir):
    """
    Function:
        Get the total number of images, prepare for the sequence number

    Args:
        img_dir             ->          image directory for counting
    """
    all_files = glob(target_dir + "*.npy")
    return len(all_files)

def readRAD(radar_dir, frame_id):
    """
    Function:
        Get the 3D FFT results of the current frame.
    
    Args:
        radar_dir           ->          radar directory
        frame_id            ->          frame that is about to be processed.
    """
    return np.load(radar_dir + "%.6d.npy"%(frame_id))

def ReadPcl(pcl_dir, pcl_name_prefix, frame_index):
    """
    Function:
        Read points from sensors

    Args:
        pcl_dir             ->      point cloud directory
        pcl_name_prefix     ->      point cloud file name prefix
        frame_index         ->      which frame to be read
    """
    pcl_filename = pcl_dir + pcl_name_prefix + str(frame_index) + ".npy"
    if os.path.exists(pcl_filename):
        pcl = np.load(pcl_filename)
    else:
        pcl = None
    return pcl

def readCam(stereo_imgs_dir, frame_id):
    """
    Function:
        Read images to verify the radar

    Args:
        stereo_imgs_dir     ->          stereo directory
        frame_id            ->          frame that is about to be processed.
    """
    return cv2.imread(stereo_imgs_dir + "%d.jpg"%(frame_id))[..., ::-1]

def getMagnitude(target_array):
    """
    Function:
        Get the magnitude of the complex array
    """
    return np.absolute(target_array)

def getLog(target_array, log_10=True):
    """
    Function:
        Get the log of the complex array
    """
    if log_10:
        return np.log10(target_array)
    else:
        return np.log(target_array)

def getNormalized(target_array):
    """
    Function:
        Normalize the target array.
    """
    target_array /= (target_array.max() + 0.1)
    return target_array

def getSumDim(target_array, target_axis):
    """
    Function:
        Sum up one dimension of a  3D matrix.
    
    Args:
        column_index            ->          which column to be deleted
    """
    output = np.sum(target_array, axis=target_axis)
    return output 

def averageFilter(radar_RAD_mag, bg_val=0., alpha=1.):
    """
    Function:
        Apply average filter on the whole RAD magnitude array.
    """
    output = np.where(radar_RAD_mag >= alpha * radar_RAD_mag.mean(), \
            radar_RAD_mag, bg_val)
    return output
 
def createCartesianIdx(range_resolution, angle_resolution, range_fft_size, angle_fft_size):
    """
    Function:
        transfer all the indexes of polar heat map to cartesian heat map.
    
    Args:
        range_resolution            ->          maximum_range / range_bin_size
        angle_resolution            ->          pi / angle_bin_size
        range_fft_size              ->          range_bin_size
        angle_fft_size              ->          angle_bin_size
    """
    lookup_idxs = []
    for range_idx in range(range_fft_size):
        for angle_idx in range(angle_fft_size):
            # get adjusted indices
            pxl_polar_h = (range_fft_size - 1) - range_idx  # upside down
            pxl_polar_w = angle_idx - (angle_fft_size/2) # shift to center
            # get range and angle
            polar_angle = pxl_polar_w * angle_resolution
            polar_range = pxl_polar_h * range_resolution
            # polar to cat - calculate X and Z
            output_h = polar_range * (np.cos(polar_angle))  # *(np.cos(0.0))
            output_w = polar_range * (np.sin(polar_angle))  # *(np.cos(0.0))
            # get Cartesian adjusted indicies
            cart_z_h = int(np.round(output_h / range_resolution))
            cart_x_w = int(np.round(output_w / range_resolution) + (angle_fft_size / 2))
            # append the cartesian index to lookup table of polar
            lookup_idxs.append((cart_z_h * angle_fft_size) + cart_x_w)
    return lookup_idxs

def cartMap(RA_map, range_resolution, angle_resolution, range_fft_size, angle_fft_size):
    """
    Function:
        Transfer Range-Angle polar heat map to cartesian heat map

    Args:
        range_resolution            ->          maximum_range / range_bin_size
        angle_resolution            ->          pi / angle_bin_size
        range_fft_size              ->          range_bin_size
        angle_fft_size              ->          angle_bin_size
        RA_map                      ->          Range-Azimuth map
    """
    lookup_idxs = createCartesianIdx(range_resolution, angle_resolution, range_fft_size, 
                                    angle_fft_size)
    # flatten the inference map
    RA_map = RA_map.reshape(-1)
    # use the indicies to map polar inference to Cartesian
    cart_map_flat = np.zeros(shape=[range_fft_size * angle_fft_size], dtype=np.float32)
    np.put(cart_map_flat, lookup_idxs, RA_map)
    # reshape to Cartesian 2D map
    cart_map = np.reshape(cart_map_flat, [range_fft_size, angle_fft_size])
    # invert the vertical axis (up-side down)
    cart_map = cart_map[::-1, :]
    return cart_map

def cartToPolar(pcl_3d):
    """
    Function:
        Transfer cartesian coordinates to polar coordinates

    Args:
        pcl_3d                  ->              3d point cloud
    """
    ranges = np.sqrt(pcl_3d[..., 0] ** 2 + pcl_3d[..., 2] ** 2)
    angles = np.arctan2(pcl_3d[..., 2], pcl_3d[..., 0])
    angles = np.where(angles < 0, angles + np.pi, angles)
    output = np.concatenate([np.expand_dims(ranges, -1), np.expand_dims(angles, -1)], -1)
    return output 

def transferPclToRA(pcl, range_resolution, angle_resolution, range_fft_size,
                    angle_fft_size):
    """
    Function:
        Transfer cartesian point cloud to polar with same resolution as RA_map.

    Args:
        pcl                         ->          target 3D point cloud
        range_resolution            ->          maximum_range / range_bin_size
        angle_resolution            ->          pi / angle_bin_size
    """
    polar_2d = cartToPolar(pcl)
    RA_heatmap = np.zeros((range_fft_size, angle_fft_size))
    for i in range(len(polar_2d)):
        range_ind = int(np.round(polar_2d[i, 0] / range_resolution))
        range_ind = (range_fft_size - 1) - range_ind
        angle_ind = int(np.round((np.pi - polar_2d[i, 1]) / angle_resolution))
        if range_ind > range_fft_size or angle_ind > angle_fft_size:
            continue
        RA_heatmap[range_ind, angle_ind] += 1
    # normalization
    RA_heatmap /= (RA_heatmap.max() + 0.1)
    return RA_heatmap

###################### PLOTTING FUNCTIONS START #########################
def prepareFigure(num_axes):
    assert num_axes <= 4
    # plt.ion()
    fig = plt.figure()
    if num_axes == 1:
        ax1 = fig.add_subplot(111)
        return fig, [ax1]
    if num_axes == 2: 
        ax1 = fig.add_subplot(121)
        ax2 = fig.add_subplot(122)
        return fig, [ax1, ax2]
    if num_axes == 3:
        ax1 = fig.add_subplot(131)
        ax2 = fig.add_subplot(132)
        ax3 = fig.add_subplot(133)
        return fig, [ax1, ax2, ax3]
    if num_axes == 4:
        ax1 = fig.add_subplot(221)
        ax2 = fig.add_subplot(222)
        ax3 = fig.add_subplot(223)
        ax4 = fig.add_subplot(224)
        return fig, [ax1, ax2, ax3, ax4]

def clearAxes(ax_list):
    assert len(ax_list) >=1 
    plt.cla()
    for ax_i in ax_list:
        ax_i.clear()

def pointScatter(points_list, colors_list, labels_list, 
                ax, xlimits, ylimits, title):
    assert len(points_list) == len(colors_list)
    for i in range(len(colors_list)):
        current_pnts = points_list[i]
        current_color = colors_list[i]
        current_label = labels_list[i]
        ax.scatter(current_pnts[:, 0], current_pnts[:, 2], s=0.4, c=current_color, 
                    label = current_label)
    ax.set_xlim(xlimits)
    ax.set_ylim(ylimits)
    ax.legend()
    ax.set_title(title)

def imgPlot(img, ax, title):
    ax.imshow(img)
    ax.axis('off')
    ax.set_title(title)

def keepDrawing(fig, time_duration):
    fig.canvas.draw()
    plt.pause(time_duration)

def saveFigure(save_dir, name_prefix, frame_i):
    plt.savefig(save_dir + name_prefix + str(frame_i) + ".png")
###################### PLOTTING FUNCTIONS END #########################

def main(time_stamp, frame_delay, plot=True):
    """
    MAIN FUNCTION
    """
    ###### setting paths ######
    radar_dir = "/DATA/" + time_stamp + "/ral_outputs_" + time_stamp + "/RAD_numpy/"
    stereo_imgs_dir = "../stereo_process/left/"
    stereo_reflector_pcl_dir = "../stereo_process/mask_reflector/"
    radar_reflector_pcl_dir = "../radar_process/radar_ral_process/radar_pcl/"
    stereo_reflector_pcl_prefix = "reflector_pcl_"
    radar_reflector_pcl_prefix = "radar_pcl_"
    save_dir = "./samples/"

    ###### setting parameters ######
    range_fft_size = 256
    angle_fft_size = 256
    range_resolution = 50 / range_fft_size
    angle_resolution = np.pi / angle_fft_size

    sequences = [0, getTotoalNum(radar_dir)]

    if plot:
        fig, axes = prepareFigure(3)

    for frame_id in tqdm(range(sequences[0], sequences[1])):
        ##### read and process radar-stereo pcls #####
        stereo_reflector_pcl = ReadPcl(stereo_reflector_pcl_dir, stereo_reflector_pcl_prefix,\
                                frame_id)
        radar_reflector_info = ReadPcl(radar_reflector_pcl_dir, radar_reflector_pcl_prefix,\
                                frame_id + frame_delay)
        if stereo_reflector_pcl is None or radar_reflector_info is None:
            continue
        stereo_pcl_polar = transferPclToRA(stereo_reflector_pcl, range_resolution, angle_resolution, \
                                            range_fft_size, angle_fft_size)
        stereo_RA_cart = cartMap(stereo_pcl_polar, range_resolution, angle_resolution, range_fft_size,
                            angle_fft_size)
        ###### read and process R-A-D arrays ###### 
        radar_RAD = readRAD(radar_dir, frame_id)
        radar_RAD_mag = getMagnitude(radar_RAD)
        # radar_RAD_mag = averageFilter(radar_RAD_mag, bg_val=1., alpha=50)
        # radar_RAD_mag = getLog(radar_RAD_mag, log_10=True)
        radar_RAD_mag = getNormalized(radar_RAD_mag)

        RD_img = getSumDim(radar_RAD_mag, 1)
        RA_img = getSumDim(radar_RAD_mag, -1)
        RA_cart = cartMap(RA_img, range_resolution, angle_resolution, range_fft_size, 
                                angle_fft_size)
        stereo_img = readCam(stereo_imgs_dir, frame_id)

        if plot:
            clearAxes(axes)
            imgPlot(RA_cart, axes[0], "radarRA")
            imgPlot(stereo_RA_cart, axes[1], "stereoRA")
            # pointScatter([stereo_reflector_pcl, radar_reflector_info], ['r', 'b'], 
                    # ['stereo reflector', 'radar_pcl'], axes[2], [-15, 15], [0, 50],
                    # "Points")
            imgPlot(stereo_img, axes[2], "camera")
            saveFigure(save_dir, "polar_compare_", frame_id) 
            # keepDrawing(fig, 0.1)

if __name__ == "__main__":
    time_stamp = "2020-02-28-13-56-29"
    frame_delay = 0
    main(time_stamp, frame_delay)
