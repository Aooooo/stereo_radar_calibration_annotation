import os
import numpy as np

import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib.patches import Ellipse

import cv2
import pickle
import colorsys

from glob import glob
from tqdm import tqdm

def readRegistrationMatrix(registration_matrix_dir):
    """
    Function:
        Read the calibration matrix

    Args:
        registration_matrix_dir             ->          registration matrix directory
    """
    filename = registration_matrix_dir + "calibration_matrix.npy"
    if os.path.exists(filename):
        registr_mat = np.load(filename)
    else:
        raise ValueError("Pleaaaaaaase calibrate the sensors before you do this")
    return registr_mat

def readStereoObjPcl(stereo_obj_pcl_dir, frame_i):
    """
    Function:
        Read the stereo object detection point cloud w.r.t frame i

    Args:
        stereo_obj_pcl_dir          ->          stereo point cloud directory
        frame_i                     ->          frame id

    Attention:
        The output stereo_obj_pcl has two keys, 
        "class" : current object class string name 
        "pcl" : current object point cloud with format (num_pnts, 3) 
    """
    filename = stereo_obj_pcl_dir + "stereo_obj_pcl_" + str(frame_i) + ".pickle"
    if os.path.exists(filename):
        with open(filename, "rb") as f:
            stereo_obj_pcl = pickle.load(f)
        if len(stereo_obj_pcl['class']) == 0:
            stereo_obj_pcl = None
    else:
        stereo_obj_pcl = None
    return stereo_obj_pcl

def getStereoObjPclSample(stereo_obj_pcl_dir, frame_i):
    """
    Function:
        Since we only need one object at each time to verify the calibration, 
    we here extract one object of the point cloud we get from stereo.

    Args:
        stereo_obj_pcl_dir          ->          stereo point cloud directory
        frame_i                     ->          frame id
    """
    stereo_obj_pcl = readStereoObjPcl(stereo_obj_pcl_dir, frame_i)
    if stereo_obj_pcl is not None:
        return stereo_obj_pcl["pcl"][0]
    else:
        return None
    
def setColors(depth_max=25, depth_min=0, depth_resolution=0.1):
    """
    Function:
        Set different colors to different distances, according to the depth.

    Args:
        depth_max               ->          maximum range  
        depth_min               ->          minimum range, normally 0
        depth_resolution        ->          resolution of the range for coloring
    """
    color_length = int((depth_max - depth_min) / depth_resolution)
    colors = cm.rainbow(np.linspace(0, 1, color_length))
    return colors

def assignColor(pcl, all_colors, depth_max, depth_min, depth_resolution):
    """
    Function:
        Assign colors to the point cloud w.r.t different depths
    
    Args:
        pcl                     ->          point cloud sample
        all_colors              ->          all colors that are gonna be assigned
        depth_max               ->          maximum range  
        depth_min               ->          minimum range, normally 0
        depth_resolution        ->          resolution of the range for coloring
    """
    ##### filter out the points that are outta maximum range #####
    pcl_doable = pcl[pcl[:, 2] <= depth_max]
    ##### start color assignment #####
    pcl_color = []
    for point_ind in range(len(pcl_doable)):
        current_pnt = pcl_doable[point_ind]
        color_index = int(round((current_pnt[2] - depth_min) / depth_resolution))
        pcl_color.append(all_colors[color_index])
    pcl_color = np.array(pcl_color)
    return pcl_doable, pcl_color

def addonesToLstCol(target_array):
    """
    Function:
        Add ones to the last column of the target array

    Args:
        target_array            ->          array to be changed
    """
    adding_ones = np.ones([target_array.shape[0], 1])
    output_array = np.concatenate([target_array, adding_ones], axis=-1)
    return output_array

def calibrateStereoToRadar(stereo_pcl, registration_matrix):
    """
    Function:
        Transfer stereo point cloud to radar frame.
    
    Args:
        stereo_pcl              ->          stereo point cloud
        registration_matrix     ->          registration matrix 
    """
    return np.matmul(addonesToLstCol(stereo_pcl), registration_matrix)

###################### PLOTTING FUNCTIONS START #########################
def prepareFigure(num_axes):
    assert num_axes <= 4
    plt.ion()
    fig = plt.figure()
    if num_axes == 1:
        ax1 = fig.add_subplot(111)
        return fig, ax1
    if num_axes == 2:
        ax1 = fig.add_subplot(121)
        ax2 = fig.add_subplot(122)
        return fig, ax1, ax2
    if num_axes == 3:
        ax1 = fig.add_subplot(131)
        ax2 = fig.add_subplot(132)
        ax3 = fig.add_subplot(133)
        return fig, ax1, ax2, ax3
    if num_axes == 4:
        ax1 = fig.add_subplot(221)
        ax2 = fig.add_subplot(222)
        ax3 = fig.add_subplot(223)
        ax4 = fig.add_subplot(224)
        return fig, ax1, ax2, ax3, ax4

def clearAxes(ax_list):
    assert len(ax_list) >=1 
    plt.cla()
    for ax_i in ax_list:
        ax_i.clear()

def pointScatter(points, colors, all_colors, ax, xlimits, ylimits, title):
    for color_i in range(len(all_colors)):
        current_color = colors[color_i]
        current_color_pnts = points[np.all(colors==current_color, axis=-1)]
        ax.scatter(current_color_pnts[:, 0], current_color_pnts[:, 1], \
                    s=0.4, c=[current_color])
    ax.set_xlim(xlimits)
    ax.set_ylim(ylimits)
    ax.set_title(title)

def imgPlot(img, ax, title):
    ax.imshow(img)
    ax.axis('off')
    ax.set_title(title)

def keepDrawing(fig, time_duration):
    fig.canvas.draw()
    plt.pause(time_duration)

def saveFigure(save_dir, name_prefix, frame_i):
    plt.savefig(save_dir + name_prefix + str(frame_i) + ".png")
###################### PLOTTING FUNCTIONS END #########################
 
def main(registration_matrix_dir, stereo_obj_pcl_dir, save_dir, sequence):
    """
    MAIN FUNCTION
    """
    ##### set the depth range and resolution (for coloring and plotting) #####
    x_limits = [-15, 15]
    y_limits = [0, 10]
    y_resolution = 0.01
    ##### set colors #####
    all_colors = setColors(y_limits[1], y_limits[0], y_resolution)
    ##### get registration matrix #####
    registr_mat = readRegistrationMatrix(registration_matrix_dir)
    ##### plot #####
    fig, ax1, ax2 = prepareFigure(2)

    ##### main loop #####
    for frame_i in tqdm(range(sequence[0], sequence[1])):
        verification_sample_init = getStereoObjPclSample(stereo_obj_pcl_dir, frame_i)
        if verification_sample_init is None:
            continue
        stereo_sample, stereo_sample_color = assignColor(verification_sample_init, \
                                    all_colors, y_limits[1], y_limits[0], y_resolution)
        stereo_sample_to_radar = calibrateStereoToRadar(stereo_sample, registr_mat)
        stereo_sample_2d = np.delete(stereo_sample, 1, -1)
        ##### plot #####
        clearAxes([ax1, ax2])
        pointScatter(stereo_sample_2d, stereo_sample_color, all_colors, ax1, \
                        x_limits, y_limits, "stereo_stereo_frame")
        pointScatter(stereo_sample_to_radar, stereo_sample_color, all_colors, ax2, \
                        x_limits, y_limits, "stereo_radar_frame")
        saveFigure(save_dir, "verification_", frame_i)
        keepDrawing(fig, 0.1)

if __name__ == "__main__" :
    registration_matrix_dir = "./registration_matrix/"
    stereo_obj_pcl_dir = "../annotation/stereo_object_pcl/"
    save_dir = "./samples/"
    sequence = [2185, 2335]
    main(registration_matrix_dir, stereo_obj_pcl_dir, save_dir, sequence)


