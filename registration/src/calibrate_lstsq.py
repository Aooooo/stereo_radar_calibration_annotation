import numpy as np
import scipy.linalg as la
import cv2
import os
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
from tqdm import tqdm
from glob import glob
from sklearn import mixture

def TransferTo2DGM(mean, covariance):
    """
    Function:
        Transfer mean and covariance into 2d for plotting ellipse
    
    Args:
        mean        ->      mean vector from gaussian model
        covariance  ->      covariance vector from gaussian model.
    """
    mean_xz = np.array([mean[0], mean[2]])
    covariance_xz = np.array([[covariance[0,0], covariance[0,2]],\
                            [covariance[2,0], covariance[2,2]]])
    return mean_xz, covariance_xz

def ReadReflectorInfo(pcl_dir):
    """
    Function:
        Read all information about reflector

    Args:
        pcl_dir         ->          directory that saves info
    """
    ##### the minimum points for calibration #####
    minimum_positions = 4

    hm_positions = len(glob(os.path.join(pcl_dir, "stereo_pcl_") + "*.npy")) 
    if hm_positions <= minimum_positions:
        raise ValueError("You need more points if you want to calibration the sensors.")
    
    all_stereo_points = []
    all_radar_points = []
    all_stereo_means = []
    all_stereo_covariances = []
    all_radar_means = []
    all_radar_covariance = []
    for position_i in range(hm_positions):
        stereo_pcl = np.load(os.path.join(pcl_dir, "stereo_pcl_") + str(position_i) + ".npy")
        radar_pcl = np.load(os.path.join(pcl_dir, "radar_pcl_") + str(position_i) + ".npy")
        stereo_mean = np.load(os.path.join(pcl_dir, "stereo_GM_mean_") + str(position_i) + ".npy")
        stereo_cov = np.load(os.path.join(pcl_dir, "stereo_GM_cov_") + str(position_i) + ".npy")
        radar_mean = np.load(os.path.join(pcl_dir, "radar_GM_mean_") + str(position_i) + ".npy")
        radar_cov = np.load(os.path.join(pcl_dir, "radar_GM_cov_") + str(position_i) + ".npy")

        ##### transfer the mean and covariance to 2D, namely x,z #####
        radar_mean_2d, radar_cov_2d = TransferTo2DGM(radar_mean, radar_cov)

        all_stereo_points.append(stereo_pcl)
        all_radar_points.append(radar_pcl)
        all_stereo_means.append(stereo_mean)
        all_stereo_covariances.append(stereo_cov)
        all_radar_means.append(radar_mean_2d)
        all_radar_covariance.append(radar_cov_2d)

    all_stereo_points = np.array(all_stereo_points)
    all_radar_points = np.array(all_radar_points)
    all_stereo_means = np.array(all_stereo_means)
    all_stereo_covariances = np.array(all_stereo_covariances)
    all_radar_means = np.array(all_radar_means)
    all_radar_covariance = np.array(all_radar_covariance)

    return all_stereo_points, all_radar_points, all_stereo_means, all_stereo_covariances, \
            all_radar_means, all_radar_covariance

def AddonesToLstCol(target_array):
    """
    Function:
        Add ones to the last column of the target array

    Args:
        target_array            ->          array to be changed
    """
    adding_ones = np.ones([target_array.shape[0], 1])
    output_array = np.concatenate([target_array, adding_ones], axis=-1)
    return output_array

# def CalibrationLstsq(stereo_means, radar_means, save_mat=True):
    # """
    # Fuction:
        # Use only the centers of points w.r.t two sensors to calibrate them.
    
    # Args:
        # stereo_means            ->          centers of stereo points
        # radar_means             ->          centers of radar points
    # """
    # stereo_means = AddonesToLstCol(stereo_means)
    # calib_results = la.lstsq(stereo_means, radar_means)
    # ##### decide whether to add transpose #####
    # # calib_matrix = np.transpose(calib_matrix_transpose[0])
    # calib_matrix = calib_results[0]
    # ###########################################
    # calib_mat_transpose_residues = calib_results[1]
    # calib_mat_transpose_rank = calib_results[2]
    # calib_mat_transpose_eigen = calib_results[3]
    # if save_mat:
        # np.save("./registration_matrix/calibration_matrix.npy", calib_matrix)
        # np.save("./registration_matrix/calibration_matrix_residues.npy", calib_mat_transpose_residues)
        # np.save("./registration_matrix/calibration_matrix_rank.npy", calib_mat_transpose_rank)
        # np.save("./registration_matrix/calibration_matrix_singular.npy", calib_mat_transpose_eigen)
    # return calib_matrix

def CalibrationLstsqWithCov(stereo_means, radar_means, stereo_covs, radar_covs, \
                            result_dir, save_mat=True):
    """
    Function:
        Similar as above but take noise covariance into consideration.
    
    Args:
        stereo_means            ->          centers of stereo points
        radar_means             ->          centers of radar points
        stereo_covs             ->          stereo points noise
        radar_covs              ->          radar points noise
        result_dir              ->          directory to save the registration matrices
    """
    def GetMatrix(means, covs, if_stereo=True):
        if if_stereo:
            means = np.expand_dims(AddonesToLstCol(means), axis=-1)
            covs = np.delete(covs, 1, 2)
            covs = np.concatenate([covs, np.ones((covs.shape[0], 1, covs.shape[2]))],\
                    axis=1)
            output = np.concatenate([means, covs], axis=-1)
        else:
            means = np.expand_dims(means, -1)
            output = np.concatenate([means, covs], -1)
        output = np.transpose(output, (0,2,1))
        output = output.reshape(-1, output.shape[-1])
        return output
    stereo_mat = GetMatrix(stereo_means, stereo_covs, True)
    radar_mat = GetMatrix(radar_means, radar_covs, False)
    calib_results = la.lstsq(stereo_mat, radar_mat)
    calib_matrix = calib_results[0]
    calib_mat_transpose_residues = calib_results[1]
    calib_mat_transpose_rank = calib_results[2]
    calib_mat_transpose_eigen = calib_results[3]
    if save_mat:
        np.save(os.path.join(result_dir, "calibration_matrix.npy"), calib_matrix)
        np.save(os.path.join(result_dir, "calibration_matrix_residues.npy"), calib_mat_transpose_residues)
        np.save(os.path.join(result_dir, "calibration_matrix_rank.npy"), calib_mat_transpose_rank)
        np.save(os.path.join(result_dir, "calibration_matrix_singular.npy"), calib_mat_transpose_eigen)
    return calib_matrix
 
def GaussianModel(pcl):
    """
    Function:
        Get the center and covariance from gaussian model.

    Args:
        pcl             ->          point cloud data
    """
    model = mixture.GaussianMixture(n_components=1, covariance_type='full')
    model.fit(pcl)
    return model.means_[0], model.covariances_[0]

def DrawEllipse(means, covariances, color_edge, scale_factor=1):
    """
    Function:
        Draw 2D Gaussian Ellipse.

    Args:
        means           ->          center of the Gaussian
        covariances     ->          covariance of Gaussian
    """
    eigen, eigen_vec = np.linalg.eig(covariances)
    eigen_root_x = np.sqrt(1 + eigen[0]) * scale_factor
    eigen_root_y = np.sqrt(1 - eigen[1]) * scale_factor
    ell = Ellipse(xy = (means[0], means[1]), width = eigen_root_x,
                height = eigen_root_y, angle = np.rad2deg(np.arccos(eigen_vec[0,0])),
                facecolor = 'none', edgecolor = color_edge) 
    return ell

def Plotting(stereo_pcl, radar_pcl, ax, count, calibration=False):
    """
    Function:
        Find if we get good results on calibration
    
    Args:
        stereo_pcl_before               ->              stereo pcl before calibration
        stereo_pcl_after                ->              stereo pcl after calibration
        radar_pcl                       ->              radar pcl
    """
    stereo_means, stereo_covs = GaussianModel(stereo_pcl)
    radar_means, radar_covs = GaussianModel(radar_pcl)
    if calibration:
        stereo_means_2d = stereo_means
        stereo_covs_2d = stereo_covs
    else:
        stereo_means_2d, stereo_covs_2d = TransferTo2DGM(stereo_means, stereo_covs)
    radar_means_2d, radar_covs_2d = TransferTo2DGM(radar_means, radar_covs)

    # ax.clear()
    if count == 0:
        radar_label = "radar points"
        stereo_label = "stereo points"
    else:
        radar_label = None
        stereo_label = None

    ax.scatter(radar_pcl[..., 0], radar_pcl[..., 2], s=0.2, c='b',
                label=radar_label)
    if calibration:
        ax.scatter(stereo_pcl[..., 0], stereo_pcl[..., 1], s=0.2, c='r',
                    label=stereo_label)
    else:
        ax.scatter(stereo_pcl[..., 0], stereo_pcl[..., 2], s=0.2, c='r',
                    label=stereo_label)
    ellipse_stereo = DrawEllipse(stereo_means_2d, stereo_covs_2d, color_edge='orange')
    ellipse_radar = DrawEllipse(radar_means_2d, radar_covs_2d, color_edge='green')
    ax.add_artist(ellipse_stereo)
    ax.add_artist(ellipse_radar)
    ax.set_xlim([-30, 30])
    ax.set_ylim([0, 50])
    if calibration:
        ax.set_title("after calibration")
    else:
        ax.set_title("before calibration")
    if count == 0:
        ax.legend()
    
def main(pcl_dir, result_dir, fig_dir, if_plot=True):
    """
    MAIN FUNCTION
    """
    stereo_pnts, radar_pnts, stereo_means, stereo_covs, radar_means, radar_covs = \
                                                        ReadReflectorInfo(pcl_dir)
    # calibration_matrix = CalibrationLstsq(stereo_means, radar_means, save_mat=True)
    calibration_matrix = CalibrationLstsqWithCov(stereo_means, radar_means, stereo_covs, \
                                            radar_covs, result_dir, save_mat=True)

    if if_plot:
        ##### setting up for matplotlib #####
        fig = plt.figure(figsize=(16,8))
        ax1 = fig.add_subplot(121)
        ax2 = fig.add_subplot(122)

    count = 0

    for position_id in tqdm(range(len(stereo_pnts))):
        stereo_current_pnts = stereo_pnts[position_id]
        stereo_current_pnts_addones = AddonesToLstCol(stereo_current_pnts)
        radar_current_pnts = radar_pnts[position_id]
        stereo_current_pnts_calib = np.matmul(stereo_current_pnts_addones, calibration_matrix)

        if if_plot:
            Plotting(stereo_current_pnts, radar_current_pnts, ax1, count, calibration=False)
            Plotting(stereo_current_pnts_calib, radar_current_pnts, ax2, count, calibration=True)
            # plt.savefig(os.path.join(fig_dir, "calibration_results_") + str(position_id) + ".png")
            # fig.canvas.draw()
            # plt.pause(5)
            count += 1
    if if_plot:
        plt.savefig(os.path.join(fig_dir, "calibration_results_vis_") + ".png")
        # fig.canvas.draw()
        # plt.pause(5)

if __name__ == "__main__":
    pcl_dir = "../match_sensors/"
    result_dir = "../registration_matrix/"
    fig_dir = "../sensors_match_samples/"
    main(pcl_dir, result_dir, fig_dir, if_plot = True)
