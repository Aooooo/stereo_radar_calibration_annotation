import numpy as np
import cv2
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
from sklearn import mixture
from tqdm import tqdm
import os
from sklearn.cluster import DBSCAN
from collections import Counter

def ReadTxtFile(txt_filepath):
    """
    Function:
        Read sequence numbers for each point from txt file
    """
    sequences = []
    with open(txt_filepath, "r") as f:
        lines = f.readlines()
        num_calibration_pnts = len(lines) - 1 
        for i in range(num_calibration_pnts):
            line_id = i + 1
            current_line = lines[line_id].split(",")
            if len(current_line) != 2:
                continue
            sequences.append([int(current_line[0]), int(current_line[1])])
    sequences = np.array(sequences)
    return sequences

def ReadPcl(pcl_dir, pcl_name_prefix, frame_index):
    """
    Function:
        Read points from sensors

    Args:
        pcl_dir             ->      point cloud directory
        pcl_name_prefix     ->      point cloud file name prefix
        frame_index         ->      which frame to be read
    """
    pcl_filename = os.path.join(pcl_dir, pcl_name_prefix) + str(frame_index) + ".npy"
    if os.path.exists(pcl_filename):
        pcl = np.load(pcl_filename)
    else:
        pcl = None
    return pcl

def GaussianModel(pcl):
    """
    Function:
        Get the center and covariance from gaussian model.

    Args:
        pcl             ->          point cloud data
    """
    model = mixture.GaussianMixture(n_components=1, covariance_type='full')
    model.fit(pcl)
    return model.means_[0], model.covariances_[0]

def TransferTo2DGM(mean, covariance):
    """
    Function:
        Transfer mean and covariance into 2d for plotting ellipse
    
    Args:
        mean        ->      mean vector from gaussian model
        covariance  ->      covariance vector from gaussian model.
    """
    mean_xz = np.array([mean[0], mean[2]])
    covariance_xz = np.array([[covariance[0,0], covariance[0,2]],\
                            [covariance[2,0], covariance[2,2]]])
    return mean_xz, covariance_xz

def DrawEllipse(means, covariances, scale_factor=1):
    """
    Function:
        Draw 2D Gaussian Ellipse.

    Args:
        means           ->          center of the Gaussian
        covariances     ->          covariance of Gaussian
    """
    eigen, eigen_vec = np.linalg.eig(covariances)
    eigen_root_x = np.sqrt(1 + eigen[0]) * scale_factor
    eigen_root_y = np.sqrt(1 - eigen[1]) * scale_factor
    ell = Ellipse(xy = (means[0], means[1]), width = eigen_root_x,
                height = eigen_root_y, angle = np.rad2deg(np.arccos(eigen_vec[0,0])),
                facecolor = 'none', edgecolor = 'red')
    return ell

def ThresholdPcl(pcl, x_threshold = None, z_threshold=None):
    """
    Function:
        Constrain the distance with pre-defined thresholds

    Args:
        pcl             ->          target point cloud
        x_threshold     ->          threshold along x
        z_threshold     ->          Threshold along z
    """
    assert len(pcl.shape) == 2
    assert pcl.shape[-1] == 3
    output_pcl = pcl
    if x_threshold is not None:
        output_pcl = output_pcl[output_pcl[:, 0] <= x_threshold]
        output_pcl = output_pcl[output_pcl[:, 0] >= -x_threshold]
    if z_threshold is not None:
        output_pcl = output_pcl[output_pcl[:, 2] <= z_threshold]
    return output_pcl

def DbscanDenoise(pcl, epsilon=0.3, minimum_samples=300):
    """
    Function:
        Using DBSCAN for filtering out the noise data.

    Args:
        pcl             ->          point cloud to be denoised
        epsilon         ->          maximum distance to be considered as an object
        minimum_samples ->          miminum points to be considered as an object
    """
    clustering = DBSCAN(eps=epsilon, min_samples=minimum_samples).fit(pcl)
    output_labels = clustering.labels_
    if len(np.unique(output_labels)) == 1:
        output_pcl = np.zeros([0,3])
    else:
        counts = Counter(output_labels)
        output_pcl = pcl[output_labels == counts.most_common(1)[0][0]]
    return output_pcl

def main(sequence, frame_delay, stereo_reflector_pcl_dir, radar_reflector_pcl_dir, \
        stereo_reflector_pcl_prefix, radar_reflector_pcl_prefix, save_dir, fig_dir, \
        x_threshold, z_threshold, mag_threshold, save_everything=True, if_plot=False):
    """
    MAIN FUNCTION
    """
    if if_plot:
        # plt.ion()
        fig = plt.figure(figsize = (16, 8))
        ax1 = fig.add_subplot(121)
        ax2 = fig.add_subplot(122)
        
    count = 0
    for pos_i in tqdm(range(len(sequence))):
        position_id = sequence[pos_i]
        stereo_pcl_current_pos = []
        radar_pcl_current_pos = []
        for frame_id in range(position_id[0], position_id[1]):
            stereo_reflector_pcl = ReadPcl(stereo_reflector_pcl_dir, stereo_reflector_pcl_prefix,\
                                        frame_id)
            radar_reflector_info = ReadPcl(radar_reflector_pcl_dir, radar_reflector_pcl_prefix,\
                                        frame_id + frame_delay)
            if stereo_reflector_pcl is not None and radar_reflector_info is not None:
                if stereo_reflector_pcl.shape[0] != 0 :
                    radar_reflector_pcl = radar_reflector_info[..., :3]
                    radar_power_mag = radar_reflector_info[..., -1]
                    radar_reflector_pcl = radar_reflector_pcl[radar_power_mag >= mag_threshold]
                    stereo_pcl_current_pos.append(stereo_reflector_pcl)
                    radar_pcl_current_pos.append(radar_reflector_pcl)
        if len(stereo_reflector_pcl) == 0 or len(radar_pcl_current_pos) == 0:
            print("Reflector not detected in position [{}, {}]".format(position_id[0], position_id[1]))
            continue
        ############### stereo points ############
        stereo_pcl_current_pos = np.concatenate(stereo_pcl_current_pos, axis=0)
        ############### radar points #############
        radar_pcl_current_pos = np.concatenate(radar_pcl_current_pos, axis=0)
        if len(radar_pcl_current_pos) <= 1:
            continue
        radar_pcl_current_pos = DbscanDenoise(radar_pcl_current_pos, epsilon=0.5, minimum_samples=5)

        if len(radar_pcl_current_pos) <= 1:
            continue

        stereo_mean, stereo_covariance = GaussianModel(stereo_pcl_current_pos)
        radar_mean, radar_covariance = GaussianModel(radar_pcl_current_pos)

        # if np.sqrt(np.square(stereo_mean - radar_mean)[0] + np.square(stereo_mean - radar_mean)[2]) >= 3:
            # print("radar pnts and stereo pnts are too far away in position [{}, {}]".format\
                    # (position_id[0], position_id[1]))
            # continue

        if save_everything:
            np.save(os.path.join(save_dir, "stereo_pcl_") + str(count) + ".npy", stereo_pcl_current_pos)
            np.save(os.path.join(save_dir, "radar_pcl_") + str(count) + ".npy", radar_pcl_current_pos)
            np.save(os.path.join(save_dir, "stereo_GM_mean_") + str(count) + ".npy", stereo_mean)
            np.save(os.path.join(save_dir, "stereo_GM_cov_") + str(count) + ".npy", stereo_covariance)
            np.save(os.path.join(save_dir, "radar_GM_mean_") + str(count) + ".npy", radar_mean)
            np.save(os.path.join(save_dir, "radar_GM_cov_") + str(count) + ".npy", radar_covariance)

        stereo_mean_xz, stereo_covariance_xz = TransferTo2DGM(stereo_mean, stereo_covariance)
        radar_mean_xz, radar_covariance_xz = TransferTo2DGM(radar_mean, radar_covariance)
        
        if if_plot:
            ax1.clear()
            ax1.scatter(stereo_pcl_current_pos[..., 0], stereo_pcl_current_pos[..., 2], s=0.2)
            ax1.scatter(stereo_mean[0], stereo_mean[2], s=3, c='r')
            ellipse_stereo = DrawEllipse(stereo_mean_xz, stereo_covariance_xz)
            ax1.add_artist(ellipse_stereo)
            ax1.set_xlim([-30, 30])
            ax1.set_ylim([0, 50])
            ax1.set_title("stereo points")

            ax2.clear()
            ax2.scatter(radar_pcl_current_pos[..., 0], radar_pcl_current_pos[..., 2], s=0.2)
            ax2.scatter(radar_mean[0], radar_mean[2], s=3, c='r')
            ellipse_radar = DrawEllipse(radar_mean_xz, radar_covariance_xz)
            ax2.add_artist(ellipse_radar)
            ax2.set_xlim([-30, 30])
            ax2.set_ylim([0, 50])
            ax2.set_title("radar points")
            plt.savefig(os.path.join(fig_dir, "match_") + str(position_id[0]) + \
                        "_" + str(position_id[1]) + ".png")
            # fig.canvas.draw()
            # plt.pause(1)
        count += 1

if __name__ == "__main__":
    main(sequence, frame_delay, stereo_reflector_pcl_dir, radar_reflector_pcl_dir, \
        stereo_reflector_pcl_prefix, radar_reflector_pcl_prefix, save_dir, fig_dir, \
        x_threshold=None, z_threshold=None, mag_threshold=50, save_everything=True, if_plot=False)
