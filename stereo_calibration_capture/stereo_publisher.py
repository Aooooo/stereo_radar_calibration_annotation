#!/usr/bin/env python3
#
# Copyright (c) 2019, Sensor Cortek Inc.
# All rights reserved.
#
# Author: Erlik Now
#
#   ROS camera node to acquire camera data and distribute it
#

import rospy
#from std_msgs.msg import String
########################################################
from data_capture_ros.msg import cam_stereo as cam_message
 
import gi
import cv2
import time
import numpy as np
import multiprocessing as mp

gi.require_version("Gst", "1.0")
gi.require_version("Tcam", "0.1")

from gi.repository import Tcam, Gst, GLib, GObject

resize = False

class TIS(object):
    def __init__(self, serial_num):
        """
        Initialization
        """
        Gst.init()  # init gstreamer

        self.pipeline = Gst.parse_launch("tcambin name=source"
                                        " ! video/x-raw,format=BGRx,width=1440,height=1080,framerate=120/1"
                                        " ! videoconvert"
                                        " ! appsink name=sink")
        # self.pipeline.set_state(Gst.State.READY)

        if serial_num is not None:
            self.serial = serial_num
            # Query a pointer to our source, so we can set properties.
            self.camera = self.pipeline.get_by_name("source")
            self.camera.set_property("serial", self.serial)

        # Query a pointer to the appsink, so we can assign the callback function.
        self.appsink = self.pipeline.get_by_name("sink")
        self.appsink.set_property("max-buffers", 100)
        self.appsink.set_property("drop", 1)
        self.appsink.set_property("emit-signals", True)

    def ToNumpy(self, sample):
        ''' Convert a GStreamer sample to a numpy array
            Sample code from https://gist.github.com/cbenhagen/76b24573fa63e7492fb6#file-gst-appsink-opencv-py-L34

            The result is in self.img_mat.
        :return:
        '''
        buf = sample.get_buffer()
        caps = sample.get_caps()
        bpp = 4
        dtype = np.uint8
        bla = caps.get_structure(0).get_value('height')
        if( caps.get_structure(0).get_value('format') == "BGRx" ):
            bpp = 4

        if(caps.get_structure(0).get_value('format') == "GRAY8" ):
            bpp = 1

        if(caps.get_structure(0).get_value('format') == "GRAY16_LE" ):
            bpp = 1
            dtype = np.uint16

        img_mat = np.ndarray(
            (caps.get_structure(0).get_value('height'),
                caps.get_structure(0).get_value('width'),
                bpp),
            buffer=buf.extract_dup(0, buf.get_size()),
            dtype=dtype)
        return img_mat

    def StartPipe(self):
        """
        Main function, for running the code
        """
        self.pipeline.set_state(Gst.State.PLAYING)
        self.camera.set_tcam_property("Trigger Mode", True)

    def GetImage(self):
        """
        Get the image when the cmd is triggered
        """
        ret = self.camera.set_tcam_property("Software Trigger", True)
        if ret:
            image = self.appsink.get_property("last-sample")
            # if image is not None:
            #     image = self.ToNumpy(image)
            #     image = image[..., :3]
        else:
            image = None
        return image

    def StopPipeline(self):
        """
        Stop the pipeline for safety reason
        """
        self.pipeline.set_state(Gst.State.PAUSED)
        self.pipeline.set_state(Gst.State.NULL)


# class TIS(object):
#     'The Imaging Source Camera'
#     def __init__(self, serial, width, height, numerator, denumerator, color):
#         Gst.init([])
#         self.height = height
#         self.width = width
#         self.sample = None
#         self.samplelocked = False
#         self.newsample = False
#         format = "BGRx"
#         if (color == False):
#             format = "GRAY8"
#         p = 'tcambin name=source ! video/x-raw,format=%s,width=%d,height=%d,framerate=%d/%d' % (
#             format, width, height, numerator, denumerator,)
#         p += ' ! videoconvert ! appsink name=sink'
#         print(p)
#         try:
#             self.pipeline = Gst.parse_launch(p)
#         except GLib.Error as error:
#             print("Error creating pipeline: {0}".format(error))
#             raise
#         # self.pipeline.set_state(Gst.State.READY)
#         # self.pipeline.get_state(Gst.CLOCK_TIME_NONE)
#         # Query a pointer to our source, so we can set properties.
#         self.source = self.pipeline.get_by_name("source")
#         self.source.set_property("serial", serial)
#         # Query a pointer to the appsink, so we can assign the callback function.
#         self.appsink = self.pipeline.get_by_name("sink")
#         self.appsink.set_property("max-buffers", 100)
#         self.appsink.set_property("drop", 1)
#         self.appsink.set_property("emit-signals", 1)
#         # self.appsink.connect('new-sample', self.on_new_buffer)

#     def To_numpy(self, sample):
#         ''' Convert a GStreamer sample to a numpy array
#             Sample code from https://gist.github.com/cbenhagen/76b24573fa63e7492fb6#file-gst-appsink-opencv-py-L34

#             The result is in self.img_mat.
#         :return:
#         '''
#         buf = sample.get_buffer()
#         caps = sample.get_caps()
#         bpp = 4
#         dtype = np.uint8
#         bla = caps.get_structure(0).get_value('height')
#         if( caps.get_structure(0).get_value('format') == "BGRx" ):
#             bpp = 4

#         if(caps.get_structure(0).get_value('format') == "GRAY8" ):
#             bpp = 1

#         if(caps.get_structure(0).get_value('format') == "GRAY16_LE" ):
#             bpp = 1
#             dtype = np.uint16

#         img_mat = np.ndarray(
#             (caps.get_structure(0).get_value('height'),
#                 caps.get_structure(0).get_value('width'),
#                 bpp),
#             buffer=buf.extract_dup(0, buf.get_size()),
#             dtype=dtype)
#         return img_mat

    # def StartPipe(self):
    #     try:
    #         self.pipeline.set_state(Gst.State.PLAYING)
    #         self.pipeline.get_state(Gst.CLOCK_TIME_NONE)
    #         self.source.set_tcam_property('Trigger Mode', True)

    #     except GLib.Error as error:
    #         print("Error starting pipeline: {0}".format(error))
    #         raise

#     def GetImage(self):
#         self.source.set_tcam_property('Software Trigger', True)
#         image = self.appsink.get_property('last-sample')
#         if image is not None:
#             image = self.To_numpy(image)
#             image = image[..., :3]
#         # self.source.set_tcam_property('Software Trigger', False)
#         return image

#     def StopPipeline(self):
#         self.pipeline.set_state(Gst.State.PAUSED)
#         self.pipeline.set_state(Gst.State.READY)
#         self.pipeline.set_state(Gst.State.NULL)

def ToNumpy(sample):
    ''' Convert a GStreamer sample to a numpy array
        Sample code from https://gist.github.com/cbenhagen/76b24573fa63e7492fb6#file-gst-appsink-opencv-py-L34

        The result is in self.img_mat.
    :return:
    '''
    buf = sample.get_buffer()
    caps = sample.get_caps()
    bpp = 4
    dtype = np.uint8
    bla = caps.get_structure(0).get_value('height')
    if( caps.get_structure(0).get_value('format') == "BGRx" ):
        bpp = 4

    if(caps.get_structure(0).get_value('format') == "GRAY8" ):
        bpp = 1

    if(caps.get_structure(0).get_value('format') == "GRAY16_LE" ):
        bpp = 1
        dtype = np.uint16

    img_mat = np.ndarray(
        (caps.get_structure(0).get_value('height'),
            caps.get_structure(0).get_value('width'),
            bpp),
        buffer=buf.extract_dup(0, buf.get_size()),
        dtype=dtype)
    return img_mat

def setup_camera(cam_serial):
    """
    Setup the camera access
    Input:
        cam_serial: TIS camera serial
    Return:
        cam: handler to the camera
    """
    cam = TIS(cam_serial)
    # cam = TIS(cam_serial, 1440, 1080, 120, 1, True)

    cam.StartPipe()
    return cam

import cv2
def get_image(cam_l, cam_r):
    image_l = cam_l.GetImage()
    t2 = time.time()
    image_r = cam_r.GetImage()
    t3 = time.time()
    print("the time duration between two images: \t", (t3 - t2) * 1000, " ms")

    if image_l is None:
        print("no left image received.")
    if image_r is None:
        print("no right image received.")

    if image_l is not None and image_r is not None:
        image_l = ToNumpy(image_l)
        image_l = image_l[..., :3]
        # image_l = cv2.fastNlMeansDenoisingColored(image_l,None,8,8,5,9)
        image_r = ToNumpy(image_r)
        image_r = image_r[..., :3]
    return image_l, image_r

def read_frame(image_l, image_r):
    """
    Read and return a frame from the camera
    Input:
        ia: Image acquirer
    Return:
        frame: Dictionary with timestamp (ns) and image data
    """
    # image_l = cam_l.Get_image()
    # image_r = cam_r.Get_image()

    data_out = {'left': image_l, 'right': image_r, 'ts': 0}
    return data_out

def prepare_camera_message(ts_ros, frames):
    """
    Create a camera message
    Input:
        ts_ros: ROS timestamp (ns)
        frames: Dictionary containing image_left, image_right and camera timestamp (ns)
    Return:
        msg: ROS message
    """
    msg = cam_message()
    #
    msg.ts = ts_ros
    msg.width = frames['left'].shape[1] # columns
    msg.height = frames['left'].shape[0] # rows
    try: # if it is a color image
        msg.channels = frames['left'].shape[2] # depth
    except: # if it only has two dimensions (GrayScale)
        msg.channels = 0
    msg.data_left = frames['left'].astype(np.uint8).tostring()
    msg.data_right = frames['right'].astype(np.uint8).tostring()    
    return msg


serial_l = "20910096"
serial_r = "20910094"

def sensor_publisher(save_imgs = True):
    """
    Setup camera, access frame data, and distribute the proper message on ROS
    """
    cam_l = setup_camera(serial_l)
    cam_r = setup_camera(serial_r)

    pub = rospy.Publisher('stereo_cams', cam_message, queue_size=100)
    rospy.init_node('stereo_publisher', anonymous=True)
    rate = rospy.Rate(20) # 10hz
    ts_ros_old = 0
    ts_frm_old = 0
    count =  0

    while not rospy.is_shutdown():
        #### Main loop
        ts_ros = rospy.get_rostime().to_nsec()

        image_left, image_right = get_image(cam_l, cam_r)

        if image_left is not None and image_right is not None:
            frames = read_frame(image_left, image_right)
            ts_frm_old = frames['ts']

            msg_camera = prepare_camera_message(ts_ros, frames)
            pub.publish(msg_camera)
            print('ns - dt=', int((ts_ros-ts_ros_old)/1000000), 'ms -', frames['left'].shape)
            if save_imgs:
                cv2.imwrite('./left/%.6d.jpg'%(count), image_left)
                cv2.imwrite('./right/%.6d.jpg'%(count), image_right)

            double_display = np.concatenate([cv2.resize(frames['left'], (640, 480)), \
                                        cv2.resize(frames['right'], (640, 480))], 1)
            # cv2.imshow('left', cv2.resize(frames['left'], (640, 480)))
            # cv2.imshow('right', cv2.resize(frames['right'], (640, 480)))
            cv2.imshow('stereo', double_display)

            cv2.waitKey(1)
        count += 1
        rate.sleep()
        ts_ros_old = ts_ros

    cam_l.StopPipeline()
    cam_r.StopPipeline()
    
if __name__ == '__main__':
    save_imgs = True
    try:
        sensor_publisher(save_imgs)
    except rospy.ROSInterruptException:
        pass
