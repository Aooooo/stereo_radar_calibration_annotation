import csv
import os
from glob import glob
import numpy as np
import cv2
from sklearn import mixture
from sklearn.cluster import DBSCAN
from sklearn.linear_model import LinearRegression
from collections import Counter
import matplotlib.pyplot as plt
from tqdm import tqdm
import time

def createCsvReader(csv_path):
    """
    Function:
        Create a scv reader to avoid reading the file multiple times

    Args:
        csv_path            ->          csv file path
    """
    with open(csv_path, 'r') as f:
        reader = list(csv.reader(f))
    return reader

def DbscanDenoise(pcl, epsilon=0.3, minimum_samples=100, dominant_op=False):
    """
    Function:
        Using DBSCAN for filtering out the noise data.

    Args:
        pcl             ->          point cloud to be denoised
        epsilon         ->          maximum distance to be considered as an object
        minimum_samples ->          miminum points to be considered as an object
    """
    clustering = DBSCAN(eps=epsilon, min_samples=minimum_samples).fit(pcl)
    output_labels = clustering.labels_
    if not dominant_op:
        output_pcl = []
        for label_i in np.unique(output_labels):
            if label_i == -1:
                continue
            output_pcl.append(pcl[output_labels == label_i])
    else:
        if len(np.unique(output_labels)) == 1:
            output_pcl = np.zeros([0,3])
        else:
            counts = Counter(output_labels)
            output_pcl = pcl[output_labels == counts.most_common(1)[0][0]]
    return output_pcl

def GaussianModel(pcl):
    """
    Function:
        Get the center and covariance from gaussian model.

    Args:
        pcl             ->          point cloud data
    """
    model = mixture.GaussianMixture(n_components=1, covariance_type='full')
    model.fit(pcl)
    return model.means_[0], model.covariances_[0]

def getRadarFramePoints(reader, frame_id):
    """
    Function:
        Read the radar scv file and get the all wanted frames
    
    Args:
        reader              ->              csv reader
        frame_id            ->              frame index to be read
    """
    radar_points = []
    frame_name = (6-len(str(frame_id))) * '0' + str(frame_id) + '.png'
    for line in reader:
        if line[0] == frame_name:
            this_point = []
            for info_id in range(1, len(line)):
                this_point.append(float(line[info_id]))
            radar_points.append(this_point)
    radar_points = np.array(radar_points)
    return radar_points

def readRadarCsv(csv_reader, sequence):
    """
    Function:
        Read the radar points from frame to frame.

    Args:
        csv_path            ->          csv file path
        sequence            ->          sequence to be processed

    Attention:
        radar_points        ->          [x, y, z, velocity, magnitude]
    """
    output_points = []
    for frame_id in range(sequence[0], sequence[1]):
        radar_points = getRadarFramePoints(csv_reader, frame_id)
        if len(radar_points) == 0:
            continue
        output_points.append(np.delete(radar_points[:, :3], 1, 1))
    if len(output_points) != 0:
        output_points = np.concatenate(output_points, axis=0)
    return output_points

def main(csv_path, radar_sequences, stereo_sequences, save_dir):
    """ MAIN FUNCTION """
    if not os.path.exists(save_dir):
        os.mkdir(save_dir)
    else:
        for _ in glob(os.path.join(save_dir, "*.png")):
            os.remove(_)
    fig = plt.figure(figsize=(16, 8))
    ax1 = fig.add_subplot(111)
    ax1.clear()

    csv_reader = createCsvReader(csv_path)

    radar_border_pnts = []
    for r_sequence in radar_sequences:
        radar_locs = readRadarCsv(csv_reader, r_sequence)
        if len(radar_locs) == 0:
            continue
        radar_locs_denoised = DbscanDenoise(radar_locs, epsilon=0.5, minimum_samples=2, \
                                dominant_op=True)
        scatterPnts(ax1, radar_locs_denoised, "r")
        radar_locs_mean, _ = GaussianModel(radar_locs_denoised)
        if len(radar_border_pnts) == 0:
            radar_border_pnts.append(np.zeros((1,2)))
        radar_border_pnts.append(np.expand_dims(radar_locs_mean, 0))
    radar_border_pnts = np.concatenate(radar_border_pnts, 0)
    r_reg = LinearRegression().fit(radar_border_pnts[:, 0:1], radar_border_pnts[:, 1:2])
    r_coef = r_reg.coef_[0][0]
    r_intercept = r_reg.intercept_[0]
    print("radar fov: y = ax + b; a = %.10f; b=%.10f"%(r_coef, r_intercept))

    stereo_border_pnts = []
    for s_sequence in stereo_sequences:
        stereo_locs = readRadarCsv(csv_reader, s_sequence)
        if len(stereo_locs) == 0:
            continue
        stereo_locs_denoised = DbscanDenoise(stereo_locs, epsilon=0.5, minimum_samples=2, \
                                dominant_op=True)
        scatterPnts(ax1, stereo_locs_denoised, "b")
        stereo_locs_mean, _ = GaussianModel(stereo_locs_denoised)
        stereo_border_pnts.append(np.expand_dims(stereo_locs_mean, 0))
    stereo_border_pnts = np.concatenate(stereo_border_pnts, 0)
    s_reg = LinearRegression().fit(stereo_border_pnts[:, 0:1], stereo_border_pnts[:, 1:2])
    s_coef = s_reg.coef_[0][0]
    s_intercept = s_reg.intercept_[0]
    print("stereo fov: y = ax + b; a = %.10f; b=%.10f"%(s_coef, s_intercept))

    plotLine(ax1, r_coef, r_intercept, "r", "radar_fov")
    plotLine(ax1, s_coef, s_intercept, "b", "stereo_fov")
    ax1.set_xlim([-50, 50])
    ax1.set_ylim([0, 50])
    ax1.legend()
    plt.savefig(save_dir + "fovs.png")
    plt.show()

def scatterPnts(ax, pnts, color):
    """ Function: Plot points """
    ax.scatter(pnts[:,0], pnts[:,1], s=0.5, c=color)

def plotLine(ax, coef, inter, color, label):
    """ Function: Plot line """
    x = np.linspace(-50,50)
    y1 = x * coef + inter
    y2 = x * coef * -1 + inter
    ax.plot(x, y1, color)
    ax.plot(x, y2, color, label=label)

if __name__ == "__main__":
    time_stamp = "2020-06-26-10-33-53"
    radar_csv_path = "/DATA/" + time_stamp + "/ral_outputs_" + time_stamp + "/DOAPoints.csv"
    save_dir = "./output_figures"
    # radar_sequences = [[186, 189], [298, 302], [620, 621]]
    radar_sequences = [[186, 189], [309, 316]]
    # radar_sequences = [[186, 189]]

    stereo_sequences = [[124, 126], [378, 381], [526, 530]]

    main(radar_csv_path, radar_sequences, stereo_sequences, save_dir)
