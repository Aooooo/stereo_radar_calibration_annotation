import cv2
import numpy as np
import glob
from tqdm import tqdm
import os

def currentTimeStamp():
    """
    Function:
        Get the time stamp from the current dir's name
    """
    current_dir = os.getcwd()
    dir_names = current_dir.split("/")
    ts_string = dir_names[-1]
    return ts_string

def ExtractImgTimeStamps(img_folder):
    """
    Function:
        Extract image time stamps for sync.
    """ 
    all_files = glob.glob(img_folder + "/*.png")
    all_ts = []
    for name in all_files:
        filename = name.replace(img_folder, '')
        res = ''.join(filter(lambda i: i.isdigit(), filename))
        ts_num = int(res)
        if ts_num not in all_ts:
            all_ts.append(ts_num)
    all_ts = np.array(all_ts)
    return all_ts

def BuildFolders(save_dir):
    """
    Function:
        Build folders for saving the stereo images.
    """
    if not os.path.isdir(save_dir):
        os.mkdir(save_dir)

def GetSequence(time_stamp, img_folder, save_folder):
    """
    Function:
        Get the sequence of camera data w.r.t valida radar data.
    """
    all_img_ts = ExtractImgTimeStamps(img_folder)
    count = 0
    with open('adc_ts_' + time_stamp + '.txt', 'r') as f:
        a = f.readlines()
        for i in tqdm(range(len(a))):
            sequence_num = int(a[i])
            if sequence_num != 0:
                ts_diff = np.abs(all_img_ts - sequence_num) 
                ts_min_ind = np.argmin(ts_diff)
                img_ts = all_img_ts[ts_min_ind]
                img_name = img_folder + str(img_ts) + ".png"
                image = cv2.imread(img_name)
                # image_left = cv2.resize(image_left, (640, 480))
                # image_right = cv2.resize(image_right, (640, 480))
                # display = np.concatenate([image_left, image_right], axis = 1)
                # cv2.imshow('img', display)
                # cv2.waitKey(100)
                save_name = save_folder + (6 - len(str(count))) * '0' + str(count) + '.png' 
                cv2.imwrite(save_name, image)
                count += 1

if __name__ == "__main__":
    time_stamp = currentTimeStamp()
    image_folder = "/DATA/" + time_stamp + "/datasets/sc_radardata/thermal_images/"
    save_folder = "./thermal_imgs/"
    BuildFolders(save_folder)
    GetSequence(time_stamp, image_folder, save_folder)
