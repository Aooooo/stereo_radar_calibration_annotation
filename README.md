# Stereo Radar Research

researching on the stereo radar project. Keep updating for some important notes.

## Stereo Data Capturing

goto `stereo_cams_reading`

## Stereo Processing

goto `stereo_process`

## Radar Processing

goto `radar_process`

## Calibration

After finishing `stereo_process` and `radar_process`, for registration (calibration) of two sensors goto `registration`.

## Radar Annotation

goto `annotation`

## Radar annotation manual correction

goto `manual_correction`

## QUICK CLEAN

do
```
./clean_all.sh
```
for clearning all data in the all subdirectories.
