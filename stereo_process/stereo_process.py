import numpy as np
import cv2
import os
import json
from glob import glob
from tqdm import tqdm

import src.data_preparation as stereo_data
import src.save_intrin_extrin as stereo_calibration
import src.ThreeD_reconstruction_mp as stereo_reconstruct
import src.find_reflector as stereo_reflector_HSV
import src.locate_reflector as stereo_reflector_pcl

def readConfig():
    """
    Function:
        Read the stereo processing config file in the same directory.
    """
    config_file_name = "./stereo_config.json"
    with open(config_file_name) as json_file:
        stereo_config = json.load(json_file)
    return stereo_config

if __name__ == "__main__":
    # read config file
    stereo_config = readConfig()
    # stereo calibration config
    stereo_calibration_config = stereo_config['calibration']
    # stereo reconstruction config
    stereo_reconstruct_config = stereo_config['3d_reconstruction']
    # stereo registration config
    stereo_registr_config = stereo_config['registration']
    # ---------------------- calibration ------------------
    # do the stereo calibration
    if stereo_calibration_config['do_calibration']:
        if not stereo_calibration_config['skip_data_reading']:
            stereo_data.main(stereo_calibration_config['do_calibration'], 
                            stereo_calibration_config['calibration_datapath'])
        stereo_calibration.main(tuple(stereo_calibration_config['chess_board_size']),
                                stereo_calibration_config['chess_board_grid'],
                                stereo_calibration_config['calibration_img_indexes'])
    # ---------------------- 3d reconstruction ------------------
    if stereo_reconstruct_config['do_reconstruction']:
        if not stereo_reconstruct_config['skip_data_reading']:
            # process stereo images
            stereo_data.main(False, stereo_reconstruct_config['stereo_process_datapath'])
        # 3d reconstruction
        stereo_reconstruct.main(stereo_reconstruct_config['save_pcl'],
                                stereo_reconstruct_config['save_meshlab'],
                                stereo_reconstruct_config['show_disparity'],
                                stereo_reconstruct_config['pcl_constraints'],
                                stereo_reconstruct_config['sequence_nums'])
    # ---------------------- reflector finding ------------------
    if stereo_registr_config['do_registration']:
        if stereo_registr_config['find_hsv_threshold']:
            # find reflector using HSV threshold
            stereo_reflector_HSV.main(stereo_reconstruct_config['sequence_nums'],
                    stereo_registr_config['index_to_find_threshold'],
                    stereo_registr_config['find_hsv_threshold'],
                    np.array(stereo_registr_config['hsv_lower_bound'], dtype="uint8"),
                    np.array(stereo_registr_config['hsv_higher_bound'], dtype="uint8"))
        else:
            # find reflector using HSV threshold
            stereo_reflector_HSV.main(stereo_reconstruct_config['sequence_nums'],
                    stereo_registr_config['index_to_find_threshold'],
                    stereo_registr_config['find_hsv_threshold'],
                    np.array(stereo_registr_config['hsv_lower_bound'], dtype="uint8"),
                    np.array(stereo_registr_config['hsv_higher_bound'], dtype="uint8"))
            # find reflector point cloud
            stereo_reflector_pcl.main(stereo_reconstruct_config['sequence_nums'],
                                    stereo_registr_config['x_threshold'],
                                    stereo_registr_config['z_threshold'],
                                    stereo_registr_config['reflector_plots_on'],
                                    stereo_registr_config['save_reflector_pcl'],
                                    stereo_registr_config['use_multiprocessing'])

