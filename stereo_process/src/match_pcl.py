import numpy as np
import cv2
from tqdm import tqdm
import matplotlib.pyplot as plt
from glob import glob

def ReadPcl(frame_id, gr_dir, file_name_prefix, sequences):
    """
    Function:
        read all the point cloud in the current directory.

    Args:
        file_dir            ->          point could directory
        sequences           ->          from which frame to which frame
    """
    pcl_name = gr_dir + file_name_prefix + str(frame_id) + ".npy"
    pcl = np.load(pcl_name)
    return pcl

def ReadRadarDoa(doa_dir, frame_id):
    """
    Function:
        read radar doa data for comparision with stereo pcl.

    Args:
        doa_dir             ->          doa images directory
        frame_id            ->          frame needed to be read
    """
    doa_file_name = doa_dir + (6 - len(str(frame_id))) * '0' + str(frame_id) + '.png' 
    doa_img = cv2.imread(doa_file_name)
    return doa_img

def ReadPclImages(pcl_img_dir, frame_id):
    """
    Function:
        read point cloud images saved from the previous step.

    Args:
        pcl_img_dir             ->          point cloud images directory
        frame_id                ->          frame needed to be read
    """
    pcl_file_name = pcl_img_dir + "pcl_" + str(frame_id) + ".png"
    pcl_img = cv2.imread(pcl_file_name)
    return pcl_img
 
def ComparePclDoa(pcl_img, doa_img):
    """
    Function:
        resize point cloud images and doa images to the same size and 
    concatenate them.

    Args:
        pcl_img             ->          point cloud images
        doa_img             ->          doa images
    """
    h_pcl, w_pcl, ch_pcl = pcl_img.shape
    h_doa, w_doa, ch_doa = doa_img.shape
    w = np.amin([w_pcl, w_doa])
    h = np.amin([h_pcl, h_doa])
    size = (w, h)
    pcl_img_resized = cv2.resize(pcl_img, size)
    doa_img_resized = cv2.resize(doa_img, size)
    output_img = np.concatenate([pcl_img_resized, doa_img_resized], axis = 1)
    return output_img

def GeneratePclImages(pcl_dir, save_dir, name_prefix, sequences):
    """
    Function:
        Generate point cloud images from stereo camera

    Args: 
        pcl_dir          ->          point cloud directory
        save_dir         ->          directory for saving the files
        name_prefix      ->          point cloud file prefix
        sequences        ->          frames to be read
    """
    fig = plt.figure(figsize = (12, 12))
    ax1 = fig.add_subplot(111)
    for frame_id in tqdm(range(sequences[0], sequences[1])):
        pcl = ReadPcl(frame_id, pcl_dir, name_prefix, sequences)
        ax1.clear()
        ax1.scatter(pcl[:,0], pcl[:,2], 0.2, 'b')
        ax1.set_xlim(-10, 10)
        ax1.set_ylim(0, 40)
        plt.savefig(save_dir + "pcl_" + str(frame_id) + ".png")

def main():
    """
    Main Function
    """
    # some basic parameters
    sequences = [2000, 3880]
    save_dir = "./pcl_doa_fig/"
    # ground remove pcl
    gr_dir = "./gr_pcl/"
    gr_name_prefix = "gr_"
    # original pcl
    original_dir = "./original_pcl/"
    original_name_prefix = "organized_"
    # radar folder
    doa_dir = "/DATA/2020-01-11-12-27-08/ral_outputs_2020-01-11-12-27-08/doa/"

    # generate all 2d point cloud files
    pcl_img_files = glob(save_dir + "pcl_" + "*.png")
    num_saved_files = len(pcl_img_files)
    if num_saved_files != (sequences[1] - sequences[0]):
        GeneratePclImages(gr_dir, save_dir, gr_name_prefix, sequences)

    # generate compare images with doa
    for frame_id in tqdm(range(sequences[0], sequences[1])):
        doa_img = ReadRadarDoa(doa_dir, frame_id)
        pcl_img = ReadPclImages(save_dir, frame_id)
        compare_img = ComparePclDoa(pcl_img, doa_img)
        # cv2.imshow('img', compare_img)
        cv2.imwrite(save_dir + "compare_" + str(frame_id) + ".png", compare_img)
        cv2.waitKey(1)

if __name__ == "__main__":
    main()
    
