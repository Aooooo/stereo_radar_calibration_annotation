"""
This code is for removing all ground points in the generated point cloud
from code 3D_reconstruction.py
"""
import numpy
import scipy # use numpy if scipy unavailable
import scipy.linalg # use numpy if scipy unavailable
import numpy as np
import cv2
from tqdm import tqdm
import matplotlib.pyplot as plt

def ransac(data,model,n,k,t,d,debug=True,return_all=True):
    """fit model parameters to data using the RANSAC algorithm
    
This implementation written from pseudocode found at
http://en.wikipedia.org/w/index.php?title=RANSAC&oldid=116358182

{{{
Given:
    data - a set of observed data points
    model - a model that can be fitted to data points
    n - the minimum number of data values required to fit the model
    k - the maximum number of iterations allowed in the algorithm
    t - a threshold value for determining when a data point fits a model
    d - the number of close data values required to assert that a model fits well to data
Return:
    bestfit - model parameters which best fit the data (or nil if no good model is found)
iterations = 0
bestfit = nil
besterr = something really large
while iterations < k {
    maybeinliers = n randomly selected values from data
    maybemodel = model parameters fitted to maybeinliers
    alsoinliers = empty set
    for every point in data not in maybeinliers {
        if point fits maybemodel with an error smaller than t
             add point to alsoinliers
    }
    if the number of elements in alsoinliers is > d {
        % this implies that we may have found a good model
        % now test how good it is
        bettermodel = model parameters fitted to all points in maybeinliers and alsoinliers
        thiserr = a measure of how well model fits these points
        if thiserr < besterr {
            bestfit = bettermodel
            besterr = thiserr
        }
    }
    increment iterations
}
return bestfit
}}}
"""
    iterations = 0
    bestfit = None
    besterr = numpy.inf
    best_inlier_idxs = None
    while iterations < k:
        maybe_idxs, test_idxs = random_partition(n,data.shape[0])
        maybeinliers = data[maybe_idxs,:]
        test_points = data[test_idxs]
        maybemodel = model.fit(maybeinliers)
        test_err = model.get_error( test_points, maybemodel)
        also_idxs = test_idxs[test_err < t] # select indices of rows with accepted points
        alsoinliers = data[also_idxs,:]
        if debug:
            print('test_err.min()',test_err.min())
            print('test_err.max()',test_err.max())
            print('numpy.mean(test_err)',numpy.mean(test_err))
            print('iteration %d:len(alsoinliers) = %d'%(
                iterations,len(alsoinliers)))
        if len(alsoinliers) > d:
            betterdata = numpy.concatenate( (maybeinliers, alsoinliers) )
            bettermodel = model.fit(betterdata)
            better_errs = model.get_error( betterdata, bettermodel)
            thiserr = numpy.mean( better_errs )
            if thiserr < besterr:
                bestfit = bettermodel
                besterr = thiserr
                best_inlier_idxs = numpy.concatenate( (maybe_idxs, also_idxs) )
        iterations+=1
        # if iterations % 100 == 0:
        #     print("Current iteration", iterations)
    if bestfit is None:
        raise ValueError("did not meet fit acceptance criteria")
    if return_all:
        return bestfit, {'inliers':best_inlier_idxs}
    else:
        return bestfit

def random_partition(n,n_data):
    """return n random rows of data (and also the other len(data)-n rows)"""
    all_idxs = numpy.arange( n_data )
    numpy.random.shuffle(all_idxs)
    idxs1 = all_idxs[:n]
    idxs2 = all_idxs[n:]
    return idxs1, idxs2

class PlaneLeastSquareModelCustomized:
    """
    Plane model solved using linear least squares
    """
    def __init__(self,input_columns,output_columns,debug=False):
        self.input_columns = input_columns
        self.output_columns = output_columns
        self.debug = debug
    def fit(self, data):
        A = numpy.vstack([data[:,i] for i in self.input_columns]).T
        B = numpy.vstack([data[:,i] for i in self.output_columns]).T
        x,resids,rank,s = scipy.linalg.lstsq(A,B)
        return x
    def get_error( self, data, model):
        A = numpy.vstack([data[:,i] for i in self.input_columns]).T
        B = numpy.vstack([data[:,i] for i in self.output_columns]).T
        B_fit = scipy.dot(A,model)
        # err_per_point = numpy.mean((B-B_fit)**2)
        err_per_point = numpy.sum((B-B_fit)**2, axis=1) 
        shape = len(err_per_point.shape)
        while shape != 1:
            err_per_point = numpy.sum(err_per_point, axis=-1) 
            shape = len(err_per_point.shape)
        return err_per_point 

    def CalculateEuclideanDistance(model, data):
        distance = np.absolute(scipy.dot(data, model) - 1) / np.sqrt(np.sum(np.square(model)))
        return distance

def EuclideanDistance(model, data):
    """
    Functionality:
        calculate L2 distance from every point of the dataset to the plane model we found

    Inputs:
        model                           ->              ax + by + cz = 1
        data                            ->              all points in the pcl

    outputs:
        distance                        ->              distance w.r.t each point

    """
    distance = np.absolute(scipy.dot(data, model) - 1) / np.sqrt(np.sum(np.square(model)))
    return distance

def ReadOriginalPcl(frame_index):
    """
    Function:
        Read the point cloud data for specific frame.

    Args:
        frame_index         ->          index of the frame to be processed
    """
    original_pcl = np.load("original_pcl/initial_" + str(frame_index) + ".npy")
    thresholed_pcl = np.load("original_pcl/organized_" + str(frame_index) + ".npy")
    thresholed_color = np.load("original_pcl/color_" + str(frame_index) + ".npy")
    return original_pcl, thresholed_pcl, thresholed_color

def RanSacPlane(input_points, minimal_points_portion = 0.5, num_iteration = 1000,
                expand_points_threshold = 0.03, num_points_assert_plane = 2000):
    """
    Function:
        run RANSAC algorithm to build the plane model.abs

    Args:
        input_points            ->          input points for RANSAC, formatting (num_points, 3)
    """
    data_size = input_points.shape[0]
    expanded_ones = np.ones((data_size, 1))
    data_expand = np.concatenate([input_points, expanded_ones], axis = -1)

    input_columns = range(data_expand.shape[1] - 1)
    output_columns = [data_expand.shape[1] - 1]
    debug = False
    model = PlaneLeastSquareModelCustomized(input_columns,output_columns,debug=debug)
    min_points_required = int(minimal_points_portion * data_size)

    # run RANSAC algorithm
    ransac_fit, ransac_data = ransac(data_expand, model,
                                     min_points_required, num_iteration, 
                                     expand_points_threshold, num_points_assert_plane, 
                                     debug=debug, return_all=True)

    # Print the plane parameter values to see whether it is right
    # normally, in ax + by + cz = 1, with a, c nearly 0, and b nearly the 1/height of the sensor.
    # print("The RANSAC plane function: \n", ransac_fit)
    return ransac_fit

def InitialPlane(original_pcl, plane_indexes):
    """
    Function:
        Using ransac to find the intial plane using manually annotated points.

    Args:
        original_pcl          ->          the orginal point cloud with format (w, h, pos)
        plane_indexes       ->          indexed of the (w, h) for the manually found pcl

    Note:
        plane_indexes format: [w1, h1, w2, h2], which could form a square area in the image.
    """
    input_points = []
    for i in range(len(plane_indexes)):
        w1, h1, w2, h2 = plane_indexes[i]
        current_input_points = original_pcl[w1:w2, h1:h2, ...]
        current_input_points = current_input_points.reshape(-1, current_input_points.shape[-1])
        input_points.append(current_input_points)
    input_points = np.concatenate(input_points, axis = 0)
    initial_plane = RanSacPlane(input_points)
    return initial_plane

def SequentPlane(thresholed_pcl, last_frame_plane, distance_threshold = 0.2):
    """
    Function:
        Using the gorund plane we found in the last frame to find the ground of 
    the current plane. Hopefully, this method could improve the accuracy of the 
    ground plane finding algorithm.

    Args:
        thresholed_pcl          ->          point cloud after x, y, z thresholds
        last_frame_plane        ->          the plane function we found in the last frame
    """
    distance_to_last_plane = EuclideanDistance(last_frame_plane, thresholed_pcl)
    dist_mask =  distance_to_last_plane <= distance_threshold
    input_points = thresholed_pcl[dist_mask[:, 0]]
    sequent_plane = RanSacPlane(input_points)
    return sequent_plane

def RemoveGroundPoints(thresholed_pcl, thresholed_color, ground_plane, 
                        ground_threshold = 0.2,  floor_threshold = 2.):
    """
    Function:
        remove all the ground points on the point cloud.

    Args:
        thresholed_pcl          ->          point cloud after x, y, z constraints
        ground_plane            ->          ground plane that found previously
    """
    distance_to_ground = EuclideanDistance(ground_plane, thresholed_pcl) 
    # remove ground plane points
    ground_remove_mask = distance_to_ground >= ground_threshold
    # remove points that are toohigh
    floor_remove_mask = distance_to_ground <=  floor_threshold
    # remove -y points
    ground_above_mask = np.matmul(thresholed_pcl, ground_plane) < 1 
    # combine all masks 
    filter_mask = ground_remove_mask & floor_remove_mask & ground_above_mask
    # filter out all unnecessary points
    output_points = thresholed_pcl[filter_mask[:, 0]]
    output_colors = thresholed_color[filter_mask[:, 0]]
    return output_points, output_colors 

def DebugInitialPlane(frame_id):
    """
    Function:
        for debug mode.
    """
    # fig = plt.figure()
    # ax1 = fig.add_subplot(121)
    # ax2 = fig.add_subplot(122)
    original_pcl, thresholed_pcl, thresholed_color = ReadOriginalPcl(frame_id)
    # ax1.imshow(original_pcl)
    
    given_y = 1.4
    original_pcl_y = original_pcl[..., 1]
    # mask = original_pcl_y > 0 
    mask = np.abs(original_pcl_y - given_y) <= 0.2

    # for i in range(mask.shape[0]):
    #     for j in range(mask.shape[1]):
    #         if mask[i,j] == True:
    #             original_pcl[i, j] = original_pcl[i, j]
    #         else:
    #             original_pcl[i, j] = 0.

    # original_pcl[..., 1] = - original_pcl[..., 1]
    # ax2.imshow(original_pcl)
    # plt.show()

    input_points = original_pcl[mask]
    initial_plane = RanSacPlane(input_points)
    return initial_plane

def main(sequences, initial_plane_index):
    """
    Function:
        Main function for removing the ground points.
    """
    ground_plane = None
    last_frame_plane = None

    for frame_id in tqdm(range(sequences[0], sequences[1])):
        original_pcl, thresholed_pcl, thresholed_color = ReadOriginalPcl(frame_id)
        if frame_id == sequences[0]:
            # initial_plane = InitialPlane(original_pcl, initial_plane_index)
            ##### attention: currently under debug, formally remove debug #####
            initial_plane = DebugInitialPlane(frame_id)
            ground_plane = initial_plane
        else:
            ground_plane = SequentPlane(thresholed_pcl, last_frame_plane)
        last_frame_plane = ground_plane

        # Print the plane parameter values to see whether it is right
        # normally, in ax + by + cz = 1, with a, c nearly 0, and b nearly the 1/height of the sensor.
        print("The ground plane: \n", ground_plane)

        gr_pcl, gr_colors = RemoveGroundPoints(thresholed_pcl, thresholed_color, ground_plane, 
                                                ground_threshold = 0.25)

        # save point cloud as npy
        np.save("gr_pcl/gr_" + str(frame_id) + ".npy", gr_pcl)

        # save meshlab pcl
        output_file = "./gr_meshlab_pcl/mesh_" + str(frame_id) + ".ply"
        colors = gr_colors.reshape(-1,3)
        vertices = np.hstack([gr_pcl.reshape(-1,3), colors])
        ply_header = '''ply
            format ascii 1.0
            element vertex %(vert_num)d
            property float x
            property float y
            property float z
            property uchar red
            property uchar green
            property uchar blue
            end_header
            '''
        with open(output_file, 'w') as f:
            f.write(ply_header %dict(vert_num=len(vertices)))
            np.savetxt(f,vertices,'%f %f %f %d %d %d')

if __name__ == "__main__":
    # some basic parameter settings 
    sequences = [2000, 3880]
    initial_plane_index = np.array([[305, 299, 384, 361],
                                    [263, 361, 434, 480]])
    main(sequences, initial_plane_index)
