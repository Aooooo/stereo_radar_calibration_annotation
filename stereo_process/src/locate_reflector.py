import numpy as np
import cv2
import os
from glob import glob
import matplotlib.pyplot as plt
from tqdm import tqdm
import multiprocessing as mp
from functools import partial
from sklearn.cluster import DBSCAN
from collections import Counter

def getTotoalImgNum(img_dir):
    """
    Function:
        Get the total number of images, prepare for the sequence number

    Args:
        img_dir             ->          image directory for counting
    """
    all_imgs = glob(img_dir + "*.jpg")
    return len(all_imgs)

def ReadOriginalPcl(pcl_dir, frame_index):
    """
    Function:
        Read the point cloud data for specific frame.

    Args:
        pcl_dir             ->          directory that saves the point cloud
        frame_index         ->          index of the frame to be processed
    """
    original_pcl = np.load(pcl_dir + "initial_" + str(frame_index) + ".npy")
    thresholed_pcl = np.load(pcl_dir + "organized_" + str(frame_index) + ".npy")
    thresholed_color = np.load(pcl_dir + "color_" + str(frame_index) + ".npy")
    return original_pcl, thresholed_pcl, thresholed_color

def ReadReflectorMask(mask_dir, frame_index):
    """
    Function:
        Read the segmentation mask of the reflector for current frame.

    Args:
        mask_dir            ->          directory that saves the masks
        frame_index         ->          which frame to be processed
    """
    reflector_mask = np.load(mask_dir + "mask_reflect_" + str(frame_index) + ".npy")
    return reflector_mask

def ReflectorPcl(original_pcl, mask):
    """
    Function:
        apply mask onto the original pcl to find the reflector.
    """
    mask = np.expand_dims(mask, -1)
    reflector_pcl = np.where(mask == 255, original_pcl, 0)
    non_reflector_pcl = np.where(mask == 0, original_pcl, 0)
    return reflector_pcl, non_reflector_pcl

def DbscanDenoise(pcl, epsilon=0.3, minimum_samples=300):
    """
    Function:
        Using DBSCAN for filtering out the noise data.

    Args:
        pcl             ->          point cloud to be denoised
        epsilon         ->          maximum distance to be considered as an object
        minimum_samples ->          miminum points to be considered as an object
    """
    clustering = DBSCAN(eps=epsilon, min_samples=minimum_samples).fit(pcl)
    output_labels = clustering.labels_
    if len(np.unique(output_labels)) == 1 and np.unique(output_labels)[0] == -1:
        output_pcl = np.zeros([0,3])
    else:
        counts = Counter(output_labels)
        output_pcl = pcl[output_labels == counts.most_common(1)[0][0]]
    return output_pcl

def ThresholdPcl(pcl, x_threshold = None, z_threshold=None):
    """
    Function:
        Constrain the distance with pre-defined thresholds

    Args:
        pcl             ->          target point cloud
        x_threshold     ->          threshold along x
        z_threshold     ->          Threshold along z
    """
    assert len(pcl.shape) == 2
    assert pcl.shape[-1] == 3
    output_pcl = pcl
    if x_threshold is not None:
        output_pcl = output_pcl[output_pcl[:, 0] <= x_threshold]
        output_pcl = output_pcl[output_pcl[:, 0] >= -x_threshold]
    if z_threshold is not None:
        output_pcl = output_pcl[output_pcl[:, 2] <= z_threshold]
    return output_pcl

def TransferToScatterPoints(points_3d):
    """
    Function:
        Transfer the 3d matrices pcl to 2d matrices pcl

    Args:
        points_3d           ->          target 3d points
    """
    non_zero_points = points_3d.copy()
    non_zero_mask = non_zero_points[..., -1] > 0
    non_zero_points = non_zero_points[non_zero_mask]
    return non_zero_points

def ExtractReflector(pcl_dir, mask_dir, sequence, save_dir, x_threshold, \
                    z_threshold, debug_mode=False, figure_mode=True, save_pcl=True):
    """
    Function:
        Using mask to filter out unnecessary points

    Args:
        pcl_dir             ->          directory that saves the point cloud
        mask_dir            ->          directory that saves the masks
        sequence            ->          the sequence number that waited to be processed
    """
    if debug_mode:
        figure_mode = False
        save_pcl = False
        ##### setup for matplotlib #####
        plt.ion()
        fig = plt.figure()
        ax1 = fig.add_subplot(131)
        ax2 = fig.add_subplot(132)
        ax3 = fig.add_subplot(133)

    if figure_mode:
        plt.ion()
        fig = plt.figure()
        ax1 = fig.add_subplot(111)

    for frame_i in tqdm(range(sequence[0], sequence[1])):
        original_pcl, thresholed_pcl, thresholed_color = ReadOriginalPcl(pcl_dir, frame_i)
        reflector_mask = ReadReflectorMask(mask_dir, frame_i)
        reflector_pcl_3d, non_reflector_pcl_3d = ReflectorPcl(original_pcl, reflector_mask)
        reflector_pcl = TransferToScatterPoints(reflector_pcl_3d)
        reflector_pcl = ThresholdPcl(reflector_pcl, x_threshold=x_threshold, \
                                    z_threshold=z_threshold)
        if len(reflector_pcl) != 0:
            reflector_pcl = DbscanDenoise(reflector_pcl, epsilon=1, minimum_samples=3)
        non_reflector_pcl = TransferToScatterPoints(non_reflector_pcl_3d)

        if save_pcl:
            np.save(save_dir + "reflector_pcl_" + str(frame_i) + ".npy", reflector_pcl)

        if figure_mode:
            ##### opencv version of plotting #####
            # display_imgs = np.concatenate([non_reflector_pcl_3d[..., -1], reflector_pcl_3d[..., -1]], axis=1) / non_reflector_pcl_3d[..., -1].max() * 255
            # display_imgs = display_imgs.astype(np.uint8)
            # display_imgs = cv2.applyColorMap(display_imgs, cv2.COLORMAP_HOT)
            # cv2.imwrite(save_dir + "depthmap_" + str(frame_i) + ".jpg", display_imgs)
            # cv2.waitKey(10)
            ##### scatter and save
            plt.cla()
            ax1.clear()
            # ax1.scatter(non_reflector_pcl[:, 0], non_reflector_pcl[:, 2], s=0.2, c='b', label="background points")
            ax1.scatter(reflector_pcl[:, 0], reflector_pcl[:, 2], s=0.5, c='r', label="reflector")
            ax1.set_xlim([-25, 25])
            ax1.set_ylim([0, 50])
            ax1.legend()
            fig.canvas.draw()
            plt.savefig(save_dir + "stereo_scatter_" + str(frame_i) + ".png")
            plt.pause(0.001)

        if debug_mode:
            #### matplotlib version of plotting #####
            ax1.clear()
            ax1.imshow(reflector_pcl_3d[..., -1])
            ax2.clear()
            ax2.imshow(non_reflector_pcl_3d[..., -1])
            ax3.clear()
            ax3.scatter(non_reflector_pcl[:, 0], non_reflector_pcl[:, 2], s=0.2, c='b')
            ax3.scatter(reflector_pcl[:, 0], reflector_pcl[:, 2], s=0.4, c='r')
            ax3.set_xlim([-10, 10])
            ax3.set_ylim([0, 40])
            fig.canvas.draw()
            plt.pause(0.03) 

def ExtractReflectorMP(frame_i, pcl_dir, mask_dir, save_dir, x_threshold, \
                        z_threshold, save_pcl=True):
    """
    Function:
        Using mask to filter out unnecessary points

    Args:
        pcl_dir             ->          directory that saves the point cloud
        mask_dir            ->          directory that saves the masks
        sequence            ->          the sequence number that waited to be processed
    """
    original_pcl, thresholed_pcl, thresholed_color = ReadOriginalPcl(pcl_dir, frame_i)
    reflector_mask = ReadReflectorMask(mask_dir, frame_i)
    reflector_pcl_3d, non_reflector_pcl_3d = ReflectorPcl(original_pcl, reflector_mask)
    reflector_pcl = TransferToScatterPoints(reflector_pcl_3d)
    reflector_pcl = ThresholdPcl(reflector_pcl, x_threshold=x_threshold, \
                                z_threshold=z_threshold)
    if len(reflector_pcl) != 0:
        reflector_pcl = DbscanDenoise(reflector_pcl, epsilon=1, minimum_samples=3)
    non_reflector_pcl = TransferToScatterPoints(non_reflector_pcl_3d)

    if save_pcl:
        np.save(save_dir + "reflector_pcl_" + str(frame_i) + ".npy", reflector_pcl)

def main(sequence, x_thres, z_thres, figure_mode, save_pcl, use_mp=False):
    pcl_dir = "./pcl/"
    mask_dir = "./stereo_reflector/"
    save_dir = "./stereo_reflector/"
    if not use_mp:
        ExtractReflector(pcl_dir, mask_dir, sequence, save_dir, x_thres, z_thres, \
                        debug_mode=False, figure_mode=figure_mode, save_pcl=save_pcl)
    else:
        pool = mp.Pool(4)
        Extract_1arg = partial(ExtractReflectorMP, \
                            pcl_dir = pcl_dir, \
                            mask_dir = mask_dir, \
                            save_dir = save_dir, \
                            x_threshold = x_thres, \
                            z_threshold = z_thres, \
                            save_pcl = save_pcl)
        for _ in tqdm(pool.imap_unordered(Extract_1arg, \
                    range(sequence[0], sequence[1])), \
                    total = sequence[1] - sequence[0]):
            pass
        pool.close()
        pool.join()
        pool.close()

if __name__ == "__main__":
    sequence = [0, getTotoalImgNum("./left/")]
    figure_mode = False
    save_pcl = True
    use_mp = False
    main(sequence, figure_mode, save_pcl, use_mp)
    
