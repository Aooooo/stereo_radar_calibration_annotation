'''
This code is for using the result of calibration and rectification for computing
disparity and recontruct it into 3D point cloud.
'''
from mpl_toolkits.mplot3d import Axes3D 
import matplotlib.pyplot as plt
import numpy as np
import cv2
from glob import glob
import scipy
import os
from tqdm import tqdm
import multiprocessing as mp
from functools import partial

def getTotoalImgNum(img_dir):
    """
    Function:
        Get the total number of images, prepare for the sequence number

    Args:
        img_dir             ->          image directory for counting
    """
    all_imgs = glob(img_dir + "*.jpg")
    return len(all_imgs)

def checkoutDir(dir_name):
    """
    Function:
        Checkout if the directory is exists, if not then create one

    Args:
        dir_name            ->          directory name
    """
    if not os.path.exists(dir_name):
        os.mkdir(dir_name)
    else:
        file_list = glob(os.path.join(dir_name, '*'))
        if len(file_list) != 0:
            for f in file_list:
                os.remove(f)

def ReadParameters():
    """
    Functionality:
        Read all parameters saved by save_intrin_extrin.py
    
    Outputs:
        left_maps                           ->          calibration and rectification remap parameter of left images
        right_maps                          ->          calibration and rectification remap parameter of right images
        ProjLeft                            ->          projection matrices for left images
        ProjRight                           ->          projection matrices for right images
        Q                                   ->          used for depth estimation from disparity (triangulation)
    """
    left_maps = np.load("./calibration_results/left_maps.npy")
    right_maps = np.load("./calibration_results/right_maps.npy")
    ProjLeft = np.load("./calibration_results/ProjLeft.npy")
    ProjRight = np.load("./calibration_results/ProjRight.npy")
    roiL = np.load("./calibration_results/roiL.npy")
    roiR = np.load("./calibration_results/roiR.npy")
    Q = np.load("./calibration_results/Q.npy")

    # transfer np.array to tuple (since the input of these two parameters should be formed in tuple)
    roiL = tuple(map(int, roiL))
    roiR = tuple(map(int, roiR))

    return left_maps, right_maps, ProjLeft, ProjRight, roiL, roiR, Q

def BMBuildDisparity(Left_Remap, Right_Remap, roiL, roiR):
    """
    Functionality:
        Build the disparity using Block Matching.
    
    Inputs:
        left_maps                           ->          calibration and rectification remap parameter of left images
        right_maps                          ->          calibration and rectification remap parameter of right images
        roiL, roiR                          ->          outputs of cv2.stereoRectify() for telling the size of the images

    Outputs:
        disparity                           ->          disparity maps
    """
    stereo = cv2.StereoBM_create()
    stereo.setMinDisparity(0)
    stereo.setNumDisparities(16*10)
    stereo.setBlockSize(5)
    stereo.setROI1(roiL)
    stereo.setROI2(roiR)
    stereo.setSpeckleRange(11)
    stereo.setSpeckleWindowSize(0)

    left_matcher = stereo
    right_matcher = cv2.ximgproc.createRightMatcher(left_matcher)
    lmbda = 80000
    sigma = 1.2
    visual_multiplier = 1.0

    wls_filter = cv2.ximgproc.createDisparityWLSFilter(matcher_left=left_matcher)
    wls_filter.setLambda(lmbda)
    wls_filter.setSigmaColor(sigma)

    ############ left_matcher is the traditional matcher (default) #############
    disparity_left = left_matcher.compute(Left_Remap, Right_Remap)
    disparity_right = right_matcher.compute(Right_Remap, Left_Remap)

    filtered_img = wls_filter.filter(disparity_left, Left_Remap, None, disparity_right).astype(np.float32) / 16.

    # filtered_img = disparity_left.astype(np.float32) / 16.0
    
    return filtered_img

def SBGMBuildDisparity(Left_Remap, Right_Remap):
    """
    Functionality:
        Build the disparity using Semi-Global Block Matching
    
    Inputs:
        left_maps                           ->          calibration and rectification remap parameter of left images
        right_maps                          ->          calibration and rectification remap parameter of right images

    Outputs:
        disparity                           ->          disparity maps
    """
    win_size = 7
    min_disp = 0
    # num_disp = 16*10 # Needs to be divisible by 16
    num_disp = 64 # Needs to be divisible by 16

    # other modes: MODE_SGBM, MODE_HH, MODE_SGBM_3WAY, MODE_HH4, where MODE_HH is 
    # for the image size (640, 480). and also some HD pics
    left_matcher = cv2.StereoSGBM_create(
        minDisparity= min_disp,
        numDisparities = num_disp,
        blockSize = win_size,
        uniquenessRatio = 10,
        speckleWindowSize = 150,
        speckleRange = 1,
        disp12MaxDiff = 0,
        P1 = 8*3*win_size**2,#8*3*win_size**2,
        P2 = 32*3*win_size**2,
        preFilterCap = 0,
        # mode=cv2.STEREO_SGBM_MODE_SGBM_3WAY) #32*3*win_size**2)
        mode=cv2.STEREO_SGBM_MODE_HH)

    right_matcher = cv2.ximgproc.createRightMatcher(left_matcher)
    lmbda = 8000
    sigma = 1.2
    visual_multiplier = 1.0

    wls_filter = cv2.ximgproc.createDisparityWLSFilter(matcher_left=left_matcher)
    wls_filter.setLambda(lmbda)
    wls_filter.setSigmaColor(sigma)

    ############ left_matcher is the traditional matcher (default) #############
    disparity_left = left_matcher.compute(Left_Remap, Right_Remap)
    disparity_right = right_matcher.compute(Right_Remap, Left_Remap)

    filtered_img = wls_filter.filter(disparity_left, Left_Remap, None, disparity_right).astype(np.float32) / 16.0

    # filtered_img = disparity_left.astype(np.float32) / 16.0
    
    return filtered_img

def DisparityNormalization(disparity_map):
    """
    Function:
        normalize the disparity map for visualizaton.
    """
    disparity_map = np.int16(disparity_map * 16)
    disparity_map = cv2.normalize(src=disparity_map, dst=disparity_map, beta=0, \
                                alpha=255, norm_type=cv2.NORM_MINMAX)
    disparity_map = np.uint8(disparity_map)
    return disparity_map

def DisparityBuilding(frame_num, left_maps, right_maps, roiL, roiR, show_disparity):
    """
    Functionality:
        Using StereoBM and StereoSGBM for building disparity map with sepecific frames.
    
    Inputs:
        frame_num                       ->              input frame number
        left_maps                       ->              Rectification parameters from cv2
        right_maps                      ->              Rectification parameters from cv2
        roiL                            ->              Rectification parameters from cv2
        roiR                            ->              Rectification parameters from cv2
        show_disparity                  ->              Boolean mask for showing different disparites
        save_left_right                 ->              Boolean mask for saving samples of left and right
    """
    # define a display size, for avoiding over-sized
    display_shape = (640, 480)

    # read left and right images    
    left_test_file = "./left/" + str(frame_num) + ".jpg"
    right_test_file = "./right/" + str(frame_num) + ".jpg"
    image_l_test = cv2.imread(left_test_file)
    image_r_test = cv2.imread(right_test_file)

    # using the parameters to undistort and rectify the images (transfer to gray for better disparity)
    left_rect = cv2.remap(image_l_test, left_maps[0], left_maps[1], cv2.INTER_LINEAR)
    left_rect_gray = cv2.cvtColor(left_rect.copy(), cv2.COLOR_BGR2GRAY)
    right_rect = cv2.remap(image_r_test, right_maps[0], right_maps[1], cv2.INTER_LINEAR)
    right_rect_gray = cv2.cvtColor(right_rect.copy(), cv2.COLOR_BGR2GRAY)

    # disparity of BM
    # disparity_map_sec = BMBuildDisparity(left_rect_gray, right_rect_gray, roiL, roiR)
    # disparity of SGBM
    disparity_map = SBGMBuildDisparity(left_rect, right_rect)
    
    # for rectified images
    left_display = left_rect.copy()
    left_display = cv2.resize(left_display, display_shape)
    right_display = right_rect.copy()
    right_display = cv2.resize(right_display, display_shape)
    rectify_display = np.concatenate([left_display, right_display], axis = 1)
    # if show_disparity:
        # cv2.imshow("rect_test", rectify_display)
    cv2.imwrite("disparity/rect_imgs_" + str(frame_num) + ".jpg", rectify_display)
    # cv2.waitKey(0.1)
    # for disparity maps
    disp1_display = disparity_map.copy()
    disp1_display = DisparityNormalization(disp1_display)
    disp1_display = cv2.resize(disp1_display, display_shape)
    if show_disparity:
        disp2_display = disparity_map_sec.copy()
        disp2_display = DisparityNormalization(disp2_display)
        disp2_display = cv2.resize(disp2_display, display_shape)
        disparty_compare = np.concatenate([disp1_display, disp2_display], axis = 1)
        disparty_compare = cv2.applyColorMap(disparty_compare, cv2.COLORMAP_JET)
        cv2.imshow("gray", np.concatenate([disparty_compare, rectify_display], axis = 0))
    cv2.imwrite("disparity/disp_maps_" + str(frame_num) + ".jpg", disp1_display)
    cv2.waitKey(1)

    colors = cv2.cvtColor(left_rect, cv2.COLOR_BGR2RGB)
    return disparity_map, colors

def PclConstruction(frame_num, disparity_map, Q, colors, constraints):
    """
    Functionality:
        Using the disparity map to re-construction 3D point cloud
    
    Inputs:
        frame_num                   ->                  current frame number
        disparity_map               ->                  disparity map from cv2
        colors                      ->                  colors according to the original left image
        save_pcl                    ->                  boolean mask for saving pcl as npy
        plot_depth_heat             ->                  boolean mask for plotting depth heat map and save it
        scatter_2d                  ->                  boolean mask for plotting 2d scatters
    """

    """
    x axis          ->          horizontal, perpendicular to the depth
    y axis          ->          vertial, height
    z axis          ->          horizontal, depth 
    """
    # detection area restrictions (unit: meters)
    x_min = constraints[0]
    x_max = constraints[1]
    y_min = constraints[2]
    y_max = constraints[3]
    z_min = constraints[4]
    z_max = constraints[5]

    # reproject to 3D point cloud
    points_3D = cv2.reprojectImageTo3D(disparity_map, Q)

    # save position and depth points info
    pos_estimator = points_3D[..., 0]
    height_estimator = points_3D[..., 1]
    depth_estimator = points_3D[..., 2]
    estimator_w, estimator_h = depth_estimator.shape
    for ei in range(estimator_w):
        for ej in range(estimator_h):
            if depth_estimator[ei, ej] < z_min or depth_estimator[ei, ej] > z_max :
                depth_estimator[ei, ej] = z_min
            if height_estimator[ei, ej] < y_min or height_estimator[ei, ej] > y_max:
                height_estimator[ei, ej] = y_min
            if pos_estimator[ei, ej] < x_min or pos_estimator[ei, ej] > x_max:
                pos_estimator[ei, ej] = x_min

    saved_point_cloud = []
    wanted_colors = []
    for ei in range(estimator_w):
        for ej in range(estimator_h):
            if pos_estimator[ei, ej] > x_min and height_estimator[ei, ej] > y_min and depth_estimator[ei, ej] > z_min:
                saved_point_cloud.append([pos_estimator[ei, ej], height_estimator[ei, ej], depth_estimator[ei, ej]])
                wanted_colors.append(colors[ei, ej])
    saved_point_cloud = np.array(saved_point_cloud)
    wanted_colors = np.array(wanted_colors)
    return saved_point_cloud, wanted_colors, points_3D

def Recontruction3D(img_num, save_point_cloud, save_meshlab, show_disparity, constraints):
    """
    Function: main function

    Args:
        save_point_cloud            ->          bool type, whether save point cloud output
        save_meshlab                ->          save the meshlab format output
        show_disparity              ->          bool type, whether plot disparity maps
        constraints                 ->          [x_min, x_max, y_min, y_max, z_min, z_max]
    """
    assert len(constraints) == 6
    # get saved paramteres
    left_maps, right_maps, ProjLeft, ProjRight, roiL, roiR, Q = ReadParameters()
    # print("The reprojection matrix Q is:")
    # print(Q)

    disparity_map, colors = DisparityBuilding(img_num, left_maps, right_maps, \
                                        roiL, roiR, show_disparity)

    # save the point cloud as numpy array
    if save_point_cloud:
        # reconstruct point clouds
        detect_point_cloud, detect_colors, detect_3D = PclConstruction(img_num, disparity_map, Q, colors, constraints)
        # save all outputs as 
        np.save("pcl/initial_" + str(img_num) + ".npy", detect_3D)
        np.save("pcl/organized_" + str(img_num) + ".npy", detect_point_cloud)
        np.save("pcl/color_" + str(img_num) + ".npy", detect_colors)

    # save into mesh file
    if save_meshlab:
        output_file = "./pcl/meshlab_" + str(img_num) + ".ply"
        colors = detect_colors.reshape(-1,3)
        vertices = np.hstack([detect_point_cloud.reshape(-1,3), colors])
        ply_header = '''ply
            format ascii 1.0
            element vertex %(vert_num)d
            property float x
            property float y
            property float z
            property uchar red
            property uchar green
            property uchar blue
            end_header
            '''
        with open(output_file, 'w') as f:
            f.write(ply_header %dict(vert_num=len(vertices)))
            np.savetxt(f,vertices,'%f %f %f %d %d %d')

def main(save_point_cloud, save_meshlab, show_disparity, constraints, sequences):
    # build output folder
    checkoutDir("./disparity")
    checkoutDir("./pcl")
    # define multiprocessing-pool
    pool = mp.Pool(4)
    # start processing using multi-processing
    Reconstr_3D_1arg = partial(Recontruction3D, 
                            save_point_cloud = save_point_cloud,
                            save_meshlab = save_meshlab,
                            show_disparity = show_disparity, 
                            constraints = constraints)
    # process the data in the sequence
    for _ in tqdm(pool.imap_unordered(Reconstr_3D_1arg, \
                range(sequences[0], sequences[1])), \
                total = sequences[1] - sequences[0]):
        pass
    pool.close()
    pool.join()
    pool.close()

if __name__ == "__main__":
    # whether to save the 3D point cloud into meshlab file
    # NOTE: this also saves all the disparity maps and rectified images
    save_point_cloud = True
    save_meshlab = True
    # whether to plot disparity maps using different methods
    show_disparity = False
    # constraints, format [x_min, x_max, y_min, y_max, z_min, z_max]
    constraints = [-10, 10, -10, 10, 0, 50]
    # define sequence numbers wanted
    sequences = [0, getTotoalImgNum("./left/")]
    # main 
    main(save_point_cloud, save_meshlab, show_disparity, constraints, sequences)
