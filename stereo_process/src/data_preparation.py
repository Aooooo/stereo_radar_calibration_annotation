import os
import cv2
import re
import numpy as np
from glob import glob
import matplotlib.pyplot as plt
from tqdm import tqdm

# OUTPUT_SHAPE = (720, 540)
OUTPUT_SHAPE = (640, 480)
# OUTPUT_SHAPE = (360, 270)

def ReadLeftAndRightImages(left_dir, right_dir, left_save, right_save, 
                        checkout_save, for_calibration=False):
    """
    Function:
        read the left and right images from stereo camera, then save them seperately into
    different folders.

    Args:
        left_dir        ->      directory saving stereo left images
        right_dir       ->      directory saving stereo right images
        left_save       ->      direcotry for all left images to be saved (number sequence sepcified)
        right_save      ->      direcotry for all right images to be saved (number sequence sepcified)
        checkout_save   ->      for the concatenation of both left and right images, for validation of calibration.
    """
    print("Number of images to be read:\t", len(glob(os.path.join(left_dir, "*.jpg"))))
    
    left_names = sorted(glob(os.path.join(left_dir, "*.jpg")))
    right_names = sorted(glob(os.path.join(right_dir, "*.jpg")))

    if len(left_names) != len(right_names):
        raise ValueError("\n ---------------------- \
                        Unequal number of left and right images. \
                        \n -----------------------")
    
    all_img_sequence_num = []
    for img_name in left_names:
        img_num = int(re.findall(r'\d+', img_name.replace(left_dir, ''))[0])
        all_img_sequence_num.append(img_num)
    all_img_sequence_num = np.array(all_img_sequence_num)
    all_img_sequence_num = np.sort(all_img_sequence_num)

    counter = 0

    print("Start reading data ...")
    for i in tqdm(range(len(all_img_sequence_num))):
        ind = all_img_sequence_num[i]
        left_id = os.path.join(left_dir, '%.6d.jpg'%(ind))
        right_id = os.path.join(right_dir, '%.6d.jpg'%(ind))

        left_img = cv2.imread(left_id)
        right_img = cv2.imread(right_id)

        left_img = cv2.resize(left_img, OUTPUT_SHAPE)
        right_img = cv2.resize(right_img, OUTPUT_SHAPE)

        display_img = np.concatenate([left_img, right_img], axis = 1)
        save_name = str(counter) + ".jpg"
        cv2.imwrite(os.path.join(left_save, save_name), left_img)
        cv2.imwrite(os.path.join(right_save, save_name), right_img)
        if for_calibration:
            cv2.imwrite(os.path.join(checkout_save, save_name), display_img)
        counter += 1

def checkoutDir(dir_name):
    """
    Function:
        Checkout if the directory is exists, if not then create one

    Args:
        dir_name            ->          directory name
    """
    if not os.path.exists(dir_name):
        os.mkdir(dir_name)
    else:
        file_list = glob(os.path.join(dir_name, '*'))
        if len(file_list) is not 0:
            for f in file_list:
                os.remove(f)

def main(do_calibration, data_dir):
    left_dir = os.path.join(data_dir, "left") 
    right_dir = os.path.join(data_dir, "right") 
    if not os.path.exists(left_dir) or not os.path.exists(right_dir):
        raise ValueError("Please input the right datapath in the configure file.")

    left_save = "./left/"
    right_save = "./right/"
    checkout_save = "./stereoImgs_visualization/"

    checkoutDir(left_save)
    checkoutDir(right_save)
    if do_calibration:
        checkoutDir(checkout_save)

    ReadLeftAndRightImages(left_dir,  right_dir, left_save, right_save, 
                            checkout_save, for_calibration=do_calibration)

if __name__ == "__main__":
    main(False, "/DATA/2020-06-01-14-12-19")
