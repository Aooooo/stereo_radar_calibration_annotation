import os
import cv2
import time
import numpy as np
from tqdm import tqdm
from pangolin_draw import *

def readStereoPcl(pcl_dir, frame_i):
    if os.path.exists(os.path.join(pcl_dir, "initial_%.d.npy"%(frame_i))):
        original_pcl = np.load(os.path.join(pcl_dir, "initial_%.d.npy"%(frame_i)))
        thresholed_pcl = np.load(os.path.join(pcl_dir, "organized_%.d.npy"%(frame_i)))
    else:
        original_pcl = None
        thresholed_pcl = None
    return original_pcl, thresholed_pcl

def readImage(img_dir, frame_i):
    if os.path.exists(os.path.join(img_dir, "rect_imgs_%.d.jpg"%(frame_i))):
        img = cv2.imread(os.path.join(img_dir, "rect_imgs_%.d.jpg"%(frame_i)))
        img = img[:, :int(img.shape[1]/2), :]
    else:
        img = None
    return img

def matchPclWithColor(pcl, image, depth_threshold=1000):
    assert pcl.shape == image.shape
    pcl_out = pcl.reshape(-1, 3)
    color_out = image.reshape(-1, 3)
    color_out = color_out.astype(np.float) / 255.
    pcl_output = []
    color_output = []
    for i in range(pcl_out.shape[0]):
        if pcl_out[i][2] <= depth_threshold:
            pcl_output.append(pcl_out[i])
            color_output.append(color_out[i])
    pcl_output = np.array(pcl_output)
    color_output = np.array(color_output)
    return pcl_output, color_output  

def main(stereo_pcl_dir, image_dir, frame_i):
    pangolin_vis = PangolinDraw()
    image = readImage(image_dir, frame_i)
    original_pcl, _ =readStereoPcl(stereo_pcl_dir, frame_i)
    if image is not None and original_pcl is not None:
        pcl, color = matchPclWithColor(original_pcl, image)
        start_time = time.time()

        filter_pcl = []
        filter_color = []
        for i in range(len(pcl)):
            if pcl[i, 2] > 0:
                filter_pcl.append(pcl[i])
                filter_color.append(color[i])
        pcl = np.array(filter_pcl)
        color = np.array(filter_color)

        # while (time.time() - start_time) < 10000:
        while True:
            pangolin_vis(pcl, color, 2, None, None, None, "points")

if __name__ == "__main__":
    stereo_pcl_dir = "../../pcl"
    image_dir = "../../disparity"
    frame_id = 0
    main(stereo_pcl_dir, image_dir, frame_id)



