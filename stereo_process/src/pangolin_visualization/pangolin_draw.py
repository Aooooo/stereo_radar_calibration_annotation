import sys
sys.path.append("/home/ao/Documents/Pangolin/build/src")

import pypangolin as pangolin
from OpenGL.GL import *
import numpy as np

class PangolinDraw:
    def __init__(self):
        """
        initialization: setting up basic functions for pangolin
        """
        self.win = pangolin.CreateWindowAndBind("display", 640, 480)
        glEnable(GL_DEPTH_TEST)

        self.pm = pangolin.ProjectionMatrix(640, 480, 420, 420, 320, 240, 0.2, 100)
        self.mv = pangolin.ModelViewLookAt(-2, 2, -2, 0, 0, 0, pangolin.AxisY)
        self.s_cam = pangolin.OpenGlRenderState(self.pm, self.mv)

        # self.ui_width = 180
        self.ui_width = 1

        self.handler = pangolin.Handler3D(self.s_cam)
        self.d_cam = (
            pangolin.CreateDisplay()
            .SetBounds(
                pangolin.Attach(0),
                pangolin.Attach(1),
                pangolin.Attach.Pix(self.ui_width),
                pangolin.Attach(1),
                -640.0 / 480.0,
            )
            .SetHandler(self.handler)
        )

    def drawPoints(self, points, colors, point_size=2):
        """
        Function: draw points with their colors.

        Args:
            points          ->          np.array with format [n, 3]
            colors          ->          np.array with format [n, 3], color range [0, 1]
            point_size      ->          size of points to be drawn
        """
        assert len(points) == len(colors)
        assert len(points.shape) == 2
        assert points.shape[1] == 3
        glPointSize(2)
        glBegin(GL_POINTS)
        for i in range(points.shape[0]):
            glColor3f(colors[i][0], colors[i][1], colors[i][2])
            glVertex3f(points[i][0], points[i][1], points[i][2])
        glEnd()

    def drawLines(self, points, line_color, line_width):
        assert len(points.shape) == 2
        assert isinstance(line_color, list)
        assert len(line_color) == 4
        assert points.shape[1] == 3
        for i in range(points.shape[0]):
            if i ==0 :
                continue
            glLineWidth(line_width)
            glColor4f(line_color[0], line_color[1], line_color[2], line_color[3])
            glBegin(GL_LINES)
            glVertex3f(points[i-1][0], points[i-1][1], points[i-1][2])
            glVertex3f(points[i][0], points[i][1], points[i][2])
            glEnd()

    def __call__(self, points, colors, point_size, 
                line_points, line_color, line_width, mode="points"):
        assert (mode in ["points", "lines", "both"])
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        self.d_cam.Activate(self.s_cam)
        if mode == "points":
            self.drawPoints(points, colors, point_size) 
        elif mode == "lines":
            self.drawLines(line_points, line_color, line_width)
        else:
            self.drawPoints(points, colors, point_size) 
            self.drawLines(line_points, line_color, line_width)
        pangolin.FinishFrame()

if __name__ == "__main__":
    panglin_draw = PangolinDraw()
    while(1):
        points = np.random.random((10000,3)) * 10
        colors = np.zeros((len(points), 3))
        colors[:,1] = 1 - points[:, 0]/10
        colors[:,2] = 1 - points[:, 1]/10
        colors[:,0] = 1 - points[:, 2]/10

        line_points = np.random.random((10, 3)) * 10
        line_color = [0., 1., 0., 0.6]

        panglin_draw(points, colors, 2, None, None, None, "points")
        # panglin_draw(None, None, None, line_points, line_color, 2, "lines")
    
