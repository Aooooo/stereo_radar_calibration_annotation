'''
This code is for using chessboard pictures from left and right cameras to 
calibrate the intrinsic and extrinsic matrices, then save all the parameters
as numpy arrays into the folder $saved_para.
'''
from mpl_toolkits.mplot3d import Axes3D 
import matplotlib.pyplot as plt
import numpy as np
import cv2
import glob
import os
from multiprocessing import Process

def ReadTxtFile(txt_filepath):
    """
    Functionality:
        Read the manually selected calibration images index from the txt file.
    """
    all_index = []
    with open(txt_filepath) as f:
        for line in f:
            all_index.append([int(i) for i in line.split(",")])

    all_index = np.array(all_index)
    all_index = all_index[0]

    return all_index

def checkoutDir(dir_name):
    """
    Function:
        Checkout if the directory is exists, if not then create one

    Args:
        dir_name            ->          directory name
    """
    if not os.path.exists(dir_name):
        os.mkdir(dir_name)

def FindChessBoardCorner(images_selected, left_calibration_directory, right_calibration_directory, chess_board_size,
                        grid_size, max_calibration_numimg = 10000, plot_corners = True):
    """
    Functionality:
        Find the chessboard of all the left and right calibrations images. If all points found, return all 
        necessary paramters for further calibration and rectification.
    
    Inputs:
        images_selected                     ->          image indexes for the calibration
        left_calibration_directory          ->          directory that stores all left calibration iamges
        right_calibration_directory         ->          directory that stores all right calibration iamges
        chess_board_size                    ->          how many grids in the chessboard width and length 
        max_calibration_numimg              ->          the maximum number of calibration images you wish to use
        plot_corners                        ->          whether to plot chessboard corners

    Outputs:
        objpoints                           ->          real world coordiante points defined inside the function                          
        imgpointsL                          ->          all the points that found in the chessboard of left images
        imgpointsR                          ->          all the points that found in the chessboard of right images
        image_size                          ->          image size of the calibration images
        objpointsL                          ->          real world coordiante points defined(for left camera intrinsic calibration only)
        objpointsR                          ->          real world coordiante points defined(for right camera intrinsic calibration only)
        imgpointsL_single                   ->          chessboard points(for left camera intrinsic calibration only)
        imgpointsR_single                   ->          chessboard points(for right camera intrinsic calibration only)
    """

    # flags of findChessboardCorners()
    chessboard_flags = (cv2.CALIB_CB_ADAPTIVE_THRESH + cv2.CALIB_CB_NORMALIZE_IMAGE + cv2.CALIB_CB_FILTER_QUADS)

    # termination criteria
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 100, 1e-4)

    # prepare object points
    object_points = np.zeros((chess_board_size[0] * chess_board_size[1], 3), np.float32)
    object_points[:,:2] = grid_size * np.mgrid[0:chess_board_size[0], 0:chess_board_size[1]].T.reshape(-1,2).astype(np.float32)

    # find out how many manual seleted images
    num_seleted_images = len(images_selected)
    if max_calibration_numimg > num_seleted_images:
        max_calibration_numimg = num_seleted_images
    
    # arrays to store object points and image points from all the images
    objpoints = []
    objpointsL = []
    objpointsR = []

    imgpointsL = []
    imgpointsR = []
    imgpointsL_single = []
    imgpointsR_single = []
    
    for i in range(max_calibration_numimg):
        # read selected images index
        current_img_number = images_selected[i]
        N = str(current_img_number)

        # read the left selected image
        img_path_l = left_calibration_directory + N + ".jpg"
        img_l = cv2.imread(img_path_l)

        if img_l is None:
            continue

        # transfer the image into gray for better result
        gray_l = cv2.cvtColor(img_l, cv2.COLOR_BGR2GRAY)

        # pass image size for further use
        image_size = gray_l.shape[::-1]

        # read the left selected image
        img_path_r = right_calibration_directory + N + ".jpg"
        img_r = cv2.imread(img_path_r)
        if img_r is None:
            continue
        gray_r = cv2.cvtColor(img_r, cv2.COLOR_BGR2GRAY)

        # find the chessboard on the gray image
        ret_l, corners_l = cv2.findChessboardCorners(gray_l, chess_board_size, chessboard_flags)
        ret_r, corners_r = cv2.findChessboardCorners(gray_r, chess_board_size, chessboard_flags)

        # if found, add object points, image points
        if (ret_l == True) and (ret_r == True):
            print("current number:\t", current_img_number)

            objpoints.append(object_points)
            objpointsL.append(object_points)
            objpointsR.append(object_points)

            cv2.cornerSubPix(gray_l, corners_l, (5,5), (-1,-1), criteria)
            cv2.cornerSubPix(gray_r, corners_r, (5,5), (-1,-1), criteria)

            imgpointsL.append(corners_l)
            imgpointsR.append(corners_r)
            imgpointsL_single.append(corners_l)
            imgpointsR_single.append(corners_r)
            
            # plot the chessboard corners to verify the qualtiy of the images
            if plot_corners:
                print(current_img_number)
                img_display_l = cv2.drawChessboardCorners(img_l, chess_board_size, corners_l, ret_l)
                img_display_r = cv2.drawChessboardCorners(img_r, chess_board_size, corners_r, ret_r)
                chessboard_display = np.concatenate([img_display_l, img_display_r], axis = 1)
                small_display = cv2.resize(chessboard_display, (0,0), fx=0.5, fy=0.5) 
                cv2.imshow('display', small_display)
                # cv2.imwrite("/DATA/calib_ao/stereo_corners/" + str(current_img_number) + ".jpg", small_display)
                cv2.waitKey(500)
        
        # store the parameters for intrinsic calibration of the left camera
        elif (ret_l == True):
            objpointsL.append(object_points)
            cv2.cornerSubPix(gray_l, corners_l, (5,5), (-1,-1), criteria)
            imgpointsL_single.append(corners_l)

        # store the parameters for intrinsic calibration of the left camera
        elif (ret_r == True):
            objpointsR.append(object_points)
            cv2.cornerSubPix(gray_r, corners_r, (5,5), (-1,-1), criteria)
            imgpointsR_single.append(corners_r)

    if plot_corners:
        cv2.destroyAllWindows()

    # print the number of images that stored for rectification, left intrinsic calibration and 
    # right instrinsic calibration
    print("---------See how many images are saved-----------")
    print("Double Cams", len(objpoints))
    print("Left Cam", len(objpointsL))
    print("Right Cam", len(objpointsR))

    return objpoints, imgpointsL, imgpointsR, image_size, objpointsL, imgpointsL_single, objpointsR, imgpointsR_single


def CalibrationAndRectification(objpoints, imgpointsL, imgpointsR, image_size, objpointsL, imgpointsL_single, objpointsR, 
                                imgpointsR_single, single_calibration = False):
    """
    Functionality:
        Use the result of the chessboard parameters in FindChessBoardCorner() to do the calibration and rectification.
    
    Inputs:
        objpoints                           ->          real world coordiante points defined inside the function                          
        imgpointsL                          ->          all the points that found in the chessboard of left images
        imgpointsR                          ->          all the points that found in the chessboard of right images
        image_size                          ->          image size of the calibration images
        objpointsL                          ->          real world coordiante points defined(for left camera intrinsic calibration only)
        objpointsR                          ->          real world coordiante points defined(for right camera intrinsic calibration only)
        imgpointsL_single                   ->          chessboard points(for left camera intrinsic calibration only)
        imgpointsR_single                   ->          chessboard points(for right camera intrinsic calibration only)
        single_calibration                  ->          whether do the intrinsic calibration of left and right cameras before extrinsic calibration

    Outputs:
        left_maps                           ->          calibration and rectification remap parameter of left images
        right_maps                          ->          calibration and rectification remap parameter of right images
        ProjLeft                            ->          projection matrices for left images
        ProjRight                           ->          projection matrices for right images
        Q                                   ->          used for depth estimation from disparity (triangulation)
    """
    # Do the single camera calibration first, in order to get a better result
    if single_calibration:
        single_cali_criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 100, 1e-4)
        ret_l, camMatrix_l, distCoe_l, revc_l, tvecs_l = cv2.calibrateCamera(objpointsL, imgpointsL_single, image_size, None, None, criteria = single_cali_criteria)
        ret_r, camMatrix_r, distCoe_r, revc_r, tvecs_r = cv2.calibrateCamera(objpointsR, imgpointsR_single, image_size, None, None, criteria = single_cali_criteria)
    else:
        camMatrix_l, distCoe_l = None, None
        camMatrix_r, distCoe_r = None, None


    # Set the criteria and flags of stereoCalibrate, for the details, check opencv function describtion
    stereocalib_criteria = (cv2.TERM_CRITERIA_MAX_ITER + cv2.TERM_CRITERIA_EPS, 300, 1e-6)

    # If confident in the intrinsic matrices we got from single camera calibration, use these flags
    # stereocalib_flags = cv2.CALIB_USE_INTRINSIC_GUESS | cv2.CALIB_FIX_FOCAL_LENGTH | cv2.CALIB_FIX_PRINCIPAL_POINT # | cv2.CALIB_ZERO_TANGENT_DIST | cv2.CALIB_RATIONAL_MODEL | cv2.CALIB_FIX_K3 | cv2.CALIB_FIX_K4 | cv2.CALIB_FIX_K5
    stereocalib_flags = cv2.CALIB_FIX_ASPECT_RATIO | cv2.CALIB_ZERO_TANGENT_DIST | cv2.CALIB_RATIONAL_MODEL | cv2.CALIB_FIX_K3 | cv2.CALIB_FIX_K4 | cv2.CALIB_FIX_K5

    # do the stereoClibration to find the extrinsic matrices and optimize intrinsic matrices
    stereocalib_retval, cameraMatrix1, distCoeffs1, cameraMatrix2, distCoeffs2, Rotation, Transformation, E, F = cv2.stereoCalibrate(objpoints,imgpointsL,imgpointsR, 
                                                                                                                camMatrix_l, distCoe_l, camMatrix_r, distCoe_r, 
                                                                                                                image_size, criteria = stereocalib_criteria, flags = stereocalib_flags)

    # print out Enssential and Fundamental matrices for checkingout the values
    # print('Essential Matrix is ')
    # print(E)
    # print('Fundamental Matrix is')
    # print(F)

    rectify_scale = 0 # 0=full crop, 1=no crop

    # rectify the pictures
    RectLeft, RectRight, ProjLeft, ProjRight, Q, roiL, roiR = cv2.stereoRectify(cameraMatrix1, distCoeffs1, cameraMatrix2, distCoeffs2, 
                                                                                image_size, Rotation, Transformation, alpha = rectify_scale)

    # rectify both images
    # check the flags, change from CV_32FC1 or CV_16SC2 to the implementation. 
    # CV_32FC1 means float, the other means int
    left_maps = cv2.initUndistortRectifyMap(cameraMatrix1, distCoeffs1, RectLeft, ProjLeft, image_size, cv2.CV_32FC1)
    right_maps = cv2.initUndistortRectifyMap(cameraMatrix2, distCoeffs2, RectRight, ProjRight, image_size, cv2.CV_32FC1)
    
    return left_maps, right_maps, ProjLeft, ProjRight, roiL, roiR, Q

def BMBuildDisparity(Left_Remap, Right_Remap, roiL, roiR):
    """
    Functionality:
        Build the disparity using Block Matching.
    
    Inputs:
        left_maps                           ->          calibration and rectification remap parameter of left images
        right_maps                          ->          calibration and rectification remap parameter of right images
        roiL, roiR                          ->          outputs of cv2.stereoRectify() for telling the size of the images

    Outputs:
        disparity                           ->          disparity maps
    """
    stereo = cv2.StereoBM_create()
    stereo.setMinDisparity(0)
    stereo.setNumDisparities(64)
    stereo.setBlockSize(9)
    stereo.setROI1(roiL)
    stereo.setROI2(roiR)
    stereo.setSpeckleRange(10)
    stereo.setSpeckleWindowSize(1)
    disparity = stereo.compute(Left_Remap,Right_Remap)

    return disparity

def SBGMBuildDisparity(Left_Remap, Right_Remap):
    """
    Functionality:
        Build the disparity using Semi-Global Block Matching
    
    Inputs:
        left_maps                           ->          calibration and rectification remap parameter of left images
        right_maps                          ->          calibration and rectification remap parameter of right images

    Outputs:
        disparity                           ->          disparity maps
    """
    win_size = 10
    min_disp = 0
    num_disp = 64 # Needs to be divisible by 16

    stereo = cv2.StereoSGBM_create(minDisparity= min_disp,
        numDisparities = num_disp,
        blockSize = 9,
        uniquenessRatio = 0,
        speckleWindowSize = 0,
        speckleRange = 5,
        disp12MaxDiff = 1,
        P1 = 8*3*win_size**2,#8*3*win_size**2,
        P2 =32*3*win_size**2,
        preFilterCap = 32,
        mode=cv2.STEREO_SGBM_MODE_SGBM_3WAY) #32*3*win_size**2)

    disparity = stereo.compute(Left_Remap, Right_Remap)

    return disparity

def SaveCalibrationAndRectificationParameters(chess_board_size, grid_size, calibration_indexes):
    """
    Main Function:
        Store all parameters as numpy arrays.
    """
    left_calibration_directory = "./left/"
    right_calibration_directory = "./right/"

    objpoints, imgpointsL, imgpointsR, image_size, objpointsL, imgpointsL_single, objpointsR, imgpointsR_single = FindChessBoardCorner(calibration_indexes, left_calibration_directory, 
                                                                                            right_calibration_directory, chess_board_size, grid_size, plot_corners = True)
    left_maps, right_maps, ProjLeft, ProjRight, roiL, roiR, Q = CalibrationAndRectification(objpoints, imgpointsL, imgpointsR, image_size, objpointsL, 
                                                                                            imgpointsL_single, objpointsR, imgpointsR_single)
    
    checkoutDir("./calibration_results")
    np.save("./calibration_results/left_maps.npy", left_maps)
    np.save("./calibration_results/right_maps.npy", right_maps)
    np.save("./calibration_results/ProjLeft.npy", ProjLeft)
    np.save("./calibration_results/ProjRight.npy", ProjRight)
    np.save("./calibration_results/roiL.npy", roiL)
    np.save("./calibration_results/roiR.npy", roiR)
    np.save("./calibration_results/Q.npy", Q)

def main(chess_board_size, grid_size, calibration_indexes):
    """
    Input necessary parameters here.
    """
    p = Process(target=SaveCalibrationAndRectificationParameters, args=(chess_board_size, grid_size, calibration_indexes))
    p.start()
    p.join()
    
if __name__ == "__main__":
    # how many corners there are in the chessboard
    chess_board_size = (8, 6)
    # size of each grid, please make the unit to "meters"
    grid_size = 0.025
    # create indexes
    all_index = ReadTxtFile("./selected.txt")
    # main
    main(chess_board_size, grid_size, all_index)
