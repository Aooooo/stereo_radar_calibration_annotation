import numpy as np

##### middle distance
designated_indexes = [217, 245, 267, 320, 365, 431, 485, 530, 569, 600, 755, 
                    789, 831, 850, 885, 1005, 1041, 1092, 1122, 1154, 1206,
                    1253, 1289, 1325, 1358, 1410, 1450, 1511, 1536, 1557, 1583, 
                    1675, 1710, 1733, 1769, 1790, 1849, 1881, 1912, 1949, 1979, 
                    2036, 2071, 2089, 2115, 2145, 2226, 2256, 2279, 2309, 2331, 
                    2495, 2519, 2542, 2576, 2597, 2658, 2684, 2704, 2731, 2756,
                    2819, 2840, 2863, 2889, 2921, 2975, 3003, 3038, 3066, 3095, 
                    3128, 3161, 3180, 3247, 3330, 3357, 3385, 3464, 3583, 3633, 
                    3660, 3700, 3749, 3773, 3795, 3820 ,3850, 3895, 3922, 3958, 
                    4012, 4053, 4081, 4114]

indexes  = designated_indexes

# # all indexes for calibration 
# calib_total_num = 692
# indexes = np.arange(calib_total_num)

# # select part of it to speed up
# calibration_num = 100
# np.random.shuffle(indexes)
# selected_indexes = indexes[:calibration_num]

# otherwise
selected_indexes = indexes

print("Total number of calibration images: \t", len(selected_indexes))

# write into the file
with open("selected.txt", "w") as f: 
    for i in range(len(selected_indexes)):
        num = str(selected_indexes[i])
        if i == 0:
            f.write(num)
        else:
            f.write("," + num)
