import numpy as np
import cv2
import os
from glob import glob
import matplotlib.pyplot as plt
from tqdm import tqdm

def getTotoalImgNum(img_dir):
    """
    Function:
        Get the total number of images, prepare for the sequence number

    Args:
        img_dir             ->          image directory for counting
    """
    all_imgs = glob(img_dir + "*.jpg")
    return len(all_imgs)

def checkoutDir(dir_name):
    """
    Function:
        Checkout if the directory is exists, if not then create one

    Args:
        dir_name            ->          directory name
    """
    if not os.path.exists(dir_name):
        os.mkdir(dir_name)
    else:
        file_list = glob(os.path.join(dir_name, '*'))
        for f in file_list:
            os.remove(f)

def GenerateDispRectImages(img_dir, sequence):
    """
    Function:
        Generate produced rectified images.
    
    Args:
        img_dir           ->          directory of rectified images.
    Returns:
        disp_left         ->          disparity map (based on left image)
        rect_left         ->          rectified image (left)
    """
    for img_i in tqdm(range(sequence[0], sequence[1])):
        disp_img_name = img_dir + "disp_maps_" + str(img_i + 1) + ".jpg"
        rect_img_name = img_dir + "rect_imgs_" + str(img_i + 1) + ".jpg"
        disp_img = cv2.imread(disp_img_name) 
        rect_img = cv2.imread(rect_img_name)
        if disp_img is not None:
            # disp_left = disp_img[:, :disp_img.shape[1]//2, ...]
            disp_left = disp_img
            rect_left = rect_img[:, :rect_img.shape[1]//2, ...]
            yield disp_left, rect_left, img_i 

def ColorFinder(rect_img, color_lower_bound, color_higher_bound, if_hsv = True):
    """
    Function:
        Find the reflector according to the color.

    Args:
        rect_img        ->          rectified images

    Returns:
        color_mask      ->          mask that contains only selected color
    """
    rect_img_hsv = cv2.cvtColor(rect_img, cv2.COLOR_BGR2HSV)
    color_mask = cv2.inRange(rect_img_hsv, color_lower_bound, color_higher_bound)
    if color_mask is None:
        color_mask = np.zeros([rect_img.shape[0], rect_img.shape[1]])
    return color_mask

def run(disp_rect_dir, save_dir, sequence, color_lower_bound_paper, color_higher_bound_paper):
    """
    Main Function
    """
    checkoutDir(save_dir)

    #### main loop ####
    for disp_img, rect_img, img_count in GenerateDispRectImages(disp_rect_dir, sequence):
        ##### when there is only one material in a scenario ##### 
        color_lower_bound = color_lower_bound_paper
        color_higher_bound = color_higher_bound_paper

        mask = ColorFinder(rect_img, color_lower_bound, color_higher_bound)
        mask_save_name = save_dir + "mask_reflect_" + str(img_count) + ".npy"
        mask_img_name = save_dir + "mask_img_" + str(img_count) + ".jpg"

        np.save(mask_save_name, mask)
        cv2.imshow('rect', rect_img)
        cv2.imshow('img', mask)
        cv2.imwrite(os.path.join(save_dir, '%.d.jpg'%(img_count)), mask)
        cv2.waitKey(1)

def FindThresholdManually(img_dir, checkout_num):
    """
    Debug Function:
        For finding the right H-S-V value for the thresholds.
    
    Method:
        Transfer RGB to HSV, than using opencv show the image. The values
    of the "R", "G", "B" of a specific pixel is the values of "V", "S", "H".

    Note:
        Pay attention to the order of RBG and HSV (opencv flips the channels)
    """
    test_img1 = img_dir + "rect_imgs_" + str(checkout_num) + ".jpg"
    test1 = cv2.imread(test_img1)

    test1_hsv = cv2.cvtColor(test1, cv2.COLOR_BGR2HSV)
    cv2.imshow('img', test1_hsv)
    cv2.waitKey(10000000)

def main(sequence, checkout_num, find_thresholds, lower_bound, higher_bound):
    disp_rect_dir = "./disparity/"
    save_dir = "./stereo_reflector/"
    checkoutDir(save_dir)
    if find_thresholds:
        FindThresholdManually(disp_rect_dir, checkout_num)
    else:
        run(disp_rect_dir, save_dir, sequence, lower_bound, higher_bound)

if __name__ == "__main__":
    sequence = [0, getTotoalImgNum("./left/")]
    checkout_num = 699
    find_thresholds = False
    #### here as H-S-V format ####
    lower_bound = np.array([90, 70, 30], dtype = "uint8")
    higher_bound = np.array([120, 130, 90], dtype = "uint8")
    # main
    main(sequence, checkout_num, find_thresholds, lower_bound, higher_bound)
