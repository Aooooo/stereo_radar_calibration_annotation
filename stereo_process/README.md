# Stereo Image Processing

## How to use it

- 1. change the configure `stereo_config.json` to personal preference.
- 2. run `stereo_process`.

## Note

- 1. `left` stores all left images
- 2. `right` stores all right images
- 3. `stereoImgs_visualization` stores all images for calbiration.
- 4. `calibration_results` stores all the stereo calibration matrices.
- 5. `disparity` stores disparity maps and rectified images.
- 6. `pcl` stores point cloud information (in numpy array format).
- 7. `stereo_reflector` stores all necessary information for registration.
- 8. `src` stores all source codes.

## Hidden Features

- 1. For stereo point cloud visualization(pangolin), goto `src/pangolin_visualization` and run `stereo_pcl_vis.py`. However, it is extremely slow because too many points i guess :P.
