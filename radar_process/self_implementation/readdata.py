import matplotlib
matplotlib.use('tkagg')
import matplotlib.pyplot as plt
import numpy as np
import time
from scipy.io import loadmat
from single_image_test import *
from mpl_toolkits.mplot3d import Axes3D


def Read_ADC(Filename, samples_per_chirp, num_chirps, num_ADCBits, num_RX, isReal):
    f = open(Filename,'r')
    RawADC = np.fromfile(f, dtype = np.int16, count= samples_per_chirp * num_chirps * 300)
    if num_ADCBits != 16:
        lengthMax = 2^(num_ADCBits-1) -1
        RawADC[RawADC > lengthMax-1] = RawADC[RawADC > lengthMax-1] - 2^num_ADCBits
    f.close()
    FileSize = len(RawADC)
    if isReal:
        totalnumChirps = int(FileSize / samples_per_chirp / num_RX)
        TrueData = np.zeros(int(FileSize))
        TrueData = np.reshape(RawADC, samples_per_chirp * num_RX, totalnumChirps)
    else:
        totalnumChirps = int(FileSize / samples_per_chirp / num_RX / 2)
        TrueData = np.zeros(int(FileSize/2), dtype=complex)
        counter = 0
        for i in range(0,FileSize-2,4):

            TrueData[counter] = complex(RawADC[i], RawADC[i+2])
            TrueData[counter+1] = complex(RawADC[i+1], RawADC[i+3])
            counter = counter + 2
    RawADCData = np.reshape(TrueData, (totalnumChirps, samples_per_chirp * num_RX))

    ADCData = np.zeros((samples_per_chirp, totalnumChirps, num_RX), dtype = complex)
    for i in range(num_RX):
        for j in range(totalnumChirps):
            for k in range(samples_per_chirp):
                ADCData[k,j,i] = RawADCData[j, i * samples_per_chirp + k]
    return ADCData

def Chosen_Data(samples_per_chirp, num_chirps, num_RX, numStart, ADC_Data):
    wanted_data = np.zeros((samples_per_chirp, num_chirps, num_RX),dtype = complex)
    wanted_data = ADC_Data[:, (numStart) * num_chirps : (numStart + 1) * num_chirps, :]
    
    return wanted_data

def Windowed_Data(chosen_data, window_dim = "range"):
    data_size = np.shape(chosen_data)
    if window_dim == "range":
        dim = 0
        hann_window_coef = np.hanning(data_size[dim])
        hann_window_coef = np.expand_dims(np.expand_dims(hann_window_coef, axis = -1), axis = -1)
    elif window_dim == "doppler":
        dim = 1
        hann_window_coef = np.hanning(data_size[dim])
        hann_window_coef = np.expand_dims(np.expand_dims(hann_window_coef, axis = 0), axis = -1)
    else: print("Dimension input error: must input a valid dimension name.")

    windowed_data = chosen_data * hann_window_coef

    return windowed_data

def Load_Range_hannwin(window_filename, samples_per_chirp, num_chirps):
    range_hannwin = loadmat(window_filename)
    range_hannwin = np.array(range_hannwin['range_temp']).reshape(-1, order = "F")

    win_len = len(range_hannwin)
    range_hannwin_coef = np.ones(samples_per_chirp)
    range_hannwin_coef[0: win_len -1] = range_hannwin[0: (win_len - 1)]
    range_hannwin_coef[samples_per_chirp - win_len: samples_per_chirp] = (range_hannwin_coef[::-1])[win_len:]

    range_hannwin_coef = np.repeat(np.expand_dims(range_hannwin_coef, 1), repeats=num_chirps, axis=1)

    return range_hannwin_coef

def get_doppler_hannwin(samples_per_chirp, num_chirps):
    doppler_hannwin = np.hanning(num_chirps)
    win_len_ori = int(num_chirps / 2) + 1
    doppler_hannwin = [doppler_hannwin[x] for x in range(1, win_len_ori)]

    win_len = len(doppler_hannwin)
    doppler_hannwin_coef = np.ones(num_chirps)
    # doppler_hannwin_coef[0: win_len - 1] = doppler_hannwin[0: (win_len - 1)]
    # doppler_hannwin_coef[num_chirps - win_len : num_chirps] = (doppler_hannwin_coef[::-1])[win_len:]

    doppler_hannwin_coef = np.repeat(np.expand_dims(doppler_hannwin_coef, 1).transpose(), repeats = samples_per_chirp, axis = 0)

    return doppler_hannwin_coef

def data_FFT(samples_per_chirp, num_chirps, num_RX, numStart, ADC_Data, scale_range, scale_doppler):
    range_fft_out = np.zeros((samples_per_chirp, num_chirps, num_RX), dtype = complex)
    range_doppler_out = np.zeros((samples_per_chirp, num_chirps, num_RX), dtype = complex)
    visible_result = np.zeros((samples_per_chirp, num_chirps))

    chosen_data = Chosen_Data(samples_per_chirp, num_chirps, num_RX, num_start, adc_data)

    ##### DC offset compensation #####
    chosen_data = chosen_data - chosen_data.mean(axis = 0)

    ##### range FFT #####
    range_windowed_data = Windowed_Data(chosen_data, "range")
    range_fft_out = np.fft.fft(range_windowed_data, axis = 0)
    ''' Scale factor: to make the figure more attractive '''
    # range_fft_out = range_fft_out * scale_range

    ##### doppler FFT #####
    doppler_windowed_data = Windowed_Data(range_fft_out, "doppler")
    range_doppler_out = np.fft.fft(doppler_windowed_data, axis = 1)
    ''' Scale factor: to make the figure more attractive '''
    # range_doppler_out = range_doppler_out * scale_doppler
    range_doppler_out = np.fft.fftshift(range_doppler_out, axes = (1,))

    ##### create visible result #####
    visible_result = np.sum(pow(abs(range_doppler_out), 2), axis = -1)

    ##### transfer to dB, also need to try if not doing this when analyse it #####
    visible_result = 10 * np.log10(visible_result + 1)

    return range_doppler_out, visible_result

def PlotMagWithHOG(FFT_mag, alpha = 8.5, beta = 1.1, gamma = 3.5):

    w ,h = FFT_mag.shape

    fd, hog_image = hog(FFT_mag, orientations=8, pixels_per_cell=(2, 2),
                cells_per_block=(1, 1), visualize=True, feature_vector = True, multichannel=False)

    image = np.float32(FFT_mag) / 255.
    gradient_x = cv2.Sobel(image, cv2.CV_32F, 1, 0, ksize = 1)
    gradient_y = cv2.Sobel(image, cv2.CV_32F, 0, 1, ksize = 1)

    gradient_mag, gradient_angle = cv2.cartToPolar(gradient_x, gradient_y, angleInDegrees = True)

    mag_average = np.mean(FFT_mag)
    hog_average = np.mean(hog_image)
    gradient_mag_average = np.mean(gradient_mag)

    mag_threshold = beta * mag_average
    hog_threshold = alpha * hog_average
    gradient_mag_threshold = gamma * gradient_mag_average

    mag_filter = np.where(FFT_mag > mag_threshold, 1., 0.)
    hog_filter = np.where(hog_image > hog_threshold, 1., 0.)
    gradient_mag_filter = np.where(gradient_mag > gradient_mag_threshold, 1., 0.)

    """ Remove the high magnitude noise at low range and high range """
    ##### remove all objects before range 6 and after range 243, could be tunned in the future for the better result #####
    for wi in range(w):
        if wi <= 6 or wi >= 243:
            mag_filter[wi] = 0
            hog_filter[wi] = 0
            gradient_mag_filter[wi] = 0

    return FFT_mag, mag_filter, hog_image, hog_filter, gradient_mag, gradient_mag_filter

if __name__ == "__main__":
    Filename = "/home/azhang/Documents/RADAR_test/outdoor_samples/main_sample_1min/adc_data.bin"
    window_filename = "range_hanning.mat"
    num_ADCBits = 16
    num_RX = 4
    samples_per_chirp = 256
    num_chirps = 256
    num_lanes = 2
    isReal = 0
    scale_factor = np.dot([0.0625,0.03125,0.015625,0.0078125,0.00390625,0.001953125,0.0009765625,0.00048828125],4)
    scale_range = scale_factor[int(np.log2(samples_per_chirp) - 4)]
    scale_doppler = scale_factor[int(np.log2(num_chirps) - 4)]

    adc_data = Read_ADC(Filename, samples_per_chirp, num_chirps, num_ADCBits, num_RX, isReal)
    num_frames = adc_data.shape[1] // num_chirps

    plotgradient = 1

    if plotgradient == 1:
        plt.ion()
        fig = plt.figure(figsize = (12, 8))
        ax1 = fig.add_subplot(231)
        ax2 = fig.add_subplot(232)
        ax3 = fig.add_subplot(233)
        ax4 = fig.add_subplot(234)
        ax5 = fig.add_subplot(235)
        ax6 = fig.add_subplot(236)

        for num_start in range(num_frames):
            fft_out, plot_out = data_FFT(samples_per_chirp, num_chirps, num_RX, num_start, adc_data, scale_range, scale_doppler)

            """ Remove static objects """
            ##### there exit three column of static objects (due to hanning window before doppler FFT)
            plot_out[:, int(256/2) - 1] = plot_out[:, int(256/2) - 2]
            plot_out[:, int(256/2)] = (plot_out[:, int(256/2) - 3] + plot_out[:, int(256/2) + 3]) / 2.
            plot_out[:, int(256/2) + 1] =  plot_out[:, int(256/2) + 2]

            FFT_mag, mag_filter, hog_image, hog_filter, gradient_mag, gradient_mag_filter = PlotMagWithHOG(plot_out)

            plt.cla()

            ax1.clear()
            ax2.clear()
            ax3.clear()
            ax4.clear()

            ax1.axis("off")
            ax1.imshow(FFT_mag, aspect='auto')
            
            ax2.axis("off")
            ax2.imshow(hog_image, aspect='auto')

            ax3.axis("off")
            ax3.imshow(gradient_mag, aspect='auto')

            ax4.axis("off")
            ax4.imshow(mag_filter, aspect='auto')
            
            ax5.axis("off")
            ax5.imshow(hog_filter, aspect='auto')
            
            ax6.axis("off")
            ax6.imshow(gradient_mag_filter, aspect='auto')


            fig.canvas.draw()
            time.sleep(0.01)

    elif plotgradient == 2:
        plt.ion()
        fig = plt.figure(figsize = (5, 5))
        ax1 = fig.add_subplot(111, projection = '3d')

        for num_start in range(num_frames):
            fft_out, plot_out = data_FFT(samples_per_chirp, num_chirps, num_RX, num_start, adc_data, scale_range, scale_doppler)
            FFT_mag_out = plot_out

            w, h = FFT_mag_out.shape
            
            plt.cla()
            x = []
            y = []
            mag = []
            for i in range(w):
                x_row = []
                y_row = []
                mag_row = []
                for j in range(h):
                    x_row.append(i)
                    y_row.append(j)
                    mag_row.append(FFT_mag_out[i,j])
                x.append(x_row)
                y.append(y_row)
                mag.append(mag_row)
            
            x = np.array(x)
            y = np.array(y)
            mag = np.array(mag)

            ax1.clear()
            ax1.plot_surface(x, y, mag)

            fig.canvas.draw()
            time.sleep(0.01)
        else:
            plt.ion()
            fig = plt.figure(figsize = (8,8))
            ax1 = fig.add_subplot(111)

            for num_start in range(num_frames):
                fft_out, plot_out = data_FFT(samples_per_chirp, num_chirps, num_RX, num_start, adc_data, scale_range, scale_doppler)

                plt.cla()
                ax1.clear()
                ax1.imshow(plot_out, aspect='auto')
                fig.canvas.draw()
                time.sleep(0.01)