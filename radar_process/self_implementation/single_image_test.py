import matplotlib
matplotlib.use('tkagg')
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
import cv2
from skimage.feature import hog
from skimage import exposure


def OriginalPlot():
    FFT_mag_out = np.load("test1.npy")

    beta = 1.15
    average = np.mean(FFT_mag_out)
    threshold = beta * average

    mag_filter = np.where(FFT_mag_out > threshold, 1., 0.)
    
    fig = plt.figure(figsize = (20, 10))
    ax1 = fig.add_subplot(121)
    ax2 = fig.add_subplot(122)

    ax1.axis("off")
    ax1.imshow(FFT_mag_out, aspect='auto')
    
    ax2.axis("off")
    ax2.imshow(mag_filter, aspect='auto')
    
    plt.show()
    

def FindGradient():
    # img = cv2.imread("testfig.png", 1)
    FFT_mag_out = np.load("test1.npy")
    
    switch_button = 0

    fig = plt.figure(figsize = (12,6))
    ax1 = fig.add_subplot(121)
    ax2 = fig.add_subplot(122)

    if switch_button:
        image = img
    else: image = FFT_mag_out

    image = np.float32(image) / 255.
    gx = cv2.Sobel(image, cv2.CV_32F, 1, 0, ksize = 1)
    gy = cv2.Sobel(image, cv2.CV_32F, 0, 1, ksize = 1)

    mag, angle = cv2.cartToPolar(gx, gy, angleInDegrees = True)

    gamma = 3.5
    mag_av = np.mean(mag)
    thres = gamma * mag_av

    mag_filter = np.where(mag > thres, 1.,  0.)

    ax1.imshow(mag, aspect='auto')
    ax2.imshow(mag_filter, aspect='auto')

    plt.show()

def ThreeDPlot(image):
    FFT_mag_out = image

    w, h = FFT_mag_out.shape

    fig = plt.figure(figsize = (10, 10))
    ax1 = fig.add_subplot(111, projection = '3d')

    x = []
    y = []
    mag1 = []
    for i in range(w):
        x_row = []
        y_row = []
        mag_row1 = []
        for j in range(h):
            x_row.append(i)
            y_row.append(j)
            mag_row1.append(FFT_mag_out[i,j])
        x.append(x_row)
        y.append(y_row)
        mag1.append(mag_row1)

    
    x = np.array(x)
    y = np.array(y)
    mag1 = np.array(mag1)

    # print()
    ax1.plot_surface(x, y, mag1)
    plt.show()

def try_hog():
    FFT_mag_out = np.load("test1.npy")

    fd, hog_image = hog(FFT_mag_out, orientations=8, pixels_per_cell=(2, 2),
                    cells_per_block=(1, 1), visualize=True, feature_vector = True, multichannel=False)
    
    # hog_image = exposure.rescale_intensity(hog_image, in_range=(0, 10))

    hog_tobe_process = hog_image
    average = np.mean(hog_tobe_process)
    alpha = 7
    threshold = alpha * average

    hog_filter = np.where(hog_tobe_process > threshold, 1., 0.)
    test_plot = np.zeros((256,256))

    # fig = plt.figure(figsize = (20, 10))
    # ax1 = fig.add_subplot(121)
    # ax2 = fig.add_subplot(122)

    # ax1.axis("off")
    # ax1.imshow(hog_image, aspect='auto')
    
    # ax2.axis("off")
    # ax2.imshow(hog_filter, aspect='auto')
    
    # plt.show()

    return hog_image

def PlotMagWithHOG(FFT_mag, alpha = 7.9, beta = 1.12):

    fd, hog_image = hog(FFT_mag_out, orientations=8, pixels_per_cell=(2, 2),
                cells_per_block=(1, 1), visualize=True, feature_vector = True, multichannel=False)

    mag_average = np.mean(FFT_mag)
    hog_average = np.mean(hog_image)

    mag_threshold = beta * mag_average
    hog_threshold = alpha * hog_average

    mag_filter = np.where(FFT_mag > mag_threshold, 1., 0.)
    hog_filter = np.where(hog_image > hog_threshold, 1., 0.)

    fig = plt.figure(figsize = (24, 6))
    ax1 = fig.add_subplot(141)
    ax2 = fig.add_subplot(142)
    ax3 = fig.add_subplot(143)
    ax4 = fig.add_subplot(144)

    ax1.axis("off")
    ax1.imshow(FFT_mag, aspect='auto')
    
    ax2.axis("off")
    ax2.imshow(mag_filter, aspect='auto')
    
    ax3.axis("off")
    ax3.imshow(hog_image, aspect='auto')
    
    ax4.axis("off")
    ax4.imshow(hog_filter, aspect='auto')
    
    plt.show()

    

if __name__ == "__main__":
    # FindGradient()
    # ThreeDPlot()
    # OriginalPlot()
    # hog = try_hog()
    # ThreeDPlot(hog)
    FFT_mag_out = np.load("test1.npy")
    PlotMagWithHOG(FFT_mag_out)
