import matplotlib
matplotlib.use('tkagg')
import matplotlib.pyplot as plt
import numpy as np
import time
from scipy.io import loadmat
from single_image_test import *
from mpl_toolkits.mplot3d import Axes3D


def Read_ADC(Filename, samples_per_chirp, num_chirps, num_ADCBits, num_RX, isReal):
    f = open(Filename,'r')
    RawADC = np.fromfile(f, dtype = np.int16) #, count= samples_per_chirp * num_chirps * 300)
    if num_ADCBits != 16:
        lengthMax = 2^(num_ADCBits-1) -1
        RawADC[RawADC > lengthMax-1] = RawADC[RawADC > lengthMax-1] - 2^num_ADCBits
    f.close()
    FileSize = len(RawADC)
    if isReal:
        totalnumChirps = int(FileSize / samples_per_chirp / num_RX)
        TrueData = np.zeros(int(FileSize))
        TrueData = np.reshape(RawADC, samples_per_chirp * num_RX, totalnumChirps)
    else:
        totalnumChirps = int(FileSize / samples_per_chirp / num_RX / 2)
        TrueData = np.zeros(int(FileSize/2), dtype=complex)
        counter = 0
        for i in range(0,FileSize-2,4):

            TrueData[counter] = complex(RawADC[i], RawADC[i+2])
            TrueData[counter+1] = complex(RawADC[i+1], RawADC[i+3])
            counter = counter + 2
    RawADCData = np.reshape(TrueData, (totalnumChirps, samples_per_chirp * num_RX))

    ADCData = np.zeros((samples_per_chirp, totalnumChirps, num_RX), dtype = complex)
    for i in range(num_RX):
        for j in range(totalnumChirps):
            for k in range(samples_per_chirp):
                ADCData[k,j,i] = RawADCData[j, i * samples_per_chirp + k]
    return ADCData

def Chosen_Data(samples_per_chirp, num_chirps, num_RX, numStart, ADC_Data):
    wanted_data = np.zeros((samples_per_chirp, num_chirps, num_RX),dtype = complex)
    wanted_data = ADC_Data[:, (numStart) * num_chirps : (numStart + 1) * num_chirps, :]
    
    return wanted_data

def Windowed_Data(chosen_data, window_dim = "range"):
    data_size = np.shape(chosen_data)
    if window_dim == "range":
        dim = 0
        hann_window_coef = np.hanning(data_size[dim])
        hann_window_coef = np.expand_dims(np.expand_dims(hann_window_coef, axis = -1), axis = -1)
    elif window_dim == "doppler":
        dim = 1
        hann_window_coef = np.hanning(data_size[dim])
        hann_window_coef = np.expand_dims(np.expand_dims(hann_window_coef, axis = 0), axis = -1)
    else: print("Dimension input error: must input a valid dimension name.")

    windowed_data = chosen_data * hann_window_coef

    return windowed_data

def Load_Range_hannwin(window_filename, samples_per_chirp, num_chirps):
    range_hannwin = loadmat(window_filename)
    range_hannwin = np.array(range_hannwin['range_temp']).reshape(-1, order = "F")

    win_len = len(range_hannwin)
    range_hannwin_coef = np.ones(samples_per_chirp)
    range_hannwin_coef[0: win_len -1] = range_hannwin[0: (win_len - 1)]
    range_hannwin_coef[samples_per_chirp - win_len: samples_per_chirp] = (range_hannwin_coef[::-1])[win_len:]

    range_hannwin_coef = np.repeat(np.expand_dims(range_hannwin_coef, 1), repeats=num_chirps, axis=1)

    return range_hannwin_coef

def get_doppler_hannwin(samples_per_chirp, num_chirps):
    doppler_hannwin = np.hanning(num_chirps)
    win_len_ori = int(num_chirps / 2) + 1
    doppler_hannwin = [doppler_hannwin[x] for x in range(1, win_len_ori)]

    win_len = len(doppler_hannwin)
    doppler_hannwin_coef = np.ones(num_chirps)
    # doppler_hannwin_coef[0: win_len - 1] = doppler_hannwin[0: (win_len - 1)]
    # doppler_hannwin_coef[num_chirps - win_len : num_chirps] = (doppler_hannwin_coef[::-1])[win_len:]

    doppler_hannwin_coef = np.repeat(np.expand_dims(doppler_hannwin_coef, 1).transpose(), repeats = samples_per_chirp, axis = 0)

    return doppler_hannwin_coef

def data_FFT(samples_per_chirp, num_chirps, num_RX, numStart, ADC_Data, scale_range, scale_doppler):
    range_fft_out = np.zeros((samples_per_chirp, num_chirps, num_RX), dtype = complex)
    range_doppler_out = np.zeros((samples_per_chirp, num_chirps, num_RX), dtype = complex)
    visible_result = np.zeros((samples_per_chirp, num_chirps))

    chosen_data = Chosen_Data(samples_per_chirp, num_chirps, num_RX, num_start, adc_data)

    ##### DC offset compensation #####
    chosen_data = chosen_data - chosen_data.mean(axis = 0)

    ##### range FFT #####
    range_windowed_data = Windowed_Data(chosen_data, "range")
    range_fft_out = np.fft.fft(range_windowed_data, axis = 0)
    ''' Scale factor: to make the figure more attractive '''
    # range_fft_out = range_fft_out * scale_range

    ##### doppler FFT #####
    doppler_windowed_data = Windowed_Data(range_fft_out, "doppler")
    range_doppler_out = np.fft.fft(doppler_windowed_data, axis = 1)
    ''' Scale factor: to make the figure more attractive '''
    # range_doppler_out = range_doppler_out * scale_doppler
    range_doppler_out = np.fft.fftshift(range_doppler_out, axes = (1,))

    ##### create visible result #####
    visible_result = np.sum(pow(abs(range_doppler_out), 2), axis = -1)

    ##### transfer to dB, also need to try if not doing this when analyse it #####
    visible_result = 10 * np.log10(visible_result + 1)

    return range_doppler_out, visible_result

def WhiteNoiseFilter(FFT_mag, FFT_original, alpha = 8.5, beta = 1.1, gamma = 3.5):
    mag_average = np.mean(FFT_mag)
    mag_threshold = beta * mag_average
    mag_filter = FFT_mag > mag_threshold
    plot_test = np.where(FFT_mag > mag_threshold, 1., 0.)
    size_range, size_doppler = FFT_mag.shape
    range_index_all = []
    doppler_index_all = []
    FFTrest_index_all = []
    for rang_ind in range(size_range):
        for doppler_ind in range(size_doppler):
            if(rang_ind >= 6 and rang_ind <= 243) and (mag_filter[rang_ind, doppler_ind]):
                range_index_all.append(rang_ind)
                doppler_index_all.append(doppler_ind)
                FFTrest_index_all.append(FFT_original[rang_ind, doppler_ind])

    return range_index_all,doppler_index_all,FFTrest_index_all, plot_test

def AzimuthFFT(rangeidx, doppleridx, FFTrest, samples_per_chirp, AngleResolution=256, SampleRate = 1*(10**7), FreqSlope = 29.982*(10**12), lightspped = 3*(10**8)):
    XX = np.zeros((len(rangeidx)))
    YY = np.zeros((len(rangeidx)))
    Azimuth_FO = np.zeros((AngleResolution))
    for i in range(len(rangeidx)):
        ranranidx = rangeidx[i]
        dplidx = doppleridx[i]

        Azimuth_FO = pow(abs(np.fft.fft(FFTrest[i], AngleResolution)), 2)

        ##### Calculate the range according to the range index of the objects. #####
        currange = ranranidx/samples_per_chirp * SampleRate/ FreqSlope * lightspped/2

        ##### Find the peak value of Azimth frequency. #####
        maxIdx = np.argmax(Azimuth_FO)

        ##### Set the range from 0~180 to -90~90. #####
        if maxIdx > (AngleResolution/2 - 1):
            maxIdx = maxIdx - AngleResolution
        else:
            maxIdx = maxIdx       
        
        '''
        According to the definition: Theta = arcsin(s*lambda / d*num_RX), where, 
        s is the index of peak value, d is defined by official doc as lambda/2.
        '''
        ##### The result we get should be sin(theta). #####
        Wx = 2 * maxIdx/AngleResolution
        x = currange * Wx

        ##### Triangulation property. #####
        y = np.sqrt(currange**2 - x**2)
        XX[i] = x
        YY[i] = y

    return XX, YY

   
if __name__ == "__main__":
    Filename = "/home/azhang/Documents/RADAR_test/outdoor_samples/main_sample_1min/adc_data.bin"
    window_filename = "range_hanning.mat"
    num_ADCBits = 16
    num_RX = 4
    samples_per_chirp = 256
    num_chirps = 256
    num_lanes = 2
    isReal = 0
    scale_factor = np.dot([0.0625,0.03125,0.015625,0.0078125,0.00390625,0.001953125,0.0009765625,0.00048828125],4)
    scale_range = scale_factor[int(np.log2(samples_per_chirp) - 4)]
    scale_doppler = scale_factor[int(np.log2(num_chirps) - 4)]

    adc_data = Read_ADC(Filename, samples_per_chirp, num_chirps, num_ADCBits, num_RX, isReal)
    num_frames = adc_data.shape[1] // num_chirps

    plt.ion()
    fig = plt.figure(figsize = (16,8))
    ax1 = fig.add_subplot(121)
    ax2 = fig.add_subplot(122)

    for num_start in range(num_frames):
        fft_out, fft_mag_dB = data_FFT(samples_per_chirp, num_chirps, num_RX, num_start, adc_data, scale_range, scale_doppler)
        rang_t, doppler_t, FFTr_t, plot_test = WhiteNoiseFilter(fft_mag_dB, fft_out)
        X, Y = AzimuthFFT(rang_t, doppler_t, FFTr_t, samples_per_chirp)


        plt.cla()
        ax1.clear()
        ax1.axis("off")
        ax1.imshow(plot_test, aspect='auto')

        ax2.clear()
        ax2.scatter(X,Y)
        ax2.set_xlim([-12,12])
        ax2.set_ylim([0,50])
        ax2.set_title('Azimuth FFT')
        ax2.set_xlabel('x position')
        ax2.set_ylabel('y position')
        ax2.grid(which='major', axis='both', linestyle='-', alpha=0.5)

        fig.canvas.draw()
        time.sleep(0.01)
