import numpy as np
import time
import scipy.fftpack as fft
from numpy import linalg as LA
import math

class RadarRNP:
    def __init__(self, Filename, numADCBits, numRX, SamplesperChirp, numChirps, isReal):
        self.Filename = Filename
        self.numADCBits = numADCBits
        self.numRX = numRX
        self.SamplesperChirp = SamplesperChirp
        self.numChirps = numChirps
        self.numlanes = 2
        self.isReal = isReal



    def readADC(self):
        f = open(self.Filename,'r')
        RawADC = np.fromfile(f,dtype = np.int16)
        if self.numADCBits != 16:
            lengthMax = 2^(self.numADCBits-1) -1
            RawADC[RawADC > lengthMax-1] = RawADC[RawADC > lengthMax-1] - 2^self.numADCBits
        f.close()
        FileSize = len(RawADC)
        if self.isReal:
            totalnumChirps = int(FileSize/self.SamplesperChirp/self.numRX)
            TrueData = np.zeros(int(FileSize))
            TrueData = np.reshape(RawADC, self.SamplesperChirp*self.numRX, totalnumChirps)
        else:
            totalnumChirps = int(FileSize/self.SamplesperChirp/self.numRX/2)
            TrueData = np.zeros(int(FileSize/2), dtype=complex)
            counter = 0
            for i in range(0,FileSize-2,4):
                TrueData[counter] = complex(RawADC[i], RawADC[i+2])
                TrueData[counter+1] = complex(RawADC[i+1], RawADC[i+3])
                counter = counter + 2
        RawADCData = np.reshape(TrueData, (totalnumChirps, self.SamplesperChirp*self.numRX))

        ADCData = np.zeros((self.SamplesperChirp, totalnumChirps, self.numRX), dtype = complex)
        for i in range(self.numRX):
            for j in range(totalnumChirps):
                for k in range(self.SamplesperChirp):
                    ADCData[k,j,i] = RawADCData[j,i*self.SamplesperChirp+k]
        return ADCData



    def ChosenData(self, numStart, ADCData):
        # ADCData = self.readADC()
        WantedData = np.zeros((self.SamplesperChirp, self.numChirps, self.numRX),dtype = complex)
        for i in range(self.numChirps):
            WantedData[:, i, :] = ADCData[:, (numStart-1)*self.numChirps+i,:]
        return WantedData
    


    def WindowedData(self, numStart, ADCData):
        PreData = self.ChosenData(numStart, ADCData)
        WinData = np.zeros((np.shape(PreData)), dtype = complex)
        hannWin = np.hanning(self.SamplesperChirp)
        hannWin = hannWin/sum(hannWin)/self.numChirps
        for i in range(self.numChirps):
            for j in range(self.numRX):
                for k in range(self.SamplesperChirp):
                    WinData[k,i,j] = hannWin[k]*PreData[k,i,j]
        return WinData




class RadarDP:
    def __init__(self, numRX, SamplesperChirp, numChirps, lightspped, FreqSlope, SampleRate):
        self.numRX = numRX
        self.SamplesperChirp = SamplesperChirp
        self.numChirps = numChirps
        self.lightspped = lightspped
        self.FreqSlope = FreqSlope
        self.SampleRate = SampleRate

    

    def RangDopFFTPowerSquare(self, ADCInputData):
        RDFFTout = np.zeros((self.SamplesperChirp,self.numChirps))
        for i in range(self.numRX):
            FFT2out = abs(np.fft.fft2(ADCInputData[:,:,i]))
            # FFT2out2 = abs(np.transpose(np.fft.fft(np.transpose(np.fft.fft(np.squeeze(ADCInputData[:,:,i]))))))
            RDFFTout += np.multiply(FFT2out,FFT2out)
        RDFFTout = RDFFTout/self.numRX
        return RDFFTout
    


    def CFAR2D(self, WindowSize, GaurdSize, Scaler, RangeOrder, DopplerOder, FFT2Dout):
        (Xlen, Ylen) = np.shape(FFT2Dout)
        CFARThreshold2D = np.zeros((Xlen, Ylen))
        WindowInitial = np.zeros((2*WindowSize[0] + 2*GaurdSize[0] + 1, Ylen))
        Region = np.zeros((2*WindowSize[0] + 2*GaurdSize[0] + 1, 2*WindowSize[1] + 2*GaurdSize[1] + 1))

        for i in range(Xlen):
            if (i < GaurdSize[0]) or (i > Xlen - GaurdSize[0] -1):
                CFARThreshold2D[i,:] = np.amax(FFT2Dout[0:2*WindowSize[0]+2*GaurdSize[0], :])
                continue
            elif (i > GaurdSize[0]-1) and (i < WindowSize[0] + GaurdSize[0] +1):
                WindowInitial[:,:] = FFT2Dout[0:2*WindowSize[0]+2*GaurdSize[0]+1, :]
            elif (i < Xlen - GaurdSize[0] +1) and (i > Xlen - WindowSize[0] - GaurdSize[0] - 1):
                WindowInitial[:,:] = FFT2Dout[Xlen - 2*WindowSize[0] - 2*GaurdSize[0] -1::,:]
            else:
                WindowInitial[:,:] = FFT2Dout[i-WindowSize[0]-GaurdSize[0]-1: i+WindowSize[0]+GaurdSize[0], :]
            for j in range(Ylen):
                if (j < GaurdSize[1]) or (j > Ylen - GaurdSize[1] -1):
                    CFARThreshold2D[i,j] = np.amax(FFT2Dout[:,0:2*WindowSize[1]+2*GaurdSize[1]])
                    continue
                elif (j > GaurdSize[1]-1) and (j < WindowSize[1] + GaurdSize[1] +1):
                    Region = FFT2Dout[:,0:2*WindowSize[1]+2*GaurdSize[1]+1]
                elif (j < Xlen - GaurdSize[1] +1) and (j > Ylen - WindowSize[1] - GaurdSize[1] - 1):
                    Region = FFT2Dout[:,Ylen - 2*WindowSize[1] - 2*GaurdSize[1] -1::]
                else:
                    Region = FFT2Dout[:,j-WindowSize[1]-GaurdSize[1]-1: j+WindowSize[1]+GaurdSize[1]]
                middle = WindowSize + GaurdSize
                Region[middle[0]-GaurdSize[0]:middle[0]+GaurdSize[0]+1, middle[1]-GaurdSize[1]:middle[1]+GaurdSize[1]+1] = 0
                RegionOrder = np.flip(np.sort(Region))
                CFARThreshold2D[i,j] = Scaler*RegionOrder[RangeOrder-1,DopplerOder-1]
        
        TargetFFT = np.zeros((Xlen, Ylen))
        rangeidx = []
        doppleridx = []
        for i in range(Xlen):
            for j in range(Ylen):
                if FFT2Dout[i,j] > CFARThreshold2D[i,j]:
                    TargetFFT[i,j] = 10000
                    rangeidx.append(i)
                    doppleridx.append(j)
                else:
                    TargetFFT[i,j] = 1
        return TargetFFT,rangeidx,doppleridx, CFARThreshold2D



    # def rootMUSIC(self,rangeidx, doppleridx, WinData):
    #     DataAveraging = np.zeros((self.SamplesperChirp,self.numRX),dtype=complex)
    #     for i in range(self.numRX):
    #         for j in range(self.SamplesperChirp):
    #             DataAveraging[j,i] = np.mean(WinData[j,:,i])
    #     RangeFFT = np.zeros((self.numRX, self.SamplesperChirp),dtype=complex)
    #     for i in range(self.numRX):
    #         RangeFFT[i,:] = np.fft.fft(DataAveraging[:,i])
    #     # theta = np.array([i-90 for i in range(181)])
    #     theta = np.zeros((len(rangeidx)))
    #     Xposition = np.zeros((len(rangeidx)))
    #     Yposition = np.zeros((len(rangeidx)))
    #     hmOb = 1
    #     for i in range(len(rangeidx)):
    #         rranindex = rangeidx[i]
    #         rangecurrent = rranindex/self.SamplesperChirp*self.SampleRate/self.FreqSlope*self.lightspped/2
    #         ExtractData = np.matrix(RangeFFT[:,rranindex])
    #         Covar = ExtractData.getH() * ExtractData
    #         Evalue, Evector = LA.eig(Covar)
    #         EigvalSort = np.flip(np.argsort(Evalue))
    #         Evector = Evector[:,EigvalSort]
    #         EigenNoise = Evector[:,hmOb::]
    #         C = EigenNoise * EigenNoise.getH()
    #         CL = np.zeros(2*len(C)-1,dtype = complex)
    #         for l in range(len(C)-1):
    #             for n in range(len(C)-(l+1)):
    #                 CL[l] = CL[l] + C[n,l+n]
    #         PolynomialCoefficient = np.concatenate([np.flip(CL), [np.sum(np.diag(C))], np.conjugate(CL)])
    #         Zroot = np.roots(PolynomialCoefficient)
    #         sortidx = np.argsort(abs(np.array([abs(Zroot[k])-1 for k in range(len(Zroot))])))
    #         Zsure = Zroot[sortidx[0]]
    #         theta[i] = np.arcsin(np.arctan(np.imag(Zsure)/np.real(Zsure))/np.pi)
    #         Xposition[i] = rangecurrent*np.sin(theta[i])
    #         Yposition[i] = rangecurrent*np.cos(theta[i])
    #     return theta, Xposition, Yposition


    

    def AzimuthFFT(self, rangeidx, WinData):
        AngleResolution = 256
        # AngleLineSpace = np.array([x for x in np.arange(-math.pi, math.pi, 2*math.pi/AngleResolution)])
        DataAveraging = np.zeros((self.SamplesperChirp,self.numRX),dtype=complex)
        for i in range(self.numRX):
            for j in range(self.SamplesperChirp):
                DataAveraging[j,i] = np.mean(WinData[j,:,i])
        RangeAzimuthFFT = abs(np.fft.fft2(DataAveraging,s=[self.SamplesperChirp,AngleResolution]))
        XX = np.zeros((len(rangeidx)))
        YY = np.zeros((len(rangeidx)))
        for i in range(len(rangeidx)):
            ranranidx = rangeidx[i]
            currange = ranranidx/self.SamplesperChirp*self.SampleRate/self.FreqSlope*self.lightspped/2
            AzimuthFreq = RangeAzimuthFFT[ranranidx,:]
            maxIdx = np.argmax(AzimuthFreq)
            # maxIdx = maxIdx - AngleResolution/2
            if maxIdx > (AngleResolution/2 - 1):
                maxIdx = maxIdx - AngleResolution
            else:
                maxIdx = maxIdx
            Wx = 2 * maxIdx/AngleResolution
            x = currange * Wx
            y = np.sqrt(currange**2 - x**2)
            XX[i] = x
            YY[i] = y
        return XX, YY
            


    




