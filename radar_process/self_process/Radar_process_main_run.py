import numpy as np
import matplotlib.pyplot as plt
import time
from Radar_process_main import RadarDataProcessing

if __name__ == "__main__":
    plt.ion()
    fig = plt.figure(figsize=(12,6))
    ax1 = fig.add_subplot(121)
    ax2 = fig.add_subplot(122)

    Filename = '/Users/aozhang/Library/Mobile Documents/com~apple~CloudDocs/Study in Canada/VIVA/captureddata/outdoor_samples/main_sample_1min/adc_data.bin'
    Position = RadarDataProcessing(Filename)

    ADCdata = Position.readADC()

    for numStart in range(1,300):
    # numStart = 10

        NormalizedData = Position.WindowedData(numStart, ADCdata)
        # print(np.shape(NormalizedData))

        RPwhole, RP = Position.RgDplFFT(NormalizedData)

        # Xpos, Ypos = Position.AzimuthFFT(rangeidx, RangeAzimuth)

        plt.cla()
        ax1.clear()
        # RangeDoppler = 20*np.log10(RangeDoppler)
        ax1.imshow(20*np.log10(RP), aspect='auto', extent=[-60,60,50,0])

        rangeidx, doppleridx, TargetFFT = Position.CFAR2D(RP)
        
        Xpos, Ypos = Position.AzimuthFFT(rangeidx, doppleridx, RPwhole)
        
        ax2.clear()
        ax2.scatter(Xpos,Ypos)
        ax2.set_xlim([-8,8])
        ax2.set_ylim([0,50])
        ax2.set_title('Azimuth FFT')
        ax2.set_xlabel('x position')
        ax2.set_ylabel('y position')
        ax2.grid(which='major', axis='both', linestyle='-', alpha=0.5)


        # fig.savefig('/home/ao/Desktop/SaveFig2/%d.png'%numStart)
        
        fig.canvas.draw()
        
        # time.sleep(0.001)
        
        plt.pause(0.0001)
        # plt.show()


