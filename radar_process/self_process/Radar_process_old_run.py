import numpy as np
import matplotlib.pyplot as plt
import time
import scipy
from Radar_process_old import RadarRNP
from Radar_process_old import RadarDP

if __name__ == "__main__":
    plt.ion()
    fig = plt.figure(num=None, figsize=(15,8))
    # ax = fig.add_subplot(111)
    # ax1 = fig.add_subplot(131)
    # ax2 = fig.add_subplot(132)
    ax = fig.add_subplot(111)
    # ax4 = fig.add_subplot(144)
    Filename = '/Users/aozhang/Library/Mobile Documents/com~apple~CloudDocs/Study in Canada/VIVA/captureddata/outdoor_samples/main_sample_1min/adc_data.bin'
    numADCBits = 16
    numRX = 4
    SamplesperChirp = 256
    numChirps = 128
    isReal = 0
    c = 3*(10**8)
    FreqSlope = 29.982*(10**12)
    SampleRate = 1*(10**7)


    WindowSize = [16,8]
    GaurdSize = [3,2]
    Scaler = 3.5
    RangeOrder = 1
    DopplerOder = 1

    ADC = RadarRNP(Filename, numADCBits, numRX, SamplesperChirp,numChirps,isReal)
    Process = RadarDP(numRX, SamplesperChirp, numChirps, c, FreqSlope, SampleRate)

    ADCdata = ADC.readADC()

    for numStart in range(18):
    # numStart = 5

        plt.cla()
        Raw = ADC.WindowedData(numStart, ADCdata)
        FFTO = Process.RangDopFFTPowerSquare(Raw)

        FFTO = np.fft.fftshift(FFTO,axes=1)


        # ax1.imshow(20*np.log10(FFTO), aspect='auto', extent=[-64,64,50,0])
        # ax1.set_title('Range Doppler Spectrum')
        # ax1.set_xlabel('velocity')
        # ax1.set_ylabel('range')

        
        # FFTO = np.fft.fftshift(FFTO)

       
        Target,rangeidx, doppleridx, threshold = Process.CFAR2D(WindowSize, GaurdSize, Scaler, RangeOrder, DopplerOder, FFTO)
        # test11, test2, test3 = Process.rootMUSIC(rangeidx,doppleridx,Raw)
        test4,test5 = Process.AzimuthFFT(rangeidx, Raw)


        # Target = np.fft.fftshift(Target)

        # ax2.imshow(20*np.log10(Target), aspect='auto', extent=[-64,64,50,0])
        # ax2.set_title('Range Doppler After OS CFAR')
        # ax2.set_xlabel('velocity')
        # ax2.set_ylabel('range')

        # ax3.clear()
        # ax3.scatter(test2,test3)
        # ax3.set_xlim([-3,3])
        # ax3.set_ylim([0,50])
        # ax3.set_title('root MUSIC')
        # ax3.set_xlabel('x position')
        # ax3.set_ylabel('y position')

        ax.clear()
        ax.scatter(test4,test5)
        ax.set_xlim([-3,3])
        ax.set_ylim([0,50])
        ax.set_title('Azimuth FFT')
        ax.set_xlabel('x position')
        ax.set_ylabel('y position')

        fig.canvas.draw()
        time.sleep(0.1)
