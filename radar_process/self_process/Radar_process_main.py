import numpy as np

class RadarDataProcessing:
    '''
    Initialize the all necessary parameters.
    '''
    def __init__(self, Filename):
        self.Filename = Filename
        self.numADCBits = 16
        self.numRX = 4
        self.SamplesperChirp = 256
        self.numChirps = 128
        self.numlanes = 2
        self.isReal = 0
        self.lightspped = 3*(10**8)
        self.FreqSlope = 29.982*(10**12)
        self.SampleRate = 1*(10**7)

        ##### Feel free to tune this parameters. #####
        self.WindowSize = [5,5]
        self.GaurdSize = [3,3]
        self.Scaler = 15
        self.RangeOrder = 3 
        self.DopplerOder = 1 


        ##### I think with this zero-padding resolution, it works. #####
        self.AngleResolution = 256


    '''
    Basically the same as the Matlab Data Parse Code
    '''
    def readADC(self):
        f = open(self.Filename,'r')
        RawADC = np.fromfile(f,dtype = np.int16, count= self.SamplesperChirp*self.numChirps*1200)
        if self.numADCBits != 16:
            lengthMax = 2^(self.numADCBits-1) -1
            RawADC[RawADC > lengthMax-1] = RawADC[RawADC > lengthMax-1] - 2^self.numADCBits
        f.close()
        FileSize = len(RawADC)
        if self.isReal:
            totalnumChirps = int(FileSize/self.SamplesperChirp/self.numRX)
            TrueData = np.zeros(int(FileSize))
            TrueData = np.reshape(RawADC, self.SamplesperChirp*self.numRX, totalnumChirps)
        else:
            totalnumChirps = int(FileSize/self.SamplesperChirp/self.numRX/2)
            TrueData = np.zeros(int(FileSize/2), dtype=complex)
            counter = 0
            for i in range(0,FileSize-2,4):
                TrueData[counter] = complex(RawADC[i], RawADC[i+2])
                TrueData[counter+1] = complex(RawADC[i+1], RawADC[i+3])
                counter = counter + 2
        RawADCData = np.reshape(TrueData, (totalnumChirps, self.SamplesperChirp*self.numRX))

        ADCData = np.zeros((self.SamplesperChirp, totalnumChirps, self.numRX), dtype = complex)
        for i in range(self.numRX):
            for j in range(totalnumChirps):
                for k in range(self.SamplesperChirp):
                    ADCData[k,j,i] = RawADCData[j,i*self.SamplesperChirp+k]
        return ADCData


    '''
    This only works when a bunch of data is stored and you want to use it as time step.
    '''
    def ChosenData(self, numStart,ADCData):
        # ADCData = self.readADC()
        WantedData = np.zeros((self.SamplesperChirp, self.numChirps, self.numRX),dtype = complex)

        ##### Currently, for Doppler FFT, I take each 128 chirps as a frame. Could ######
        ##### be changed if needed. ######
        for i in range(self.numChirps):
            WantedData[:, i, :] = ADCData[:, (numStart-1)*self.numChirps+i,:]
        return WantedData

    
    '''
    Hanning window funciton to normalize the Raw ADC Data before Fast Fourier Transform.
    '''
    def WindowedData(self, numStart, ADCData):
        PreData = self.ChosenData(numStart, ADCData)
        WinData = np.zeros((np.shape(PreData)), dtype = complex)

        ##### Hanning Window: to reduce the influence of power leakage. #####
        hannWin = np.hanning(self.SamplesperChirp)
        hannWin = hannWin/sum(hannWin)/self.numChirps
        for i in range(self.numChirps):
            for j in range(self.numRX):
                for k in range(self.SamplesperChirp):
                    WinData[k,i,j] = hannWin[k]*PreData[k,i,j]
        return WinData
    

    '''
    Get the Range-Doppler FFT as preperation of Object-Detection.
    '''
    def RgDplFFT(self, ADCInputData):
        RangeDopplerFFT_Whole = np.zeros((self.SamplesperChirp, self.numChirps, self.numRX), dtype = complex)
        RangeDopplerFFT = np.zeros((self.SamplesperChirp, self.numChirps))

        for RXidx in range(self.numRX):
            RangeDopplerFFT_Whole[:,:,RXidx] = np.fft.fftshift(np.fft.fft2(ADCInputData[:,:,RXidx]), axes=(1,))
            RangeDopplerFFT[:,:] += pow(abs(RangeDopplerFFT_Whole[:,:,RXidx]), 2)
        
        RangeDopplerFFT = RangeDopplerFFT/self.numRX

        return RangeDopplerFFT_Whole, RangeDopplerFFT

    

    '''
    Object Detection Algorithm: Order Static Constant False Alarm Rate.
    '''
    def CFAR2D(self, FFT2Dout):
        FFT2Dout[:,int(self.numChirps/2)] = FFT2Dout[:,int(self.numChirps/2) + 1]
        (Xlen, Ylen) = np.shape(FFT2Dout)
        CFARThreshold2D = np.zeros((Xlen, Ylen))

        WholewinRowL = 2*self.WindowSize[0] + 2*self.GaurdSize[0] + 1
        WholewinColL = 2*self.WindowSize[1] + 2*self.GaurdSize[1] + 1
        XwinL = self.WindowSize[0] + self.GaurdSize[0]
        YwinL = self.WindowSize[1] + self.GaurdSize[1]

        for splidx in range(Xlen):
            for chpidx in range(Ylen):
                WholWindow = np.zeros((WholewinRowL, WholewinColL))
                if (splidx-XwinL) < 0:
                    if (chpidx-YwinL) < 0:
                        WholWindow[XwinL-splidx:WholewinRowL-1, YwinL-chpidx:WholewinColL-1] = FFT2Dout[0:splidx+XwinL, 0:chpidx+YwinL]
                    elif (chpidx+YwinL) >= Ylen:
                        WholWindow[XwinL-splidx:WholewinRowL-1, 0:YwinL-chpidx+Ylen-1] = FFT2Dout[0:splidx+XwinL, chpidx-YwinL:Ylen-1]
                    else:
                        WholWindow[XwinL-splidx:WholewinRowL-1, 0:WholewinColL-1] = FFT2Dout[0:splidx+XwinL, chpidx-YwinL:chpidx+YwinL]
                elif (splidx+XwinL) >= Xlen:
                    if (chpidx-YwinL) < 0:
                        WholWindow[0:XwinL-splidx+Xlen-1, YwinL-chpidx:WholewinColL-1] = FFT2Dout[splidx-XwinL:Xlen-1, 0:chpidx+YwinL]
                    elif (chpidx+YwinL) >= Ylen:
                        WholWindow[0:XwinL-splidx+Xlen-1, 0:YwinL-chpidx+Ylen-1] = FFT2Dout[splidx-XwinL:Xlen-1, chpidx-YwinL:Ylen-1]
                    else:
                        WholWindow[0:XwinL-splidx+Xlen-1, 0:WholewinColL-1] = FFT2Dout[splidx-XwinL:Xlen-1, chpidx-YwinL:chpidx+YwinL]
                else:
                    if (chpidx-YwinL) < 0:
                        WholWindow[0:WholewinRowL-1, YwinL-chpidx:WholewinColL-1] = FFT2Dout[splidx-XwinL:splidx+XwinL, 0:chpidx+YwinL]
                    elif (chpidx+YwinL) >= Ylen:
                        WholWindow[0:WholewinRowL-1, 0:YwinL-chpidx+Ylen-1] = FFT2Dout[splidx-XwinL:splidx+XwinL, chpidx-YwinL:Ylen-1]
                    else:
                        WholWindow[0:WholewinRowL-1, 0:WholewinColL-1] = FFT2Dout[splidx-XwinL:splidx+XwinL, chpidx-YwinL:chpidx+YwinL]
                
                WholWindow[self.WindowSize[0]:self.WindowSize[0]+2*self.GaurdSize[0], self.WindowSize[1]:self.WindowSize[1]+2*self.GaurdSize[1]] = 0

                CFARThreshold2D[splidx, chpidx] = self.Scaler * np.mean(WholWindow)
            
        #### If the magnitude of the current cell is greater than the threshold, store it as object. #####
        TargetFFT = np.zeros((Xlen, Ylen))
        rangeidx = []
        doppleridx = []
        for i in range(Xlen):
            for j in range(Ylen):
                if FFT2Dout[i,j] > CFARThreshold2D[i,j]:
                    TargetFFT[i,j] = 100
                    rangeidx.append(i)
                    doppleridx.append(j)
                else:
                    TargetFFT[i,j] = 1
        return rangeidx, doppleridx, TargetFFT



    '''
    Angle of Arrival: using Azimuth FFT with zero paddings to improve the resolution.
    '''
    def AzimuthFFT(self, rangeidx, doppleridx, RangDplWhole):
        XX = np.zeros((len(rangeidx)))
        YY = np.zeros((len(rangeidx)))
        Azimuth_FO = np.zeros((self.AngleResolution))
        for i in range(len(rangeidx)):
            ranranidx = rangeidx[i]
            dplidx = doppleridx[i]

            Azimuth_FO = pow(abs(np.fft.fft(RangDplWhole[ranranidx, dplidx, :], self.AngleResolution)), 2)

            ##### Calculate the range according to the range index of the objects. #####
            currange = ranranidx/self.SamplesperChirp*self.SampleRate/self.FreqSlope*self.lightspped/2

            ##### Find the peak value of Azimth frequency. #####
            maxIdx = np.argmax(Azimuth_FO)

            ##### Set the range from 0~180 to -90~90. #####
            if maxIdx > (self.AngleResolution/2 - 1):
                maxIdx = maxIdx - self.AngleResolution
            else:
                maxIdx = maxIdx       
            
            '''
            According to the definition: Theta = arcsin(s*lambda / d*num_RX), where, 
            s is the index of peak value, d is defined by official doc as lambda/2.
            '''
            ##### The result we get should be sin(theta). #####
            Wx = 2 * maxIdx/self.AngleResolution
            x = currange * Wx

            ##### Triangulation property. #####
            y = np.sqrt(currange**2 - x**2)
            XX[i] = x
            YY[i] = y

        return XX, YY



