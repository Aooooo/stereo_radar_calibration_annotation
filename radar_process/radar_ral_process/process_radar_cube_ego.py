import numpy as np
import cv2
import os
import matplotlib.pyplot as plt
import math
from scipy.signal import find_peaks
from tqdm import tqdm
from glob import glob 
import time
from multiprocessing import Process
from collections import Counter
from sklearn.cluster import DBSCAN
from skimage.morphology import disk
from skimage.filters import gaussian, median
import multiprocessing as mp
from functools import partial

RANGE_SIZE = 256
DOPPLER_SIZE = 64
# Radar Configuration
# RANGE_RESOLUTION = 0.1953125 # m
VELOCITY_RESOLUTION = 0.41968030701528203 # m/s
RANGE_SIZE = 256
RANGE_RESOLUTION = 50 / RANGE_SIZE
AZIMUTH_SIZE = 256 
RADAR_CONFIG_FREQ = 77 # GHz
DESIGNED_FREQ = 76.8 # GHz
################# problem: why it is so weird when remove 2 ###############
ANGULAR_RESOLUTION = 0.5 * np.pi / AZIMUTH_SIZE # radians

def checkoutDir(dir_name):
    """
    Function:
        Checkout if the directory is exists, if not then create one

    Args:
        dir_name            ->          directory name
    """
    if not os.path.exists(dir_name):
        os.mkdir(dir_name)
    else:
        file_list = glob(os.path.join(dir_name, '*'))
        if len(file_list) != 0:
            for f in file_list:
                os.remove(f)

def getTotoalNum(target_dir):
    """
    Function:
        Get the total number of images, prepare for the sequence number

    Args:
        img_dir             ->          image directory for counting
    """
    all_files = glob(target_dir + "*.npy")
    return len(all_files)

def readRAD(radar_dir, frame_id):
    """
    Function:
        Get the 3D FFT results of the current frame.
    
    Args:
        radar_dir           ->          radar directory
        frame_id            ->          frame that is about to be processed.
    """
    if os.path.exists(radar_dir + "%.6d.npy"%(frame_id)):
        return np.load(radar_dir + "%.6d.npy"%(frame_id))
    else:
        return None

def readCam(stereo_imgs_dir, frame_id):
    """
    Function:
        Read images to verify the radar

    Args:
        stereo_imgs_dir     ->          stereo directory
        frame_id            ->          frame that is about to be processed.
    """
    return cv2.imread(stereo_imgs_dir + "%d.jpg"%(frame_id))[..., ::-1]

def readValidFrame(filename):
    """
    Function:
        Read the frame valid information to filter out some cropped frames.

    Args:
        filename            ->          e.x. adc_valid_2020-06-01-14-44-13.txt
    """
    output = []
    with open(filename, "r") as f:
        all_lines = f.readlines()
        for line in all_lines:
            if line == "True\n":
                output.append(True)
            else:
                output.append(False)
    return output

def getMagnitude(target_array, power_order=2):
    """
    Function:
        Get the magnitude of the complex array
    """
    target_array = np.abs(target_array)
    target_array = pow(target_array, power_order)
    return target_array 

def getLog(target_array, scalar=1., log_10=True):
    """
    Function:
        Get the log of the complex array
    """
    if log_10:
        return scalar * np.log10(target_array + 1.)
    else:
        return target_array

def getSumDim(target_array, target_axis):
    """
    Function:
        Sum up one dimension of a  3D matrix.
    
    Args:
        column_index            ->          which column to be deleted
    """
    output = np.sum(target_array, axis=target_axis)
    return output 

def getNormalized(target_array):
    """
    Function:
        Normalize the target array.
    """
    target_array /= target_array.max()
    return target_array

def averageFilter(radar_RAD_mag, bg_val=1., alpha=1.):
    """
    Function:
        Apply average filter on the whole RAD magnitude array.
    """
    scaled_mean_val = alpha * radar_RAD_mag.mean()
    output = np.where(radar_RAD_mag >= scaled_mean_val, \
            radar_RAD_mag, bg_val)
    return output

def azimuthPeaks(azimuth_FFT_mag, azimuth_cfar_indexes, peak_cell_num=None):
    """
    Function:
        Choose peak_cell_num highest magnitude of azimuth FFT as final 
    azimuth mask indexes
    
    Args:
        azimuth_FFT_mag             ->          azimuth FFT magnitude
        azimuth_cfar_indexes        ->          azimuth mask indexes after CFAR
    """
    edge_cell_num = 2
    sec_amp_portion = 0.8
    is_mask = False
    internal_mask = np.zeros(azimuth_cfar_indexes.shape)
    output_mask = np.zeros(azimuth_cfar_indexes.shape)
    all_peaks_idxes = []
    for i in range(0, len(azimuth_cfar_indexes)):
        if is_mask == False:
            if azimuth_cfar_indexes[i] == 1:
                is_mask = True
                internal_mask[i] = 1.
        else:
            if azimuth_cfar_indexes[i] == 1 and i < len(azimuth_cfar_indexes)-1:
                internal_mask[i] = 1.
            else:
                is_mask = False
                filtered_mag = azimuth_FFT_mag * internal_mask
                local_max, _ = find_peaks(filtered_mag)
                for peak_i in range(len(local_max)):
                    output_mask[local_max[peak_i]] = 1.
                all_peaks_idxes.append(local_max)
                internal_mask = np.zeros(azimuth_cfar_indexes.shape)

    output_mask = np.zeros(azimuth_cfar_indexes.shape)
    if peak_cell_num is None:
        if len(all_peaks_idxes) > 0:
            all_peaks_idxes = np.concatenate(all_peaks_idxes, axis=0)
            for i in range(len(all_peaks_idxes)):
                k = all_peaks_idxes[i]
                if k <= edge_cell_num or k >= len(azimuth_FFT_mag)-1-edge_cell_num:
                    continue
                output_mask[k] = 1.
            output_mask = np.where(output_mask > 0, 1, 0)
        return output_mask
    else:
        if len(all_peaks_idxes) > 0:
            all_peaks_idxes = np.concatenate(all_peaks_idxes, axis=0)
            all_peaks_mag = azimuth_FFT_mag[all_peaks_idxes]
            constrained_peaks_idxes = all_peaks_idxes[np.argsort(\
                            all_peaks_mag)[::-1][:peak_cell_num]]
            peak_max_mag = 0
            for i in range(len(constrained_peaks_idxes)):
                k = constrained_peaks_idxes[i]
                if k <= edge_cell_num or k >= len(azimuth_FFT_mag)-1-edge_cell_num:
                    continue
                if i == 0:
                    output_mask[k] = 1.
                    peak_max_mag = azimuth_FFT_mag[k]
                else:
                    if azimuth_FFT_mag[k] >= sec_amp_portion * peak_max_mag:
                        output_mask[k] = 1.
            output_mask = np.where(output_mask > 0, 1, 0)
        return output_mask

def threeDCFAR(radar_RAD_mag, ref_win, guard_win, order_statistic, 
                OS_scalar, CA_scalar, mode):
    """
    Function:
        Apply 3D CFAR on Range-Azimuth-Doppler.
    
    Args:
        radar_RAD_mag           ->          radar RAD magnitude
        ref_win                 ->          reference window size ON EACH SIDE, (bla, bla, bla)
        guard_win               ->          guard window size ON EACH SIDE, (bla, bla, bla)
        thres_order             ->          for OS CFAR, the order that taken as threshold
        mode                    ->          "CA-CFAR" or "OS-CFAR"
    """
    if mode not in ["OS-CFAR", "CA-CFAR"]:
        raise ValueError ("Wrong mode input.")
    assert len(ref_win) == 3
    assert len(guard_win) == 3
    data_size = radar_RAD_mag.shape
    # initialize threshold matrix
    threshold_mat = np.zeros(data_size)
    # pad data
    pad_size = (ref_win[0]+guard_win[0], ref_win[1]+guard_win[1], ref_win[2]+guard_win[2])
    radar_RAD_zeropad = np.pad(radar_RAD_mag, ((pad_size[0], pad_size[0]), 
                                        (pad_size[1], pad_size[1]),
                                        (pad_size[2], pad_size[2])), 
                                        'constant', constant_values=radar_RAD_mag.mean())
    # build a sliding window, use this for multiply each cell
    sliding_window = np.ones([pad_size[0]*2 + 1, pad_size[1]*2 + 1, pad_size[2]*2 + 1])
    for i in range(ref_win[0], ref_win[0] + guard_win[0]*2 + 1):
        for j in range(ref_win[1], ref_win[1] + guard_win[1]*2 + 1):
            for k in range(ref_win[2], ref_win[2] + guard_win[2]*2 + 1):
                sliding_window[i,j,k] = 0.

    # start multiplying for CFAR thresholds
    for i in tqdm(range(pad_size[0], pad_size[0] + data_size[0])):
        for j in range(pad_size[1], pad_size[1] + data_size[1]):
            for k in range(pad_size[2], pad_size[2] + data_size[2]):
                data_cell = radar_RAD_zeropad[i - pad_size[0]: i + pad_size[0] + 1, \
                                        j - pad_size[1]: j + pad_size[1] + 1, \
                                        k - pad_size[2]: k + pad_size[2] + 1]
                window_val = sliding_window * data_cell
                if mode == "OS-CFAR":
                    window_val_order = np.sort(window_val.reshape(-1))
                    current_threshold = OS_scalar * window_val_order[int(order_statistic * len(window_val_order))]
                elif mode == "CA-CFAR":
                    current_threshold = CA_scalar * window_val.mean() 
                threshold_mat[i - pad_size[0], j - pad_size[1], k - pad_size[2]] = current_threshold

    # define output
    output_mat = radar_RAD_mag - threshold_mat
    output_mat = np.where(output_mat >= 0, 1., 0.)
    return output_mat

def twoDCFAR(twoD_array, ref_win, guard_win, os, scalar, mode):
    """
    Function:
        Apply 3D CFAR on Range-Azimuth-Doppler.
    
    Args:
        radar_RAD_mag           ->          radar RAD magnitude
        ref_win                 ->          reference window size ON EACH SIDE, (bla, bla, bla)
        guard_win               ->          guard window size ON EACH SIDE, (bla, bla, bla)
        thres_order             ->          for OS CFAR, the order that taken as threshold
        mode                    ->          "CA-CFAR" or "OS-CFAR"
    """
    if mode not in ["OS-CFAR", "CA-CFAR"]:
        raise ValueError ("Wrong mode input.")
    assert len(ref_win) == 2
    assert len(guard_win) == 2
    data_size = twoD_array.shape
    # initialize threshold matrix
    threshold_mat = np.zeros(data_size)
    # pad data
    pad_size = (ref_win[0]+guard_win[0], ref_win[1]+guard_win[1])
    pad_top = twoD_array[twoD_array.shape[0]-pad_size[0]:, :]
    pad_bot = twoD_array[:pad_size[0], :]
    pad_left = twoD_array[:, twoD_array.shape[1]-pad_size[1]:]
    pad_right = twoD_array[:, :pad_size[1]]
    pad_top_extleft = pad_top[:, pad_top.shape[1]-pad_size[1]:]
    pad_top_extright = pad_top[:, :pad_size[1]]
    pad_top = np.concatenate([pad_top_extleft, pad_top, pad_top_extright], axis=1)
    pad_bot_extleft = pad_bot[:, pad_bot.shape[1]-pad_size[1]:]
    pad_bot_extright = pad_bot[:, :pad_size[1]]
    pad_bot = np.concatenate([pad_bot_extleft, pad_bot, pad_bot_extright], axis=1)
    data_pad_lr = np.concatenate([pad_left, twoD_array, pad_right], axis=1)
    data_pad = np.concatenate([pad_top, data_pad_lr, pad_bot], axis=0)

    # data_pad = np.pad(twoD_array, ((pad_size[0], pad_size[0]), 
                                        # (pad_size[1], pad_size[1])),
                                        # 'constant', constant_values=twoD_array.min())
    # build a sliding window, use this for multiply each cell
    sliding_window = np.ones([pad_size[0]*2 + 1, pad_size[1]*2 + 1])
    for i in range(ref_win[0], ref_win[0] + guard_win[0]*2 + 1):
        for j in range(ref_win[1], ref_win[1] + guard_win[1]*2 + 1):
            sliding_window[i,j] = 0.

    ###################### TESTING CFAR IMPROVISED #####################
    sliding_window[..., ref_win[1]: ref_win[1]+guard_win[1]*2+1] = 0.
    ###################### TESTING CFAR IMPROVISED #####################

    # start multiplying for CFAR thresholds
    for i in range(pad_size[0], pad_size[0] + data_size[0]):
        for j in range(pad_size[1], pad_size[1] + data_size[1]):
            data_cell = data_pad[i - pad_size[0]: i + pad_size[0] + 1, \
                                    j - pad_size[1]: j + pad_size[1] + 1] 
            window_val = sliding_window * data_cell
            if mode == "OS-CFAR":
                window_val_order = np.sort(window_val.reshape(-1))
                current_threshold = scalar * window_val_order[int(os * len(window_val_order))]
            elif mode == "CA-CFAR":
                current_threshold = scalar * window_val.mean() 
            threshold_mat[i - pad_size[0], j - pad_size[1]] = current_threshold

    # define output
    output_mat = twoD_array - threshold_mat
    output_mat = np.where(output_mat >= 0, 1., 0.)
    return threshold_mat, output_mat

def oneDCFAR(array, ref_win, guard_win, scalar, order_statistic, mode):
    """
    Function:
        Apply one dimension CFAR on an array-like data.

    Args:
        array           ->          target data
        ref_win         ->          reference window size
        guard_win       ->          guard window size
        scalar          ->          final scalar
        order_statistic ->          order to take, if OS-CFAR
        mode            ->          OS-CFAR or CA-CFAR
    """
    assert len(np.shape(array)) == 1
    assert isinstance(ref_win, int)
    assert isinstance(guard_win, int)
    if mode not in ["OS-CFAR", "CA-CFAR"]:
        raise ValueError ("Wrong mode input.")
    cfar_threshold = np.zeros(array.shape)
    pad_size = ref_win + guard_win
    pad_left = array[len(array) - pad_size:]
    pad_right = array[:pad_size]
    array_pad = np.concatenate([pad_left, array, pad_right], axis=0)
    
    sliding_window = np.ones(pad_size*2 + 1)
    for i in range(ref_win, ref_win + guard_win*2 + 1):
        sliding_window[i] = 0.

    for i in range(pad_size, pad_size + array.shape[0]):
        cell = array_pad[i - pad_size: i + pad_size + 1]
        cell_val = sliding_window * cell
        if mode == "OS-CFAR":
            cell_val_order = np.sort(cell_val.reshape(-1))
            threshold = scalar * cell_val_order[int(order_statistic * len(cell_val_order))]
        elif mode == "CA-CFAR":
            threshold = scalar * cell_val.mean()
        cfar_threshold[i - pad_size] = threshold
    output_mat = array - cfar_threshold
    output_mat = np.where(output_mat >= 0, 1., 0.)
    return cfar_threshold, output_mat

def separateR_D_A(radar_RAD_mag, fig = None, axes = None):
    """
    Function:
        Do R-A CFAR first, then use the filtered index for masking.
    
    Args:
        radar_RAD_mag           ->          radar R-A-D matrix with magnitude only
    """
    RD_img = getLog(getSumDim(radar_RAD_mag, 1), scalar=10, log_10=True)
    range_mag = getLog(getSumDim(RD_img, -1), scalar=10, log_10=True)

    range_threshold, range_cfar_indexes = oneDCFAR(range_mag, ref_win=6, guard_win=4, \
                            scalar=1.023, order_statistic=0.7, mode="OS-CFAR")
    ################ plot to see range cfar ##################
    if fig is not None:
        clearAxes(axes)
        axes[0].plot(range_mag, 'r')
        axes[0].plot(range_threshold, 'b')
        keepDrawing(fig, 0.1)
    
    RAD_mask = []
    for i in range(len(range_cfar_indexes)): 
        if range_cfar_indexes[i] != 0:
            doppler_mag = RD_img[i]
            doppler_threshold, doppler_cfar_indexes = oneDCFAR(doppler_mag, ref_win=5, guard_win=3, \
                                    scalar=1.15, order_statistic=0.6, mode="OS-CFAR")
            ################### plot to see doppler cfar #####################
            # if fig is not None:
                # clearAxes(axes)
                # axes[0].plot(doppler_mag, 'r')
                # axes[0].plot(doppler_threshold, 'b')
                # axes[0].set_title("the %s -th range axies" % str(i))
                # keepDrawing(fig, 1)
            AD_mask = []
            for j in range(len(doppler_cfar_indexes)):
                if doppler_cfar_indexes[j] != 0:
                    azimuth_mag = radar_RAD_mag[i, :, j]
                    azimuth_threshold, azimuth_cfar_indexes = oneDCFAR(azimuth_mag, ref_win=80, \
                                    guard_win=5, scalar=1.25, order_statistic=0.8, mode="CA-CFAR")
                    ################### plot to see doppler cfar #####################
                    # if fig is not None:
                        # clearAxes(axes)
                        # axes[0].plot(azimuth_mag, 'r')
                        # axes[0].plot(azimuth_threshold, 'b')
                        # axes[0].set_title("the %s -th range and %s -th doppler axies" %(str(i), str(j)))
                        # keepDrawing(fig, 0.5)
                    azimuth_mask = np.expand_dims(np.expand_dims(azimuth_cfar_indexes, -1), 0)
                else:
                    azimuth_mask = np.zeros([1, radar_RAD_mag.shape[1], 1])
                AD_mask.append(azimuth_mask)
            AD_mask = np.concatenate(AD_mask, -1)
        else:
            AD_mask = np.zeros([1, radar_RAD_mag.shape[1], radar_RAD_mag.shape[-1]])
        RAD_mask.append(AD_mask)
    RAD_mask = np.concatenate(RAD_mask, 0)
    return RAD_mask

# def separateRD_A(radar_RAD_mag, frame_id, fig = None, axes = None, moving_only=False):
    # """
    # Function:
        # Apply 2D CFAR on Range-Doppler, then go for azimuth.
    
    # Args:
        # radar_RAD_mag           ->          radar RAD magnitude
    # """
    # # setting parameters for CFAR

    # # range_win = 10 # big 20/256 for large range, lower down if necessary
    # # doppler_win = 5 # big 10/64 for large doppler, lower down if necessary
    # # range_guard = 6
    # # doppler_guard = 3
    # # # s = 300 # 1.23 ish for strict masks
    # # s = 10 # 1.23 ish for strict masks
    # # # s = 1.15 # 1.23 ish for strict masks
    # # os = 0.75 # 0.9 ish for strict masks

    # # range_win = 10 # big 20/256 for large range, lower down if necessary
    # # doppler_win = 5 # big 10/64 for large doppler, lower down if necessary
    # # range_guard = 5
    # # doppler_guard = 3
    # # # s = 3 # 1.23 ish for strict masks
    # # s = 20 # 1.23 ish for strict masks
    # # os = 0.65 # 0.9 ish for strict masks

    # range_win = 15 # big 20/256 for large range, lower down if necessary
    # doppler_win = 10 # big 10/64 for large doppler, lower down if necessary
    # range_guard = 8
    # doppler_guard = 5
    # # s = 3 # 1.23 ish for strict masks
    # s = 20 # 1.23 ish for strict masks
    # os = 0.75 # 0.9 ish for strict masks

    # # range_win = 8 # big 20/256 for large range, lower down if necessary
    # # doppler_win = 4 # big 10/64 for large doppler, lower down if necessary
    # # range_guard = 4
    # # doppler_guard = 2
    # # # s = 7 # 1.23 ish for strict masks
    # # s = 10 # 1.23 ish for strict masks
    # # os = 0.65 # 0.9 ish for strict masks
    # # # os = 0.50 # 0.9 ish for strict masks

    # RD_original = getLog(getSumDim(radar_RAD_mag, 1), scalar=10, log_10=True)

    # # RD_img = getLog(getSumDim(radar_RAD_mag, 1), scalar=10, log_10=True)
    # RD_img = getLog(getSumDim(radar_RAD_mag, 1), scalar=10, log_10=False)
    
    # # for moving objects
    # RD_moving = RD_img.copy()
    # RD_moving[..., int(RD_img.shape[1]/2)] = 0
    # RD_static = RD_img[..., int(RD_img.shape[1]/2)]
    # RD_threshold, RD_mask = twoDCFAR(RD_moving, ref_win=(range_win, doppler_win), \
                # guard_win=(range_guard, doppler_guard), scalar=s, os=os, mode="OS-CFAR")
    # # for static objects
    # if not moving_only:
        # static_threshold, static_index = oneDCFAR(RD_static, ref_win=range_win,  \
                    # guard_win=range_guard, scalar=s, order_statistic=os, mode="OS-CFAR")
        # static_mask = np.zeros(RD_img.shape)
        # static_mask[..., int(static_mask.shape[1]/2)] = static_index
        # RD_mask = RD_mask + static_mask 
    # ################ plot to see range cfar ##################
    # # if fig is not None:
        # # clearAxes(axes)
        # # axes[0].plot(RD_static, 'r')
        # # axes[0].plot(static_threshold, 'b')
        # # keepDrawing(fig, 0.1)
    # ################ plot to see RD cfar ##################
    # # RD_post_mask = postRD(RD_mask, mode="dilated+morpho")
    # RD_original_mask = RD_mask.copy()
    # RD_mask = postRD(RD_mask, mode="morpho")

    # ###############################################################
    # # range_static_para = 0.50
    # # R_nums = []
    # # for dplr_i in range(RD_mask.shape[1]):
        # # R_mask = RD_mask[:, dplr_i]
        # # R_nums.append(len(R_mask[R_mask > 0]))
    # # i_max = np.argmax(R_nums)
    # # if R_nums[i_max] >= RANGE_SIZE * range_static_para:
        # # RD_moving[..., i_max] = 0
    # # RD_threshold, RD_mask = twoDCFAR(RD_moving, ref_win=(range_win, doppler_win), \
                # # guard_win=(range_guard, doppler_guard), scalar=s, os=os, mode="OS-CFAR")
    # # RD_mask = postRD(RD_mask, mode="morpho")
    # ###############################################################

    # # if fig is not None:
    # # clearAxes(axes)
    # # imgPlot(RD_original, axes[0], "viridis", 1, "Range-Doppler")
    # # imgPlot(RD_original_mask, axes[1], "viridis", 1, "original")
    # # imgPlot(RD_mask, axes[2], "viridis", 1, "filtered")
    # # saveFigure("./radar_RAD_mask/", "mask_ext_", frame_id)
    # clearAxes(axes)
    # # keepDrawing(fig, 0.1)
    # RAD_mask = []
    # for i in range(RD_img.shape[0]):
        # AD_mask = []
        # for j in range(RD_img.shape[1]):
            # if RD_mask[i,j] != 0:
                # azimuth_FFT_mag = getLog(radar_RAD_mag[i,:,j], scalar=10, log_10=False)
                # azimuth_threshold, azimuth_cfar_indexes = oneDCFAR(azimuth_FFT_mag, ref_win=150, \
                            # guard_win=5, scalar=3, order_statistic=0.65, mode="CA-CFAR")
                # azimuth_cfar_indexes = azimuthPeaks(azimuth_FFT_mag, azimuth_cfar_indexes, \
                                                    # peak_cell_num = 1)
                # azimuth_mask = np.expand_dims(np.expand_dims(azimuth_cfar_indexes, -1), 0)
                # ################### plot to see doppler cfar #####################
                # # if fig is not None:
                    # # clearAxes(axes)
                    # # # axes[0].plot(radar_RAD_mag[i,:,j], 'r')
                    # # axes[0].plot(azimuth_FFT_mag, 'r')
                    # # axes[0].plot(azimuth_threshold, 'b')
                    # # axes[0].set_title("the %s -th range and %s -th doppler axies" %(str(i), str(j)))
                    # # keepDrawing(fig, 0.5)
            # else:
                # azimuth_mask = np.zeros([1, radar_RAD_mag.shape[1], 1])
            # AD_mask.append(azimuth_mask)
        # AD_mask = np.concatenate(AD_mask, -1)
        # RAD_mask.append(AD_mask)
    # RAD_mask = np.concatenate(RAD_mask, 0)
    
    # ####################### GLOBAL MAGNITUDE FILTER ########################
    # # filter_order = 0.7
    # # masked_mag = radar_RAD_mag * RAD_mask
    # # has_mask_mag = masked_mag[masked_mag>0]
    # # filtered_number = int((1. - filter_order) * len(has_mask_mag))
    # # has_mask_mag = np.sort(has_mask_mag)
    # # filter_threshold = has_mask_mag[filtered_number]
    # # RAD_mask = np.where(masked_mag > filter_threshold, 1., 0.)
    # ####################### GLOBAL MAGNITUDE FILTER ########################
    # return RAD_mask

def separateRD_A(radar_RAD_mag, frame_id, fig = None, axes = None, moving_only=False):
    """
    Function:
        Apply 2D CFAR on Range-Doppler, then go for azimuth.
    
    Args:
        radar_RAD_mag           ->          radar RAD magnitude
    """
    # setting parameters for CFAR

    # range_win = 15 # big 20/256 for large range, lower down if necessary
    # doppler_win = 10 # big 10/64 for large doppler, lower down if necessary
    # range_guard = 4
    # doppler_guard = 2
    # s = 5 # 1.23 ish for strict masks
    # # s = 30 # 1.23 ish for strict masks
    # os = 0.75 # 0.9 ish for strict masks

    range_win = 8 # big 20/256 for large range, lower down if necessary
    doppler_win = 4 # big 10/64 for large doppler, lower down if necessary
    range_guard = 4
    doppler_guard = 2
    s = 3 # 1.23 ish for strict masks
    # s = 4 # 1.23 ish for strict masks
    os = 0.65 # 0.9 ish for strict masks

    RD_original = getLog(getSumDim(radar_RAD_mag, 1), scalar=10, log_10=True)
    RD_img = getLog(getSumDim(radar_RAD_mag, 1), scalar=10, log_10=False)
    RD_threshold, RD_mask = twoDCFAR(RD_img, ref_win=(range_win, doppler_win), \
                guard_win=(range_guard, doppler_guard), scalar=s, os=os, mode="OS-CFAR")

    ##################### TODO: moving static separation #######################
    RD_mask_initial = RD_mask.copy()
    RD_moving = RD_img.copy()
    range_static_para = 0.60
    R_nums = []
    RD_static = []
    for dplr_i in range(RD_mask.shape[1]):
        R_mask = RD_mask[:, dplr_i]
        R_nums.append(len(R_mask[R_mask > 0]))
    i_max = np.argmax(R_nums)

    if R_nums[i_max] >= (range_static_para * RD_mask.shape[0]):
        ####### separate moving static with neighbour cells #######
        neighbour = 0
        for i_potential in range(i_max-neighbour, i_max+neighbour+1):
            if i_potential < 0 or i_potential > RD_mask.shape[1]:
                continue
            # RD_moving[..., i_potential] = 0
            RD_moving[..., i_potential] = np.sort(RD_moving)[..., int(0.5*RD_moving.shape[1])]
            # RD_moving[..., i_potential] = (RD_moving[..., i_max-neighbour-1] + \
                                        # RD_moving[..., i_max+neighbour+1]) / 2
            RD_static.append([RD_img[..., i_potential], i_potential])
        RD_threshold, RD_mask = twoDCFAR(RD_moving, ref_win=(range_win, doppler_win), \
                    guard_win=(range_guard, doppler_guard), scalar=s, os=os, mode="OS-CFAR")
    #############################################################################

    RD_mask = postRD(RD_mask, mode="morpho")

    ################# 2nd part: static objects masking #########################
    RD_mask_initial = RD_mask.copy()
    if len(RD_static) > 0:
        for stat_i in range(len(RD_static)):
            RD_static_i = RD_static[stat_i][0]
            RD_static_i = getLog(RD_static_i, scalar=10, log_10=True)
            RD_static_dpl_ind = RD_static[stat_i][1]
            RD_static_thre, RD_static_mask = oneDCFAR(RD_static_i, ref_win=20, \
                    guard_win=5, scalar=1.00, order_statistic=0.60, mode="OS-CFAR")
            RD_mask[..., RD_static_dpl_ind] += RD_static_mask
            ### plot for static ###
            if fig is not None:
                masked_signal = RD_static_i*RD_static_mask
                clearAxes(axes)
                axes[0].plot(RD_static_i, 'r')
                axes[0].plot(RD_static_thre, 'b')
                axes[0].plot(np.where(RD_static_mask>0)[0], masked_signal[masked_signal>0], "x")
                keepDrawing(fig, 1)
        RD_mask = np.where(RD_mask > 0, 1., 0.)
    ##################################################################################

    # if fig is not None:
        # clearAxes(axes)
        # imgPlot(RD_original, axes[0], "viridis", 1, "Range-Doppler")
        # imgPlot(RD_mask_initial, axes[1], "viridis", 1, "original")
        # imgPlot(RD_mask, axes[2], "viridis", 1, "filtered")
        # saveFigure("./origin/", "mask_ext_", frame_id)
        # # keepDrawing(fig, 0.1)

    RAD_mask = []
    for i in range(RD_img.shape[0]):
        AD_mask = []
        for j in range(RD_img.shape[1]):
            if RD_mask[i,j] != 0:
                azimuth_FFT_mag = getLog(radar_RAD_mag[i,:,j], scalar=10, log_10=False)
                # azimuth_threshold = 1.5 * np.sort(azimuth_FFT_mag)[int(0.7 *len(azimuth_FFT_mag))]
                # azimuth_cfar_indexes = np.where(azimuth_FFT_mag > azimuth_threshold, 1., 0.)
                # azimuth_threshold = np.ones(azimuth_FFT_mag.shape) * azimuth_threshold
                azimuth_threshold, azimuth_cfar_indexes = oneDCFAR(azimuth_FFT_mag, ref_win=123, \
                            guard_win=5, scalar=4, order_statistic=0.65, mode="CA-CFAR")
                azimuth_cfar_peaks = azimuthPeaks(azimuth_FFT_mag, azimuth_cfar_indexes, \
                                                    # peak_cell_num = None)
                                                    peak_cell_num = 1)
                peaks_test = np.where(azimuth_cfar_peaks > 0)[0]
                azimuth_mask = np.expand_dims(np.expand_dims(azimuth_cfar_peaks, -1), 0)
                ################### plot to see doppler cfar #####################
                # if fig is not None:
                    # clearAxes(axes)
                    # # axes[0].plot(radar_RAD_mag[i,:,j], 'r')
                    # axes[0].plot(azimuth_FFT_mag, 'r')
                    # axes[0].plot(azimuth_threshold, 'b')
                    # axes[0].plot(peaks_test, azimuth_FFT_mag[peaks_test], 'x')
                    # axes[0].set_title("the %s -th range and %s -th doppler axies" %(str(i), str(j)))
                    # keepDrawing(fig, 1)
            else:
                azimuth_mask = np.zeros([1, radar_RAD_mag.shape[1], 1])
            AD_mask.append(azimuth_mask)
        AD_mask = np.concatenate(AD_mask, -1)
        RAD_mask.append(AD_mask)
    RAD_mask = np.concatenate(RAD_mask, 0)
    
    return RAD_mask


def InstancizeRDMask(RD_mask):
    """
    Function:
        Separate RAD mask w.r.t each instance.

    Args:
        radar_RAD_mask          ->          RAD mask
    """
    indexes_clutters = []
    for i in range(RD_mask.shape[0]):
        for j in range(RD_mask.shape[1]):
            if RD_mask[i,j] != 0:
                indexes_clutters.append(np.array([i,j]))
    indexes_clutters = np.array(indexes_clutters)
    if len(indexes_clutters) != 0:
        indexes_instances = DbscanDenoise(indexes_clutters, epsilon=5, minimum_samples=3)
        instances_masks = []
        for instance_i in range(len(indexes_instances)):
            current_instance = indexes_instances[instance_i]
            instance_mask = np.zeros(RD_mask.shape)
            for pnt_i in range(len(current_instance)):
                pnt_ind = current_instance[pnt_i]
                instance_mask[pnt_ind[0], pnt_ind[1]] = 1.
            instances_masks.append([instance_mask, current_instance])
    else:
        instances_masks = []
    return instances_masks

def InstancizeRADMask(radar_RAD_mask):
    """
    Function:
        Separate RAD mask w.r.t each instance.

    Args:
        radar_RAD_mask          ->          RAD mask
    """
    indexes_clutters = []
    for i in range(radar_RAD_mask.shape[0]):
        for j in range(radar_RAD_mask.shape[1]):
            for k in range(radar_RAD_mask.shape[2]):
                if radar_RAD_mask[i,j,k] != 0:
                    indexes_clutters.append(np.array([i,j,k]))
    indexes_clutters = np.array(indexes_clutters)
    indexes_instances = DbscanDenoise(indexes_clutters, epsilon=8, minimum_samples=2)
    instances_masks = []
    for instance_i in range(len(indexes_instances)):
        current_instance = indexes_instances[instance_i]
        ############# TODO: use RA or Cartesian for second denoise ###########
        instance_RA = current_instance[:, :2]
        point_range = ((RANGE_SIZE-1) - instance_RA[:, 0]) * RANGE_RESOLUTION
        point_angle = (instance_RA[:, 1] * (2*math.pi/AZIMUTH_SIZE) - math.pi) / \
                        (2*math.pi*0.5*RADAR_CONFIG_FREQ/DESIGNED_FREQ)
        point_angle = np.arcsin(point_angle)
        point_zx = polarToCartesian(point_range, point_angle)
        instance_RA_cart = np.concatenate([np.expand_dims(point_zx[1],-1), \
                            np.expand_dims(point_zx[0],-1)], -1)
        instance_cart, instance_idx = DbscanDenoise(instance_RA_cart, epsilon=3, \
                                        minimum_samples=3, dominant_op=False, return_idx=True)
        if len(instance_idx) == 0:
            continue
        target_instance = []
        for i in range(len(instance_idx)):
            current_ist_idx = instance_idx[i]
            tmp_instance = current_instance[current_ist_idx]
            target_instance.append(tmp_instance)
        if len(target_instance) == 0:
            continue
        current_instance = np.concatenate(target_instance, axis=0)
        # current_instance = current_instance[instance_idx]
        ######################################################################
        instance_mask = np.zeros(radar_RAD_mask.shape)
        for pnt_i in range(len(current_instance)):
            pnt_ind = current_instance[pnt_i]
            instance_mask[pnt_ind[0], pnt_ind[1], pnt_ind[2]] = 1.
        instances_masks.append([instance_mask, current_instance])
    return instances_masks

def transfer2Scatter(RA_mask):
    """
    Function:
        Transfer RD indexes to pcl, for verifying quality

    Args:
        RA_mask              ->          Range-Azimuth mask
    """
    # Radar Configuration
    # RANGE_RESOLUTION = 0.1953125 # m
    VELOCITY_RESOLUTION = 0.41968030701528203 # m/s
    RANGE_SIZE = RA_mask.shape[0]
    RANGE_RESOLUTION = 50 / RANGE_SIZE
    AZIMUTH_SIZE = RA_mask.shape[1]
    ################# problem: why it is so weird when remove 2 ###############
    ANGULAR_RESOLUTION = 0.5 * np.pi / AZIMUTH_SIZE # radians

    output_pcl = []
    for i in range(RA_mask.shape[0]):
        for j in range(RA_mask.shape[1]):
            if RA_mask[i, j] == 1:
                point_range = ((RANGE_SIZE-1) - i) * RANGE_RESOLUTION
                point_angle = (j - (AZIMUTH_SIZE/2)) * ANGULAR_RESOLUTION
                point_zx = polarToCartesian(point_range, point_angle)
                output_pcl.append(np.array([[point_zx[1], point_zx[0]]]))
    output_pcl = np.concatenate(output_pcl, axis=0)
    return output_pcl

def generateRACartesianImage(RA_img, RA_mask):
    """
    Function:
        Generate RA image in Cartesian Coordinate.

    Args:
        RA_img          ->          RA FFT magnitude
        RA_mask         ->          Mask generated 
    """
    # Radar Configuration
    # RANGE_RESOLUTION = 0.1953125 # m
    VELOCITY_RESOLUTION = 0.41968030701528203 # m/s
    RANGE_SIZE = RA_mask.shape[0]
    RANGE_RESOLUTION = 50 / RANGE_SIZE
    AZIMUTH_SIZE = RA_mask.shape[1]
    RADAR_CONFIG_FREQ = 77 # GHz
    DESIGNED_FREQ = 76.8 # GHz
    ################# problem: why it is so weird when remove 2 ###############
    ANGULAR_RESOLUTION = 0.5 * np.pi / AZIMUTH_SIZE # radians

    output_img = np.zeros([RA_img.shape[0], RA_img.shape[0]*2])
    for i in range(RA_img.shape[0]):
        for j in range(RA_img.shape[1]):
            if RA_mask[i,j] == 1:
                point_range = ((RANGE_SIZE-1) - i) * RANGE_RESOLUTION
                # point_angle = (j - (AZIMUTH_SIZE/2)) * ANGULAR_RESOLUTION
                ####################### Prince's method ######################
                point_angle = (j * (2*math.pi/AZIMUTH_SIZE) - math.pi) / \
                                (2*math.pi*0.5*RADAR_CONFIG_FREQ/DESIGNED_FREQ)
                point_angle = math.asin(point_angle)
                ##############################################################
                point_zx = polarToCartesian(point_range, point_angle)
                new_i = int(output_img.shape[0] - \
                        np.round(point_zx[0]/RANGE_RESOLUTION)-1)
                new_j = int(np.round((point_zx[1]+50)/RANGE_RESOLUTION)-1)
                output_img[new_i,new_j] = RA_img[i,j] 
    norm_sig = plt.Normalize()
    # color mapping 
    output_img = plt.cm.viridis(norm_sig(output_img))
    output_img = output_img[..., :3]
    return output_img

def DbscanDenoise(pcl, epsilon=0.3, minimum_samples=100, dominant_op=False, return_idx=False):
    """
    Function:
        Using DBSCAN for filtering out the noise data.

    Args:
        pcl             ->          point cloud to be denoised
        epsilon         ->          maximum distance to be considered as an object
        minimum_samples ->          miminum points to be considered as an object
    """
    clustering = DBSCAN(eps=epsilon, min_samples=minimum_samples).fit(pcl)
    output_labels = clustering.labels_
    if not dominant_op:
        output_pcl = []
        output_idx = []
        for label_i in np.unique(output_labels):
            if label_i == -1:
                continue
            output_pcl.append(pcl[output_labels == label_i])
            output_idx.append(np.where(output_labels == label_i)[0])
        if not return_idx:
            return output_pcl
        else:
            return output_pcl, output_idx
    else:
        if len(np.unique(output_labels)) == 1 and np.unique(output_labels)[0] == -1:
            output_pcl = np.zeros([0,2])
            output_idx = []
        else:
            counts = Counter(output_labels)
            output_pcl = pcl[output_labels == counts.most_common(1)[0][0]]
            output_idx = np.where(output_labels == counts.most_common(1)[0][0])[0]
        if not return_idx:
            return output_pcl
        else:
            return output_pcl, output_idx  

def postRD(RD_mask, mode):
    """
    Function:
        RD mask post processing.
    
    Args:
        RD_mask             ->          RD mask derived from 2D cfar
        mode                ->          3 options: "dilated", "morpho", "polygon"
    """
    RD_original = RD_mask.copy()
    RD_instances = InstancizeRDMask(RD_mask)
    if len(RD_instances) == 0:
        output = np.zeros(RD_mask.shape)
    else:
        output = np.zeros(RD_mask.shape)
        for instance_i in range(len(RD_instances)):
            current_instance = RD_instances[instance_i][0]
            RD_cv_try = current_instance.copy()*255.
            RD_cv_try = RD_cv_try.astype(np.uint8)
            if mode == "dilated" or mode == "polygon" or mode == "dilated+morpho":
                ret, thresh = cv2.threshold(RD_cv_try, 0, 255, cv2.THRESH_BINARY)
                cnts, hierarchy = cv2.findContours(thresh, cv2.RETR_EXTERNAL, \
                                    cv2.CHAIN_APPROX_SIMPLE)
                if mode == "dilated":
                    kernel = np.ones((2,2), np.uint8)
                    dilated = cv2.dilate(thresh, kernel, iterations=1)
                    current_mask = np.where(dilated > 0, 1., 0.)
                elif mode == "polygon":
                    empty_img = np.zeros(RD_cv_try.shape, np.uint8)
                    for i in range(len(cnts)):
                        epsilon = 0.00001 * cv2.arcLength(cnts[i], True)
                        approx = cv2.approxPolyDP(cnts[i], epsilon, True)
                        cv2.drawContours(empty_img, [approx], 0, (255,255,255), 3)
                    current_mask = np.where(empty_img > 0, 1., 0.)
                elif mode == "dilated+morpho":
                    # kernel = np.ones((2,2), np.uint8)
                    kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (3, 3))
                    dilated = cv2.dilate(thresh, kernel, iterations=1)
                    # rect_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
                    rect_kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (3, 3))
                    di_mo = cv2.morphologyEx(dilated, cv2.MORPH_CLOSE, rect_kernel)
                    current_mask = np.where(di_mo > 0, 1., 0.)
            elif mode == "morpho":
                # rect_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 5))
                rect_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
                # rect_kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (1, 1))
                threshed = cv2.morphologyEx(RD_cv_try, cv2.MORPH_CLOSE, rect_kernel)
                current_mask = np.where(threshed > 0, 1., 0.)
            else:
                raise ValueError("Mode not recognized.")
            
            output += current_mask
        output = np.where(output > 0, 1., 0.)
    return output

##################### coordinate transformation ######################
def cartesianToPolar(x, y):
    rho = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    return(rho, phi)

def polarToCartesian(rho, phi):
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return(x, y)

def norm2Image(array):
    norm_sig = plt.Normalize()
    img = plt.cm.viridis(norm_sig(array))
    img *= 255.
    img = img.astype(np.uint8)
    return img

def applyMask(image, mask, color, alpha=0.5):
    """
    Apply the given mask to the image.
    """
    for c in range(3):
        image[:, :, c] = np.where(mask == 1,
                                 image[:, :, c] *
                                (1 - alpha) + alpha * color[c] * 255,
                                 image[:, :, c])
    return image

###################### PLOTTING FUNCTIONS START #########################
def prepareFigure(num_axes, figsize=None):
    assert num_axes <= 4
    if figsize is not None:
        fig = plt.figure(figsize=figsize)
    else:
        fig = plt.figure()
    if num_axes == 1:
        ax1 = fig.add_subplot(111)
        return fig, [ax1]
    if num_axes == 2: 
        ax1 = fig.add_subplot(121)
        ax2 = fig.add_subplot(122)
        return fig, [ax1, ax2]
    if num_axes == 3:
        ax1 = fig.add_subplot(131)
        ax2 = fig.add_subplot(132)
        ax3 = fig.add_subplot(133)
        return fig, [ax1, ax2, ax3]
    if num_axes == 4:
        ax1 = fig.add_subplot(221)
        ax2 = fig.add_subplot(222)
        ax3 = fig.add_subplot(223)
        ax4 = fig.add_subplot(224)
        return fig, [ax1, ax2, ax3, ax4]

def clearAxes(ax_list):
    assert len(ax_list) >=1 
    plt.cla()
    for ax_i in ax_list:
        ax_i.clear()

def imgPlot(img, ax, cmap, alpha, title=None):
    ax.imshow(img, cmap=cmap, alpha=alpha)
    ax.axis('off')
    if title is not None:
        ax.set_title(title)

def pclScatter(pcl, color, label, ax, xlimits, ylimits, title):
    ax.scatter(pcl[:, 0], pcl[:, 1], s=1, c=color, label=label)
    ax.set_xlim(xlimits)
    ax.set_ylim(ylimits)
    ax.set_title(title)

def keepDrawing(fig, time_duration):
    fig.canvas.draw()
    plt.pause(time_duration)

def saveFigure(save_dir, name_prefix, frame_i):
    plt.savefig(save_dir + name_prefix + str(frame_i) + ".png")
###################### PLOTTING FUNCTIONS END #########################
 
def main(sequences, time_stamp, save_mask=True, plot=True, save_fig=False):
    """
    MAIN FUNCTION
    """
    radar_dir = "/DATA/" + time_stamp + "/ral_outputs_" + time_stamp + "/RAD_numpy/"
    stereo_imgs_dir = "../../stereo_process/left/"
    save_dir = "./radar_RAD_mask/"
    name_prefix = "RDAoscfar_" 
    mask_prefix = "RAD_mask_"
    frame_valid_filename = "/DATA/" + time_stamp + "/adc_valid_" + time_stamp + ".txt"
    frame_valid_info = readValidFrame(frame_valid_filename)
    # checkoutDir(save_dir)

    if plot:
        fig, axes = prepareFigure(4, figsize=(20,10))
        # fig, axes = prepareFigure(2)
    else:
        # fig, axes = prepareFigure(3)
        fig, axes = prepareFigure(1)

    for frame_id in tqdm(range(sequences[0], sequences[1])):
        # filter out corrupted frames
        if frame_valid_info[frame_id] is False:
            continue

        radar_RAD = readRAD(radar_dir, frame_id)
        if radar_RAD is None:
            continue
        radar_RAD_mag = getMagnitude(radar_RAD, power_order=2)
        
        # pre signal processing for getting object information.j
        # radar_RAD_mag = getLog(radar_RAD_mag, scalar=10, log_10=True)

        #------------------ TRY DIFFERENT FILTER ALGORITHMS FOR MASKING ----------------
        #################### NOTE: use this when only moving objects ########################
        # radar_RAD_mag = averageFilter(radar_RAD_mag, bg_val=1., alpha=2.3)
        #################### TODO: Tune all the parameters ########################
        # radar_RAD_mag = threeDCFAR(radar_RAD_mag, ref_win=(4,4,2), guard_win=(2,2,2), 
                                    # order_statistic=0.8, OS_scalar=2, 
                                    # CA_scalar=1, mode="OS-CFAR")
        #################### NOTE: Separate R-D-A ####################
        # if plot:
            # RAD_mask = separateR_D_A(radar_RAD_mag)
        # else:
        #     RAD_mask = separateR_D_A(radar_RAD_mag, fig, axes)
        #################### TODO: Separate DR-A  ####################
        if plot:
            RAD_mask = separateRD_A(radar_RAD_mag, frame_id, moving_only=True)
        else:
            RAD_mask = separateRD_A(radar_RAD_mag, frame_id, fig, axes, moving_only=True)
        #------------------------------------------------------------------------------
        if len(np.where(RAD_mask != 0)[0]) == 0:
            continue

        # #################### TODO:Instancize RAD for visualization ################
        # radar_mask_instances = InstancizeRADMask(RAD_mask)
        # RAD_mask = np.zeros(RAD_mask.shape)
        # for inst_i in range(len(radar_mask_instances)):
            # mask = radar_mask_instances[inst_i][0]
            # RAD_mask += mask
        # RAD_mask = np.where(RAD_mask > 0, 1., 0.)
        # ##########################################################################

        RD_mask = np.where(getSumDim(RAD_mask, 1) >= 1, 1, 0)
        RA_mask = np.where(getSumDim(RAD_mask, -1) >= 1, 1, 0)
        RD_img = getLog(getSumDim(radar_RAD_mag, 1), scalar=10, log_10=True)
        RA_img = getLog(getSumDim(radar_RAD_mag, -1), scalar=10, log_10=True)
        stereo_img = readCam(stereo_imgs_dir, frame_id)

        # if RA_mask.max() == 0.:
            # continue

        # RA_cartesian = transfer2Scatter(RA_mask)
        RA_cart_img = generateRACartesianImage(RA_img, RA_mask)
        
        if save_mask:
            np.save(save_dir + mask_prefix + "%.d.npy"%(frame_id), RAD_mask)
        if plot:
            RD_img = norm2Image(RD_img)
            RA_img = norm2Image(RA_img)
            applyMask(RD_img, RD_mask, [1., 0., 0.])
            applyMask(RA_img, RA_mask, [1., 0., 0.])
            clearAxes(axes)
            # imgPlot(RD_img, axes[0], "viridis", 1, "Range-Doppler")
            # imgPlot(RD_mask, axes[0], "jet", 0.5)
            # imgPlot(RA_img, axes[1], "viridis", 1,  "Range-Azimuth")
            # imgPlot(RA_mask, axes[1], "jet", 0.5)
            imgPlot(RD_img, axes[0], None, 1, "Range-Doppler")
            imgPlot(RA_img, axes[1], None, 1, "Range-Doppler")
            imgPlot(stereo_img, axes[2], None, 1, "Camera")
            # pclScatter(RA_cartesian, "b", "RA_points", axes[3], [-25, 25], \
                        # [0,50], "RA_pcl")
            imgPlot(RA_cart_img, axes[3], None, 1, "RA_Cartesian")
            # keepDrawing(fig, 0.1)
            if save_fig:
                saveFigure(save_dir, name_prefix, frame_id)
 
def main_for_mp(frame_id, fig, axes, radar_dir, stereo_imgs_dir, save_dir, \
                name_prefix, mask_prefix, frame_valid_info, save_mask=True, \
                plot=True, save_fig=False):
    """ FOR multi processing """
    go_on = True
    if frame_valid_info[frame_id] is False:
        go_on = False
    
    if go_on:
        radar_RAD = readRAD(radar_dir, frame_id)
        if radar_RAD is not None:
            radar_RAD_mag = getMagnitude(radar_RAD, power_order=2)
            
            #################### TODO: Separate DR-A  ####################
            if plot:
                RAD_mask = separateRD_A(radar_RAD_mag, frame_id, moving_only=True)
            else:
                RAD_mask = separateRD_A(radar_RAD_mag, frame_id, fig, axes, moving_only=True)
            #------------------------------------------------------------------------------
            if len(np.where(RAD_mask != 0)[0]) == 0:
                go_on = False
            
            if go_on:
                #################### TODO:Instancize RAD for visualization ################
                radar_mask_instances = InstancizeRADMask(RAD_mask)
                RAD_mask = np.zeros(RAD_mask.shape)
                for inst_i in range(len(radar_mask_instances)):
                    mask = radar_mask_instances[inst_i][0]
                    RAD_mask += mask
                RAD_mask = np.where(RAD_mask > 0, 1., 0.)
                ##########################################################################

                RD_mask = np.where(getSumDim(RAD_mask, 1) >= 1, 1, 0)
                RA_mask = np.where(getSumDim(RAD_mask, -1) >= 1, 1, 0)
                RD_img = getLog(getSumDim(radar_RAD_mag, 1), scalar=10, log_10=True)
                RA_img = getLog(getSumDim(radar_RAD_mag, -1), scalar=10, log_10=True)
                stereo_img = readCam(stereo_imgs_dir, frame_id)

                RA_cart_img = generateRACartesianImage(RA_img, RA_mask)
                
                if save_mask:
                    np.save(save_dir + mask_prefix + "%.d.npy"%(frame_id), RAD_mask)
                if plot:
                    RD_img = norm2Image(RD_img)
                    RA_img = norm2Image(RA_img)
                    applyMask(RD_img, RD_mask, [1., 0., 0.])
                    applyMask(RA_img, RA_mask, [1., 0., 0.])
                    clearAxes(axes)
                    # imgPlot(RD_img, axes[0], "viridis", 1, "Range-Doppler")
                    # imgPlot(RD_mask, axes[0], "jet", 0.5)
                    # imgPlot(RA_img, axes[1], "viridis", 1,  "Range-Azimuth")
                    # imgPlot(RA_mask, axes[1], "jet", 0.5)
                    imgPlot(RD_img, axes[0], None, 1, "Range-Doppler")
                    imgPlot(RA_img, axes[1], None, 1, "Range-Doppler")
                    imgPlot(stereo_img, axes[2], None, 1, "Camera")
                    # pclScatter(RA_cartesian, "b", "RA_points", axes[3], [-25, 25], \
                                # [0,50], "RA_pcl")
                    imgPlot(RA_cart_img, axes[3], None, 1, "RA_Cartesian")
                    # keepDrawing(fig, 0.1)
                    if save_fig:
                        saveFigure(save_dir, name_prefix, frame_id)

def main_mp(sequences, time_stamp, save_mask, plot, save_fig):
    """ multiprocessing """
    radar_dir = "/DATA/" + time_stamp + "/ral_outputs_" + time_stamp + "/RAD_numpy/"
    stereo_imgs_dir = "../../stereo_process/left/"
    save_dir = "./radar_RAD_mask/"
    name_prefix = "RDAoscfar_" 
    mask_prefix = "RAD_mask_"
    frame_valid_filename = "/DATA/" + time_stamp + "/adc_valid_" + time_stamp + ".txt"
    frame_valid_info = readValidFrame(frame_valid_filename)
    checkoutDir(save_dir)

    pool = mp.Pool(6)

    if plot:
        fig, axes = prepareFigure(4, figsize=(20,10))
        # fig, axes = prepareFigure(2)
    else:
        fig, axes = prepareFigure(3)

    main_1arg = partial(main_for_mp,
                        fig = fig,
                        axes = axes,
                        radar_dir = radar_dir,
                        stereo_imgs_dir = stereo_imgs_dir,
                        save_dir = save_dir,
                        name_prefix = name_prefix,
                        mask_prefix = mask_prefix,
                        frame_valid_info = frame_valid_info,
                        save_mask = save_mask,
                        plot = plot,
                        save_fig = save_fig)
    
    for _ in tqdm(pool.imap_unordered(main_1arg, \
                    range(sequences[0], sequences[1])), \
                    total = sequences[1] - sequences[0]):
        pass
    pool.close()
    pool.join()
    pool.close()
    

if __name__ == "__main__":
    sequences = [0, 3000]
    time_stamp = "2020-09-03-12-30-11"
    # main(sequences, time_stamp, save_mask = True, plot = True, save_fig = True)
    main_mp(sequences, time_stamp, save_mask = True, plot = True, save_fig = True)
