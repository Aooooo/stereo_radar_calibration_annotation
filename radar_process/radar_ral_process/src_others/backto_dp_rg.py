import numpy as np
import cv2
import matplotlib.pyplot as plt
from tqdm import tqdm
import time
import os

def ReadNpy(npy_dir, i):
    """
    Function:
        Read the saved radar pcl according to wanted sequence.

    Args:
        npy_dir             ->          directory for saving radar pcl
        i                   ->          current sequence number
    """
    file_name = npy_dir + "radar_pcl_" + str(i) + ".npy"
    if os.path.exists(file_name):
        radar_pcl = np.load(file_name)
    else:
        radar_pcl = None
    return radar_pcl

def ReadDpRg(dp_rg_dir, i):
    """
    Function:
        Read the doppler range png according to the wanted sequence.

    Args:
        dp_rg_dir           ->          directory for saving doppler range info
        i                   ->          current sequence number
    """
    file_name = dp_rg_dir + "%.6d.png"%(i) 
    dp_rg_img = cv2.imread(file_name)
    return dp_rg_img

def DopplerIndRangeInd(radar_pcl, doppler_axis):
    """
    Function:
        Find the doppler and range index of each point in the radar pcl

    Args:
        radar_pcl               ->          radar point cloud of current frame
        doppler_axis            ->          axis of doppler ind to be removed 
                                            (0: doppleInd_org, 1: dopplerInd)
    """
    output = np.delete(radar_pcl[..., 4:7], doppler_axis, -1) 
    output = np.unique(output, axis = 0)
    return output

def applyMask(image, mask, color, alpha=0.5):
    """
    Apply the given mask to the image.
    """
    for c in range(3):
        image[:, :, c] = np.where(mask == 1,
                                 image[:, :, c] *
                                (1 - alpha) + alpha * color[c] * 255,
                                 image[:, :, c])
    return image

def GetDpRgMask(radar_pcl_dp_rg_ind, dp_rg_img):
    """
    Function:
        put pcl mask on doppler range images.

    Args:
        radar_pcl_dp_rg_ind             ->          radar point cloud doppler range indexes
        dp_rg_img                       ->          doppler range images
    """
    dp_rg_obj_mask = np.zeros(shape=(dp_rg_img.shape[0], dp_rg_img.shape[1]))
    for i in range(len(radar_pcl_dp_rg_ind)):
        doppler_ind, range_ind = radar_pcl_dp_rg_ind[i]
        range_ind = dp_rg_img.shape[0] - range_ind
        dp_rg_obj_mask[int(range_ind), int(doppler_ind)] = 1
    ##### apply mask onto the image to see whether it is right #####
    color = (0.0, 0.0, 1.0) # typical red
    masked_img = applyMask(dp_rg_img, dp_rg_obj_mask, color)
    return masked_img

def main(radar_ral_dir, radar_pcl_dir, sequence):
    """ 
    MAIN FUNCTION
    """
    dp_rg_dir = radar_ral_dir + "doppler_range/"
    for frame_i in tqdm(range(sequence[0], sequence[1])):
        radar_pcl = ReadNpy(radar_pcl_dir, frame_i)
        if radar_pcl is None:
            print("===== frame %d has no object detected ====="%(frame_i))
            continue
        radar_pcl_dp_rg_ind = DopplerIndRangeInd(radar_pcl, 1)
        dp_rg_img = ReadDpRg(dp_rg_dir, frame_i)
        masked_img = GetDpRgMask(radar_pcl_dp_rg_ind, dp_rg_img)
        # cv2.imshow('img', cv2.resize(masked_img, (0,0), fx=3, fy=3))
        cv2.imwrite("./radar_doppler_range_mask/dp_rg_mask_" + str(frame_i) + ".png", masked_img)
        cv2.waitKey(10)

if __name__ == "__main__":
    radar_ral_dir = "/DATA/2020-02-08-12-42-43/ral_outputs_2020-02-08-12-42-43/"
    radar_pcl_dir = "./radar_pcl/"
    sequence = [0, 2635]
    main(radar_ral_dir, radar_pcl_dir, sequence)
