import os
from mpl_toolkits.mplot3d import Axes3D 
import matplotlib.pyplot as plt
import numpy as np
import cv2
import glob
import scipy
from tqdm import tqdm

def getTotoalImgNum(img_dir):
    """
    Function:
        Get the total number of images, prepare for the sequence number

    Args:
        img_dir             ->          image directory for counting
    """
    all_imgs = glob.glob(img_dir + "*.jpg")
    return len(all_imgs)

def GenerateVideo(images, video_dir):
    """
    Function:
        generate video from image array.
    """
    h, w, ch = images[0].shape
    size = (w, h)
    # video = cv2.VideoWriter(video_dir, cv2.VideoWriter_fourcc(*'DIVX'), 20, size)
    video = cv2.VideoWriter(video_dir, cv2.VideoWriter_fourcc('M','J','P','G'), 20, size)
    for i in tqdm(range(len(images))):
        current_img = images[i]
        video.write(current_img)
    video.release()

def checkDoa(stereo_left_dir, doa_dir, doa_pnts_dir, radar_pose_pic_name,
            video_file_location, sequences):
    """
    Function:
        Check how good is the doa generated from radar.
    """
    output_img_shape = (640, 480)
    output_doa_shape = (480, 480)
    assert output_img_shape[1] == output_doa_shape[1]
    size = (output_img_shape[0] * 2 + output_doa_shape[0] * 2 , output_img_shape[1])
    video = cv2.VideoWriter(video_file_location, cv2.VideoWriter_fourcc('M','J','P','G'),
            20, size)
    radar_pos_img = cv2.imread(radar_pose_pic_name)
    if radar_pos_img is None:
        raise ValueError("Radar Position Image does not exist")
    for frame_ind in tqdm(range(sequences[0], sequences[1])):
        cam_filename = stereo_left_dir + "%.6d.jpg"%(frame_ind)
        doa_filename = doa_dir + "%.6d.png"%(frame_ind)
        doa_pnts_filename = doa_pnts_dir + "%.6d.png"%(frame_ind)
        if not os.path.exists(cam_filename) or not os.path.exists(doa_filename) \
                or not os.path.exists(doa_pnts_filename):
            continue
        cam_img = cv2.imread(cam_filename)
        doa_img = cv2.imread(doa_filename)
        doa_pnts_img = cv2.imread(doa_pnts_filename)
        if cam_img is None or doa_img is None:
            continue
        radar_pos_img = cv2.resize(radar_pos_img, (output_img_shape[0], 
                                                    output_img_shape[1]))
        cam_img = cv2.resize(cam_img, (output_img_shape[0], 
                                        output_img_shape[1]))
        doa_img = cv2.resize(doa_img, (output_doa_shape[0], 
                                        output_doa_shape[1]))
        doa_pnts_img = cv2.resize(doa_pnts_img, (output_doa_shape[0], 
                                                output_doa_shape[1]))
        display_img = np.concatenate([radar_pos_img, cam_img, doa_img, doa_pnts_img], \
                                    axis = 1)
        cv2.imshow("img", display_img)
        cv2.waitKey(20)
        video.write(display_img)
    video.release()

def main():
    time_stamp = "2020-03-11-10-31-04"
    radar_pose_pic_name = glob.glob("/DATA/" + time_stamp + "/*.jpg")[0] 
    doa_pnts_dir = "/DATA/" + time_stamp + "/ral_outputs_" + time_stamp + "/doa_pnts/"
    doa_dir = "/DATA/" + time_stamp + "/ral_outputs_" + time_stamp + "/doa/"
    stereo_left_dir = "/DATA/" + time_stamp + "/stereo_imgs/left/"
    video_file_location = "./radar_verify_video/radar_checkout.avi"
    sequences = [0, getTotoalImgNum(stereo_left_dir)]
    checkDoa(stereo_left_dir, doa_dir, doa_pnts_dir, radar_pose_pic_name,
            video_file_location, sequences)

if __name__ == "__main__":
    main()
