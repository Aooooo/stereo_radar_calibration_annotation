import csv
import numpy as np
import cv2
import matplotlib.pyplot as plt
from tqdm import tqdm
import time

def CreateCsvReader(csv_path):
    """
    Function:
        Create a scv reader to avoid reading the file multiple times

    Args:
        csv_path            ->          csv file path
    """
    with open(csv_path, 'r') as f:
        reader = list(csv.reader(f))
    return reader

def GetRadarFramePoints(reader, frame_id):
    """
    Function:
        Read the radar scv file and get the all wanted frames
    
    Args:
        reader              ->              csv reader
        frame_id            ->              frame index to be read
    """
    radar_points = []
    frame_name = (6-len(str(frame_id))) * '0' + str(frame_id) + '.png'
    for line in reader:
        if line[0] == frame_name:
            this_point = []
            for info_id in range(1, len(line)):
                this_point.append(float(line[info_id]))
            radar_points.append(this_point)
    radar_points = np.array(radar_points)
    return radar_points

def FilterRadarPoints(radar_points, x_abs_lim=10, z_min=0, z_max=20):
    """
    Function:
        remove all unecessary depth points of radar pcl
    """
    all_valid_pnts = []
    for i in range(len(radar_points)):
        current_point = radar_points[i]
        if current_point[2] > z_min and np.abs(current_point[0]) < x_abs_lim and current_point[2] < z_max:
            all_valid_pnts.append(current_point)
    all_valid_pnts = np.array(all_valid_pnts)
    return all_valid_pnts

def ReadRadarCsv(csv_path, sequence, save_dir, magnitude_threshold=0, find_magnitude=False):
    """
    Function:
        Read the radar points from frame to frame.

    Args:
        csv_path            ->          csv file path
        sequence            ->          sequence to be processed
        save_dir            ->          directory to save the result figures

    Attention:
        radar_points        ->          [x, y, z, velocity, magnitude]
    """
    fig = plt.figure()
    ax1 = fig.add_subplot(111)

    csv_reader = CreateCsvReader(csv_path)
    for frame_id in tqdm(range(sequence[0], sequence[1])):
        radar_points = GetRadarFramePoints(csv_reader, frame_id)
        if len(radar_points) == 0:
            continue
        if find_magnitude:
            print("-----------")
            print("current frame id: ", frame_id)
            print("highest magnitude: ", radar_points[:, -1].max())
            print("average magnitude: ", radar_points[:, -1].mean())
            time.sleep(1)
        else:
            radar_points = np.where(np.expand_dims(radar_points[:, -1],-1) > magnitude_threshold, radar_points, 0) 
            x_lim = 50
            z_min = 0
            z_max = 50
            radar_points = FilterRadarPoints(radar_points, x_lim, z_min, z_max) 
            if len(radar_points) == 0:
                continue
            ##### save the 2d point cloud as numpy array #####
            np.save(save_dir + "radar_pcl_" + str(frame_id) + ".npy", radar_points)
            ##### plot the 2d point cloud and save as figure #####
            ax1.clear()
            ax1.scatter(radar_points[:, 0], radar_points[:, 2], s=2, c='black', label="Radar DOA") 
            ax1.set_xlim([-30, 30])
            ax1.set_ylim([0, 50])
            ax1.legend()
            plt.savefig(save_dir + "radar_doa_" + str(frame_id) + ".png")
            # fig.canvas.draw()
            # plt.pause(0.05)

if __name__ == "__main__":
    time_stamp = "2020-09-03-13-43-28"
    radar_csv_path = "/DATA/" + time_stamp + "/ral_outputs_" + time_stamp + "/DOAPoints.csv"
    save_dir = "./radar_pcl/"
    sequence = [0, 2158]

    ReadRadarCsv(radar_csv_path, sequence, save_dir, magnitude_threshold=45,\
                find_magnitude=False)
