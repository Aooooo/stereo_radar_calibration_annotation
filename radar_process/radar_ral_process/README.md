# Radar DOA Post-processing for Calibration

This sub-directory is for reading the radar doa file and filter the point could.

## [ALTERNATIVE] 
### Read Radar doa CSV file.

To read and filter the radar points using csv file generated from repo "Radar Data Access Library", run
```
read_radar_csv.py
```

**Note:** The output plots and numpy array points are stored in the folder `radar_pcl`. Also, feel free to tune the parameters for filtering inside the code.

### Find doppler range indexes

To trace back to range-doppler figure, run
```
backto_dp_rg.py
```

## [ALTERNATIVE] 
### Generating Range-Azimuth-Doppler masks

To generate R-A-D mask with the same size as R-A-D matrix, run
```
process_radar_cube.py
```

**Note:** The output masks are saved as .npy file in the directory `radar_RAD_mask`.
