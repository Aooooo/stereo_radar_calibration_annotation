import os
import cv2
import numpy as np
import math
import pickle
import random
import colorsys
import matplotlib.pyplot as plt

from sklearn.cluster import DBSCAN
from collections import Counter

from glob import glob
from tqdm import tqdm


def main(radar_dir, adc_dir, instance_dir, img_dir, sequences):
    for i in tqdm(range(sequences[0], sequences[1])):
        RAD_file = os.path.join(radar_dir, "%.6d.npy"%(i))
        ADC_file = os.path.join(adc_dir, "%.6d.npy"%(i))
        instance_file = os.path.join(instance_dir, "radar_obj_%.d.pickle"%(i))
        img_file = os.path.join(img_dir, "rect_imgs_%.d.jpg"%(i))

        if not os.path.exists(RAD_file) or not os.path.exists(ADC_file):
            continue

        if not os.path.exists(instance_file):
            os.remove(RAD_file)
            os.remove(ADC_file)
            if os.path.exists(img_file):
                os.remove(img_file)

if __name__ == "__main__":
    time_stamp = "2020-09-03-12-30-11"
    radar_dir = "/DATA/" + time_stamp + "/ral_outputs_" + time_stamp + "/RAD_numpy"
    adc_dir = "/DATA/" + time_stamp + "/ral_outputs_" + time_stamp + "/ADC"
    instance_dir = "../annotation/radar_annotation"
    img_dir = "../stereo_process/disparity"
    sequences = [0, 3000]
    main(radar_dir, adc_dir, instance_dir, img_dir, sequences)
