# Manual Correction

## Manually Correct the inaccurate annotations

The manual correction is basd on OpenCV/Python. Thus, it is relatively slow. Also, all annotation correction is down on the RA Cartesian image (doa). 

To start the correction, run
```
correct_RAD.py
```

**Note**: `mouse_left` is for selecting points with rectangle; `mouse_middle` is for resetting the rectangle; `mouse_right` confirm after finishing the rectangle. After the box is generated, enter the class number for the annotation. Class numbers `[1, 2, 3, 4, 5, 6, 7, 8]` stand for `["person", "bicycle", "car", "motorcycle", "bus", "train", "truck", "boat"]`.

**Also Note**: For the image navigation system, use `n`(next) to jump to the next frame; `b`(back) to jump to the previous frame.

## Manually remove all inaccurate annotations

This code is to filter out all inaccurate annotations by hand.

To start filtering, run
```
filter_RAD.py
```

**Note**: Use `n` as "next" when you feel the current annotation is correct; otherwise, use `d` as "delete" to remove the current annotation.
