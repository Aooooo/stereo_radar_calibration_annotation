import os
import shutil
import cv2
import numpy as np
import math
import pickle
import random
import colorsys
import matplotlib.pyplot as plt

from sklearn.cluster import DBSCAN
from collections import Counter

from glob import glob
from tqdm import tqdm
import sys
sys.path.insert(0, "/home/ao/Documents/sensorsdata/Radar_Object_Detection")
import util.drawer as drawer 
import util.helper as helper

# road user classes for this research
ROAD_USERS = ['person', 'bicycle', 'car', 'motorcycle',
            'bus', 'train', 'truck', 'boat']

# currently define these classes for model
DATASET_CLASSES = ['person', 'bicycle', 'car', 'motorcycle', 'bus', 'truck']

# Radar Configuration
RADAR_CONFIG_FREQ = 77 # GHz
DESIGNED_FREQ = 76.8 # GHz
RANGE_RESOLUTION = 0.1953125 # m
VELOCITY_RESOLUTION = 0.41968030701528203 # m/s
RANGE_SIZE = 256
DOPPLER_SIZE = 64
AZIMUTH_SIZE = 256
ANGULAR_RESOLUTION = np.pi / 2 / AZIMUTH_SIZE # radians
VELOCITY_MIN = - VELOCITY_RESOLUTION * DOPPLER_SIZE/2
VELOCITY_MAX = VELOCITY_RESOLUTION * DOPPLER_SIZE/2

def readRAD(filename):
    if os.path.exists(os.path.join(filename)):
        return np.load(os.path.join(filename))
    else:
        return None

def readRadarInstances(filename):
    if os.path.exists(filename):
        with open(filename, "rb") as f:
            radar_instances = pickle.load(f)
        if len(radar_instances['classes']) == 0:
            radar_instances = None
    else:
        radar_instances = None
    return radar_instances

def writeRadarObjPcl(radar_dict, filename):
    with open(filename, "wb") as f:
        pickle.dump(radar_dict, f)

def getMagnitude(target_array, power_order=2):
    target_array = np.abs(target_array)
    target_array = pow(target_array, power_order)
    return target_array 

def getLog(target_array, scalar=1., log_10=True):
    if log_10:
        return scalar * np.log10(target_array + 1.)
    else:
        return target_array

def getSumDim(target_array, target_axis):
    output = np.sum(target_array, axis=target_axis)
    return output 

def norm2Image(array):
    norm_sig = plt.Normalize()
    img = plt.cm.viridis(norm_sig(array))
    img *= 255.
    img = img.astype(np.uint8)
    return img

def DbscanDenoise(pcl, epsilon=0.3, minimum_samples=100, n_jobs=1, dominant_op=False):
    clustering = DBSCAN(eps=epsilon, min_samples=minimum_samples, n_jobs=n_jobs).fit(pcl)
    output_labels = clustering.labels_
    if not dominant_op:
        output_pcl = []
        for label_i in np.unique(output_labels):
            if label_i == -1:
                continue
            output_pcl.append(pcl[output_labels == label_i])
    else:
        if len(np.unique(output_labels)) == 1 and np.unique(output_labels)[0] == -1:
            output_pcl = np.zeros([0,2])
            output_idx = []
        else:
            counts = Counter(output_labels)
            output_pcl = pcl[output_labels == counts.most_common(1)[0][0]]
            output_idx = np.where(output_labels == counts.most_common(1)[0][0])[0]
    return output_pcl

def toCartesianMask(RA_mask):
    output_mask = np.zeros([RA_mask.shape[0], RA_mask.shape[0]*2])
    for i in range(RA_mask.shape[0]):
        for j in range(RA_mask.shape[1]):
            if RA_mask[i, j] > 0:
                point_range = ((RANGE_SIZE-1) - i) * RANGE_RESOLUTION
                point_angle = (j * (2*np.pi/AZIMUTH_SIZE) - np.pi) / \
                                (2*np.pi*0.5*RADAR_CONFIG_FREQ/DESIGNED_FREQ)
                point_angle = np.arcsin(point_angle)
                point_zx = polarToCartesian(point_range, point_angle)
                new_i = int(output_mask.shape[0] - \
                        np.round(point_zx[0]/RANGE_RESOLUTION)-1)
                new_j = int(np.round((point_zx[1]+50)/RANGE_RESOLUTION)-1)
                output_mask[new_i,new_j] = RA_mask[i, j] 
    return output_mask

def cartesianToPolar(x, y):
    rho = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    return(rho, phi)

def polarToCartesian(rho, phi):
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return(x, y)

def applyMask(image, mask, color, alpha=1):
    for c in range(3):
        image[:, :, c] = np.where(mask == 1,
                                 image[:, :, c] *
                                (1 - alpha) + alpha * color[c] * 255,
                                 image[:, :, c])
    return image

def getSequenceNumbers(radar_dir, data_format=".npy"):
    sequence_numbers = []
    for this_file in glob(os.path.join(radar_dir, "*"+data_format)):
        digits = int("".join((s for s in this_file.replace(radar_dir, "") \
                    if s.isdigit())))
        sequence_numbers.append(digits)
    if len(sequence_numbers) == 0:
        sequence_numbers = None
    else:
        sequence_numbers = np.sort(sequence_numbers)
    return sequence_numbers

def getBox(radar_masks, edge_agp=2):
    bbox = []
    for mask_i in range(len(radar_masks)):
        mask = radar_masks[mask_i]
        indexes = []
        for i in range(mask.shape[0]):
            for j in range(mask.shape[1]):
                for k in range(mask.shape[2]):
                    if mask[i,j,k] > 0:
                        indexes.append([i, j, k])
        indexes = np.array(indexes)
        x_min, x_max = np.amin(indexes[:, 0]), np.amax(indexes[:, 0])
        y_min, y_max = np.amin(indexes[:, 1]), np.amax(indexes[:, 1])
        z_min, z_max = np.amin(indexes[:, 2]), np.amax(indexes[:, 2])
        if np.abs(z_min) <= edge_agp and np.abs(mask.shape[2]-z_max) <= edge_agp:
            idx_clusters = DbscanDenoise(indexes, epsilon=15, minimum_samples=1, \
                                            n_jobs=6)
            need_shift = True
            for clust in idx_clusters:
                z_min_tmp, z_max_tmp = np.amin(clust[:, 2]), np.amax(clust[:, 2])
                if np.abs(z_min_tmp) <= edge_agp and \
                        np.abs(mask.shape[2]-z_max_tmp) <= edge_agp:
                    need_shift = False
            # print(need_shift)
            if need_shift:
                all_maxs = []
                all_mins = []
                for clust in idx_clusters:
                    z_min, z_max = np.amin(clust[:, 2]), np.amax(clust[:, 2])
                    if np.abs(z_min) <= edge_agp:
                        all_maxs.append(z_max)
                    else:
                        all_mins.append(z_min)
                z_min = np.amin(all_mins) - mask.shape[2]
                z_max = np.amax(all_maxs)
                z_center = (z_min+z_max)/2
                if z_center < 0:
                    z_center += mask.shape[2]
                bbox.append([(x_min+x_max)/2, (y_min+y_max)/2, z_center, \
                                x_max-x_min+1, y_max-y_min+1, z_max-z_min+1])
                # print([(x_min+x_max)/2, (y_min+y_max)/2, z_center, \
                                # x_max-x_min+1, y_max-y_min+1, z_max-z_min+1])
            else:
                bbox.append([(x_min+x_max)/2, (y_min+y_max)/2, (z_min+z_max)/2, \
                                x_max-x_min+1, y_max-y_min+1, z_max-z_min+1])
                # print([(x_min+x_max)/2, (y_min+y_max)/2, (z_min+z_max)/2, \
                                # x_max-x_min+1, y_max-y_min+1, z_max-z_min+1])
        else:
            bbox.append([(x_min+x_max)/2, (y_min+y_max)/2, (z_min+z_max)/2, \
                            x_max-x_min+1, y_max-y_min+1, z_max-z_min+1])
    return np.array(bbox)

def getCartBox(masks):
    bbox = []
    for i in range(masks.shape[0]):
        mask_i = np.where(getSumDim(masks[i], -1)>0, 1., 0.)
        mask = toCartesianMask(mask_i)
        indexes = []
        for i in range(mask.shape[0]):
            for j in range(mask.shape[1]):
                if mask[i,j] > 0:
                    indexes.append([i, j])
        indexes = np.array(indexes)
        x_min, x_max = np.amin(indexes[:, 0]), np.amax(indexes[:, 0])
        y_min, y_max = np.amin(indexes[:, 1]), np.amax(indexes[:, 1])
        bbox.append([(x_min+x_max)/2, (y_min+y_max)/2, x_max-x_min+1, y_max-y_min+1])
    return np.array(bbox)


def main(starting_idx, read_dir_list, read_root, dataset_rootdir):
    dataset_gt_dir = os.path.join(dataset_rootdir, "gt")
    dataset_gtbox_dir = os.path.join(dataset_rootdir, "gt_box")
    dataset_ADC_dir = os.path.join(dataset_rootdir, "ADC")
    dataset_RAD_dir = os.path.join(dataset_rootdir, "RAD")
    dataset_img_dir = os.path.join(dataset_rootdir, "images")
    dataset_vis_dir = os.path.join(dataset_rootdir, "visual")
    dataset_radarvis_dir = os.path.join(dataset_rootdir, "vis_radar_only")
    
    fig, axes = drawer.prepareFigure(3, figsize=(25, 8))
    colors = drawer.RandomColors(len(DATASET_CLASSES))
    count = starting_idx
    for folder in read_dir_list:
        read_rootdir = os.path.join(read_root, folder)

        read_gt_dir = os.path.join(read_rootdir, "gt")
        read_img_dir = os.path.join(read_rootdir, "img")
        read_RAD_dir = os.path.join(read_rootdir, "input/RAD_numpy")
        read_ADC_dir = os.path.join(read_rootdir, "input/ADC")
        read_vis_dir = os.path.join(read_rootdir, "visualization")

        read_all_nums = getSequenceNumbers(read_gt_dir, ".pickle")
        for ii in tqdm(range(len(read_all_nums))):
            seq_num = read_all_nums[ii]
            print("current sequence number: ", seq_num)
            gt_file = os.path.join(read_gt_dir, "radar_obj_%.d.pickle"%(seq_num))
            RAD_file = os.path.join(read_RAD_dir, "%.6d.npy"%(seq_num))
            ADC_file = os.path.join(read_ADC_dir, "%.6d.npy"%(seq_num))
            img_file = os.path.join(read_img_dir, "rect_imgs_%.d.jpg"%(seq_num))
            vis_file = os.path.join(read_vis_dir, "sensorcortek_%.d.png"%(seq_num))
            if not os.path.exists(gt_file):
                print("gt file not found")
                continue
            if not os.path.exists(RAD_file):
                print("RAD file not found")
                continue
            if not os.path.exists(ADC_file):
                print("ADC file not found")
                continue
            if not os.path.exists(img_file):
                print("img file not found")
                continue
            if not os.path.exists(vis_file):
                print("visualization file not found")
                continue
            target_gt_file = os.path.join(dataset_gt_dir, "%.6d.pickle"%(count))
            target_gt_box_file = os.path.join(dataset_gtbox_dir, "%.6d.pickle"%(count))
            target_RAD_file = os.path.join(dataset_RAD_dir, "%.6d.npy"%(count))
            target_ADC_file = os.path.join(dataset_ADC_dir, "%.6d.npy"%(count))
            target_img_file = os.path.join(dataset_img_dir, "%.6d.jpg"%(count))
            target_vis_file = os.path.join(dataset_vis_dir, "%.6d.png"%(count))


            RAD = readRAD(RAD_file)
            gt = readRadarInstances(gt_file)

            RAD_mag = getMagnitude(RAD, power_order=2)
            RD_img = norm2Image(getLog(getSumDim(RAD_mag, 1), \
                                scalar=10, log_10=True))[..., :3]
            RA_img = norm2Image(getLog(getSumDim(RAD_mag, -1), \
                                scalar=10, log_10=True))[..., :3]
            RA_cart = toCartesianMask(np.zeros((RA_img.shape[0], \
                                    RA_img.shape[1])).astype(np.float32))

            radar_masks = gt["masks"] 
            radar_classes = gt["classes"] 
            radar_boxes = gt["boxes"]

            ############ NOTE: only continue when have target classes #########
            ############ comment if not needed                        #########
            masks_left = []
            classes_left = []
            for inst_i in range(len(radar_classes)):
                if radar_classes[inst_i] not in DATASET_CLASSES:
                    continue
                masks_left.append(np.expand_dims(radar_masks[inst_i], 0))
                classes_left.append(radar_classes[inst_i])
            if len(classes_left) == 0:
                continue
            radar_masks = np.concatenate(masks_left, 0)
            radar_classes = classes_left
            ##################################################################

            new_boxes = getBox(radar_masks)

            new_dict = {}
            new_dict["masks"] = radar_masks
            new_dict["classes"] = radar_classes
            new_dict["boxes"] = new_boxes

            boxonly_dict = {}
            boxonly_dict["classes"] = radar_classes
            boxonly_dict["boxes"] = new_boxes
            boxonly_dict["cart_boxes"] = getCartBox(radar_masks)

            drawer.clearAxes(axes)
            drawer.drawRadarBoxes(RD_img, RA_img, RA_cart, boxonly_dict, \
                            DATASET_CLASSES, colors, axes)
            drawer.saveFigure(dataset_radarvis_dir, "vis_", count)

            writeRadarObjPcl(new_dict, target_gt_file)
            writeRadarObjPcl(boxonly_dict, target_gt_box_file)
            shutil.move(RAD_file, target_RAD_file)
            shutil.move(ADC_file, target_ADC_file)
            shutil.move(img_file, target_img_file)
            shutil.move(vis_file, target_vis_file)

            count += 1
    

if __name__ == "__main__":
    starting_idx = 2572
    read_dir_list = ["static_2"]
    read_rootdir = "/DATA/manual_corrected_data"
    dataset_rootdir = "/home/ao/Documents/radar_DATA"
    main(starting_idx, read_dir_list, read_rootdir, dataset_rootdir)
