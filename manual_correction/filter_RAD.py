import os
import cv2
import numpy as np
import math
import pickle
import random
import colorsys
import matplotlib.pyplot as plt

from sklearn.cluster import DBSCAN
from collections import Counter

from glob import glob
from tqdm import tqdm

from cv_mouse import *

# detectron2 all classes
CLASS_NAMES = ['person', 'bicycle', 'car', 'motorcycle', 'airplane', 'bus', \
        'train', 'truck', 'boat', 'traffic light', 'fire hydrant', 'stop sign', \
        'parking meter', 'bench', 'bird', 'cat', 'dog', 'horse', 'sheep', 'cow', \
        'elephant', 'bear', 'zebra', 'giraffe', 'backpack', 'umbrella', 'handbag', \
        'tie', 'suitcase', 'frisbee', 'skis', 'snowboard', 'sports ball', 'kite', \
        'baseball bat', 'baseball glove', 'skateboard', 'surfboard', \
        'tennis racket', 'bottle', 'wine glass', 'cup', 'fork', 'knife', 'spoon', \
        'bowl', 'banana', 'apple', 'sandwich', 'orange', 'broccoli', 'carrot', \
        'hot dog', 'pizza', 'donut', 'cake', 'chair', 'couch', 'potted plant', \
        'bed', 'dining table', 'toilet', 'tv', 'laptop', 'mouse', 'remote', \
        'keyboard', 'cell phone', 'microwave', 'oven', 'toaster', 'sink', \
        'refrigerator', 'book', 'clock', 'vase', 'scissors', 'teddy bear', \
        'hair drier', 'toothbrush']

# road user classes for this research
ROAD_USERS = ['person', 'bicycle', 'car', 'motorcycle',
            'bus', 'train', 'truck', 'boat']

# Radar Configuration
RADAR_CONFIG_FREQ = 77 # GHz
DESIGNED_FREQ = 76.8 # GHz
RANGE_RESOLUTION = 0.1953125 # m
VELOCITY_RESOLUTION = 0.41968030701528203 # m/s
RANGE_SIZE = 256
DOPPLER_SIZE = 64
AZIMUTH_SIZE = 256
ANGULAR_RESOLUTION = np.pi / 2 / AZIMUTH_SIZE # radians
VELOCITY_MIN = - VELOCITY_RESOLUTION * DOPPLER_SIZE/2
VELOCITY_MAX = VELOCITY_RESOLUTION * DOPPLER_SIZE/2

def RandomColors(N, bright=True): 
    brightness = 1.0 if bright else 0.7
    hsv = [(i / N, 1, brightness) for i in range(N)]
    colors = list(map(lambda c: colorsys.hsv_to_rgb(*c), hsv))
    # detectron uses random shuffle to give the differences
    random.seed(8888)
    random.shuffle(colors)
    return colors

def roadUsersColors():
    colors_out = []
    all_colors = RandomColors(len(CLASS_NAMES))
    for class_i in ROAD_USERS:
        class_ind = CLASS_NAMES.index(class_i)
        colors_out.append(all_colors[class_ind])
    return colors_out

def readRAD(radar_dir, frame_id):
    if os.path.exists(os.path.join(radar_dir, "%.6d.npy"%(frame_id))):
        return np.load(os.path.join(radar_dir, "%.6d.npy"%(frame_id)))
    else:
        return None
    
def readRADMask(mask_dir, frame_i):
    filename = os.path.join(mask_dir, "RAD_mask_%.d.npy"%(frame_i))
    if os.path.exists(filename):
        RAD_mask = np.load(filename)
    else:
        RAD_mask = None
    return RAD_mask

def readStereoMrcnnImg(stereo_mrcnn_img_dir, frame_i):
    filename = os.path.join(stereo_mrcnn_img_dir, "stereo_mrcnn_img_%.d.jpg"%(frame_i))
    if os.path.exists(filename):
        img = cv2.imread(filename)[..., ::-1]
    else:
        img = None
    return img

def readRadarInstances(instance_dir, frame_i):
    filename = os.path.join(instance_dir, "radar_obj_%.d.pickle"%(frame_i))
    if os.path.exists(filename):
        with open(filename, "rb") as f:
            radar_instances = pickle.load(f)
        if len(radar_instances['classes']) == 0:
            radar_instances = None
    else:
        radar_instances = None
    return radar_instances

def writeRadarObjPcl(instance_dir, radar_dict, frame_i):
    filename = os.path.join(instance_dir, "radar_obj_%.d.pickle"%(frame_i))
    with open(filename, "wb") as f:
        pickle.dump(radar_dict, f)

def getMagnitude(target_array, power_order=2):
    target_array = np.abs(target_array)
    target_array = pow(target_array, power_order)
    return target_array 

def getLog(target_array, scalar=1., log_10=True):
    if log_10:
        return scalar * np.log10(target_array + 1.)
    else:
        return target_array

def getSumDim(target_array, target_axis):
    output = np.sum(target_array, axis=target_axis)
    return output 

def norm2Image(array):
    norm_sig = plt.Normalize()
    img = plt.cm.viridis(norm_sig(array))
    img *= 255.
    img = img.astype(np.uint8)
    return img

def DbscanDenoise(pcl, epsilon=0.3, minimum_samples=100, dominant_op=False):
    clustering = DBSCAN(eps=epsilon, min_samples=minimum_samples).fit(pcl)
    output_labels = clustering.labels_
    if not dominant_op:
        output_pcl = []
        for label_i in np.unique(output_labels):
            if label_i == -1:
                continue
            output_pcl.append(pcl[output_labels == label_i])
    else:
        # if len(np.unique(output_labels)) == 1:
            # output_pcl = np.zeros([0,3])
        # else:
        counts = Counter(output_labels)
        output_pcl = pcl[output_labels == counts.most_common(1)[0][0]]
    return output_pcl

def toCartesianMask(RA_mask):
    output_mask = np.zeros([RA_mask.shape[0], RA_mask.shape[0]*2])
    for i in range(RA_mask.shape[0]):
        for j in range(RA_mask.shape[1]):
            if RA_mask[i, j] > 0:
                point_range = ((RANGE_SIZE-1) - i) * RANGE_RESOLUTION
                point_angle = (j * (2*np.pi/AZIMUTH_SIZE) - np.pi) / \
                                (2*np.pi*0.5*RADAR_CONFIG_FREQ/DESIGNED_FREQ)
                point_angle = np.arcsin(point_angle)
                point_zx = polarToCartesian(point_range, point_angle)
                new_i = int(output_mask.shape[0] - \
                        np.round(point_zx[0]/RANGE_RESOLUTION)-1)
                new_j = int(np.round((point_zx[1]+50)/RANGE_RESOLUTION)-1)
                output_mask[new_i,new_j] = RA_mask[i, j] 
    return output_mask

def toRAMask(RA_cart_mask):
    output_mask = np.zeros([RANGE_SIZE, AZIMUTH_SIZE]) 
    for i in range(RA_cart_mask.shape[0]):
        for j in range(RA_cart_mask.shape[1]):
            if RA_cart_mask[i, j] > 0:
                z = (RA_cart_mask.shape[0] - i + 1)*RANGE_RESOLUTION
                x = (j + 1)*RANGE_RESOLUTION - 50
                range_, angle_ = cartesianToPolar(z, x)
                angle_ = np.sin(angle_)
                new_i = int( (RANGE_SIZE-1) - (range_/RANGE_RESOLUTION) )
                new_j = int( (angle_*(2*np.pi*0.5*RADAR_CONFIG_FREQ/DESIGNED_FREQ) \
                        + np.pi) / (2*np.pi/AZIMUTH_SIZE) )
                if new_i > output_mask.shape[0]-1:
                    new_i = output_mask.shape[0]-1
                if new_j > output_mask.shape[1]-1:
                    new_j = output_mask.shape[1]-1
                output_mask[new_i, new_j] = RA_cart_mask[i, j]
    return output_mask

def showRadarInstances(RA_cart_img, RD_img, radar_masks, radar_classes, all_colors):
    assert len(radar_masks) == len(radar_classes)
    for i in range(len(radar_classes)):
        mask = radar_masks[i]
        if len(mask[mask>0]) > 0:
            cls = radar_classes[i]
            color = all_colors[CLASS_NAMES.index(cls)]
            mask_RA = np.where(getSumDim(mask, -1) >= 1, 1, 0)
            mask_RD = np.where(getSumDim(mask, 1) >= 1, 1, 0)
            mask_cart = np.where(toCartesianMask(mask_RA)>0., 1., 0.)
            applyMask(RA_cart_img, mask_cart, color)
            applyMask(RD_img, mask_RD, color)
            applyBBox(RA_cart_img, mask_cart, color, cls)
            applyBBox(RD_img, mask_RD, color, cls)

def applyBBox(img, mask, color, cls):
    all_idxes = []
    for idx_i in range(mask.shape[0]):
        for idx_j in range(mask.shape[1]):
            if mask[idx_i, idx_j] > 0:
                all_idxes.append([idx_i, idx_j])
    all_idxes = np.array(all_idxes)
    y_min, y_max = np.amin(all_idxes[:, 0]), np.amax(all_idxes[:, 0])
    x_min, x_max = np.amin(all_idxes[:, 1]), np.amax(all_idxes[:, 1])
    # print([x_min, x_max, y_min, y_max])
    h = -2
    cv2.rectangle(img, (x_min, y_min), (x_max, y_max), \
                (color[0]*255, color[1]*255, color[2]*255), 1)
    cv2.putText(img, cls, (x_min+h, y_min+h), 0, 0.3, \
                (color[0]*255, color[1]*255, color[2]*255), 1)
 
def correctMask(RAD_mask, RA_mask, RA_masked_cart_mag, target_points, target_class, \
                radar_masks, radar_classes):
    for i in range(len(target_points)):
        if target_points[i][0] > RA_masked_cart_mag.shape[1]:
            target_points[i][0] = RA_masked_cart_mag.shape[1]
    x_min, x_max = np.amin(target_points[:, 0]), np.amax(target_points[:, 0])
    y_min, y_max = np.amin(target_points[:, 1]), np.amax(target_points[:, 1])
    area_RA_mask = np.zeros(RA_masked_cart_mag.shape)
    for i in range(y_min, y_max-1):
        for j in range(x_min, x_max-1):
            area_RA_mask[i,j] = 1.
    area_RA_recover = toRAMask(area_RA_mask)
    area_RAD_recover = np.zeros(RAD_mask.shape)
    index_clusteres = []

    area_RA_recover *= 255.
    area_RA_recover = area_RA_recover.astype(np.uint8)
    rect_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
    threshed = cv2.morphologyEx(area_RA_recover, cv2.MORPH_CLOSE, rect_kernel)
    area_RA_recover = np.where(threshed > 0, 1., 0.)
    for i in range(area_RAD_recover.shape[0]):
        for j in range(area_RAD_recover.shape[1]):
            if area_RA_recover[i, j] > 0:
                area_RAD_recover[i, j, :] = RAD_mask[i, j, :]
                for k in range(area_RAD_recover.shape[2]):
                    if area_RAD_recover[i, j, k] > 0:
                        index_clusteres.append([i, j, k])

    # for i in range(area_RA_recover.shape[0]):
        # for j in range(area_RA_recover.shape[1]):
            # if area_RA_recover[i, j] > 0:
                # area_RAD_recover[i, j, :] = 1.
    # for i in range(area_RAD_recover.shape[0]):
        # for j in range(area_RAD_recover.shape[1]):
            # for k in range(area_RAD_recover.shape[2]):
                # if area_RAD_recover[i,j,k] > 0:
                    # index_clusteres.append([i, j, k])
    # index_clusteres = np.array(index_clusteres)
    # i_min, i_max = np.amin(index_clusteres[:,0]), np.amax(index_clusteres[:,0])
    # j_min, j_max = np.amin(index_clusteres[:,1]), np.amax(index_clusteres[:,1])
    # index_clusteres = []
    # area_RAD_recover = np.zeros(RAD_mask.shape)
    # for i in range(i_min, i_max):
        # for j in range(j_min, j_max):
            # for k in range(RAD_mask.shape[2]):
                # if RAD_mask[i, j, k] > 0:
                    # area_RAD_recover[i, j, k] = 1.
                    # index_clusteres.append([i, j, k])

    if_remove = False
    new_radar_masks = []
    new_radar_classes = []
    for i in range(len(radar_masks)):
        radar_mask_i = radar_masks[i]
        radar_class_i = radar_classes[i]
        overlap_mask = radar_mask_i * area_RAD_recover
        if len(overlap_mask[overlap_mask>0]) > 0.6 * len(radar_mask_i[radar_mask_i>0]):
            if_remove = True
        else:
            new_radar_masks.append(radar_mask_i)
            new_radar_classes.append(radar_class_i)
    if if_remove:
        return np.array(new_radar_masks), new_radar_classes
    else:
        index_clusteres = np.array(index_clusteres)
        if len(index_clusteres) >= 3:
            indexes_instances = DbscanDenoise(index_clusteres, epsilon=15, \
                                            minimum_samples=2, dominant_op=False)
            all_lens = []
            for inst_i in range(len(indexes_instances)):
                all_lens.append(len(indexes_instances[inst_i]))
            indexes_dominant = indexes_instances[np.argmax(all_lens)]
            dominant_D = indexes_dominant[:, 2]
            dgap = 1
            for inst_i in range(len(indexes_instances)):
                is_included = False
                Ds_current = indexes_instances[inst_i][:, 2]
                if inst_i == np.argmax(all_lens):
                    continue
                for inst_j in range(len(Ds_current)):
                    if is_included:
                        continue
                    current_D = Ds_current[inst_j]
                    dmin = np.amin(np.abs(dominant_D - current_D))
                    dmin_lshift = np.amin(np.abs(dominant_D + RAD_mask.shape[2] - current_D))
                    dmin_rshift = np.amin(np.abs(dominant_D - RAD_mask.shape[2] - current_D))
                    dmin = np.amin([dmin, dmin_lshift, dmin_rshift])
                    if dmin <= dgap:
                        is_included = True
                if is_included:
                    indexes_dominant = np.concatenate([indexes_dominant, \
                                        indexes_instances[inst_i]], axis=0)
            indexes_instances = indexes_dominant
        else:
            indexes_instances = index_clusteres
        add_radar_mask = np.zeros(RAD_mask.shape)
        add_radar_class = target_class
        if len(indexes_instances) > 0:
            for index_i in range(len(indexes_instances)):
                i, j, k = indexes_instances[index_i]
                add_radar_mask[i, j, k] = 1.
        new_radar_masks.append(add_radar_mask)
        new_radar_classes.append(add_radar_class)
        return np.array(new_radar_masks), new_radar_classes

def filterInstances(radar_masks, radar_classes):
    assert len(radar_masks) == len(radar_classes) 
    output_masks = []
    output_classes = []
    for i in range(len(radar_masks)):
        mask = radar_masks[i]
        cls = radar_classes[i]
        if len(mask[mask>0]) == 0:
            continue
        output_masks.append(np.expand_dims(mask, 0))
        output_classes.append(cls)
    if len(output_masks) != 0:
        output_masks = np.concatenate(output_masks, 0)
    else:
        output_masks = np.zeros(radar_masks.shape)
        output_classes = radar_classes
    return output_masks, output_classes

##################### coordinate transformation ######################
def cartesianToPolar(x, y):
    rho = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    return(rho, phi)

def polarToCartesian(rho, phi):
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return(x, y)

def applyMask(image, mask, color, alpha=1):
    for c in range(3):
        image[:, :, c] = np.where(mask == 1,
                                 image[:, :, c] *
                                (1 - alpha) + alpha * color[c] * 255,
                                 image[:, :, c])
    return image

def main(radar_dir, mrcnn_dir, mask_dir, instance_dir, sequences):
    ##### NOTE: mouse control class: GetMousePnts #####
    cv2.namedWindow("display", cv2.WINDOW_GUI_NORMAL)

    all_colors = RandomColors(len(CLASS_NAMES))
    colors = roadUsersColors()
    frame_i = sequences[0]
    if_go_next = True
    while (frame_i >=sequences[0] and frame_i <sequences[1]):
        RAD = readRAD(radar_dir, frame_i)
        RAD_mask = readRADMask(mask_dir, frame_i)
        mrcnn = readStereoMrcnnImg(mrcnn_dir, frame_i)
        radar_instances = readRadarInstances(instance_dir, frame_i)
        if RAD is not None and RAD_mask is not None and mrcnn is not None and \
                radar_instances is not None:
            print("You are currently at the frame:  ", frame_i)
            RAD_mag = getMagnitude(RAD, power_order=2)
            RA_mag = getLog(getSumDim(RAD_mag, -1), scalar=10, log_10=True)
            RD_img = norm2Image(getLog(getSumDim(RAD_mag, 1), \
                                scalar=10, log_10=True))[..., :3]
            RA_mask = np.where(getSumDim(RAD_mask, -1) > 0., 1., 0.)
            RA_masked_cart_mag = toCartesianMask(RA_mag) * toCartesianMask(RA_mask)
            RA_cart_img = norm2Image(RA_masked_cart_mag)[..., :3]

            radar_masks = radar_instances["masks"] 
            radar_classes = radar_instances["classes"] 
            scale = RA_mag.shape[0] / mrcnn.shape[0]

            RA_cart_vis = RA_cart_img.copy()
            RD_vis = RD_img.copy()
            showRadarInstances(RA_cart_vis, RD_vis, radar_masks, radar_classes, \
                                all_colors)
            display_img = np.concatenate([RA_cart_vis[..., ::-1], RD_vis[..., ::-1], \
                                    cv2.resize(mrcnn, (int(scale*mrcnn.shape[1]), \
                                    RA_mag.shape[0]))], 1)

            instance_file = os.path.join(instance_dir, \
                            "radar_obj_%.d.pickle"%(frame_i))
            visualization_file = os.path.join(instance_dir, \
                            "visualization_%.d.png"%(frame_i))
            while 1:
                cv2.imshow("display", display_img)
                k = cv2.waitKey(1) & 0xFF
                if k== ord('n'):
                    if_go_next = True
                    frame_i += 1
                    break
                elif k== ord('b'):
                    if_go_next = False
                    frame_i -= 1
                    break
                elif k== ord('d'):
                    os.remove(instance_file)
                    os.remove(visualization_file)
                    if_go_next = True
                    frame_i += 1
                    break
        else:
            if if_go_next:
                frame_i += 1
            else:
                frame_i -= 1


if __name__ == "__main__":
    time_stamp = "2020-09-03-12-24-49"
    radar_dir = "/DATA/" + time_stamp + "/ral_outputs_" + time_stamp + "/RAD_numpy"
    mrcnn_dir = "../annotation/stereo_object_detection_output"
    mask_dir = "../radar_process/radar_ral_process/radar_RAD_mask"
    instance_dir = "../annotation/radar_annotation"
    sequences = [0, 3000]
    main(radar_dir, mrcnn_dir, mask_dir, instance_dir, sequences)





