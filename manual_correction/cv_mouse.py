import cv2
import numpy as np

class GetMousePnts:
    def __init__(self,):
        self.x_begin = None
        self.y_begin = None
        self.x_move = None
        self.y_move = None
        self.x_end = None
        self.y_end = None
        self.output = None
        self.img = None

    def getImage(self, img):
        self.x_limits = img.shape[1]
        self.y_limits = img.shape[0]
        self.image = img.copy()

    def reset(self,):
        self.x_begin = None
        self.y_begin = None
        self.x_move = None
        self.y_move = None
        self.x_end = None
        self.y_end = None
        self.output = None

    def checkPoints(self, x, y):
        if x < 0:
            x = 0
        elif x > self.x_limits:
            x = self.x_limits

        if y < 0:
            y = 0
        elif y > self.y_limits:
            y = self.y_limits
        return x, y
    
    def outputLocation(self, ):
        if self.x_begin is not None and \
                self.y_begin is not None and \
                self.x_end is not None and \
                self.y_end is not None:
            self.output = np.array([[self.x_begin, self.y_begin], \
                                    [self.x_end, self.y_end]])
        else:
            self.output = None

    def mouseCallback(self, event, x, y, flags, params):
        x, y = self.checkPoints(x, y) 
        if event == cv2.EVENT_LBUTTONDOWN:
            self.x_begin = x
            self.y_begin = y
            self.x_move = None
            self.y_move = None
            self.x_end = None
            self.y_end = None
        elif event == cv2.EVENT_MOUSEMOVE:
            self.x_move = x
            self.y_move = y
        elif event == cv2.EVENT_LBUTTONUP:
            self.x_end = x
            self.y_end = y
        elif event == cv2.EVENT_RBUTTONDOWN:
            self.outputLocation()
            # self.reset()
        elif event == cv2.EVENT_MBUTTONDOWN:
            self.reset()

    def visualizeImage(self):
        self.img = self.image.copy()
        if self.x_begin is not None and self.y_begin is not None and \
            self.x_move is not None and self.y_move is not None:
            if self.x_end is None and self.y_end is None:
                cv2.rectangle(self.img, (self.x_begin, self.y_begin), \
                             (self.x_move, self.y_move), (0, 255, 0), \
                             thickness = 2)
            else:
                cv2.rectangle(self.img, (self.x_begin, self.y_begin), \
                             (self.x_end, self.y_end), (255, 0, 0), \
                             thickness = 2)

if __name__ == "__main__":
    cv2.namedWindow("display", cv2.WINDOW_GUI_NORMAL)
    # cv2.namedWindow("display", cv2.WINDOW_NORMAL)

    img_path = "./test.jpg"
    img = cv2.imread(img_path)
    cv_mouse = GetMousePnts()
    cv2.setMouseCallback("display", cv_mouse.mouseCallback)

    cv_mouse.getImage(img)
    while(1):
        cv_mouse.visualizeImage()
        cv2.imshow("display", cv_mouse.img)
        if cv_mouse.output is not None:
            print("output pixel locations: \t", cv_mouse.output)
            cv_mouse.reset()
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    cv2.destroyAllWindows()

