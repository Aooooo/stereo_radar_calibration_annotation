import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
import cv2
import pickle
import colorsys

from sklearn import mixture
from sklearn.cluster import DBSCAN
from collections import Counter

from glob import glob
from tqdm import tqdm

# COCO Class names
# Index of the class in the list is its ID. For example, to get ID of
# the teddy bear class, use: class_names.index('teddy bear')
CLASS_NAMES = ['BG', 'person', 'bicycle', 'car', 'motorcycle', 'airplane',
            'bus', 'train', 'truck', 'boat', 'traffic light',
            'fire hydrant', 'stop sign', 'parking meter', 'bench', 'bird',
            'cat', 'dog', 'horse', 'sheep', 'cow', 'elephant', 'bear',
            'zebra', 'giraffe', 'backpack', 'umbrella', 'handbag', 'tie',
            'suitcase', 'frisbee', 'skis', 'snowboard', 'sports ball',
            'kite', 'baseball bat', 'baseball glove', 'skateboard',
            'surfboard', 'tennis racket', 'bottle', 'wine glass', 'cup',
            'fork', 'knife', 'spoon', 'bowl', 'banana', 'apple',
            'sandwich', 'orange', 'broccoli', 'carrot', 'hot dog', 'pizza',
            'donut', 'cake', 'chair', 'couch', 'potted plant', 'bed',
            'dining table', 'toilet', 'tv', 'laptop', 'mouse', 'remote',
            'keyboard', 'cell phone', 'microwave', 'oven', 'toaster',
            'sink', 'refrigerator', 'book', 'clock', 'vase', 'scissors',
            'teddy bear', 'hair drier', 'toothbrush']

def RandomColors(N, bright=True):
    """
    Generate random colors.
    To get visually distinct colors, generate them in HSV space then
    convert to RGB.
    """
    brightness = 1.0 if bright else 0.7
    hsv = [(i / N, 1, brightness) for i in range(N)]
    colors = list(map(lambda c: colorsys.hsv_to_rgb(*c), hsv))
    # random.shuffle(colors)
    return colors

def ReadStereoObjPcl(stereo_obj_pcl_dir, frame_i):
    """
    Function:
        Read the stereo object detection point cloud w.r.t frame i

    Args:
        stereo_obj_pcl_dir          ->          stereo point cloud directory
        frame_i                     ->          frame id

    Attention:
        The output stereo_obj_pcl has two keys, 
        "class" : current object class string name 
        "pcl" : current object point cloud with format (num_pnts, 3) 
    """
    filename = stereo_obj_pcl_dir + "stereo_obj_pcl_" + str(frame_i) + ".pickle"
    if os.path.exists(filename):
        with open(filename, "rb") as f:
            stereo_obj_pcl = pickle.load(f)
        if len(stereo_obj_pcl['class']) == 0:
            stereo_obj_pcl = None
    else:
        stereo_obj_pcl = None
    return stereo_obj_pcl

def ReadStereoMrcnnImg(stereo_mrcnn_img_dir, frame_i):
    """
    Function:
        Read mask rcnn results on stereo left img
    
    Args:
        stereo_mrcnn_img_dir            ->          stereo mask rcnn result imgs
        frame_i                         ->          frame id
    """
    filename = stereo_mrcnn_img_dir + "stereo_mrcnn_img_" + str(frame_i) + ".jpg"
    if os.path.exists(filename):
        img = cv2.imread(filename)[..., ::-1]
    else:
        img = None
    return img
    
def ReadRadarPcl(radar_pcl_dir, frame_i):
    """
    Function:
        Read the radar pcl w.r.t frame i 

    Args:
        radar_pcl_dir           ->          radar point cloud directory
        frame_i                 ->          frame id
    """
    filename = radar_pcl_dir + "radar_pcl_" + str(frame_i) + ".npy"
    if os.path.exists(filename):
        radar_pcl = np.load(filename)
        if len(radar_pcl) == 0:
            radar_pcl = None
    else:
        radar_pcl = None
    return radar_pcl

def ReadRegistrationMatrix(registration_matrix_dir):
    """
    Function:
        Read the calibration matrix

    Args:
        registration_matrix_dir             ->          registration matrix directory
    """
    filename = registration_matrix_dir + "calibration_matrix.npy"
    if os.path.exists(filename):
        registr_mat = np.load(filename)
    else:
        raise ValueError("Pleaaaaaaase calibrate the sensors before you do this")
    return registr_mat

def GaussianModel(pcl):
    """
    Function:
        Get the center and covariance from gaussian model.

    Args:
        pcl             ->          point cloud data
    """
    model = mixture.GaussianMixture(n_components=1, covariance_type='full')
    model.fit(pcl)
    return model.means_[0], model.covariances_[0]

def DrawEllipse(means, covariances, scale_factor=1):
    """
    Function:
        Draw 2D Gaussian Ellipse.

    Args:
        means           ->          center of the Gaussian
        covariances     ->          covariance of Gaussian
    """
    eigen, eigen_vec = np.linalg.eig(covariances)
    eigen_root_x = np.sqrt(1 + eigen[0]) * scale_factor
    eigen_root_y = np.sqrt(1 - eigen[1]) * scale_factor
    ell = Ellipse(xy = (means[0], means[1]), width = eigen_root_x,
                height = eigen_root_y, angle = np.rad2deg(np.arccos(eigen_vec[0,0])),
                facecolor = 'none', edgecolor = 'red')
    return ell

def DbscanDenoise(pcl, epsilon=0.3, minimum_samples=100):
    """
    Function:
        Using DBSCAN for filtering out the noise data.

    Args:
        pcl             ->          point cloud to be denoised
        epsilon         ->          maximum distance to be considered as an object
        minimum_samples ->          miminum points to be considered as an object
    """
    clustering = DBSCAN(eps=epsilon, min_samples=minimum_samples).fit(pcl)
    output_labels = clustering.labels_
    output_pcl = []
    for label_i in np.unique(output_labels):
        if label_i == -1:
            continue
        output_pcl.append(pcl[output_labels == label_i])
    return output_pcl

def AddonesToLstCol(target_array):
    """
    Function:
        Add ones to the last column of the target array

    Args:
        target_array            ->          array to be changed
    """
    adding_ones = np.ones([target_array.shape[0], 1])
    output_array = np.concatenate([target_array, adding_ones], axis=-1)
    return output_array

def CalibrateStereoToRadar(stereo_pcl, registration_matrix):
    """
    Function:
        Transfer stereo point cloud to radar frame.
    
    Args:
        stereo_pcl              ->          stereo point cloud
        registration_matrix     ->          registration matrix 
    """
    return np.matmul(AddonesToLstCol(stereo_pcl), registration_matrix)

def EuclideanDist(point1, point2):
    """
    Function:
        Calculate EuclideanDist
    """
    return np.sqrt(np.sum(np.square(point1 - point2), axis=-1))

def applyMask(image, mask, color, alpha=0.5):
    """
    Apply the given mask to the image.
    """
    for c in range(3):
        image[:, :, c] = np.where(mask == 1,
                                 image[:, :, c] *
                                (1 - alpha) + alpha * color[c] * 255,
                                 image[:, :, c])
    return image

###################### PLOTTING FUNCTIONS START #########################
def prepareFigure(num_axes):
    assert num_axes <= 4
    plt.ion()
    fig = plt.figure()
    if num_axes == 1:
        ax1 = fig.add_subplot(111)
        return fig, ax1
    if num_axes == 2:
        ax1 = fig.add_subplot(121)
        ax2 = fig.add_subplot(122)
        return fig, ax1, ax2
    if num_axes == 3:
        ax1 = fig.add_subplot(131)
        ax2 = fig.add_subplot(132)
        ax3 = fig.add_subplot(133)
        return fig, ax1, ax2, ax3
    if num_axes == 4:
        ax1 = fig.add_subplot(221)
        ax2 = fig.add_subplot(222)
        ax3 = fig.add_subplot(223)
        ax4 = fig.add_subplot(224)
        return fig, ax1, ax2, ax3, ax4

def clearAxes(ax_list):
    assert len(ax_list) >=1 
    plt.cla()
    for ax_i in ax_list:
        ax_i.clear()

def createColorList(class_names, class_list, all_colors):
    color_output_list = []
    for i in range(len(class_list)):
        current_class = class_list[i]
        color_i = np.array([all_colors[class_names.index(current_class)]])
        color_output_list.append(color_i)
    return color_output_list
 
def createLabelList(class_list, label_prefix):
    label_list = []
    for i in range(len(class_list)):
        current_class = class_list[i]
        label_list.append(label_prefix + "_" + current_class)
    return label_list

def pointScatter(bg_points, point_list, color_list, label_list, ax, xlimits, ylimits, title):
    assert len(point_list) == len(color_list)
    assert len(point_list) == len(label_list)
    if bg_points is not None:
        ax.scatter(bg_points[:, 0], bg_points[:, 1], s=0.2, c='blue', label="background")
    for i in range(len(point_list)):
        points_i = point_list[i]
        color_i = color_list[i]
        label_i = label_list[i]
        ax.scatter(points_i[:, 0], points_i[:,1], s=0.3, c=color_i, label=label_i)
    ax.set_xlim(xlimits)
    ax.set_ylim(ylimits)
    ax.legend()
    ax.set_title(title)

def imgPlot(img, ax, title):
    ax.imshow(img)
    ax.axis('off')
    ax.set_title(title)

def keepDrawing(fig, time_duration):
    fig.canvas.draw()
    plt.pause(time_duration)

def saveFigure(save_dir, name_prefix, frame_i):
    plt.savefig(save_dir + name_prefix + str(frame_i) + ".png")
###################### PLOTTING FUNCTIONS END #########################
   
def main(stereo_obj_pcl_dir, stereo_mrcnn_img_dir, radar_pcl_dir, registration_matrix_dir, 
        sequence, if_plot=True, if_save=True):
    """
    MAIN FUNCTION
    """
    ##### frame delay between two sensors (not sure whether it exists) #####
    frame_delay = 0

    if if_plot:
        fig, ax1 = prepareFigure(1)

    all_colors = RandomColors(len(CLASS_NAMES))
    registr_mat = ReadRegistrationMatrix(registration_matrix_dir)
    for frame_i in tqdm(range(sequence[0], sequence[1])):
        stereo_obj_pcl = ReadStereoObjPcl(stereo_obj_pcl_dir, frame_i + frame_delay) 
        radar_pcl = ReadRadarPcl(radar_pcl_dir, frame_i)
        radar_pcl_2d = np.delete(radar_pcl, 1, -1)
        if stereo_obj_pcl is None or radar_pcl is None:
            print("Frame %d has no object detected."%(frame_i))
            continue
        clearAxes([ax1])
        ax1.scatter(radar_pcl_2d[:, 0], radar_pcl_2d[:, 1], s=0.2, c='blue')
        for class_i in range(len(stereo_obj_pcl['class'])):
            current_class = stereo_obj_pcl['class'][class_i]
            stereo_pcl = stereo_obj_pcl['pcl'][class_i]
            if len(stereo_pcl) == 0:
                continue
            color_i = all_colors[CLASS_NAMES.index(current_class)]
            stereo_pcl_dbscan = DbscanDenoise(stereo_pcl, epsilon=0.5, 
                                                minimum_samples=50)
            for obj_i in range(len(stereo_pcl_dbscan)):
                current_stereo_obj = stereo_pcl_dbscan[obj_i]
                stereo_obj_to_radar = CalibrateStereoToRadar(current_stereo_obj, \
                                                            registr_mat)
                ax1.scatter(stereo_obj_to_radar[:,0], stereo_obj_to_radar[:,1], s=0.1,\
                            c="r")
        ax1.set_xlim([-30, 30])
        ax1.set_ylim([0, 50])
        keepDrawing(fig, 0.1)
                
if __name__ == "__main__":
    stereo_mrcnn_img_dir = "./stereo_object_detection_output/"
    stereo_obj_pcl_dir = "./stereo_object_pcl/"
    radar_pcl_dir = "../radar_process/radar_ral_process/radar_pcl/"
    registration_matrix_dir = "../registration/registration_matrix/"
    sequence = [0, 500]
    main(stereo_obj_pcl_dir, stereo_mrcnn_img_dir, radar_pcl_dir, registration_matrix_dir, 
        sequence, if_plot=True, if_save=True)
