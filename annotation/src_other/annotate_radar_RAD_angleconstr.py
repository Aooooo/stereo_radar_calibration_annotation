import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
import cv2
import math
import random
import pickle
import colorsys

from sklearn import mixture
from sklearn.cluster import DBSCAN
from collections import Counter

from glob import glob
from tqdm import tqdm

# detectron2 all classes
CLASS_NAMES = ['person', 'bicycle', 'car', 'motorcycle', 'airplane', 'bus', \
        'train', 'truck', 'boat', 'traffic light', 'fire hydrant', 'stop sign', \
        'parking meter', 'bench', 'bird', 'cat', 'dog', 'horse', 'sheep', 'cow', \
        'elephant', 'bear', 'zebra', 'giraffe', 'backpack', 'umbrella', 'handbag', \
        'tie', 'suitcase', 'frisbee', 'skis', 'snowboard', 'sports ball', 'kite', \
        'baseball bat', 'baseball glove', 'skateboard', 'surfboard', \
        'tennis racket', 'bottle', 'wine glass', 'cup', 'fork', 'knife', 'spoon', \
        'bowl', 'banana', 'apple', 'sandwich', 'orange', 'broccoli', 'carrot', \
        'hot dog', 'pizza', 'donut', 'cake', 'chair', 'couch', 'potted plant', \
        'bed', 'dining table', 'toilet', 'tv', 'laptop', 'mouse', 'remote', \
        'keyboard', 'cell phone', 'microwave', 'oven', 'toaster', 'sink', \
        'refrigerator', 'book', 'clock', 'vase', 'scissors', 'teddy bear', \
        'hair drier', 'toothbrush']

# original mrcnn all classes
# CLASS_NAMES = ['BG', 'person', 'bicycle', 'car', 'motorcycle', 'airplane',
            # 'bus', 'train', 'truck', 'boat', 'traffic light',
            # 'fire hydrant', 'stop sign', 'parking meter', 'bench', 'bird',
            # 'cat', 'dog', 'horse', 'sheep', 'cow', 'elephant', 'bear',
            # 'zebra', 'giraffe', 'backpack', 'umbrella', 'handbag', 'tie',
            # 'suitcase', 'frisbee', 'skis', 'snowboard', 'sports ball',
            # 'kite', 'baseball bat', 'baseball glove', 'skateboard',
            # 'surfboard', 'tennis racket', 'bottle', 'wine glass', 'cup',
            # 'fork', 'knife', 'spoon', 'bowl', 'banana', 'apple',
            # 'sandwich', 'orange', 'broccoli', 'carrot', 'hot dog', 'pizza',
            # 'donut', 'cake', 'chair', 'couch', 'potted plant', 'bed',
            # 'dining table', 'toilet', 'tv', 'laptop', 'mouse', 'remote',
            # 'keyboard', 'cell phone', 'microwave', 'oven', 'toaster',
            # 'sink', 'refrigerator', 'book', 'clock', 'vase', 'scissors',
            # 'teddy bear', 'hair drier', 'toothbrush']

ROAD_USERS = ['person', 'bicycle', 'car', 'motorcycle',
            'bus', 'train', 'truck', 'boat']

# Radar Configuration
RADAR_CONFIG_FREQ = 77 # GHz
DESIGNED_FREQ = 76.8 # GHz
RANGE_RESOLUTION = 0.1953125 # m
VELOCITY_RESOLUTION = 0.41968030701528203 # m/s
RANGE_SIZE = 256
DOPPLER_SIZE = 64
AZIMUTH_SIZE = 256
ANGULAR_RESOLUTION = np.pi / 2 / AZIMUTH_SIZE # radians

def RandomColors(N, bright=True): 
    """
    Generate random colors.
    To get visually distinct colors, generate them in HSV space then
    convert to RGB.
    """
    brightness = 1.0 if bright else 0.7
    hsv = [(i / N, 1, brightness) for i in range(N)]
    colors = list(map(lambda c: colorsys.hsv_to_rgb(*c), hsv))
    # detectron uses random shuffle to give the differences
    random.seed(8888)
    random.shuffle(colors)
    return colors

def findAIndex(FOV_lims):
    """
    Find azimuth indexes according to the FOV limitations
    """
    assert len(FOV_lims) == 2
    lower_bound = np.amin(FOV_lims) / 180. * np.pi
    upper_bound = np.amax(FOV_lims) / 180. * np.pi

    lower_ind = int(np.round((np.sin(lower_bound) * (2*np.pi*0.5*RADAR_CONFIG_FREQ/DESIGNED_FREQ) + \
                np.pi) / (2*np.pi/AZIMUTH_SIZE)))
    upper_ind = int(np.round((np.sin(upper_bound) * (2*np.pi*0.5*RADAR_CONFIG_FREQ/DESIGNED_FREQ) + \
                np.pi) / (2*np.pi/AZIMUTH_SIZE)))
    return lower_ind, upper_ind

def readRAD(radar_dir, frame_id):
    """
    Function:
        Get the 3D FFT results of the current frame.
    
    Args:
        radar_dir           ->          radar directory
        frame_id            ->          frame that is about to be processed.
    """
    if os.path.exists(os.path.join(radar_dir, "%.6d.npy"%(frame_id))):
        return np.load(os.path.join(radar_dir, "%.6d.npy"%(frame_id)))
    else:
        return None

def getMagnitude(target_array, power_order=2):
    """
    Function:
        Get the magnitude of the complex array
    """
    target_array = np.abs(target_array)
    target_array = pow(target_array, power_order)
    return target_array 

def getLog(target_array, scalar=1., log_10=True):
    """
    Function:
        Get the log of the complex array
    """
    if log_10:
        return scalar * np.log10(target_array + 1.)
    else:
        return target_array

def ReadStereoObjPcl(stereo_obj_pcl_dir, frame_i):
    """
    Function:
        Read the stereo object detection point cloud w.r.t frame i

    Args:
        stereo_obj_pcl_dir          ->          stereo point cloud directory
        frame_i                     ->          frame id

    Attention:
        The output stereo_obj_pcl has two keys, 
        "class" : current object class string name 
        "pcl" : current object point cloud with format (num_pnts, 3) 
    """
    filename = stereo_obj_pcl_dir + "stereo_obj_pcl_" + str(frame_i) + ".pickle"
    if os.path.exists(filename):
        with open(filename, "rb") as f:
            stereo_obj_pcl = pickle.load(f)
        if len(stereo_obj_pcl['class']) == 0:
            stereo_obj_pcl = None
    else:
        stereo_obj_pcl = None
    return stereo_obj_pcl

def ReadStereoMrcnnImg(stereo_mrcnn_img_dir, frame_i):
    """
    Function:
        Read mask rcnn results on stereo left img
    
    Args:
        stereo_mrcnn_img_dir            ->          stereo mask rcnn result imgs
        frame_i                         ->          frame id
    """
    filename = stereo_mrcnn_img_dir + "stereo_mrcnn_img_" + str(frame_i) + ".jpg"
    if os.path.exists(filename):
        img = cv2.imread(filename)[..., ::-1]
    else:
        img = None
    return img
    
def ReadRADMask(mask_dir, frame_i):
    """
    Function:
        Read the Range-Azimuth-Doppler mask from directory.

    Args:
        mask_dir            ->          directory that saves masks
    """
    filename = mask_dir + "RAD_mask_%.d.npy"%(frame_i)
    if os.path.exists(filename):
        RAD_mask = np.load(filename)
    else:
        RAD_mask = None
    return RAD_mask

def WriteRadarObjPcl(radar_annotate_dir, radar_dict, frame_i):
    """
    Function:
        Write the annotated radar point cloud dictionary into the directory

    Args:
        radar_annotate_dir          ->          directory for saving
        frame_i                     ->          frame id
    """
    with open(radar_annotate_dir + "radar_obj_" + str(frame_i) + ".pickle", "wb") as f:
        pickle.dump(radar_dict, f)

def ReadRegistrationMatrix(registration_matrix_dir):
    """
    Function:
        Read the calibration matrix

    Args:
        registration_matrix_dir             ->          registration matrix directory
    """
    filename = registration_matrix_dir + "calibration_matrix.npy"
    if os.path.exists(filename):
        registr_mat = np.load(filename)
    else:
        raise ValueError("Pleaaaaaaase calibrate the sensors before you do this")
    return registr_mat

def GaussianModel(pcl):
    """
    Function:
        Get the center and covariance from gaussian model.

    Args:
        pcl             ->          point cloud data
    """
    model = mixture.GaussianMixture(n_components=1, covariance_type='full')
    model.fit(pcl)
    return model.means_[0], model.covariances_[0]

def DbscanDenoise(pcl, epsilon=0.3, minimum_samples=100, dominant_op=False):
    """
    Function:
        Using DBSCAN for filtering out the noise data.

    Args:
        pcl             ->          point cloud to be denoised
        epsilon         ->          maximum distance to be considered as an object
        minimum_samples ->          miminum points to be considered as an object
    """
    clustering = DBSCAN(eps=epsilon, min_samples=minimum_samples).fit(pcl)
    output_labels = clustering.labels_
    if not dominant_op:
        output_pcl = []
        for label_i in np.unique(output_labels):
            if label_i == -1:
                continue
            output_pcl.append(pcl[output_labels == label_i])
    else:
        if len(np.unique(output_labels)) == 1:
            output_pcl = np.zeros([0,3])
        else:
            counts = Counter(output_labels)
            output_pcl = pcl[output_labels == counts.most_common(1)[0][0]]
    return output_pcl

def AddonesToLstCol(target_array):
    """
    Function:
        Add ones to the last column of the target array

    Args:
        target_array            ->          array to be changed
    """
    adding_ones = np.ones([target_array.shape[0], 1])
    output_array = np.concatenate([target_array, adding_ones], axis=-1)
    return output_array

def CalibrateStereoToRadar(stereo_pcl, registration_matrix):
    """
    Function:
        Transfer stereo point cloud to radar frame.
    
    Args:
        stereo_pcl              ->          stereo point cloud
        registration_matrix     ->          registration matrix 
    """
    return np.matmul(AddonesToLstCol(stereo_pcl), registration_matrix)

def EuclideanDist(point1, point2):
    """
    Function:
        Calculate EuclideanDist
    """
    return np.sqrt(np.sum(np.square(point1 - point2), axis=-1))

def getSumDim(target_array, target_axis):
    """
    Function:
        Sum up one dimension of a  3D matrix.
    
    Args:
        column_index            ->          which column to be deleted
    """
    output = np.sum(target_array, axis=target_axis)
    return output 

def transfer2Scatter(RA_mask):
    """
    Function:
        Transfer RD indexes to pcl, for verifying quality

    Args:
        RA_mask              ->          Range-Azimuth mask
    """
    output_pcl = []
    for i in range(RA_mask.shape[0]):
        for j in range(RA_mask.shape[1]):
            if RA_mask[i, j] == 1:
                # point_range = ((RANGE_SIZE-1) - i) * RANGE_RESOLUTION
                # point_angle = (j - (AZIMUTH_SIZE/2)) * ANGULAR_RESOLUTION
                # ####################### Prince's method ######################
                point_range = ((RANGE_SIZE-1) - i) * RANGE_RESOLUTION
                point_angle = (j * (2*np.pi/AZIMUTH_SIZE) - np.pi) / \
                                (2*np.pi*0.5*RADAR_CONFIG_FREQ/DESIGNED_FREQ)
                point_angle = np.arcsin(point_angle)
                # ##############################################################
                point_zx = polarToCartesian(point_range, point_angle)
                output_pcl.append(np.array([[point_zx[1], point_zx[0]]]))
    if len(output_pcl) != 0:
        output_pcl = np.concatenate(output_pcl, axis=0)
    return output_pcl

def generateRACartesianImage(RA_img, RA_mask):
    """
    Function:
        Generate RA image in Cartesian Coordinate.

    Args:
        RA_img          ->          RA FFT magnitude
        RA_mask         ->          Mask generated 
    """
    output_img = np.zeros([RA_img.shape[0], RA_img.shape[0]*2])
    for i in range(RA_img.shape[0]):
        for j in range(RA_img.shape[1]):
            if RA_mask[i,j] == 1:
                point_range = ((RANGE_SIZE-1) - i) * RANGE_RESOLUTION
                point_angle = (j - (AZIMUTH_SIZE/2)) * ANGULAR_RESOLUTION
                point_zx = polarToCartesian(point_range, point_angle)
                new_i = int(output_img.shape[0] - \
                        np.round(point_zx[0]/RANGE_RESOLUTION)-1)
                new_j = int(np.round((point_zx[1]+50)/RANGE_RESOLUTION)-1)
                output_img[new_i,new_j] = RA_img[i,j] 
    norm_sig = plt.Normalize()
    # color mapping 
    output_img = plt.cm.viridis(norm_sig(output_img))
    output_img = output_img[..., :3]
    return output_img

def transferMaskToCartesianMask(RA_mask):
    """
    Function:
        Transfer polar mask to cartesian mask.
    
    Args:
        RA_mask        ->          polar mask.
    """
    output_mask = np.zeros([RA_mask.shape[0], RA_mask.shape[0]*2])
    for i in range(RA_mask.shape[0]):
        for j in range(RA_mask.shape[1]):
            if RA_mask[i,j] == 1:
                point_range = ((RANGE_SIZE-1) - i) * RANGE_RESOLUTION
                # point_angle = (j - (AZIMUTH_SIZE/2)) * ANGULAR_RESOLUTION
                ####################### Prince's method ######################
                point_angle = (j * (2*np.pi/AZIMUTH_SIZE) - np.pi) / \
                                (2*np.pi*0.5*RADAR_CONFIG_FREQ/DESIGNED_FREQ)
                point_angle = np.arcsin(point_angle)
                ##############################################################
                point_zx = polarToCartesian(point_range, point_angle)
                new_i = int(output_mask.shape[0] - \
                        np.round(point_zx[0]/RANGE_RESOLUTION)-1)
                new_j = int(np.round((point_zx[1]+50)/RANGE_RESOLUTION)-1)
                output_mask[new_i,new_j] = 1.
    output_mask = np.where(output_mask > 0., 1., 0.)
    return output_mask

def getMaskEllipse(mask, color, scale_factor=1):
    """
    Function: get ellipse for current object mask.
    """
    assert len(mask.shape) == 2
    ind_pnts = []
    for i in range(mask.shape[0]):
        for j in range(mask.shape[1]):
            if mask[i, j] == 1:
                ind_pnts.append([j, i])
    ind_pnts = np.array(ind_pnts)
    obj_mean, obj_cov = GaussianModel(ind_pnts)
    ell = getEllipse(np.array(color[0]), obj_mean, \
                            obj_cov, scale_factor=4)
    return ell

def InstancizeRADMask(radar_RAD_mask):
    """
    Function:
        Separate RAD mask w.r.t each instance.

    Args:
        radar_RAD_mask          ->          RAD mask
    """
    indexes_clutters = []
    for i in range(radar_RAD_mask.shape[0]):
        for j in range(radar_RAD_mask.shape[1]):
            for k in range(radar_RAD_mask.shape[2]):
                if radar_RAD_mask[i,j,k] != 0:
                    indexes_clutters.append(np.array([i,j,k]))
    indexes_clutters = np.array(indexes_clutters)
    indexes_instances = DbscanDenoise(indexes_clutters, epsilon=10, minimum_samples=5)
    instances_masks = []
    for instance_i in range(len(indexes_instances)):
        current_instance = indexes_instances[instance_i]
        instance_mask = np.zeros(radar_RAD_mask.shape)
        for pnt_i in range(len(current_instance)):
            pnt_ind = current_instance[pnt_i]
            instance_mask[pnt_ind[0], pnt_ind[1], pnt_ind[2]] = 1.
        # instance_center = np.mean(current_instance, axis=0)
        # instances_masks.append([instance_mask, instance_center])
        instances_masks.append([instance_mask, current_instance])
    return instances_masks

def InstancizeRADMask2D1D(radar_RAD_mask):
    """
    Function:
        Separate RAD mask w.r.t each instance.

    Args:
        radar_RAD_mask          ->          RAD mask
    """
    RD_mask = np.where(getSumDim(radar_RAD_mask, 1) >= 1, 1, 0)
    RA_mask = np.where(getSumDim(radar_RAD_mask, -1) >= 1, 1, 0)
    RD_indexes = []
    RA_indexes = []
    for i in range(RD_mask.shape[0]):
        for j in range(RD_mask.shape[1]):
            if RD_mask[i, j] != 0:
                RD_indexes.append(np.array([i,j]))
    for i in range(RA_mask.shape[0]):
        for j in range(RA_mask.shape[1]):
            if RA_mask[i, j] != 0:
                RA_indexes.append(np.array([i,j]))
    RD_indexes = np.array(RD_indexes)
    RA_indexes = np.array(RA_indexes)

    RD_indexes_instances = DbscanDenoise(RD_indexes, epsilon=5, minimum_samples=5)

    RA_indexes_instances = DbscanDenoise(RA_indexes, epsilon=10, minimum_samples=5)

    instances_masks = []
    for RD_inst_i in range(len(RD_indexes_instances)):
        current_RD_instance = RD_indexes_instances[RD_inst_i]
        for RA_inst_i in range(len(RA_indexes_instances)):
            current_RA_instance = RA_indexes_instances[RA_inst_i]
            instance_mask = np.zeros(radar_RAD_mask.shape)
            current_instance = []
            for pnt_i in range(len(current_RD_instance)):
                current_RD_pnt = current_RD_instance[pnt_i]
                for pnt_j in range(len(current_RA_instance)):
                    current_RA_pnt = current_RA_instance[pnt_j]
                    if current_RD_pnt[0] == current_RA_pnt[0]:
                        instance_mask[current_RD_pnt[0], current_RA_pnt[1], current_RD_pnt[1]] = 1.
                        current_pnt = np.array([current_RD_pnt[0], current_RA_pnt[1], \
                                                    current_RD_pnt[1]])
                        current_instance.append(current_pnt)
            current_instance = np.array(current_instance)
            if np.amax(instance_mask) != 0:
                instances_masks.append([instance_mask, current_instance])
    return instances_masks

def FindRadarObjInstance(rad_mask_instance, stereo_all_pcls, stereo_all_classes, \
                        stereo_all_obj_index):
    """
    Function:
        Use calibrated stereo points to find the corresponding radar points

    Args:
        rad_mask_instance          ->          format[radar_mask, radar_mask_indexes]
        stereo_all_pcls            ->          stereo all objects' pcl
        stereo_all_classes         ->          all classes indexes w.r.t stereo_all_pcls
        stereo_all_obj_index       ->          all stereo object indexes numbers
    """
    #### testing thresholds #####
    distance_threshold = 3. 
    overlap_dist_threshold = 5.

    radar_obj_mask = rad_mask_instance[0]
    radar_obj_indexes = rad_mask_instance[1]
    ########################## Prince's Method ################################
    point_range = ((RANGE_SIZE - 1) - radar_obj_indexes[:,0]) * RANGE_RESOLUTION
    point_angle = (radar_obj_indexes[:, 1] * (2*np.pi/AZIMUTH_SIZE) - np.pi) / \
                    (2*np.pi*0.5*RADAR_CONFIG_FREQ/DESIGNED_FREQ)
    point_angle = np.arcsin(point_angle)
    point_zx = polarToCartesian(point_range, point_angle)
    radar_xz = np.concatenate([np.expand_dims(point_zx[1], -1), \
            np.expand_dims(point_zx[0], -1)], -1)
    radar_xz_mean, _ = GaussianModel(radar_xz)
    
    potential_stereo = []
    potential_stereo_numinthres = []
    for st_idx in range(len(stereo_all_pcls)):
        stereo_obj_pcl = stereo_all_pcls[st_idx] 
        stereo_obj_classidx = stereo_all_classes[st_idx] 
        stereo_obj_idx = stereo_all_obj_index[st_idx]
        min_distance = 100.
        if len(stereo_obj_pcl) == 0:
            continue
        for pnt_id in range(len(radar_xz)):
            distance_diff = np.sqrt(np.sum(np.square(stereo_obj_pcl - \
                            radar_xz[pnt_id]), axis=-1))
            min_distance = np.amin([np.amin(distance_diff), min_distance])
        if min_distance <= distance_threshold:
            distance_diff2center = np.sqrt(np.sum(np.square(stereo_obj_pcl - \
                            radar_xz_mean), axis=-1))
            mask_inthreshold = np.where(distance_diff2center <= overlap_dist_threshold, \
                            1., 0.)
            num_inthreshold = np.count_nonzero(mask_inthreshold)
            potential_stereo.append([stereo_obj_pcl, stereo_obj_classidx, \
                                    stereo_obj_idx])
            potential_stereo_numinthres.append(num_inthreshold)
    if len(potential_stereo) > 1:
        right_match_idx = np.argmax(potential_stereo_numinthres)
        stereo_right = potential_stereo[right_match_idx]
        return radar_obj_mask, stereo_right[1], stereo_right[2]
    elif len(potential_stereo) == 0:
        return None, None, None
    else:
        stereo_right = potential_stereo[0]
        return radar_obj_mask, stereo_right[1], stereo_right[2]
        
def applyMask(image, mask, color, alpha=0.5):
    """
    Apply the given mask to the image.
    """
    for c in range(3):
        image[:, :, c] = np.where(mask == 1,
                                 image[:, :, c] *
                                (1 - alpha) + alpha * color[c] * 255,
                                 image[:, :, c])
    return image

##################### coordinate transformation ######################
def cartesianToPolar(x, y):
    rho = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    return(rho, phi)

def polarToCartesian(rho, phi):
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return(x, y)

###################### PLOTTING FUNCTIONS START #########################
def prepareFigure(num_axes):
    assert num_axes <= 4
    plt.ion()
    fig = plt.figure()
    if num_axes == 1:
        ax1 = fig.add_subplot(111)
        return fig, [ax1]
    if num_axes == 2: 
        ax1 = fig.add_subplot(121)
        ax2 = fig.add_subplot(122)
        return fig, [ax1, ax2]
    if num_axes == 3:
        ax1 = fig.add_subplot(131)
        ax2 = fig.add_subplot(132)
        ax3 = fig.add_subplot(133)
        return fig, [ax1, ax2, ax3]
    if num_axes == 4:
        ax1 = fig.add_subplot(221)
        ax2 = fig.add_subplot(222)
        ax3 = fig.add_subplot(223)
        ax4 = fig.add_subplot(224)
        return fig, [ax1, ax2, ax3, ax4]

def clearAxes(ax_list):
    assert len(ax_list) >=1 
    plt.cla()
    for ax_i in ax_list:
        ax_i.clear()

def createColorList(class_names, class_list, all_colors):
    color_output_list = []
    for i in range(len(class_list)):
        current_class = class_list[i]
        color_i = np.array([all_colors[class_names.index(current_class)]])
        color_output_list.append(color_i)
    return color_output_list
 
def createLabelList(class_list, label_prefix):
    label_list = []
    for i in range(len(class_list)):
        current_class = class_list[i]
        label_list.append(label_prefix + "_" + current_class)
    return label_list

def pointScatter(bg_points, point_list, color_list, label_list, ax, xlimits, ylimits, title):
    assert len(point_list) == len(color_list)
    assert len(point_list) == len(label_list)
    if bg_points is not None:
        ax.scatter(bg_points[:, 0], bg_points[:, 1], s=0.2, c='blue', label="background")
    for i in range(len(point_list)):
        points_i = point_list[i]
        color_i = color_list[i]
        label_i = label_list[i]
        ax.scatter(points_i[:, 0], points_i[:,1], s=0.3, c=color_i, label=label_i)
    ax.set_xlim(xlimits)
    ax.set_ylim(ylimits)
    ax.legend()
    ax.set_title(title)

def pclScatter(pcl_list, color_list, label_list, ax, xlimits, ylimits, title):
    assert len(pcl_list) == len(color_list)
    if label_list is not None:
        assert len(pcl_list) == len(label_list)
    for i in range(len(pcl_list)):
        pcl = pcl_list[i]
        color = color_list[i]
        if label_list == None:
            label = None
        else:
            label = label_list[i]
        ax.scatter(pcl[:, 0], pcl[:, 1], s=1, c=color, label=label)
    ax.set_xlim(xlimits)
    ax.set_ylim(ylimits)
    ax.set_title(title)

def imgPlot(img, ax, cmap, alpha, title=None):
    ax.imshow(img, cmap=cmap, alpha=alpha)
    ax.axis('off')
    if title is not None:
        ax.set_title(title)

def keepDrawing(fig, time_duration):
    fig.canvas.draw()
    plt.pause(time_duration)

def norm2Image(array):
    norm_sig = plt.Normalize()
    img = plt.cm.viridis(norm_sig(array))
    img *= 255.
    img = img.astype(np.uint8)
    return img

def DrawEllipse(ax, color, means, covariances, scale_factor=1):
    """
    Function:
        Draw 2D Gaussian Ellipse.

    Args:
        means           ->          center of the Gaussian
        covariances     ->          covariance of Gaussian
    """
    sign = np.sign(means[0] / means[1])
    eigen, eigen_vec = np.linalg.eig(covariances)

    eigen_root_x = np.sqrt(eigen[0]) * scale_factor
    eigen_root_y = np.sqrt(eigen[1]) * scale_factor
    theta = np.degrees(np.arctan2(*eigen_vec[:,0][::-1]))

    ell = Ellipse(xy = (means[0], means[1]), width = eigen_root_x,
                height = eigen_root_y, angle = \
                theta, \
                # np.rad2deg (-sign * np.arccos(eigen_vec[0,0])), \
                facecolor = 'none', edgecolor = 1. - color)
    ax.add_artist(ell)

def getEllipse(color, means, covariances, scale_factor=1):
    """
    Function:
        Draw 2D Gaussian Ellipse.

    Args:
        means           ->          center of the Gaussian
        covariances     ->          covariance of Gaussian
    """
    sign = np.sign(means[0] / means[1])
    eigen, eigen_vec = np.linalg.eig(covariances)

    eigen_root_x = np.sqrt(eigen[0]) * scale_factor
    eigen_root_y = np.sqrt(eigen[1]) * scale_factor
    theta = np.degrees(np.arctan2(*eigen_vec[:,0][::-1]))

    ell = Ellipse(xy = (means[0], means[1]), width = eigen_root_x,
                height = eigen_root_y, angle = theta, \
                facecolor = 'none', edgecolor = 1. - color)
    return ell

def saveFigure(save_dir, name_prefix, frame_i):
    plt.savefig(save_dir + name_prefix + str(frame_i) + ".png")
###################### PLOTTING FUNCTIONS END #########################
  
def main(stereo_obj_pcl_dir, stereo_mrcnn_img_dir,  radar_ral_dir, radar_mask_dir, registration_matrix_dir, 
        radar_annotate_dir, sequence, if_plot=True, if_save=True):
    """
    MAIN FUNCTION
    """
    ##### frame delay between two sensors (not sure whether it exists) #####
    frame_delay = 0
    distance_threshold = 5.0
    name_prefix = "visualization_"

    if if_plot:
        fig, axes = prepareFigure(4)
    else:
        fig, axes = prepareFigure(2)

    all_colors = RandomColors(len(CLASS_NAMES))
    registr_mat = ReadRegistrationMatrix(registration_matrix_dir)
    azimuthind_min, azimuthind_max = findAIndex(FOV_lims)
    for frame_i in tqdm(range(sequence[0], sequence[1])):
        stereo_obj_pcl = ReadStereoObjPcl(stereo_obj_pcl_dir, frame_i + frame_delay) 
        radar_RAD_mask = ReadRADMask(radar_mask_dir, frame_i)

        RAD_FFT = readRAD(os.path.join(radar_ral_dir, "RAD_numpy"), frame_i)
        if RAD_FFT is None:
            continue
        RAD_FFT = getMagnitude(RAD_FFT,  power_order=2)
        RAD_FFT = RAD_FFT[:, azimuthind_min:azimuthind_max, :]
        RD_img = norm2Image(getLog(getSumDim(RAD_FFT, 1), scalar=10, log_10=True))
        RA_img = norm2Image(getLog(getSumDim(RAD_FFT, -1), scalar=10, log_10=True))

        if stereo_obj_pcl is None or radar_RAD_mask is None:
            print("Frame %d has no object detected."%(frame_i))
            continue
        ########## remove all static points, keep moving points #########
        radar_RAD_mask[:, :, int(radar_RAD_mask.shape[2]/2)] = 0.
        ################################################################
        
        radar_mask_instances = InstancizeRADMask(radar_RAD_mask)
        # radar_mask_instances = InstancizeRADMask2D1D(radar_RAD_mask)
        if len(radar_mask_instances) == 0:
            continue
   
        stereo_all_pcls = []
        stereo_all_classes = []
        stereo_all_obj_index = []
        for class_i in range(len(stereo_obj_pcl['class'])):
            current_class = stereo_obj_pcl['class'][class_i]
            if current_class not in ROAD_USERS:
                continue
            current_class_ind = CLASS_NAMES.index(current_class)
            stereo_pcl = stereo_obj_pcl['pcl'][class_i]
            if len(stereo_pcl) == 0:
                continue
            ###################### non-calibrated ###################
            # stereo_to_radar = np.delete(stereo_pcl, 1, -1)
            stereo_pcl = DbscanDenoise(stereo_pcl, epsilon=0.8, \
                                minimum_samples=5, dominant_op=True)
            ###################### calibrated #######################
            stereo_to_radar = CalibrateStereoToRadar(stereo_pcl, \
                                                    registr_mat)
            if len(stereo_to_radar) == 0:
                continue
            stereo_pcl_dbscan = DbscanDenoise(stereo_to_radar, epsilon=0.3, \
                                minimum_samples=5, dominant_op=True)
            if len(stereo_pcl_dbscan) == 0:
                continue
            stereo_all_pcls.append(stereo_pcl_dbscan)
            stereo_all_classes.append(current_class_ind)
            stereo_all_obj_index.append(class_i)

        radar_dict = {}
        radar_classes = []
        radar_masks = []

        radar_classes_original = []
        radar_masks_original = []
        radar_stereo_idx = []
        for inst_idx in range(len(radar_mask_instances)):
            rad_mask_instance = radar_mask_instances[inst_idx]
            rad_mask, class_ind, obj_ind = FindRadarObjInstance(rad_mask_instance, \
                    stereo_all_pcls, stereo_all_classes, stereo_all_obj_index)
            if class_ind is None:
                continue
            radar_classes_original.append(class_ind)
            radar_stereo_idx.append(obj_ind)
            radar_masks_original.append(rad_mask)
        if len(radar_masks_original) == 0:
            continue
        unique_indexes = np.unique(radar_stereo_idx)
        for u_i in range(len(unique_indexes)):
            u_idx = unique_indexes[u_i]
            obj_RAD_mask = np.zeros(radar_RAD_mask.shape)
            for orig_i in range(len(radar_stereo_idx)):
                if radar_stereo_idx[orig_i] == u_idx:
                    obj_RAD_mask += radar_masks_original[orig_i]
                    obj_RAD_class = CLASS_NAMES[radar_classes_original[orig_i]]
            obj_RAD_mask = np.where(obj_RAD_mask > 0, 1., 0.)
            radar_masks.append(np.expand_dims(obj_RAD_mask, 0))
            radar_classes.append(obj_RAD_class)

        if len(radar_masks) != 0:
            radar_masks = np.concatenate(radar_masks, 0)
            radar_dict["classes"] = radar_classes
            radar_dict["masks"] = radar_masks

            if if_save:
                WriteRadarObjPcl(radar_annotate_dir, radar_dict, frame_i)

            if if_plot:
                clearAxes(axes)
                if ReadStereoMrcnnImg(stereo_mrcnn_img_dir, frame_i+frame_delay) is None:
                    continue
                imgPlot(ReadStereoMrcnnImg(stereo_mrcnn_img_dir, frame_i+frame_delay), \
                        axes[0], None, 1,  "mrcnn")

                ellipses = []
                RA_cart_img = np.zeros([radar_RAD_mask.shape[0], \
                                    radar_RAD_mask.shape[0]*2])
                RA_cart_img = norm2Image(RA_cart_img)

                for class_id in range(len(radar_classes)):
                    obj_class = radar_classes[class_id]
                    class_num = CLASS_NAMES.index(obj_class)
                    class_color = all_colors[class_num]
                    obj_masks = radar_masks[class_id]
                    # obj_masks_combined = np.sum(obj_masks, axis=0)
                    obj_masks_combined = obj_masks
                    obj_masks_combined = np.where(obj_masks_combined > 0, 1., 0.)
                    obj_masks_combined[:, :azimuthind_min, :] = 0.
                    obj_masks_combined[:, azimuthind_max:, :] = 0.
                    RD_mask = np.where(getSumDim(obj_masks_combined, 1) >= 1, 1, 0)
                    RA_mask = np.where(getSumDim(obj_masks_combined, -1) >= 1, 1, 0)
                    RA_cart_mask = transferMaskToCartesianMask(RA_mask)
                    RA_mask = RA_mask[:, azimuthind_min: azimuthind_max]
                    applyMask(RD_img, RD_mask, class_color)
                    applyMask(RA_img, RA_mask, class_color)
                    applyMask(RA_cart_img, RA_cart_mask, class_color)
                    RA_cartesian = transfer2Scatter(RA_mask)
                    if len(RA_cartesian) == 0:
                        continue
                    class_color = np.expand_dims(np.array(class_color), 0)
                    obj_mean, obj_cov = GaussianModel(RA_cartesian)
                    ell_RD = getMaskEllipse(RD_mask, class_color,)
                    ell_RA = getMaskEllipse(RA_mask, class_color,)
                    ell_cart = getMaskEllipse(RA_cart_mask, class_color,)
                    ellipses.append([ell_RD, ell_RA, ell_cart])
                # pclScatter(RA_pcl, RA_color, None, axes[1], [-50, 50], [0, 50], \
                            # "RA Mask (Cartesian)")
                imgPlot(RA_cart_img, axes[1], None, 1, "RAD mask in cartesian")
                imgPlot(RD_img, axes[2], None, 1, "RD")
                imgPlot(RA_img, axes[3], None, 1, "RA")
                for e in ellipses:
                    axes[1].add_artist(e[2])
                    e[2].set_alpha(0.5)
                    axes[2].add_artist(e[0])
                    e[0].set_alpha(0.5)
                    axes[3].add_artist(e[1])
                    e[1].set_alpha(0.5)
                keepDrawing(fig, 0.1)
                saveFigure(radar_annotate_dir, name_prefix, frame_i)
        
if __name__ == "__main__":
    time_stamp = "2020-06-01-15-00-15"
    stereo_mrcnn_img_dir = "./stereo_object_detection_output/"
    stereo_obj_pcl_dir = "./stereo_object_pcl/"
    radar_ral_dir = "/DATA/" + time_stamp + "/ral_outputs_" + time_stamp + "/"
    radar_mask_dir = "../radar_process/radar_ral_process/radar_RAD_mask/"
    registration_matrix_dir = "../registration/registration_matrix/"
    radar_annotate_dir = "./radar_annotation_FOVadjust/"
    FOV_lims = [-20, 20] # in degrees
    sequence = [0, 1000]
    main(stereo_obj_pcl_dir, stereo_mrcnn_img_dir, radar_ral_dir, radar_mask_dir, \
    registration_matrix_dir, radar_annotate_dir, sequence, if_plot=True, if_save=False)
