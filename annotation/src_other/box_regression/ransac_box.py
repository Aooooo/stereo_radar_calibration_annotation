import numpy as np
import cv2
import scipy
import scipy.linalg # use numpy if scipy unavailable
from scipy.ndimage.interpolation import rotate
from scipy.spatial import ConvexHull
from MinimumBoundingBox import MinimumBoundingBox
import matplotlib.pyplot as plt

def minimum_bounding_rectangle(points):
    """
    Find the smallest bounding rectangle for a set of points.
    Returns a set of points representing the corners of the bounding box.

    :param points: an nx2 matrix of coordinates
    :rval: an nx2 matrix of coordinates
    """
    pi2 = np.pi/2.

    # get the convex hull for the points
    hull_points = points[ConvexHull(points).vertices]

    # calculate edge angles
    edges = np.zeros((len(hull_points)-1, 2))
    edges = hull_points[1:] - hull_points[:-1]

    angles = np.zeros((len(edges)))
    angles = np.arctan2(edges[:, 1], edges[:, 0])

    angles = np.abs(np.mod(angles, pi2))
    angles = np.unique(angles)

    # find rotation matrices
    # XXX both work
    # rotations = np.vstack([
        # np.cos(angles),
        # np.cos(angles-pi2),
        # np.cos(angles+pi2),
        # np.cos(angles)]).T
    rotations = np.vstack([
        np.cos(angles),
        -np.sin(angles),
        np.sin(angles),
        np.cos(angles)]).T
    rotations = rotations.reshape((-1, 2, 2))

    # apply rotations to the hull
    rot_points = np.dot(rotations, hull_points.T)

    # find the bounding points
    min_x = np.nanmin(rot_points[:, 0], axis=1)
    max_x = np.nanmax(rot_points[:, 0], axis=1)
    min_y = np.nanmin(rot_points[:, 1], axis=1)
    max_y = np.nanmax(rot_points[:, 1], axis=1)

    # find the box with the best area
    areas = (max_x - min_x) * (max_y - min_y)
    best_idx = np.argmin(areas)
    best_area = areas[best_idx]

    # return the best box
    x1 = max_x[best_idx]
    x2 = min_x[best_idx]
    y1 = max_y[best_idx]
    y2 = min_y[best_idx]
    r = rotations[best_idx]

    rval = np.zeros((4, 2))
    rval[0] = np.dot([x1, y2], r)
    rval[1] = np.dot([x2, y2], r)
    rval[2] = np.dot([x2, y1], r)
    rval[3] = np.dot([x1, y1], r)

    return rval, best_area

def ransacRectangle(pcl, portion=0.8, iterations=500):
    """
    Function:
        Ransac to find the box for the given point cloud.

    Args:
        pcl             ->          target point cloud
        portion         ->          portion for random sampling
        iterations      ->          iterations for finding best box
    """
    iterat = 0
    min_area = 100000000000.
    min_dist_err = 1000000000000.
    lst_area_box = None
    lst_err_box = None
    while iterat <= iterations:
        iterat += 1
        pcl_portion = randomPartition(pcl, portion)
        bbox_info = MinimumBoundingBox(pcl_portion)
        bbox = findRightOrder(bbox_info.corner_points)
        area = bbox_info.area        
        # dist_err = calculateDistErr(pcl, bbox)
        dist_err = calculateDistErr(pcl_portion, bbox)
        if area < min_area:
            min_area = area
            lst_area_box = bbox
        if dist_err < min_dist_err:
            min_dist_err = dist_err
            lst_err_box = bbox
    if lst_area_box is None:
        raise ValueError("No best box found with smallest area")
    elif lst_err_box is None:
        raise ValueError("No best box found with least distance error")
    else:
        return lst_area_box, lst_err_box

def randomPartition(data, portion):
    """
    Function:
        Random sampling.

    Args:
        data            ->          data to be sampled
        portion         ->          output data size
    """
    all_idxs = np.arange(len(data))
    np.random.shuffle(all_idxs)
    data_ind_portion = all_idxs[: int(portion*len(data))]
    data_portion = data[data_ind_portion]
    return data_portion

def calculateDistErr(pcl, bbox):
    """
    Function:
        Calculate distance errors w.r.t the current box.

    Args:
        pcl             ->          all points with shape [n,2]
        bbox            ->          4 corners with shape [4,2]
    """
    all_line_segs = []
    for i in range(len(bbox)):
        i_next = (i+1) % len(bbox)
        all_line_segs.append([bbox[i], bbox[i_next]])
    all_line_segs = np.array(all_line_segs)

    distance_err = 0.
    for i in range(len(pcl)):
        current_pnt = pcl[i] 
        dists = [] 
        for j in range(len(all_line_segs)):
            line_seg = all_line_segs[j]
            dist = lineSegDist(current_pnt, line_seg)
            dists.append(dist)
        distance_err += np.amin(dists)
    return distance_err

def getRmatrix(angle):
    return np.array([[np.cos(angle), -np.sin(angle)],\
                    [np.sin(angle), np.cos(angle)]])

def lineSegDist(point, line_seg):
    """
    Function:
        Calculate the distance between the point and the line segment.
    Args:
        point       ->      given point [x, y]
        line_seg    ->      line segment [v, w]
    Returns:
        d           ->      distance
    """
    v = line_seg[0]
    w = line_seg[1]
    vp = point - v
    vw = w - v
    # t_q = np.sum(vp * vw) / np.sum(np.square(vw))
    t_q = np.dot(vp, vw) / np.sum(np.square(vw))
    t_q = np.maximum(0, np.minimum(t_q, 1))
    q_bar = v + t_q * vw
    d = np.sqrt(np.sum(np.square(point - q_bar)))
    return d

def findRightOrder(corners):
    corners = np.array(list(corners))
    xmin = np.nanmin(corners[:, 0])
    ymin = np.nanmin(corners[:, 1])
    xmax = np.nanmax(corners[:, 0])
    ymax = np.nanmax(corners[:, 1])
    ind1 = np.where(corners == xmin)[0][0]
    ind2 = np.where(corners == ymin)[0][0]
    ind3 = np.where(corners == xmax)[0][0]
    ind4 = np.where(corners == ymax)[0][0]
    order = [ind1, ind2, ind3, ind4]
    return corners[order] 

if __name__ == "__main__":
    # random_points = np.random.uniform(0., 10., size=[50,2])
    random_points = np.random.random(size=[10,2])*10
    fig = plt.figure(figsize=(10,10))
    ax1 = fig.add_subplot(111)
    ax1.scatter(random_points[:, 0], random_points[:,1])
    bbox1 = minimum_bounding_rectangle(random_points)
    bbox2 = MinimumBoundingBox(random_points)
    angle = bbox2.unit_vector_angle
    bbox2 = findRightOrder(bbox2.corner_points) #, bbox2.unit_vector_angle)
    bbox3 = np.matmul(bbox2, getRmatrix(-angle).T)
    ax1.fill(bbox1[:,0], bbox1[:,1], "r", alpha=0.2)
    ax1.fill(bbox2[:,0], bbox2[:,1], "b", alpha=0.2)
    # ax1.fill(bbox3[:,0], bbox3[:,1], "g", alpha=0.2)
    ax1.set_xlim([-10, 20])
    ax1.set_ylim([-10, 20])
    plt.savefig("./sample.png")
