import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
import cv2
import math
import pickle
import colorsys

from sklearn import mixture
from sklearn.cluster import DBSCAN
from collections import Counter

from glob import glob
from tqdm import tqdm

import ransac_box as ransacBox

# Radar Configuration
RADAR_CONFIG_FREQ = 77 # GHz
DESIGNED_FREQ = 76.8 # GHz
RANGE_RESOLUTION = 0.1953125 # m
VELOCITY_RESOLUTION = 0.41968030701528203 # m/s
RANGE_SIZE = 256
DOPPLER_SIZE = 64
AZIMUTH_SIZE = 256
ANGULAR_RESOLUTION = np.pi / 2 / AZIMUTH_SIZE # radians

def RandomColors(N, bright=True): 
    """
    Generate random colors.
    To get visually distinct colors, generate them in HSV space then
    convert to RGB.
    """
    brightness = 1.0 if bright else 0.7
    hsv = [(i / N, 1, brightness) for i in range(N)]
    colors = list(map(lambda c: colorsys.hsv_to_rgb(*c), hsv))
    # random.shuffle(colors)
    return colors

def ReadStereoMrcnnImg(stereo_mrcnn_img_dir, frame_i):
    """
    Function:
        Read mask rcnn results on stereo left img
    
    Args:
        stereo_mrcnn_img_dir            ->          stereo mask rcnn result imgs
        frame_i                         ->          frame id
    """
    filename = stereo_mrcnn_img_dir + "stereo_mrcnn_img_" + str(frame_i) + ".jpg"
    if os.path.exists(filename):
        img = cv2.imread(filename)[..., ::-1]
    else:
        img = None
    return img
 
def readRAD(radar_dir, frame_id):
    """
    Function:
        Get the 3D FFT results of the current frame.
    
    Args:
        radar_dir           ->          radar directory
        frame_id            ->          frame that is about to be processed.
    """
    return np.load(radar_dir + "%.6d.npy"%(frame_id))

def getMagnitude(target_array, power_order=2):
    """
    Function:
        Get the magnitude of the complex array
    """
    target_array = np.abs(target_array)
    target_array = pow(target_array, power_order)
    return target_array 

def getLog(target_array, scalar=1., log_10=True):
    """
    Function:
        Get the log of the complex array
    """
    if log_10:
        return scalar * np.log10(target_array + 1.)
    else:
        return target_array

def ReadRADMask(mask_dir, frame_i):
    """
    Function:
        Read the Range-Azimuth-Doppler mask from directory.

    Args:
        mask_dir            ->          directory that saves masks
    """
    filename = mask_dir + "RAD_mask_%.d.npy"%(frame_i)
    if os.path.exists(filename):
        RAD_mask = np.load(filename)
    else:
        RAD_mask = None
    return RAD_mask

def GaussianModel(pcl):
    """
    Function:
        Get the center and covariance from gaussian model.

    Args:
        pcl             ->          point cloud data
    """
    model = mixture.GaussianMixture(n_components=1, covariance_type='full')
    model.fit(pcl)
    return model.means_[0], model.covariances_[0]

def DbscanDenoise(pcl, epsilon=0.3, minimum_samples=100, dominant_op=False):
    """
    Function:
        Using DBSCAN for filtering out the noise data.

    Args:
        pcl             ->          point cloud to be denoised
        epsilon         ->          maximum distance to be considered as an object
        minimum_samples ->          miminum points to be considered as an object
    """
    clustering = DBSCAN(eps=epsilon, min_samples=minimum_samples).fit(pcl)
    output_labels = clustering.labels_
    if not dominant_op:
        output_pcl = []
        for label_i in np.unique(output_labels):
            if label_i == -1:
                continue
            output_pcl.append(pcl[output_labels == label_i])
    else:
        if len(np.unique(output_labels)) == 1:
            output_pcl = np.zeros([0,3])
        else:
            counts = Counter(output_labels)
            output_pcl = pcl[output_labels == counts.most_common(1)[0][0]]
    return output_pcl

def AddonesToLstCol(target_array):
    """
    Function:
        Add ones to the last column of the target array

    Args:
        target_array            ->          array to be changed
    """
    adding_ones = np.ones([target_array.shape[0], 1])
    output_array = np.concatenate([target_array, adding_ones], axis=-1)
    return output_array

def EuclideanDist(point1, point2):
    """
    Function:
        Calculate EuclideanDist
    """
    return np.sqrt(np.sum(np.square(point1 - point2), axis=-1))

def getSumDim(target_array, target_axis):
    """
    Function:
        Sum up one dimension of a  3D matrix.
    
    Args:
        column_index            ->          which column to be deleted
    """
    output = np.sum(target_array, axis=target_axis)
    return output 

def transfer2Scatter(RA_mask):
    """
    Function:
        Transfer RD indexes to pcl, for verifying quality

    Args:
        RA_mask              ->          Range-Azimuth mask
    """
    output_pcl = []
    for i in range(RA_mask.shape[0]):
        for j in range(RA_mask.shape[1]):
            if RA_mask[i, j] == 1:
                # point_range = ((RANGE_SIZE-1) - i) * RANGE_RESOLUTION
                # point_angle = (j - (AZIMUTH_SIZE/2)) * ANGULAR_RESOLUTION
                # ####################### Prince's method ######################
                point_range = ((RANGE_SIZE-1) - i) * RANGE_RESOLUTION
                point_angle = (j * (2*np.pi/AZIMUTH_SIZE) - np.pi) / \
                                (2*np.pi*0.5*RADAR_CONFIG_FREQ/DESIGNED_FREQ)
                point_angle = np.arcsin(point_angle)
                # ##############################################################
                point_zx = polarToCartesian(point_range, point_angle)
                output_pcl.append(np.array([[point_zx[1], point_zx[0]]]))
    output_pcl = np.concatenate(output_pcl, axis=0)
    return output_pcl

def generateRACartesianImage(RA_img, RA_mask):
    """
    Function:
        Generate RA image in Cartesian Coordinate.

    Args:
        RA_img          ->          RA FFT magnitude
        RA_mask         ->          Mask generated 
    """
    output_img = np.zeros([RA_img.shape[0], RA_img.shape[0]*2])
    for i in range(RA_img.shape[0]):
        for j in range(RA_img.shape[1]):
            if RA_mask[i,j] == 1:
                point_range = ((RANGE_SIZE-1) - i) * RANGE_RESOLUTION
                point_angle = (j - (AZIMUTH_SIZE/2)) * ANGULAR_RESOLUTION
                point_zx = polarToCartesian(point_range, point_angle)
                new_i = int(output_img.shape[0] - \
                        np.round(point_zx[0]/RANGE_RESOLUTION)-1)
                new_j = int(np.round((point_zx[1]+50)/RANGE_RESOLUTION)-1)
                output_img[new_i,new_j] = RA_img[i,j] 
    norm_sig = plt.Normalize()
    # color mapping 
    output_img = plt.cm.viridis(norm_sig(output_img))
    output_img = output_img[..., :3]
    return output_img

def transferMaskToCartesianMask(RA_mask):
    """
    Function:
        Transfer polar mask to cartesian mask.
    
    Args:
        RA_mask        ->          polar mask.
    """
    output_mask = np.zeros([RA_mask.shape[0], RA_mask.shape[0]*2])
    for i in range(RA_mask.shape[0]):
        for j in range(RA_mask.shape[1]):
            if RA_mask[i,j] == 1:
                point_range = ((RANGE_SIZE-1) - i) * RANGE_RESOLUTION
                # point_angle = (j - (AZIMUTH_SIZE/2)) * ANGULAR_RESOLUTION
                ####################### Prince's method ######################
                point_angle = (j * (2*np.pi/AZIMUTH_SIZE) - np.pi) / \
                                (2*np.pi*0.5*RADAR_CONFIG_FREQ/DESIGNED_FREQ)
                point_angle = np.arcsin(point_angle)
                ##############################################################
                point_zx = polarToCartesian(point_range, point_angle)
                new_i = int(output_mask.shape[0] - \
                        np.round(point_zx[0]/RANGE_RESOLUTION)-1)
                new_j = int(np.round((point_zx[1]+50)/RANGE_RESOLUTION)-1)
                output_mask[new_i,new_j] = 1.
    output_mask = np.where(output_mask > 0., 1., 0.)
    return output_mask

def InstancizeRADMask(radar_RAD_mask):
    """
    Function:
        Separate RAD mask w.r.t each instance.

    Args:
        radar_RAD_mask          ->          RAD mask
    """
    indexes_clutters = []
    for i in range(radar_RAD_mask.shape[0]):
        for j in range(radar_RAD_mask.shape[1]):
            for k in range(radar_RAD_mask.shape[2]):
                if radar_RAD_mask[i,j,k] != 0:
                    indexes_clutters.append(np.array([i,j,k]))
    indexes_clutters = np.array(indexes_clutters)
    indexes_instances = DbscanDenoise(indexes_clutters, epsilon=10, minimum_samples=5)
    instances_masks = []
    for instance_i in range(len(indexes_instances)):
        current_instance = indexes_instances[instance_i]
        instance_mask = np.zeros(radar_RAD_mask.shape)
        for pnt_i in range(len(current_instance)):
            pnt_ind = current_instance[pnt_i]
            instance_mask[pnt_ind[0], pnt_ind[1], pnt_ind[2]] = 1.
        # instance_center = np.mean(current_instance, axis=0)
        # instances_masks.append([instance_mask, instance_center])
        instances_masks.append([instance_mask, current_instance])
    return instances_masks

def FindRadarObjInstanceDebug(stereo_pcl, radar_instances,):
    """
    Function:
        Use calibrated stereo points to find the corresponding radar points

    Args:
        stereo_pcl          ->          calibrated stereo 2d point cloud 
        radar_pcl           ->          radar 2d point cloud
        distance_threshold  ->          threshold used for first filtering
    """
    obj_mask = np.zeros(radar_instances[0][0].shape)
    all_polar = []
    all_pop_inds = []
    for obj in range(len(radar_instances)):
        radar_obj_indexes = radar_instances[obj][1]
        point_range = ((RANGE_SIZE - 1) - radar_obj_indexes[:,0]) * RANGE_RESOLUTION
        point_angle = (radar_obj_indexes[:, 1] * (2*np.pi/AZIMUTH_SIZE) - np.pi) / \
                        (2*np.pi*0.5*RADAR_CONFIG_FREQ/DESIGNED_FREQ)
        point_angle = np.arcsin(point_angle)
        point_zx = polarToCartesian(point_range, point_angle)
        radar_xz = np.concatenate([np.expand_dims(point_zx[1], -1), \
                np.expand_dims(point_zx[0], -1)], -1)
        all_polar.append(radar_xz)
    all_polar = np.concatenate(all_polar, 0)
    return all_polar

def applyMask(image, mask, color, alpha=0.5):
    """
    Apply the given mask to the image.
    """
    for c in range(3):
        image[:, :, c] = np.where(mask == 1,
                                 image[:, :, c] *
                                (1 - alpha) + alpha * color[c] * 255,
                                 image[:, :, c])
    return image

def findRightOrder(corners):
    corners = np.array(list(corners))
    xmin = np.nanmin(corners[:, 0])
    ymin = np.nanmin(corners[:, 1])
    xmax = np.nanmax(corners[:, 0])
    ymax = np.nanmax(corners[:, 1])
    ind1 = np.where(corners == xmin)[0][0]
    ind2 = np.where(corners == ymin)[0][0]
    ind3 = np.where(corners == xmax)[0][0]
    ind4 = np.where(corners == ymax)[0][0]
    order = [ind1, ind2, ind3, ind4]
    return corners[order] 

##################### coordinate transformation ######################
def cartesianToPolar(x, y):
    rho = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    return(rho, phi)

def polarToCartesian(rho, phi):
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return(x, y)

###################### PLOTTING FUNCTIONS START #########################
def prepareFigure(num_axes):
    assert num_axes <= 4
    plt.ion()
    fig = plt.figure(figsize=(18,12))
    if num_axes == 1:
        ax1 = fig.add_subplot(111)
        return fig, [ax1]
    if num_axes == 2: 
        ax1 = fig.add_subplot(121)
        ax2 = fig.add_subplot(122)
        return fig, [ax1, ax2]
    if num_axes == 3:
        ax1 = fig.add_subplot(131)
        ax2 = fig.add_subplot(132)
        ax3 = fig.add_subplot(133)
        return fig, [ax1, ax2, ax3]
    if num_axes == 4:
        ax1 = fig.add_subplot(221)
        ax2 = fig.add_subplot(222)
        ax3 = fig.add_subplot(223)
        ax4 = fig.add_subplot(224)
        return fig, [ax1, ax2, ax3, ax4]

def clearAxes(ax_list):
    assert len(ax_list) >=1 
    plt.cla()
    for ax_i in ax_list:
        ax_i.clear()

def createColorList(class_names, class_list, all_colors):
    color_output_list = []
    for i in range(len(class_list)):
        current_class = class_list[i]
        color_i = np.array([all_colors[class_names.index(current_class)]])
        color_output_list.append(color_i)
    return color_output_list
 
def createLabelList(class_list, label_prefix):
    label_list = []
    for i in range(len(class_list)):
        current_class = class_list[i]
        label_list.append(label_prefix + "_" + current_class)
    return label_list

def pointScatter(bg_points, point_list, color_list, label_list, ax, xlimits, ylimits, title):
    assert len(point_list) == len(color_list)
    assert len(point_list) == len(label_list)
    if bg_points is not None:
        ax.scatter(bg_points[:, 0], bg_points[:, 1], s=0.2, c='blue', label="background")
    for i in range(len(point_list)):
        points_i = point_list[i]
        color_i = color_list[i]
        label_i = label_list[i]
        ax.scatter(points_i[:, 0], points_i[:,1], s=0.3, c=color_i, label=label_i)
    ax.set_xlim(xlimits)
    ax.set_ylim(ylimits)
    ax.legend()
    ax.set_title(title)

def pclScatter(pcl_list, color_list, label_list, box_list, ax, xlimits, ylimits, title):
    assert len(pcl_list) == len(color_list)
    if label_list is not None:
        assert len(pcl_list) == len(label_list)
    for i in range(len(pcl_list)):
        pcl = pcl_list[i]
        color = color_list[i]
        if label_list == None:
            label = None
        else:
            label = label_list[i]
        ax.scatter(pcl[:, 0], pcl[:, 1], s=1, c=color, label=label)
        if box_list is not None:
            ax.fill(box_list[i][:,0], box_list[i][:,1], color=color, alpha=0.2)
    ax.set_xlim(xlimits)
    ax.set_ylim(ylimits)
    ax.set_title(title)

def imgPlot(img, ax, cmap, alpha, title=None):
    ax.imshow(img, cmap=cmap, alpha=alpha)
    ax.axis('off')
    if title is not None:
        ax.set_title(title)

def keepDrawing(fig, time_duration):
    fig.canvas.draw()
    plt.pause(time_duration)

def saveFigure(save_dir, name_prefix, frame_i):
    plt.savefig(save_dir + name_prefix + str(frame_i) + ".png")
###################### PLOTTING FUNCTIONS END #########################
 
def debug(stereo_obj_pcl_dir, stereo_mrcnn_img_dir,  radar_ral_dir, radar_mask_dir, registration_matrix_dir, 
        radar_annotate_dir, sequence, if_plot=True, if_save=True):
    """
    MAIN FUNCTION
    """
    ##### frame delay between two sensors (not sure whether it exists) #####
    frame_delay = 0
    distance_threshold = 5.0
    name_prefix = "debug_"
    save_dir = "./checkout/"

    fig, axes = prepareFigure(4)

    for frame_i in tqdm(range(sequence[0], sequence[1])):
        radar_RAD_mask = ReadRADMask(radar_mask_dir, frame_i)
        RD_img = cv2.imread(radar_ral_dir + "doppler_range/%.6d.png"%(frame_i))[..., ::-1]
        RA_img = cv2.imread(radar_ral_dir + "angle_range/%.6d.png"%(frame_i))[..., ::-1]

        if radar_RAD_mask is None:
            print("Frame %d has no object detected."%(frame_i))
            continue
        
        radar_mask_instances = InstancizeRADMask(radar_RAD_mask)
        if len(radar_mask_instances) == 0:
            continue
        
        all_colors = RandomColors(len(radar_mask_instances))

        instance_pcls = []
        instance_colors = []
        instance_boxs = []
        new_RAD_mask = np.zeros(radar_RAD_mask.shape)
        for inst_i in range(len(radar_mask_instances)):
            current_instance_mask = radar_mask_instances[inst_i][0]
            new_RAD_mask += current_instance_mask
            current_RA_mask = np.where(getSumDim(current_instance_mask, -1) > 0.,1.,0.)
            instance_pcl = transfer2Scatter(current_RA_mask)
            # lst_area_box, lst_err_box = ransacBox.ransacRectangle(instance_pcl, \
                                                    # portion=0.7, iterations=500)
            # instance_boxs.append(lst_err_box)
            instance_pcls.append(instance_pcl)
            instance_colors.append(all_colors[inst_i])
        new_RAD_mask = np.where(new_RAD_mask > 0, 1., 0.)
               
        clearAxes(axes)
        if ReadStereoMrcnnImg(stereo_mrcnn_img_dir, frame_i) is None:
            continue
        imgPlot(ReadStereoMrcnnImg(stereo_mrcnn_img_dir, frame_i), \
                axes[0], None, 1,  "mrcnn")
        imgPlot(np.where(getSumDim(new_RAD_mask, -1)>0,1,0), axes[1], "viridis", 1)
        imgPlot(np.where(getSumDim(new_RAD_mask, 1)>0,1,0), axes[3], "viridis", 1)
        # pclScatter(instance_pcls, instance_colors, None, instance_boxs, axes[2],\
                    # [-50, 50], [0, 50], "Points")
        pclScatter(instance_pcls, instance_colors, None, None, axes[2],\
                    [-50, 50], [0, 50], "Points")
        # fig.canvas.draw()
        # plt.pause(0.1)
        saveFigure(save_dir, name_prefix, frame_i)

if __name__ == "__main__":
    time_stamp = "2020-06-01-14-44-13"
    stereo_mrcnn_img_dir = "./stereo_object_detection_output/"
    stereo_obj_pcl_dir = "./stereo_object_pcl/"
    radar_ral_dir = "/DATA/" + time_stamp + "/ral_outputs_" + time_stamp + "/"
    radar_mask_dir = "../radar_process/radar_ral_process/radar_RAD_mask/"
    registration_matrix_dir = "../registration/registration_matrix/"
    radar_annotate_dir = "./radar_annotation/"
    sequence = [1000, 1500]
    debug(stereo_obj_pcl_dir, stereo_mrcnn_img_dir, radar_ral_dir, radar_mask_dir, \
        registration_matrix_dir, radar_annotate_dir, sequence, if_plot=True)
