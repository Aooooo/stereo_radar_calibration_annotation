import numpy as np
import cv2
import os
import glob
import matplotlib.pyplot as plt
from tqdm import tqdm

def GenerateDispRectImages(img_dir, img_i):
    """
    Function:
        Generate produced rectified images.
    
    Args:
        img_dir           ->          directory of rectified images.
        img_i             ->          current image sequence number
    Returns:
        disp_left         ->          disparity map (based on left image)
        rect_left         ->          rectified image (left)
    """
    disp_left = None
    rect_left = None
    disp_img_name = img_dir + "disp_maps_" + str(img_i) + ".jpg"
    rect_img_name = img_dir + "rect_imgs_" + str(img_i) + ".jpg"
    disp_img = cv2.imread(disp_img_name) 
    rect_img = cv2.imread(rect_img_name)
    if disp_img is not None and rect_img is not None:
        disp_left = disp_img
        rect_left = rect_img[:, :rect_img.shape[1]//2, ...]
    return disp_left, rect_left

def main(disp_rect_dir, save_dir, sequence):
    """
    MAIN FUNCTION
    """
    if not os.path.exists(save_dir):
        os.mkdir(save_dir)
    else:
        for _ in glob.glob(os.path.join(save_dir, "*.jpg")):
            os.remove(_)
    for frame_i in tqdm(range(sequence[0], sequence[1])):
        disp_left, rect_left = GenerateDispRectImages(disp_rect_dir, frame_i)
        if disp_left is None or rect_left is None:
            continue
        # cv2.imshow('img', rect_left)
        cv2.imwrite(save_dir + "stereo_input_" + str(frame_i) + ".jpg",\
                    rect_left)
        cv2.waitKey(1)

if __name__ == "__main__":
    disp_rect_dir = "../stereo_process/disparity/"
    save_dir = "./stereo_input/"
    sequence = [0, 2359]
    main(disp_rect_dir, save_dir, sequence)
