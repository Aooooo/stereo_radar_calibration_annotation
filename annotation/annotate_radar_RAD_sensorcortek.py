import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse, Polygon, Rectangle
import cv2
import math
import random
import pickle
import colorsys
import time

from sklearn import mixture
from sklearn.cluster import DBSCAN
from skimage.measure import find_contours
from collections import Counter

from glob import glob
from tqdm import tqdm

import multiprocessing as mp
from functools import partial

############### NOTE: following two are tunable parameters #############
STEREO_3D_DBCAN_EPSILON = 0.5
STEREO_2D_DBCAN_EPSILON = 0.3
STEREO_CAR_PCL_THRESHOLD = 20

# detectron2 all classes
CLASS_NAMES = ['person', 'bicycle', 'car', 'motorcycle', 'airplane', 'bus', \
        'train', 'truck', 'boat', 'traffic light', 'fire hydrant', 'stop sign', \
        'parking meter', 'bench', 'bird', 'cat', 'dog', 'horse', 'sheep', 'cow', \
        'elephant', 'bear', 'zebra', 'giraffe', 'backpack', 'umbrella', 'handbag', \
        'tie', 'suitcase', 'frisbee', 'skis', 'snowboard', 'sports ball', 'kite', \
        'baseball bat', 'baseball glove', 'skateboard', 'surfboard', \
        'tennis racket', 'bottle', 'wine glass', 'cup', 'fork', 'knife', 'spoon', \
        'bowl', 'banana', 'apple', 'sandwich', 'orange', 'broccoli', 'carrot', \
        'hot dog', 'pizza', 'donut', 'cake', 'chair', 'couch', 'potted plant', \
        'bed', 'dining table', 'toilet', 'tv', 'laptop', 'mouse', 'remote', \
        'keyboard', 'cell phone', 'microwave', 'oven', 'toaster', 'sink', \
        'refrigerator', 'book', 'clock', 'vase', 'scissors', 'teddy bear', \
        'hair drier', 'toothbrush']

ROAD_USERS = ['person', 'bicycle', 'car', 'motorcycle',
            'bus', 'train', 'truck', 'boat']

# Radar Configuration
RADAR_CONFIG_FREQ = 77 # GHz
DESIGNED_FREQ = 76.8 # GHz
RANGE_RESOLUTION = 0.1953125 # m
VELOCITY_RESOLUTION = 0.41968030701528203 # m/s
RANGE_SIZE = 256
DOPPLER_SIZE = 64
AZIMUTH_SIZE = 256
ANGULAR_RESOLUTION = np.pi / 2 / AZIMUTH_SIZE # radians
VELOCITY_MIN = - VELOCITY_RESOLUTION * DOPPLER_SIZE/2
VELOCITY_MAX = VELOCITY_RESOLUTION * DOPPLER_SIZE/2

def RandomColors(N, bright=True): 
    """
    Generate random colors.
    To get visually distinct colors, generate them in HSV space then
    convert to RGB.
    """
    brightness = 1.0 if bright else 0.7
    hsv = [(i / N, 1, brightness) for i in range(N)]
    colors = list(map(lambda c: colorsys.hsv_to_rgb(*c), hsv))
    # detectron uses random shuffle to give the differences
    random.seed(8888)
    random.shuffle(colors)
    return colors

def readRAD(radar_dir, frame_id):
    """
    Function:
        Get the 3D FFT results of the current frame.
    
    Args:
        radar_dir           ->          radar directory
        frame_id            ->          frame that is about to be processed.
    """
    if os.path.exists(os.path.join(radar_dir, "%.6d.npy"%(frame_id))):
        return np.load(os.path.join(radar_dir, "%.6d.npy"%(frame_id)))
    else:
        return None

def getMagnitude(target_array, power_order=2):
    """
    Function:
        Get the magnitude of the complex array
    """
    target_array = np.abs(target_array)
    target_array = pow(target_array, power_order)
    return target_array 

def getLog(target_array, scalar=1., log_10=True):
    """
    Function:
        Get the log of the complex array
    """
    if log_10:
        return scalar * np.log10(target_array + 1.)
    else:
        return target_array

def ReadStereoObjPcl(stereo_obj_pcl_dir, frame_i):
    """
    Function:
        Read the stereo object detection point cloud w.r.t frame i

    Args:
        stereo_obj_pcl_dir          ->          stereo point cloud directory
        frame_i                     ->          frame id

    Attention:
        The output stereo_obj_pcl has two keys, 
        "class" : current object class string name 
        "pcl" : current object point cloud with format (num_pnts, 3) 
    """
    filename = stereo_obj_pcl_dir + "stereo_obj_pcl_" + str(frame_i) + ".pickle"
    if os.path.exists(filename):
        with open(filename, "rb") as f:
            stereo_obj_pcl = pickle.load(f)
        if len(stereo_obj_pcl['class']) == 0:
            stereo_obj_pcl = None
    else:
        stereo_obj_pcl = None
    return stereo_obj_pcl

def ReadStereoMrcnnImg(stereo_mrcnn_img_dir, frame_i):
    """
    Function:
        Read mask rcnn results on stereo left img
    
    Args:
        stereo_mrcnn_img_dir            ->          stereo mask rcnn result imgs
        frame_i                         ->          frame id
    """
    filename = stereo_mrcnn_img_dir + "stereo_mrcnn_img_" + str(frame_i) + ".jpg"
    if os.path.exists(filename):
        img = cv2.imread(filename)[..., ::-1]
    else:
        img = None
    return img
 
def ReadStereoImg(stereo_mrcnn_img_dir, frame_i):
    """
    Function:
        Read mask rcnn results on stereo left img
    
    Args:
        stereo_mrcnn_img_dir            ->          stereo mask rcnn result imgs
        frame_i                         ->          frame id
    """
    filename = stereo_mrcnn_img_dir + "rect_imgs_" + str(frame_i) + ".jpg"
    if os.path.exists(filename):
        img = cv2.imread(filename)[..., ::-1]
        img = img[:, :int(img.shape[1]/2), :]
    else:
        img = None
    return img
   
def ReadRADMask(mask_dir, frame_i):
    """
    Function:
        Read the Range-Azimuth-Doppler mask from directory.

    Args:
        mask_dir            ->          directory that saves masks
    """
    filename = mask_dir + "RAD_mask_%.d.npy"%(frame_i)
    if os.path.exists(filename):
        RAD_mask = np.load(filename)
    else:
        RAD_mask = None
    return RAD_mask

def WriteRadarObjPcl(radar_annotate_dir, radar_dict, frame_i):
    """
    Function:
        Write the annotated radar point cloud dictionary into the directory

    Args:
        radar_annotate_dir          ->          directory for saving
        frame_i                     ->          frame id
    """
    with open(radar_annotate_dir + "radar_obj_" + str(frame_i) + ".pickle", "wb") as f:
        pickle.dump(radar_dict, f)

def WriteStereoObj(stereo_dir, stereo_dict, frame_i):
    with open(stereo_dir + "stereo_obj_" + str(frame_i) + ".pickle", "wb") as f:
        pickle.dump(stereo_dict, f)

def ReadRegistrationMatrix(registration_matrix_dir):
    """
    Function:
        Read the calibration matrix

    Args:
        registration_matrix_dir             ->          registration matrix directory
    """
    filename = registration_matrix_dir + "calibration_matrix.npy"
    if os.path.exists(filename):
        registr_mat = np.load(filename)
    else:
        raise ValueError("Pleaaaaaaase calibrate the sensors before you do this")
    return registr_mat

def GaussianModel(pcl):
    """
    Function:
        Get the center and covariance from gaussian model.

    Args:
        pcl             ->          point cloud data
    """
    model = mixture.GaussianMixture(n_components=1, covariance_type='full')
    model.fit(pcl)
    return model.means_[0], model.covariances_[0]

def DbscanDenoise(pcl, epsilon=0.3, minimum_samples=100, n_jobs=1, dominant_op=False):
    """
    Function:
        Using DBSCAN for filtering out the noise data.

    Args:
        pcl             ->          point cloud to be denoised
        epsilon         ->          maximum distance to be considered as an object
        minimum_samples ->          miminum points to be considered as an object
    """
    clustering = DBSCAN(eps=epsilon, min_samples=minimum_samples, n_jobs=n_jobs).fit(pcl)
    output_labels = clustering.labels_
    if not dominant_op:
        output_pcl = []
        for label_i in np.unique(output_labels):
            if label_i == -1:
                continue
            output_pcl.append(pcl[output_labels == label_i])
    else:
        if len(np.unique(output_labels)) == 1 and np.unique(output_labels)[0] == -1:
            output_pcl = np.zeros([0,2])
            output_idx = []
        else:
            counts = Counter(output_labels)
            output_pcl = pcl[output_labels == counts.most_common(1)[0][0]]
            output_idx = np.where(output_labels == counts.most_common(1)[0][0])[0]
    return output_pcl

def AddonesToLstCol(target_array):
    """
    Function:
        Add ones to the last column of the target array

    Args:
        target_array            ->          array to be changed
    """
    adding_ones = np.ones([target_array.shape[0], 1])
    output_array = np.concatenate([target_array, adding_ones], axis=-1)
    return output_array

def CalibrateStereoToRadar(stereo_pcl, registration_matrix):
    """
    Function:
        Transfer stereo point cloud to radar frame.
    
    Args:
        stereo_pcl              ->          stereo point cloud
        registration_matrix     ->          registration matrix 
    """
    return np.matmul(AddonesToLstCol(stereo_pcl), registration_matrix)

def EuclideanDist(point1, point2):
    """
    Function:
        Calculate EuclideanDist
    """
    return np.sqrt(np.sum(np.square(point1 - point2), axis=-1))

def getSumDim(target_array, target_axis):
    """
    Function:
        Sum up one dimension of a  3D matrix.
    
    Args:
        column_index            ->          which column to be deleted
    """
    output = np.sum(target_array, axis=target_axis)
    return output 

def transfer2Scatter(RA_mask):
    """
    Function:
        Transfer RD indexes to pcl, for verifying quality

    Args:
        RA_mask              ->          Range-Azimuth mask
    """
    output_pcl = []
    for i in range(RA_mask.shape[0]):
        for j in range(RA_mask.shape[1]):
            if RA_mask[i, j] == 1:
                # point_range = ((RANGE_SIZE-1) - i) * RANGE_RESOLUTION
                # point_angle = (j - (AZIMUTH_SIZE/2)) * ANGULAR_RESOLUTION
                # ####################### Prince's method ######################
                point_range = ((RANGE_SIZE-1) - i) * RANGE_RESOLUTION
                point_angle = (j * (2*np.pi/AZIMUTH_SIZE) - np.pi) / \
                                (2*np.pi*0.5*RADAR_CONFIG_FREQ/DESIGNED_FREQ)
                point_angle = np.arcsin(point_angle)
                # ##############################################################
                point_zx = polarToCartesian(point_range, point_angle)
                output_pcl.append(np.array([[point_zx[1], point_zx[0]]]))
    if len(output_pcl) != 0:
        output_pcl = np.concatenate(output_pcl, axis=0)
    return output_pcl

def generateRACartesianImage(RA_img, RA_mask):
    """
    Function:
        Generate RA image in Cartesian Coordinate.

    Args:
        RA_img          ->          RA FFT magnitude
        RA_mask         ->          Mask generated 
    """
    output_img = np.zeros([RA_img.shape[0], RA_img.shape[0]*2])
    for i in range(RA_img.shape[0]):
        for j in range(RA_img.shape[1]):
            if RA_mask[i,j] == 1:
                point_range = ((RANGE_SIZE-1) - i) * RANGE_RESOLUTION
                # point_angle = (j - (AZIMUTH_SIZE/2)) * ANGULAR_RESOLUTION
                ####################### Prince's method ######################
                point_angle = (j * (2*math.pi/AZIMUTH_SIZE) - math.pi) / \
                                (2*math.pi*0.5*RADAR_CONFIG_FREQ/DESIGNED_FREQ)
                point_angle = math.asin(point_angle)
                ##############################################################
                point_zx = polarToCartesian(point_range, point_angle)
                new_i = int(output_img.shape[0] - \
                        np.round(point_zx[0]/RANGE_RESOLUTION)-1)
                new_j = int(np.round((point_zx[1]+50)/RANGE_RESOLUTION)-1)
                output_img[new_i,new_j] = RA_img[i,j] 
    norm_sig = plt.Normalize()
    # color mapping 
    output_img = plt.cm.viridis(norm_sig(output_img))
    output_img = output_img[..., :3]
    return output_img

def transferPcl2Cartesian(pcl):
    output_mask = np.zeros([RANGE_SIZE, RANGE_SIZE*2])
    for i in range(len(pcl)):
        pnt = pcl[i]
        point_zx = pnt[::-1]
        new_i = int(output_mask.shape[0] - \
                np.round(point_zx[0]/RANGE_RESOLUTION)-1)
        new_j = int(np.round((point_zx[1]+50)/RANGE_RESOLUTION)-1)
        output_mask[new_i,new_j] = 1.
    output_mask = np.where(output_mask > 0., 1., 0.)
    return output_mask

def transferMaskToCartesianMask(RA_mask):
    """
    Function:
        Transfer polar mask to cartesian mask.
    
    Args:
        RA_mask        ->          polar mask.
    """
    output_mask = np.zeros([RA_mask.shape[0], RA_mask.shape[0]*2])
    for i in range(RA_mask.shape[0]):
        for j in range(RA_mask.shape[1]):
            if RA_mask[i,j] == 1:
                point_range = ((RANGE_SIZE-1) - i) * RANGE_RESOLUTION
                # point_angle = (j - (AZIMUTH_SIZE/2)) * ANGULAR_RESOLUTION
                ####################### Prince's method ######################
                point_angle = (j * (2*np.pi/AZIMUTH_SIZE) - np.pi) / \
                                (2*np.pi*0.5*RADAR_CONFIG_FREQ/DESIGNED_FREQ)
                point_angle = np.arcsin(point_angle)
                ##############################################################
                point_zx = polarToCartesian(point_range, point_angle)
                new_i = int(output_mask.shape[0] - \
                        np.round(point_zx[0]/RANGE_RESOLUTION)-1)
                new_j = int(np.round((point_zx[1]+50)/RANGE_RESOLUTION)-1)
                output_mask[new_i,new_j] = 1.
    output_mask = np.where(output_mask > 0., 1., 0.)
    return output_mask

def InstancizeRADMask(radar_RAD_mask, n_jobs=1):
    """
    Function:
        Separate RAD mask w.r.t each instance.

    Args:
        radar_RAD_mask          ->          RAD mask
    """
    indexes_clutters = []
    for i in range(radar_RAD_mask.shape[0]):
        for j in range(radar_RAD_mask.shape[1]):
            for k in range(radar_RAD_mask.shape[2]):
                if radar_RAD_mask[i,j,k] != 0:
                    indexes_clutters.append(np.array([i,j,k]))
    indexes_clutters = np.array(indexes_clutters)
    indexes_instances = DbscanDenoise(indexes_clutters, epsilon=5, \
                                    minimum_samples=3, n_jobs=n_jobs)
    instances_masks = []
    for instance_i in range(len(indexes_instances)):
        current_instance = indexes_instances[instance_i]
        instance_mask = np.zeros(radar_RAD_mask.shape)
        for pnt_i in range(len(current_instance)):
            pnt_ind = current_instance[pnt_i]
            instance_mask[pnt_ind[0], pnt_ind[1], pnt_ind[2]] = 1.
        # instance_center = np.mean(current_instance, axis=0)
        # instances_masks.append([instance_mask, instance_center])
        instances_masks.append([instance_mask, current_instance])
    return instances_masks

def InstancizeRADMask2D1D(radar_RAD_mask):
    """
    Function:
        Separate RAD mask w.r.t each instance.

    Args:
        radar_RAD_mask          ->          RAD mask
    """
    RD_mask = np.where(getSumDim(radar_RAD_mask, 1) >= 1, 1, 0)
    RA_mask = np.where(getSumDim(radar_RAD_mask, -1) >= 1, 1, 0)
    RD_indexes = []
    RA_indexes = []
    for i in range(RD_mask.shape[0]):
        for j in range(RD_mask.shape[1]):
            if RD_mask[i, j] != 0:
                RD_indexes.append(np.array([i,j]))
    for i in range(RA_mask.shape[0]):
        for j in range(RA_mask.shape[1]):
            if RA_mask[i, j] != 0:
                RA_indexes.append(np.array([i,j]))
    RD_indexes = np.array(RD_indexes)
    RA_indexes = np.array(RA_indexes)

    RD_indexes_instances = DbscanDenoise(RD_indexes, epsilon=5, minimum_samples=5)

    RA_indexes_instances = DbscanDenoise(RA_indexes, epsilon=10, minimum_samples=5)

    instances_masks = []
    for RD_inst_i in range(len(RD_indexes_instances)):
        current_RD_instance = RD_indexes_instances[RD_inst_i]
        for RA_inst_i in range(len(RA_indexes_instances)):
            current_RA_instance = RA_indexes_instances[RA_inst_i]
            instance_mask = np.zeros(radar_RAD_mask.shape)
            current_instance = []
            for pnt_i in range(len(current_RD_instance)):
                current_RD_pnt = current_RD_instance[pnt_i]
                for pnt_j in range(len(current_RA_instance)):
                    current_RA_pnt = current_RA_instance[pnt_j]
                    if current_RD_pnt[0] == current_RA_pnt[0]:
                        instance_mask[current_RD_pnt[0], current_RA_pnt[1], current_RD_pnt[1]] = 1.
                        current_pnt = np.array([current_RD_pnt[0], current_RA_pnt[1], \
                                                    current_RD_pnt[1]])
                        current_instance.append(current_pnt)
            current_instance = np.array(current_instance)
            if np.amax(instance_mask) != 0:
                instances_masks.append([instance_mask, current_instance])
    return instances_masks

def FindRadarObjInstance(rad_mask_instance, stereo_all_pcls, stereo_all_classes, \
                        stereo_all_obj_index):
    """
    Function:
        Use calibrated stereo points to find the corresponding radar points

    Args:
        rad_mask_instance          ->          format[radar_mask, radar_mask_indexes]
        stereo_all_pcls            ->          stereo all objects' pcl
        stereo_all_classes         ->          all classes indexes w.r.t stereo_all_pcls
        stereo_all_obj_index       ->          all stereo object indexes numbers
    """
    #### testing thresholds #####
    distance_threshold = 3 
    overlap_dist_threshold = 5

    radar_obj_mask = rad_mask_instance[0]
    radar_obj_indexes = rad_mask_instance[1]
    ########################## Prince's Method ################################
    point_range = ((RANGE_SIZE - 1) - radar_obj_indexes[:,0]) * RANGE_RESOLUTION
    point_angle = (radar_obj_indexes[:, 1] * (2*np.pi/AZIMUTH_SIZE) - np.pi) / \
                    (2*np.pi*0.5*RADAR_CONFIG_FREQ/DESIGNED_FREQ)
    point_angle = np.arcsin(point_angle)
    point_zx = polarToCartesian(point_range, point_angle)
    radar_xz = np.concatenate([np.expand_dims(point_zx[1], -1), \
            np.expand_dims(point_zx[0], -1)], -1)
    radar_xz_mean, _ = GaussianModel(radar_xz)
    
    potential_stereo = []
    potential_stereo_numinthres = []
    for st_idx in range(len(stereo_all_pcls)):
        stereo_obj_pcl = stereo_all_pcls[st_idx] 
        stereo_obj_classidx = stereo_all_classes[st_idx] 
        stereo_obj_idx = stereo_all_obj_index[st_idx]
        min_distance = 100.
        if len(stereo_obj_pcl) == 0:
            continue
        for pnt_id in range(len(radar_xz)):
            distance_diff = np.sqrt(np.sum(np.square(stereo_obj_pcl - \
                            radar_xz[pnt_id]), axis=-1))
            min_distance = np.amin([np.amin(distance_diff), min_distance])
        if min_distance <= distance_threshold:
            distance_diff2center = np.sqrt(np.sum(np.square(stereo_obj_pcl - \
                            radar_xz_mean), axis=-1))
            mask_inthreshold = np.where(distance_diff2center <= overlap_dist_threshold, \
                            1., 0.)
            num_inthreshold = np.count_nonzero(mask_inthreshold)
            potential_stereo.append([stereo_obj_pcl, stereo_obj_classidx, \
                                    stereo_obj_idx])
            potential_stereo_numinthres.append(num_inthreshold)
    if len(potential_stereo) > 1:
        right_match_idx = np.argmax(potential_stereo_numinthres)
        stereo_right = potential_stereo[right_match_idx]
        return radar_obj_mask, stereo_right[1], stereo_right[2]
    elif len(potential_stereo) == 0:
        return None, None, None
    else:
        stereo_right = potential_stereo[0]
        return radar_obj_mask, stereo_right[1], stereo_right[2]
        
def correctRadarMatch(matched_radar_instances, radar_RAD_mask, velocity_weight = 0.4):
    """
    Function:
        Find the right match

    Args:
        matched_radar_instances         ->      [[mask, class], [mask, class], ...]
        radar_RAD_mask                  ->      shape: [256,256,64] original mask
    """
    combined_instances = []
    ### velocity gap that can be included as one instance.
    gap_num = 2

    for i in range(len(matched_radar_instances)):
        mask = matched_radar_instances[i][0]
        cls = matched_radar_instances[i][1]
        mask_RD_dim = np.where(getSumDim(mask, 1)>0., 1., 0.)
        mask_D_dim = getSumDim(mask_RD_dim, 0)
        D_indes = np.where(mask_D_dim>0.)[0]
        is_included = False
        if len(combined_instances) > 0:
            for j in range(len(combined_instances)):
                current_inst_idx = combined_instances[j][2]
                d_dist_min = radar_RAD_mask.shape[2]
                if is_included:
                    continue

                ######################## point-wise min distance match ################
                for k in range(len(current_inst_idx)):
                    d_ind = current_inst_idx[k]
                    d_min = np.amin(np.abs(D_indes - d_ind))
                    d_lshift_min = np.amin(np.abs(D_indes + radar_RAD_mask.shape[2] - d_ind))
                    d_rshift_min = np.amin(np.abs(D_indes - radar_RAD_mask.shape[2] - d_ind))
                    d_min = np.amin([d_min, d_lshift_min, d_rshift_min])
                    if d_min <= d_dist_min:
                        d_dist_min = d_min

                ################## most-likely point min distance match ###############
                # nums_2find_Dmain = []
                # for indx in range(len(D_indes)):
                    # current_R = mask_RD_dim[:, D_indes[indx]]
                    # nums_2find_Dmain.append(len(current_R[current_R>0]))
                # D_main = D_indes[np.argmax(nums_2find_Dmain)]
                # d_min = np.amin(np.abs(D_main - current_inst_idx))
                # d_lshift_min = np.amin(np.abs(D_main + radar_RAD_mask.shape[2] - current_inst_idx))
                # d_rshift_min = np.amin(np.abs(D_main - radar_RAD_mask.shape[2] - current_inst_idx))
                # d_min = np.mean(np.abs(D_main - current_inst_idx))
                # d_lshift_min = np.mean(np.abs(D_main + radar_RAD_mask.shape[2] - current_inst_idx))
                # d_rshift_min = np.mean(np.abs(D_main - radar_RAD_mask.shape[2] - current_inst_idx))
                # d_min = np.amin([d_min, d_lshift_min, d_rshift_min])
                # if d_min <= d_dist_min:
                    # d_dist_min = d_min
                ########################################################################

                if d_dist_min <= gap_num:
                    combined_instances[j][0] += mask
                    combined_instances[j][1] = cls
                    combined_instances[j][2] = np.unique(np.concatenate(\
                            [combined_instances[j][2], D_indes], axis=0))
                    is_included = True
            if not is_included:
                combined_instances.append([mask, cls, np.unique(D_indes)])
        else:
            combined_instances.append([mask, cls, np.unique(D_indes)])

    # print("total number of matched inst", len(combined_instances))
    # for j in range(len(combined_instances)):
        # print(combined_instances[j][2])
    
    ################### TODO: choose the instance with max number of detections ###########
    # if len(combined_instances) > 1:
        # max_num = 0
        # for i in range(len(combined_instances)):
            # inst_mask = combined_instances[i][0]
            # inst_num = len(inst_mask[inst_mask>0])
            # if inst_num >= max_num:
                # inst_num = max_num
                # output_mask = combined_instances[i][0]
                # output_class = combined_instances[i][1]
    # else:
        # output_mask = combined_instances[0][0]
        # output_class = combined_instances[0][1]
    # return output_mask, output_class

    ################ TODO: choose the instance with larger speed #############
    if len(combined_instances) > 1:
        static_weight_portion = 0.4
        win_size = 1
        original_RD = np.where(getSumDim(radar_RAD_mask, 1)>0, 1., 0.)
        R_nums = getSumDim(original_RD, 0)
        RD_pad = np.concatenate([original_RD, original_RD, original_RD], axis=1)
        total_R_nums = []
        total_R_ind = []
        for i in range(0, original_RD.shape[1]):
            i_i = i + original_RD.shape[1]
            win = np.zeros(RD_pad.shape)
            win[:, i_i-win_size:i_i+win_size+1] = 1.
            res = win * RD_pad
            res_R_dim = getSumDim(res, -1)
            # print(res_R_dim.shape)
            res_gap = 0
            if len(np.where(res>0)[0]) > 0:
                res_gap = np.where(res>0)[0].max() - np.where(res>0)[0].min()
            ### range of the R indexes to decide which one is STATIC AXIS ###
            res_gap = (1-static_weight_portion)*res_gap + \
                        static_weight_portion*len(np.where(res_R_dim>0)[0])
            ### total number of the RD indexes to decide which one is STATIC AXIS ###
            # res_gap = (1-static_weight_portion)*res_gap + \
                        # static_weight_portion*len(res[res>0])
            total_R_nums.append(res_gap)
            total_R_ind.append(i)
        ind_max = np.argmax(total_R_nums)
        i_static = total_R_ind[ind_max]
        print("--- static axis is: ---", i_static)
        cost = 0
        for j in range(len(combined_instances)):
            # ind_diff = np.amin(np.abs(combined_instances[j][2] - i_static))
            # ind_diff_lshift = np.amin(np.abs(combined_instances[j][2] + len(R_nums) - i_static))
            # ind_diff_rshift = np.amin(np.abs(combined_instances[j][2] - len(R_nums) - i_static))
            ind_diff = np.mean(np.abs(combined_instances[j][2] - i_static))
            ind_diff_lshift = np.mean(np.abs(combined_instances[j][2] + len(R_nums) - i_static))
            ind_diff_rshift = np.mean(np.abs(combined_instances[j][2] - len(R_nums) - i_static))
            ### NOTE: ind_min is the velocity difference ###
            ind_min = np.amin([ind_diff, ind_diff_lshift, ind_diff_rshift])
            ### NOTE: occupancy is the total number of indexes it occupies ###
            inst_mask = combined_instances[j][0]
            occupancy = len(inst_mask[inst_mask>0])
            instance_cost = velocity_weight * ind_min + (1-velocity_weight) * occupancy
            if instance_cost >= cost:
                cost = instance_cost
                output_mask = combined_instances[j][0]
                output_class = combined_instances[j][1]
    else:
        output_mask = combined_instances[0][0]
        output_class = combined_instances[0][1]
    return output_mask, output_class

def RAD2bbox3D(radar_masks):
    """ Transfer RAD masks to 3D bounding boxes """
    bbox = []
    for mask_i in range(len(radar_masks)):
        mask = radar_masks[mask_i]
        indexes = []
        for i in range(mask.shape[0]):
            for j in range(mask.shape[1]):
                for k in range(mask.shape[2]):
                    if mask[i,j,k] > 0:
                        indexes.append([i, j, k])
        indexes = np.array(indexes)
        x_min, x_max = np.amin(indexes[:, 0]), np.amax(indexes[:, 0])+1
        y_min, y_max = np.amin(indexes[:, 1]), np.amax(indexes[:, 1])+1
        z_min, z_max = np.amin(indexes[:, 2]), np.amax(indexes[:, 2])+1
        bbox.append([x_min, x_max, y_min, y_max, z_min, z_max])
    return np.array(bbox)

##################### coordinate transformation ######################
def cartesianToPolar(x, y):
    rho = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    return(rho, phi)

def polarToCartesian(rho, phi):
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return(x, y)

###################### PLOTTING FUNCTIONS START #########################
def prepareFigure(num_axes, figsize=None):
    assert num_axes <= 4
    if figsize is not None:
        fig = plt.figure(figsize=figsize)
    else:
        fig = plt.figure()
    if num_axes == 1:
        ax1 = fig.add_subplot(111)
        return fig, [ax1]
    if num_axes == 2: 
        ax1 = fig.add_subplot(121)
        ax2 = fig.add_subplot(122)
        return fig, [ax1, ax2]
    if num_axes == 3:
        ax1 = fig.add_subplot(131)
        ax2 = fig.add_subplot(132)
        ax3 = fig.add_subplot(133)
        return fig, [ax1, ax2, ax3]
    if num_axes == 4:
        ax1 = fig.add_subplot(221)
        ax2 = fig.add_subplot(222)
        ax3 = fig.add_subplot(223)
        ax4 = fig.add_subplot(224)
        return fig, [ax1, ax2, ax3, ax4]

def clearAxes(ax_list):
    assert len(ax_list) >=1 
    plt.cla()
    for ax_i in ax_list:
        ax_i.clear()

def createColorList(class_names, class_list, all_colors):
    color_output_list = []
    for i in range(len(class_list)):
        current_class = class_list[i]
        color_i = np.array([all_colors[class_names.index(current_class)]])
        color_output_list.append(color_i)
    return color_output_list
 
def createLabelList(class_list, label_prefix):
    label_list = []
    for i in range(len(class_list)):
        current_class = class_list[i]
        label_list.append(label_prefix + "_" + current_class)
    return label_list

def pointScatter(bg_points, point_list, color_list, label_list, ax, xlimits, ylimits, title):
    assert len(point_list) == len(color_list)
    assert len(point_list) == len(label_list)
    if bg_points is not None:
        ax.scatter(bg_points[:, 0], bg_points[:, 1], s=0.2, c='blue', label="background")
    for i in range(len(point_list)):
        points_i = point_list[i]
        color_i = color_list[i]
        label_i = label_list[i]
        ax.scatter(points_i[:, 0], points_i[:,1], s=0.3, c=color_i, label=label_i)
    ax.set_xlim(xlimits)
    ax.set_ylim(ylimits)
    ax.legend()
    ax.set_title(title)

def pclScatter(pcl_list, color_list, label_list, ax, xlimits, ylimits, title):
    assert len(pcl_list) == len(color_list)
    if label_list is not None:
        assert len(pcl_list) == len(label_list)
    for i in range(len(pcl_list)):
        pcl = pcl_list[i]
        color = color_list[i]
        if label_list == None:
            label = None
        else:
            label = label_list[i]
        ax.scatter(pcl[:, 0], pcl[:, 1], s=1, c=color, label=label)
    ax.set_xlim(xlimits)
    ax.set_ylim(ylimits)
    ax.set_title(title)

def imgPlot(img, ax, cmap, alpha, title=None):
    ax.imshow(img, cmap=cmap, alpha=alpha)
    if title == "RD":
        ax.set_xticks([0, 16, 32, 48, 63])
        ax.set_xticklabels([int(VELOCITY_MIN), int(VELOCITY_MIN)/2, 0, \
                            int(VELOCITY_MAX)/2, int(VELOCITY_MAX)])
        ax.set_yticks([0, 64, 128, 192, 255])
        ax.set_yticklabels([50, 37.5, 25, 12.5, 0])
        ax.set_xlabel("velocity (m/s)")
        ax.set_ylabel("range (m)")
    elif title == "RA":
        ax.set_xticks([0, 64, 128, 192, 255])
        ax.set_xticklabels([-90, -45, 0, 45, 90])
        ax.set_yticks([0, 64, 128, 192, 255])
        ax.set_yticklabels([50, 37.5, 25, 12.5, 0])
        ax.set_xlabel("angle (degrees)")
        ax.set_ylabel("range (m)")
    elif title == "RAD mask in cartesian":
        ax.set_xticks([0, 128, 256, 384, 512])
        ax.set_xticklabels([-50, -25, 0, 25, 50])
        ax.set_yticks([0, 64, 128, 192, 255])
        ax.set_yticklabels([50, 37.5, 25, 12.5, 0])
        ax.set_xlabel("x (m)")
        ax.set_ylabel("z (m)")
    else:
        ax.axis('off')
    if title is not None:
        ax.set_title(title)

def keepDrawing(fig, time_duration):
    fig.canvas.draw()
    plt.pause(time_duration)

def norm2Image(array):
    norm_sig = plt.Normalize()
    img = plt.cm.viridis(norm_sig(array))
    img *= 255.
    img = img.astype(np.uint8)
    return img

def applyMask(image, mask, color, alpha=0.5):
    """
    Apply the given mask to the image.
    """
    for c in range(3):
        image[:, :, c] = np.where(mask == 1,
                                 image[:, :, c] *
                                (1 - alpha) + alpha * color[c] * 255,
                                 image[:, :, c])
    return image

def drawContour(mask, axe, color):
    """
    Draw mask contour onto the image.
    """
    mask_padded = np.zeros((mask.shape[0]+2, mask.shape[1]+2), dtype=np.uint8)
    mask_padded[1:-1, 1:-1] = mask
    contours = find_contours(mask_padded, 0.1, fully_connected='low')
    for verts in contours:
        verts = np.fliplr(verts) - 1
        p = Polygon(verts, facecolor="none", edgecolor=color)
        axe.add_patch(p)

def radarMaskDopplerFilter(radar_masks, gap_threshold=3, n_jobs=1):
    """
    Filter annotated masks.
    """
    output_masks = []
    for mask_i in range(len(radar_masks)):
        mask = radar_masks[mask_i]
        idxes = []
        boundings = []
        num_pnts = []
        filtered_mask = np.zeros(radar_masks[0].shape)
        for i in range(mask.shape[0]):
            for j in range(mask.shape[1]):
                for k in range(mask.shape[2]):
                    if mask[i, j, k] == 1:
                        idxes.append([i, j, k])
        idxes = np.array(idxes)
        idx_clusters = DbscanDenoise(idxes, epsilon=15, minimum_samples=1, n_jobs=n_jobs)
        if len(idx_clusters) > 1:
            for cluster_id in range(len(idx_clusters)):
                current_cluster = idx_clusters[cluster_id]
                doppler_min = np.amin(current_cluster[:, 2])
                doppler_max = np.amax(current_cluster[:, 2])
                num_pnts.append(len(current_cluster))
                boundings.append([doppler_min, doppler_max])
            index_sorted = np.argsort(num_pnts)[::-1]
            at_edge = False
            for m in range(len(index_sorted)):
                ind = index_sorted[m]
                bounds = boundings[ind]
                if m == 0:
                    for i in range(len(idx_clusters[ind])):
                        target_idxes = idx_clusters[ind][i]
                        filtered_mask[target_idxes[0], target_idxes[1], \
                                    target_idxes[2]] = 1.
                    if np.abs(bounds[0]) < gap_threshold or \
                            np.abs(bounds[1]-DOPPLER_SIZE) < gap_threshold:
                        at_edge = True
                else:
                    if at_edge and (np.abs(bounds[0]) < gap_threshold or \
                            np.abs(bounds[1]-DOPPLER_SIZE) < gap_threshold):
                        for i in range(len(idx_clusters[ind])):
                            target_idxes = idx_clusters[ind][i]
                            filtered_mask[target_idxes[0], target_idxes[1], \
                                        target_idxes[2]] = 1.
                    else:
                        continue

                # if np.abs(bounds[0]) < gap_threshold or \
                    # np.abs(bounds[1]-DOPPLER_SIZE) < gap_threshold or m==0:
                    # for i in range(len(idx_clusters[ind])):
                        # target_idxes = idx_clusters[ind][i]
                        # filtered_mask[target_idxes[0], target_idxes[1], \
                                    # target_idxes[2]] = 1.
                # else:
                    # break

            filtered_mask = np.expand_dims(filtered_mask, axis=0)
            output_masks.append(filtered_mask)
        else:
            mask = np.expand_dims(mask, axis=0)
            output_masks.append(mask)
    output_masks = np.concatenate(output_masks, axis=0)
    return output_masks

def mask2BoxOrEllipse(mask, mode="box", n_jobs=1):
    """
    Find bounding box from mask
    """
    idxes = []
    output = []
    for i in range(mask.shape[0]):
        for j in range(mask.shape[1]):
            if mask[i, j] == 1:
                idxes.append([i, j])
    idxes = np.array(idxes)
    idx_clusters = [DbscanDenoise(idxes, epsilon=20, minimum_samples=3, n_jobs=n_jobs, \
                                dominant_op=True)]
    if mode == "box":
        for cluster_id in range(len(idx_clusters)):
            current_cluster = idx_clusters[cluster_id]
            if current_cluster.shape[0] == 0:
                continue
            x_min = np.amin(current_cluster[:, 0])
            x_max = np.amax(current_cluster[:, 0])
            y_min = np.amin(current_cluster[:, 1])
            y_max = np.amax(current_cluster[:, 1])
            output.append([x_min, x_max, y_min, y_max])
        if len(output) == 0:
            output = None
        else:
            output = np.array(output)
            x_min = np.amin(output[:, 0])
            x_max = np.amax(output[:, 1])
            for i in range(len(output)):
                output[i, 0] = x_min
                output[i, 1] = x_max
        return output
    elif mode == "ellipse":
        for cluster_id in range(len(idx_clusters)):
            current_cluster = idx_clusters[cluster_id]
            cluster_mean, cluster_cov = GaussianModel(current_cluster)
            output.append([cluster_mean, cluster_cov])
        if len(output) == 0:
            output = None
        return output
    else:
        raise ValueError("Wrong input parameter ------ mode")

def drawBoxOrEllipse(inputs, class_name, axe, color, mode="box"):
    """
    Draw bounding box onto the image.
    """
    if mode == "box":
        for box in inputs:
            y1, y2, x1, x2 = box
            r = Rectangle((x1, y1), x2 - x1, y2 - y1, linewidth=1.5,
                        alpha=0.5, linestyle="dashed", edgecolor=color,
                        facecolor="none")
            axe.add_patch(r)

        # axe.text(x1+2, y1-3, class_name, size=5, verticalalignment='baseline',
        axe.text(x1+1, y1-3, class_name, size=10, verticalalignment='baseline',
                color='w', backgroundcolor="none",
                bbox={'facecolor': color, 'alpha': 0.5,
                    'pad': 2, 'edgecolor': 'none'})
    elif mode == "ellipse":
        for e in inputs:
            mean, cov = e[0], e[1]
            mean = np.flip(mean)
            cov = np.flip(cov)
            x1, y1 = mean
            ell = getEllipse(color, mean, cov, scale_factor=5)
            axe.add_patch(ell)
        axe.text(x1, y1, class_name, size=5, verticalalignment='center',
                color='w', backgroundcolor="none",
                bbox={'facecolor': color, 'alpha': 0.5,
                        'pad': 2, 'edgecolor': 'none'})
    else:
        raise ValueError("Wrong input parameter ------ mode")

def getEllipse(color, means, covariances, scale_factor=1):
    """
    Function:
        Draw 2D Gaussian Ellipse.

    Args:
        means           ->          center of the Gaussian
        covariances     ->          covariance of Gaussian
    """
    sign = np.sign(means[0] / means[1])
    eigen, eigen_vec = np.linalg.eig(covariances)

    eigen_root_x = np.sqrt(eigen[0]) * scale_factor
    eigen_root_y = np.sqrt(eigen[1]) * scale_factor
    theta = np.degrees(np.arctan2(*eigen_vec[:,0][::-1]))

    ell = Ellipse(xy = (means[0], means[1]), width = eigen_root_x,
                height = eigen_root_y, angle = theta, \
                facecolor = 'none', edgecolor = color)
    return ell

def saveFigure(save_dir, name_prefix, frame_i):
    plt.savefig(save_dir + name_prefix + str(frame_i) + ".png")
###################### PLOTTING FUNCTIONS END #########################
 
def debug_formp(frame_i, stereo_obj_pcl_dir, stereo_mrcnn_img_dir,  radar_ral_dir, \
        radar_mask_dir, radar_annotate_dir, frame_delay, distance_threshold, \
        name_prefix, fig, axes, all_colors, registr_mat,  \
        if_plot=True, if_save=True, n_jobs=1):
    """
    DEBUG FUNCTION FOR VISUALIZING (MULTI PROCESSING VERSION)
    """
    stereo_obj_pcl = ReadStereoObjPcl(stereo_obj_pcl_dir, frame_i + frame_delay) 
    radar_RAD_mask = ReadRADMask(radar_mask_dir, frame_i)
    left_img = cv2.imread(os.path.join(stereo_mrcnn_img_dir, \
                    "stereo_input_%.d.jpg"%(frame_i+frame_delay)))

    # RAD_FFT = readRAD(os.path.join(radar_ral_dir, "RAD_numpy"), frame_i)
        # RAD_FFT is None or\
    if stereo_obj_pcl is None or radar_RAD_mask is None or \
        left_img is None:
        print("Frame %d has no object detected."%(frame_i))
        if stereo_obj_pcl is None:
            print("no stereo pcl loaded")
        if radar_RAD_mask is None:
            print("no rad mask loaded")
        if left_img is None:
            print("no stereo left img loaded")
        pass
    else:
        # RAD_FFT = getMagnitude(RAD_FFT,  power_order=2)
        # RD_img = norm2Image(getLog(getSumDim(RAD_FFT, 1), scalar=10, log_10=True))
        # RA_img = norm2Image(getLog(getSumDim(RAD_FFT, -1), scalar=10, log_10=True))
        # RA_mag = getLog(getSumDim(RAD_FFT, -1), scalar=10, log_10=True)

        radar_mask_instances = InstancizeRADMask(radar_RAD_mask, n_jobs=n_jobs)
        # radar_mask_instances = InstancizeRADMask2D1D(radar_RAD_mask)
        if len(radar_mask_instances) == 0:
            pass
        else:
            radar_dict = {}
            radar_classes = []
            radar_masks = []
            stereo_all_objs = []
            stereo_all_colors = []
            stereo_all_cls = []
            stereo_all_msks = []
            for class_i in range(len(stereo_obj_pcl['class'])):
                current_class = stereo_obj_pcl['class'][class_i]
                if current_class not in ROAD_USERS:
                    continue
                stereo_pcl = stereo_obj_pcl['pcl'][class_i]
                stereo_current_mask = stereo_obj_pcl['mask'][class_i]
                if len(stereo_pcl) == 0:
                    continue
                stereo_pcl = DbscanDenoise(stereo_pcl, \
                                    epsilon=STEREO_3D_DBCAN_EPSILON, \
                                    minimum_samples=5, n_jobs=n_jobs, dominant_op=True)
                if len(stereo_pcl) == 0:
                    continue
                stereo_to_radar = CalibrateStereoToRadar(stereo_pcl, \
                                                        registr_mat)
                stereo_pcl_dbscan = stereo_to_radar
                stereo_pcl_dbscan = DbscanDenoise(stereo_to_radar, \
                                    epsilon=STEREO_2D_DBCAN_EPSILON, \
                                    minimum_samples=5, n_jobs=n_jobs, dominant_op=True)
                if len(stereo_pcl_dbscan) == 0:
                    continue
                threshold_pclmask = transferPcl2Cartesian(stereo_pcl_dbscan)
                if current_class not in ['person', 'bicycle'] \
                        and (len(stereo_pcl_dbscan) <= STEREO_CAR_PCL_THRESHOLD \
                        or len(threshold_pclmask[threshold_pclmask>0]) <= \
                                                    STEREO_CAR_PCL_THRESHOLD):
                    continue
                if current_class in ["person", "bicycle", "motorcycle"]:
                    current_class = "person"
                else:
                    current_class = "car"
                stereo_pcl_dbscan = [stereo_pcl_dbscan]
                ##### loop over each object of the current class #####
                mask_class = []
                stereo_objs = []
                for obj_i in range(len(stereo_pcl_dbscan)):
                    current_stereo_obj = stereo_pcl_dbscan[obj_i]
                    stereo_obj_to_radar = current_stereo_obj
                    stereo_objs.append(stereo_obj_to_radar)
                class_color = all_colors[CLASS_NAMES.index(current_class)]
                if len(stereo_objs) != 0:
                    stereo_objs = np.concatenate(stereo_objs, axis=0)
                    if len(stereo_objs) != 0:
                        stereo_all_objs.append(stereo_objs)
                        stereo_all_colors.append(np.expand_dims(np.array(class_color), 0))
                        stereo_all_cls.append(current_class)
                        stereo_all_msks.append(stereo_current_mask)

            if if_plot:
                clearAxes(axes)
                mrcnn_img = ReadStereoMrcnnImg(stereo_mrcnn_img_dir, frame_i+frame_delay)
                cart_image = np.zeros([radar_RAD_mask.shape[0], \
                                    radar_RAD_mask.shape[0]*2])
                cart_image = norm2Image(cart_image)
                ccccccc = RandomColors(len(radar_mask_instances))
                for ii in range(len(radar_mask_instances)):
                    mask = radar_mask_instances[ii][0]
                    RD_mask = np.where(getSumDim(mask, 1) >= 1, 1, 0)
                    RA_mask = np.where(getSumDim(mask, -1) >= 1, 1, 0)
                    Cartesian_mask = transferMaskToCartesianMask(RA_mask)
                    # color_c = ccccccc[ii]
                    color_c = [1., 1., 1.]
                    # applyMask(RD_img, RD_mask, color_c)
                    # applyMask(RA_img, RA_mask, color_c)
                    applyMask(cart_image, Cartesian_mask, color_c)
                stereo_dict = {}
                stereo_cls = []
                stereo_cart_masks = []
                stereo_cart_boxes = []
                stereo_mrcnn_boxes = []
                stereo_mrcnn_masks = []
                if not os.path.exists("./sensorcortek_stereo"):
                    os.mkdir("./sensorcortek_stereo")
                for stereo_i in range(len(stereo_all_objs)):
                    stereo_obj_ii = stereo_all_objs[stereo_i]
                    color_ii = stereo_all_colors[stereo_i][0]
                    class_ii = stereo_all_cls[stereo_i]
                    mask_ii = stereo_all_msks[stereo_i]
                    stereo_mask_i = transferPcl2Cartesian(stereo_obj_ii)
                    mode = "box"
                    stereo_mask_box = mask2BoxOrEllipse(stereo_mask_i, mode, \
                                                        n_jobs=n_jobs)
                    stereo_img_box = mask2BoxOrEllipse(mask_ii, mode, \
                                                        n_jobs=n_jobs)
                    if stereo_mask_box is not None and stereo_img_box is not None:
                        applyMask(cart_image, stereo_mask_i, color_ii)
                        drawBoxOrEllipse(stereo_mask_box, class_ii, axes[1], color_ii, mode)
                        applyMask(left_img, mask_ii, color_ii)
                        drawBoxOrEllipse(stereo_img_box, class_ii, axes[0], color_ii, mode)
                        stereo_cls.append(class_ii)
                        stereo_cart_masks.append(np.expand_dims(stereo_mask_i, 0))
                        stereo_cart_masks.append(np.expand_dims(mask_ii, 0))
                        stereo_cart_boxes.append(stereo_mask_box)
                        stereo_mrcnn_boxes.append(stereo_img_box)
                if len(stereo_cart_masks) != 0 or len(stereo_cart_boxes) != 0:
                    stereo_cart_masks = np.concatenate(stereo_cart_masks, 0)
                    stereo_cart_boxes = np.concatenate(stereo_cart_boxes, 0)
                    stereo_mrcnn_boxes = np.concatenate(stereo_mrcnn_boxes, 0)
                    stereo_mrcnn_masks = np.concatenate(stereo_mrcnn_masks, 0)
                    print("------------- classes -----------")
                    print(len(stereo_cls))
                    print("------------- cart --------------")
                    print(stereo_cart_masks.shape)
                    print(stereo_cart_boxes.shape)
                    print("------------- mrcnn -------------")
                    print(stereo_mrcnn_boxes.shape)
                    print(stereo_mrcnn_masks.shape)
                    stereo_dict["classes"] = stereo_cls
                    stereo_dict["masks"] = stereo_cart_masks
                    stereo_dict["boxes"] = stereo_cart_boxes
                    stereo_dict["mrcnn_boxes"] = stereo_mrcnn_boxes
                    stereo_dict["mrcnn_masks"] = stereo_mrcnn_masks
                    WriteStereoObj("./sensorcortek_stereo/", stereo_dict, frame_i)
                    imgPlot(left_img, axes[0], None, 1, "Mask-RCNN")
                    imgPlot(cart_image, axes[1], None, 1, "Cartesian")
                    # keepDrawing(fig, 0.1)
                    saveFigure("./sensorcortek_stereo/", "debug_", frame_i)

def main_formp(frame_i, stereo_obj_pcl_dir, stereo_mrcnn_img_dir, stereo_img_dir, \
        radar_ral_dir, radar_mask_dir, radar_annotate_dir, frame_delay, \
        distance_threshold, name_prefix, fig, axes, all_colors, registr_mat,  \
        if_plot=True, if_save=True, n_jobs=1):
    """
    MAIN FUNCTION FOR MULTI PROCESSING
    """
    stereo_obj_pcl = ReadStereoObjPcl(stereo_obj_pcl_dir, frame_i + frame_delay) 
    radar_RAD_mask = ReadRADMask(radar_mask_dir, frame_i)

    RAD_FFT = readRAD(os.path.join(radar_ral_dir, "RAD_numpy"), frame_i)
    left_img = ReadStereoImg(stereo_img_dir, frame_i+frame_delay)
    if stereo_obj_pcl is None or radar_RAD_mask is None or RAD_FFT is None or \
            left_img is None:
        # ReadStereoMrcnnImg(stereo_mrcnn_img_dir, frame_i+frame_delay) is None:
        print("Frame %d has no object detected."%(frame_i))
        if stereo_obj_pcl is None:
            print("stereo pcl not loaded.")
        if radar_RAD_mask is None:
            print("RAD mask not loaded.")
        if RAD_FFT is None:
            print("RAD data not loaded.")
        if left_img is None:
            print("stereo left image not loaded.")
        pass
    else:
        RAD_FFT = getMagnitude(RAD_FFT,  power_order=2)
        RD_img = norm2Image(getLog(getSumDim(RAD_FFT, 1), scalar=10, log_10=True))
        RA_img = norm2Image(getLog(getSumDim(RAD_FFT, -1), scalar=10, log_10=True))

        ########## remove all static points, keep moving points #########
        # radar_RAD_mask[:, :, int(radar_RAD_mask.shape[2]/2)] = 0.
        ################################################################
        
        radar_mask_instances = InstancizeRADMask(radar_RAD_mask, n_jobs=n_jobs)
        # radar_mask_instances = InstancizeRADMask2D1D(radar_RAD_mask)
        if len(radar_mask_instances) == 0:
            pass
        else:
            stereo_all_pcls = []
            stereo_all_masks = []
            stereo_all_classes = []
            stereo_all_obj_index = []
            for class_i in range(len(stereo_obj_pcl['class'])):
                current_class = stereo_obj_pcl['class'][class_i]
                if current_class not in ROAD_USERS:
                    continue
                current_class_ind = CLASS_NAMES.index(current_class)
                stereo_pcl = stereo_obj_pcl['pcl'][class_i]
                current_mask = stereo_obj_pcl['mask'][class_i]
                if len(stereo_pcl) == 0:
                    continue
                stereo_pcl = DbscanDenoise(stereo_pcl, \
                                    epsilon=STEREO_3D_DBCAN_EPSILON, \
                                    minimum_samples=5, n_jobs=n_jobs, dominant_op=True)
                if len(stereo_pcl) == 0:
                    continue
                stereo_to_radar = CalibrateStereoToRadar(stereo_pcl, \
                                                        registr_mat)
                if len(stereo_to_radar) == 0:
                    continue
                stereo_pcl_dbscan = DbscanDenoise(stereo_to_radar, \
                                    epsilon=STEREO_2D_DBCAN_EPSILON, \
                                    minimum_samples=5, n_jobs=n_jobs, dominant_op=True)
                if len(stereo_pcl_dbscan) == 0:
                    continue
                threshold_pclmask = transferPcl2Cartesian(stereo_pcl_dbscan)
                if current_class not in ['person', 'bicycle'] \
                        and (len(stereo_pcl_dbscan) <= STEREO_CAR_PCL_THRESHOLD \
                        or len(threshold_pclmask[threshold_pclmask>0]) <= \
                                                    STEREO_CAR_PCL_THRESHOLD):
                    continue
                stereo_all_pcls.append(stereo_pcl_dbscan)
                stereo_all_masks.append(current_mask)
                stereo_all_classes.append(current_class_ind)
                stereo_all_obj_index.append(class_i)

            radar_dict = {}
            radar_classes = []
            radar_masks = []
            stereo_correspond_masks = []

            radar_classes_original = []
            radar_masks_original = []
            radar_stereo_idx = []
            for inst_idx in range(len(radar_mask_instances)):
                rad_mask_instance = radar_mask_instances[inst_idx]
                rad_mask, class_ind, obj_ind = FindRadarObjInstance(rad_mask_instance, \
                        stereo_all_pcls, stereo_all_classes, stereo_all_obj_index)
                if class_ind is None:
                    continue
                radar_classes_original.append(class_ind)
                radar_stereo_idx.append(obj_ind)
                radar_masks_original.append(rad_mask)
            if len(radar_masks_original) == 0:
                pass
            else:
                unique_indexes = np.unique(radar_stereo_idx)
                for u_i in range(len(unique_indexes)):
                    u_idx = unique_indexes[u_i]
                    mask_idx = stereo_all_obj_index.index(u_idx)
                    ########### TODO: find the right radar instances ###########
                    obj_RAD_mask = np.zeros(radar_RAD_mask.shape)
                    matched_radar_instances = []
                    for orig_i in range(len(radar_stereo_idx)):
                        if radar_stereo_idx[orig_i] == u_idx:
                            # obj_RAD_mask += radar_masks_original[orig_i]
                            # obj_RAD_class = CLASS_NAMES[radar_classes_original[orig_i]]
                            matched_radar_instances.append([radar_masks_original[orig_i], \
                                                        radar_classes_original[orig_i]])
                    ######## NOTE: correct radar annotations based on velocity ######
                    if len(matched_radar_instances) >= 1:
                        obj_RAD_mask, obj_RAD_class_ind = correctRadarMatch(\
                                    matched_radar_instances, radar_RAD_mask, velocity_weight=0.4)
                        obj_RAD_class = CLASS_NAMES[obj_RAD_class_ind]

                    obj_RAD_mask = np.where(obj_RAD_mask > 0, 1., 0.)
                    if obj_RAD_mask.max() > 0:
                        radar_masks.append(np.expand_dims(obj_RAD_mask, 0))
                        radar_classes.append(obj_RAD_class)
                        stereo_correspond_masks.append(\
                                np.expand_dims(stereo_all_masks[mask_idx], 0))

                if len(radar_masks) != 0:
                    radar_masks = np.concatenate(radar_masks, 0)
                    stereo_correspond_masks = np.concatenate(stereo_correspond_masks, 0)
                    radar_masks = radarMaskDopplerFilter(radar_masks, n_jobs=n_jobs)
                    radar_dict["classes"] = radar_classes
                    radar_dict["masks"] = radar_masks
                    radar_dict["boxes"] = RAD2bbox3D(radar_masks)
                    # radar_dict["stereo_masks"] = stereo_correspond_masks

                    if if_save:
                        WriteRadarObjPcl(radar_annotate_dir, radar_dict, frame_i)

                    if if_plot:
                        clearAxes(axes)

                        ellipses = []
                        RA_cart_img = np.zeros([radar_RAD_mask.shape[0], \
                                            radar_RAD_mask.shape[0]*2])
                        RA_cart_img = norm2Image(RA_cart_img)
                        RA_original_mask = np.where(getSumDim(radar_RAD_mask, -1) > 0, 1., 0.)

                        for class_id in range(len(radar_classes)):
                            obj_class = radar_classes[class_id]
                            class_num = CLASS_NAMES.index(obj_class)
                            class_color = all_colors[class_num]
                            obj_masks = radar_masks[class_id]
                            ######## draw stereo mask and box #######
                            stereo_cor_mask = stereo_correspond_masks[class_id]
                            applyMask(left_img, stereo_cor_mask, class_color)
                            stereo_box = mask2BoxOrEllipse(stereo_cor_mask, "box", \
                                                            n_jobs=n_jobs)
                            drawBoxOrEllipse(stereo_box, obj_class, axes[0], class_color, "box")
                            ########################################
                            obj_masks_combined = obj_masks
                            obj_masks_combined = np.where(obj_masks_combined > 0, 1., 0.)
                            RD_mask = np.where(getSumDim(obj_masks_combined, 1) >= 1, 1, 0)
                            RA_mask = np.where(getSumDim(obj_masks_combined, -1) >= 1, 1, 0)
                            RA_original_mask -= RA_mask
                            RA_cart_mask = transferMaskToCartesianMask(RA_mask)
                            applyMask(RD_img, RD_mask, class_color)
                            applyMask(RA_img, RA_mask, class_color)
                            applyMask(RA_cart_img, RA_cart_mask, class_color)
                            RA_cartesian = transfer2Scatter(RA_mask)
                            if len(RA_cartesian) == 0:
                                continue
                            ############# draw mask contour ################
                            drawContour(RA_cart_mask, axes[1], class_color)
                            drawContour(RD_mask, axes[2], class_color)
                            drawContour(RA_mask, axes[3], class_color)
                            ############# draw box or ellipses ################
                            mode = "box" # either "box" or "ellipse"
                            RD_box = mask2BoxOrEllipse(RD_mask, mode, n_jobs=n_jobs)
                            RA_box = mask2BoxOrEllipse(RA_mask, mode, n_jobs=n_jobs)
                            RA_cart_box = mask2BoxOrEllipse(RA_cart_mask, mode, \
                                                            n_jobs=n_jobs)
                            if RD_box is None or RA_box is None or RA_cart_box is None:
                                continue
                            drawBoxOrEllipse(RA_cart_box, obj_class, axes[1], class_color, mode)
                            drawBoxOrEllipse(RD_box, obj_class, axes[2], class_color, mode)
                            drawBoxOrEllipse(RA_box, obj_class, axes[3], class_color, mode)

                        RA_original_mask = np.where(RA_original_mask > 0, 1., 0.)
                        RA_rest = transferMaskToCartesianMask(RA_original_mask)
                        applyMask(RA_cart_img, RA_rest, [1,1,1])
                        
                        # imgPlot(ReadStereoMrcnnImg(stereo_mrcnn_img_dir, frame_i+frame_delay), \
                                # axes[0], None, 1,  "mrcnn")
                        imgPlot(left_img, axes[0], None, 1,  "mrcnn")
                        imgPlot(RA_cart_img, axes[1], None, 1, "RAD mask in cartesian")
                        imgPlot(RD_img, axes[2], None, 1, "RD")
                        imgPlot(RA_img, axes[3], None, 1, "RA")
                        # keepDrawing(fig, 0.1)
                        saveFigure(radar_annotate_dir, name_prefix, frame_i)

def debug_mp(stereo_obj_pcl_dir, stereo_mrcnn_img_dir,  radar_ral_dir, \
        radar_mask_dir, registration_matrix_dir, radar_annotate_dir, sequence, \
        if_plot=True, if_save=True):
    ##### frame delay between two sensors (not sure whether it exists) #####
    frame_delay = 0
    distance_threshold = 5.0
    name_prefix = "debug_"

    if if_plot:
        fig, axes = prepareFigure(2, figsize=(20,10))
        # fig, axes = prepareFigure(4, figsize=(24,12))
    else:
        fig, axes = prepareFigure(4, figsize=(24,12))

    all_colors = RandomColors(len(CLASS_NAMES))
    registr_mat = ReadRegistrationMatrix(registration_matrix_dir)

    pool = mp.Pool(4)

    debug_formp_1arg = partial(debug_formp, 
                                stereo_obj_pcl_dir = stereo_obj_pcl_dir,
                                stereo_mrcnn_img_dir = stereo_mrcnn_img_dir,
                                radar_ral_dir = radar_ral_dir,
                                radar_mask_dir = radar_mask_dir,
                                radar_annotate_dir = radar_annotate_dir,
                                frame_delay = frame_delay,
                                distance_threshold = distance_threshold,
                                name_prefix = name_prefix,
                                fig = fig,
                                axes = axes,
                                all_colors = all_colors,
                                registr_mat = registr_mat,
                                if_plot = if_plot,
                                if_save = if_save)
    
    for _ in tqdm(pool.imap_unordered(debug_formp_1arg, \
                    range(sequence[0], sequence[1])), \
                    total = sequence[1] - sequence[0]):
        pass
    pool.close()
    pool.join()
    pool.close()

def debug_normal(stereo_obj_pcl_dir, stereo_mrcnn_img_dir,  radar_ral_dir, \
        radar_mask_dir, registration_matrix_dir, radar_annotate_dir, sequence, \
        if_plot=True, if_save=True, n_jobs=1):
    ##### frame delay between two sensors (not sure whether it exists) #####
    frame_delay = 0
    distance_threshold = 5.0
    name_prefix = "debug_"

    if if_plot:
        fig, axes = prepareFigure(2, figsize=(20,10))
        # fig, axes = prepareFigure(4, figsize=(24,12))
    else:
        fig, axes = prepareFigure(4, figsize=(24,12))

    all_colors = RandomColors(len(CLASS_NAMES))
    registr_mat = ReadRegistrationMatrix(registration_matrix_dir)

    for frame_i in tqdm(range(sequence[0], sequence[1])):
        debug_formp(frame_i = frame_i,
                    stereo_obj_pcl_dir = stereo_obj_pcl_dir,
                    stereo_mrcnn_img_dir = stereo_mrcnn_img_dir,
                    radar_ral_dir = radar_ral_dir,
                    radar_mask_dir = radar_mask_dir,
                    radar_annotate_dir = radar_annotate_dir,
                    frame_delay = frame_delay,
                    distance_threshold = distance_threshold,
                    name_prefix = name_prefix,
                    fig = fig,
                    axes = axes,
                    all_colors = all_colors,
                    registr_mat = registr_mat,
                    if_plot = if_plot,
                    if_save = if_save,
                    n_jobs = n_jobs)

def main_mp(stereo_obj_pcl_dir, stereo_mrcnn_img_dir, stereo_img_dir, radar_ral_dir, \
        radar_mask_dir, registration_matrix_dir, radar_annotate_dir, sequence, \
        if_plot=True, if_save=True):
    ##### frame delay between two sensors (not sure whether it exists) #####
    frame_delay = 0
    distance_threshold = 5.0
    # name_prefix = "test_"
    name_prefix = "visualization_"

    if if_plot:
        # fig, axes = prepareFigure(4, figsize=(25,15))
        fig, axes = prepareFigure(4, figsize=(20,10))
    else:
        fig, axes = prepareFigure(2)

    all_colors = RandomColors(len(CLASS_NAMES))
    registr_mat = ReadRegistrationMatrix(registration_matrix_dir)

    pool = mp.Pool(4)

    main_formp_1arg = partial(main_formp, 
                                stereo_obj_pcl_dir = stereo_obj_pcl_dir,
                                stereo_mrcnn_img_dir = stereo_mrcnn_img_dir,
                                stereo_img_dir = stereo_img_dir,
                                radar_ral_dir = radar_ral_dir,
                                radar_mask_dir = radar_mask_dir,
                                radar_annotate_dir = radar_annotate_dir,
                                frame_delay = frame_delay,
                                distance_threshold = distance_threshold,
                                name_prefix = name_prefix,
                                fig = fig,
                                axes = axes,
                                all_colors = all_colors,
                                registr_mat = registr_mat,
                                if_plot = if_plot,
                                if_save = if_save)
    
    for _ in tqdm(pool.imap_unordered(main_formp_1arg, \
                    range(sequence[0], sequence[1])), \
                    total = sequence[1] - sequence[0]):
        pass
    pool.close()
    pool.join()
    pool.close()

def main_normal(stereo_obj_pcl_dir, stereo_mrcnn_img_dir, stereo_img_dir, \
        radar_ral_dir, radar_mask_dir, registration_matrix_dir, radar_annotate_dir, \
        sequence, if_plot=True, if_save=True, n_jobs=1):
    ##### frame delay between two sensors (not sure whether it exists) #####
    frame_delay = 0
    distance_threshold = 5.0
    # name_prefix = "test_"
    name_prefix = "visualization_"

    if if_plot:
        # fig, axes = prepareFigure(4, figsize=(25,15))
        fig, axes = prepareFigure(4, figsize=(20,10))
    else:
        fig, axes = prepareFigure(2)

    all_colors = RandomColors(len(CLASS_NAMES))
    registr_mat = ReadRegistrationMatrix(registration_matrix_dir)

    for frame_i in tqdm(range(sequence[0], sequence[1])):
        main_formp(frame_i = frame_i,
                stereo_obj_pcl_dir = stereo_obj_pcl_dir,
                stereo_mrcnn_img_dir = stereo_mrcnn_img_dir,
                stereo_img_dir = stereo_img_dir,
                radar_ral_dir = radar_ral_dir,
                radar_mask_dir = radar_mask_dir,
                radar_annotate_dir = radar_annotate_dir,
                frame_delay = frame_delay,
                distance_threshold = distance_threshold,
                name_prefix = name_prefix,
                fig = fig,
                axes = axes,
                all_colors = all_colors,
                registr_mat = registr_mat,
                if_plot = if_plot,
                if_save = if_save,
                n_jobs = n_jobs)


if __name__ == "__main__":
    time_stamp = "2020-09-03-13-06-36"
    stereo_mrcnn_img_dir = "./stereo_input/"
    stereo_img_dir = "../stereo_process/disparity/"
    stereo_obj_pcl_dir = "./stereo_object_pcl/"
    radar_ral_dir = "/DATA/" + time_stamp + "/ral_outputs_" + time_stamp + "/"
    radar_mask_dir = "../radar_process/radar_ral_process/radar_RAD_mask/"
    registration_matrix_dir = "../registration/registration_matrix/"
    radar_annotate_dir = "./radar_annotation/"
    sequence = [1500, 2210]
    DEBUG = True
    save_gt = True
    if not DEBUG:
        # main_normal(stereo_obj_pcl_dir, stereo_mrcnn_img_dir, stereo_img_dir, \
                # radar_ral_dir, radar_mask_dir, registration_matrix_dir, \
                # radar_annotate_dir, sequence, if_plot=True, if_save=save_gt, n_jobs=6)
        main_mp(stereo_obj_pcl_dir, stereo_mrcnn_img_dir, stereo_img_dir, \
                radar_ral_dir, radar_mask_dir, registration_matrix_dir, \
                radar_annotate_dir, sequence, if_plot=True, if_save=save_gt)
    else:
        debug_normal(stereo_obj_pcl_dir, stereo_mrcnn_img_dir, radar_ral_dir, \
                radar_mask_dir, registration_matrix_dir, radar_annotate_dir, \
                sequence, if_plot=True, n_jobs=1)
        # debug_mp(stereo_obj_pcl_dir, stereo_mrcnn_img_dir, radar_ral_dir, \
                # radar_mask_dir, registration_matrix_dir, radar_annotate_dir, \
                # sequence, if_plot=True)
