import os
import glob
import colorsys
import pickle
import torch, torchvision
import detectron2
from detectron2.utils.logger import setup_logger
setup_logger()

# import some common libraries
import numpy as np
import cv2
import random
from tqdm import tqdm
import matplotlib.pyplot as plt

# import some common detectron2 utilities
from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2.utils.visualizer import Visualizer
from detectron2.data import MetadataCatalog

ALL_CLASSES = ['person', 'bicycle', 'car', 'motorcycle', 'airplane', 'bus', \
        'train', 'truck', 'boat', 'traffic light', 'fire hydrant', 'stop sign', \
        'parking meter', 'bench', 'bird', 'cat', 'dog', 'horse', 'sheep', 'cow', \
        'elephant', 'bear', 'zebra', 'giraffe', 'backpack', 'umbrella', 'handbag', \
        'tie', 'suitcase', 'frisbee', 'skis', 'snowboard', 'sports ball', 'kite', \
        'baseball bat', 'baseball glove', 'skateboard', 'surfboard', \
        'tennis racket', 'bottle', 'wine glass', 'cup', 'fork', 'knife', 'spoon', \
        'bowl', 'banana', 'apple', 'sandwich', 'orange', 'broccoli', 'carrot', \
        'hot dog', 'pizza', 'donut', 'cake', 'chair', 'couch', 'potted plant', \
        'bed', 'dining table', 'toilet', 'tv', 'laptop', 'mouse', 'remote', \
        'keyboard', 'cell phone', 'microwave', 'oven', 'toaster', 'sink', \
        'refrigerator', 'book', 'clock', 'vase', 'scissors', 'teddy bear', \
        'hair drier', 'toothbrush']

DESIGNATED_CLASSES = ['person', 'bicycle', 'car', 'motorcycle', 'airplane', 'bus', \
        'train', 'truck', 'boat', 'traffic light', 'fire hydrant', 'stop sign', \
        'parking meter']

def getSequenceNum(img_dir, img_name):
    """
    Function:
        Get sequence number from the image name
    """
    real_name = img_name.replace(img_dir, '')
    res = ''.join(filter(lambda i: i.isdigit(), real_name))
    sequence_num = int(res)
    return sequence_num

def applyMask(image, mask, color, alpha=0.5):
    """Apply the given mask to the image.
    """
    for c in range(3):
        image[:, :, c] = np.where(mask == 1,
                                  image[:, :, c] *
                                  (1 - alpha) + alpha * color[c] * 255,
                                  image[:, :, c])
    return image

def randomColors(N, bright=True):
    """
    Generate random colors.
    To get visually distinct colors, generate them in HSV space then
    convert to RGB.
    """
    brightness = 1.0 if bright else 0.7
    hsv = [(i / N, 1, brightness) for i in range(N)]
    colors = list(map(lambda c: colorsys.hsv_to_rgb(*c), hsv))
    random.seed(8888)
    random.shuffle(colors)
    return colors

def maskInstances(image, masks, classes, all_colors):
    """Mask all instances onto the image"""
    for i in range(len(classes)):
        class_ind = classes[i]
        mask = masks[i]
        color = all_colors[class_ind]
        applyMask(image, mask, color)
    return image

def readStereoOriginalPcl(pcl_dir, frame_i):
    """
    Function:
        Read the stereo point cloud w.r.t current frame
    
    Args:
        stereo_pcl_dir          ->          stereo point cloud directory
        frame_i                 ->          current frame to be read
    """
    if os.path.exists(os.path.join(pcl_dir, "initial_" + str(frame_i) + ".npy")):
        original_pcl = np.load(os.path.join(pcl_dir, "initial_" + str(frame_i) + ".npy"))
        thresholed_pcl = np.load(os.path.join(pcl_dir, \
                            "organized_" + str(frame_i) + ".npy"))
    else:
        original_pcl = None
        thresholed_pcl = None
    return original_pcl, thresholed_pcl

def objectPcl(original_pcl, mask):
    """
    Function:
        apply mask onto the original pcl to find the object.
    """
    mask = np.expand_dims(mask, -1)
    object_pcl = np.where(mask == 1, original_pcl, 0)
    return object_pcl

def transferToScatterPoints(points_3d):
    """
    Function:
        Transfer the 3d matrices pcl to 2d matrices pcl

    Args:
        points_3d           ->          target 3d points
    """
    non_zero_points = points_3d.copy()
    non_zero_mask = non_zero_points[..., -1] > 0
    non_zero_points = non_zero_points[non_zero_mask]
    return non_zero_points

def checkoutDir(directory):
    """ Function: checkout if dir exists. if yes, clear all files
    if not, makedir"""
    if not os.path.exists(directory):
        os.mkdir(directory)
    else:
        for _ in glob.glob(os.path.join(directory, "*")):
            os.remove(_)

def main(input_dir, save_dir, pcl_save_dir, stereo_pcl_dir, mrcnn_save, pcl_save, \
        if_plot):
    """ MAIN FUNCTION """
    input_prefix = "stereo_input_"
    checkoutDir(save_dir)
    checkoutDir(pcl_save_dir)
    if if_plot:
        fig = plt.figure()
        ax1 = fig.add_subplot(121)
        ax2 = fig.add_subplot(122)

    all_colors = randomColors(len(ALL_CLASSES))

    # model_name = "COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml"
    model_name = "COCO-InstanceSegmentation/mask_rcnn_X_101_32x8d_FPN_3x.yaml"

    cfg = get_cfg()
    cfg.MODEL.DEVICE='cpu'
    # add project-specific config (e.g., TensorMask) here if you're not running a model in detectron2's core library
    cfg.merge_from_file(model_zoo.get_config_file(model_name))
    cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.5  # set threshold for this model
    # Find a model from detectron2's model zoo. You can use the https://dl.fbaipublicfiles... url as well
    cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url(model_name)
    predictor = DefaultPredictor(cfg)

    for ind in tqdm(range(len(glob.glob(os.path.join(input_dir, "*.jpg"))))):
        image_name = glob.glob(os.path.join(input_dir, "*.jpg"))[ind]
        frame_i = getSequenceNum(input_dir, image_name)
        img = cv2.imread(image_name)
        if img is None:
            continue
        
        outputs = predictor(img)

        # We can use `Visualizer` to draw the predictions on the image.
        # v = Visualizer(im[:, :, ::-1], MetadataCatalog.get(cfg.DATASETS.TRAIN[0]), scale=1.2)
        # out = v.draw_instance_predictions(outputs["instances"].to("cpu"))
        # all_classes = v.metadata.thing_classes

        classes = outputs["instances"].pred_classes.cpu().numpy()
        pred_masks = outputs["instances"].pred_masks.cpu().numpy()
        masks = pred_masks.astype(np.float)
        masked_image = maskInstances(img[..., ::-1], masks, classes, all_colors)
        cv2.imwrite(os.path.join(save_dir, "stereo_mrcnn_img_%.d.jpg"%(frame_i)), \
                    masked_image[:, :, ::-1])
        if len(classes) == 0:
            continue
        if mrcnn_save:
            ################# mrcnn images and dictionary #############
            mrcnn_dict = {}
            mrcnn_dict["masks"] = masks
            mrcnn_dict["classes"] = classes
            with open(os.path.join(save_dir, "stereo_mrcnn_" + str(frame_i) + ".pickle"),\
                    "wb") as fp:
                pickle.dump(mrcnn_dict, fp)
        stereo_obj_pcl_dict = {'mask': [], 'class': [], 'pcl':[]}
        stereo_origin_pcl, _ = readStereoOriginalPcl(stereo_pcl_dir, frame_i)
        if stereo_origin_pcl is None:
            print("Current frame no corresponding stereo point cloud")
            continue
        if if_plot:
            ax1.clear()
            ax2.clear()
        for obj_i in range(len(classes)):
            current_class_idx = classes[obj_i]
            current_class = ALL_CLASSES[current_class_idx]
            current_mask = masks[obj_i]
            if current_class in DESIGNATED_CLASSES:
                stereo_obj_pcl = objectPcl(stereo_origin_pcl, current_mask)
                stereo_obj_pcl_3d = transferToScatterPoints(stereo_obj_pcl)
                stereo_obj_pcl_dict["mask"].append(current_mask)
                stereo_obj_pcl_dict["class"].append(current_class)
                stereo_obj_pcl_dict["pcl"].append(stereo_obj_pcl_3d)
                if if_plot:
                    current_color = np.expand_dims(np.array(\
                                all_colors[current_class_idx]), axis=0)
                    ax2.scatter(stereo_obj_pcl_3d[:, 0], stereo_obj_pcl_3d[:,2], \
                                s=0.5, c=current_color, label=current_class)
        if if_plot:
            ax1.imshow(masked_image)
            ax2.set_xlim([-50, 50])
            ax2.set_ylim([0, 50])
            fig.canvas.draw()
            plt.pause(0.1)
        if pcl_save:
            ################### stereo pcl w.r.t mrcnn #################
            with open(os.path.join(pcl_save_dir, "stereo_obj_pcl_%.d.pickle"%(frame_i)), \
                    "wb") as ff:
                pickle.dump(stereo_obj_pcl_dict, ff)

if __name__ == "__main__":
    input_dir = "./stereo_input/"
    save_dir = "./stereo_object_detection_output/"
    pcl_save_dir = "./stereo_object_pcl/" 
    stereo_pcl_dir = "../stereo_process/pcl/"
    mrcnn_save = False
    pcl_save = True
    if_plot = False
    main(input_dir, save_dir, pcl_save_dir, stereo_pcl_dir, mrcnn_save, pcl_save, if_plot)
