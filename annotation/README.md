# Auto-annotation for Radar

## 1. Stereo object detection

### Get input images
To get the wanted input images, change the sequence number in 
```
stereo_object_detection_input.py
```
and run it.

**Note:** The output images will be saved in directory `stereo_input`.

### Object Detection Using Detectron2
To get the detectron2 outputs, run 
```
stereo_detectron2.py
```

**Note:** The output masks with masked images will be saved in directory `stereo_object_detection_ouput` and the corresponding stereo point cloud will be saved in directory `stereo_object_pcl`.

## [ALTERNATIVE] RAD Masks Annotation

### Annotate radar masks for Range-Azimuth-Doppler matrices.
To annotate radar RAD masks, first run
```
frame_delay_checkout.py
```
to find out the frame delay between radar frame and stereo frame. This step requires pre-run `read_radar_csv.py` in directory `radar_process/radar_ral_process`.

Once frame delay confirmed, run
```
annotate_radar_RAD.py
```
for annotating the RAD masks.

**Note:** all the annotation results (along with figure samples) are store in the directory `radar_annotation`.

**TODO:** Tune all the parameters.

## [ALTERNATIVE] Radar Points Annotation

### Annotate radar points and doppler range figures
To annotate radar data, run 
```
annotate_radar_pcl.py
```

**Note:** annotated samples are saved in the directory `annotation_samples`, the annotation dictionary for radar is saved in `radar_annotation`.

**TODO:** 
- Tune the thresholds to get the better results.

## [OTHER_SOURCES]

### Object Detection Using Mask-RCNN (on Razor computer)

First, copy all files from `src_other/original_mrcnn` to `./`, then run the followings.

To get the mask rcnn outputs, run 
```
stereo_mrcnn.py
```
on Razor computer.

**Note:** The output masks with masked images will be saved in directory `stereo_object_detection_ouput`.

To get the output pcl, run 
```
stereo_obj_pcl.py
```

**Note:** all the point cloud info will be saved in the directory `stereo_object_pcl`. Note the format of the files are {'class': class-strings, 'pcl': points-(num, 3)}.

### RA-Cartesian points box refinement

First, copy all files from `src_other/box_regression` to `./`, then run the followings.

To get the box regression of the RA Cartesian output, run
```
RAD_box_regression.py
```
