import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
import cv2
import pickle
import colorsys

from sklearn import mixture
from sklearn.cluster import DBSCAN
from collections import Counter

from glob import glob
from tqdm import tqdm

def RandomColors(N, bright=True):
    """
    Generate random colors.
    To get visually distinct colors, generate them in HSV space then
    convert to RGB.
    """
    brightness = 1.0 if bright else 0.7
    hsv = [(i / N, 1, brightness) for i in range(N)]
    colors = list(map(lambda c: colorsys.hsv_to_rgb(*c), hsv))
    # random.shuffle(colors)
    return colors

def ReadStereoObjPcl(stereo_obj_pcl_dir, frame_i):
    """
    Function:
        Read the stereo object detection point cloud w.r.t frame i

    Args:
        stereo_obj_pcl_dir          ->          stereo point cloud directory
        frame_i                     ->          frame id

    Attention:
        The output stereo_obj_pcl has two keys, 
        "class" : current object class string name 
        "pcl" : current object point cloud with format (num_pnts, 3) 
    """
    filename = stereo_obj_pcl_dir + "stereo_obj_pcl_" + str(frame_i) + ".pickle"
    if os.path.exists(filename):
        with open(filename, "rb") as f:
            stereo_obj_pcl = pickle.load(f)
        if len(stereo_obj_pcl['class']) == 0:
            stereo_obj_pcl = None
    else:
        stereo_obj_pcl = None
    return stereo_obj_pcl

def ReadStereoMrcnnImg(stereo_mrcnn_img_dir, frame_i):
    """
    Function:
        Read mask rcnn results on stereo left img
    
    Args:
        stereo_mrcnn_img_dir            ->          stereo mask rcnn result imgs
        frame_i                         ->          frame id
    """
    filename = stereo_mrcnn_img_dir + "stereo_mrcnn_img_" + str(frame_i) + ".jpg"
    if os.path.exists(filename):
        img = cv2.imread(filename)[..., ::-1]
    else:
        img = None
    return img
    
def ReadRadarPcl(radar_pcl_dir, frame_i):
    """
    Function:
        Read the radar pcl w.r.t frame i 

    Args:
        radar_pcl_dir           ->          radar point cloud directory
        frame_i                 ->          frame id
    """
    filename = radar_pcl_dir + "radar_pcl_" + str(frame_i) + ".npy"
    if os.path.exists(filename):
        radar_pcl = np.load(filename)
        if len(radar_pcl) == 0:
            radar_pcl = None
    else:
        radar_pcl = None
    return radar_pcl

def WriteRadarObjPcl(radar_annotate_dir, radar_dict, frame_i):
    """
    Function:
        Write the annotated radar point cloud dictionary into the directory

    Args:
        radar_annotate_dir          ->          directory for saving
        frame_i                     ->          frame id
    """
    with open(radar_annotate_dir + "radar_obj_" + str(frame_i) + ".pickle", "wb") as f:
        pickle.dump(radar_dict, f)

def ReadRegistrationMatrix(registration_matrix_dir):
    """
    Function:
        Read the calibration matrix

    Args:
        registration_matrix_dir             ->          registration matrix directory
    """
    filename = registration_matrix_dir + "calibration_matrix.npy"
    if os.path.exists(filename):
        registr_mat = np.load(filename)
    else:
        raise ValueError("Pleaaaaaaase calibrate the sensors before you do this")
    return registr_mat

def GaussianModel(pcl):
    """
    Function:
        Get the center and covariance from gaussian model.

    Args:
        pcl             ->          point cloud data
    """
    model = mixture.GaussianMixture(n_components=1, covariance_type='full')
    model.fit(pcl)
    return model.means_[0], model.covariances_[0]

def DrawEllipse(means, covariances, scale_factor=1):
    """
    Function:
        Draw 2D Gaussian Ellipse.

    Args:
        means           ->          center of the Gaussian
        covariances     ->          covariance of Gaussian
    """
    eigen, eigen_vec = np.linalg.eig(covariances)
    eigen_root_x = np.sqrt(1 + eigen[0]) * scale_factor
    eigen_root_y = np.sqrt(1 - eigen[1]) * scale_factor
    ell = Ellipse(xy = (means[0], means[1]), width = eigen_root_x,
                height = eigen_root_y, angle = np.rad2deg(np.arccos(eigen_vec[0,0])),
                facecolor = 'none', edgecolor = 'red')
    return ell

def DbscanDenoise(pcl, epsilon=0.3, minimum_samples=100):
    """
    Function:
        Using DBSCAN for filtering out the noise data.

    Args:
        pcl             ->          point cloud to be denoised
        epsilon         ->          maximum distance to be considered as an object
        minimum_samples ->          miminum points to be considered as an object
    """
    clustering = DBSCAN(eps=epsilon, min_samples=minimum_samples).fit(pcl)
    output_labels = clustering.labels_
    output_pcl = []
    for label_i in np.unique(output_labels):
        if label_i == -1:
            continue
        output_pcl.append(pcl[output_labels == label_i])
    return output_pcl

def DbscanPrimeClusterMask(pcl, epsilon=0.3, minimum_samples=300):
    """
    Function:
        Using DBSCAN for filtering out the noise data.

    Args:
        pcl             ->          point cloud to be denoised
        epsilon         ->          maximum distance to be considered as an object
        minimum_samples ->          miminum points to be considered as an object
    """
    clustering = DBSCAN(eps=epsilon, min_samples=minimum_samples).fit(pcl)
    output_labels = clustering.labels_
    counts = Counter(output_labels)
    mask = output_labels == counts.most_common(1)[0][0]
    return mask

def AddonesToLstCol(target_array):
    """
    Function:
        Add ones to the last column of the target array

    Args:
        target_array            ->          array to be changed
    """
    adding_ones = np.ones([target_array.shape[0], 1])
    output_array = np.concatenate([target_array, adding_ones], axis=-1)
    return output_array

def CalibrateStereoToRadar(stereo_pcl, registration_matrix):
    """
    Function:
        Transfer stereo point cloud to radar frame.
    
    Args:
        stereo_pcl              ->          stereo point cloud
        registration_matrix     ->          registration matrix 
    """
    return np.matmul(AddonesToLstCol(stereo_pcl), registration_matrix)

def EuclideanDist(point1, point2):
    """
    Function:
        Calculate EuclideanDist
    """
    return np.sqrt(np.sum(np.square(point1 - point2), axis=-1))

def FindRadarObjPoint(stereo_pcl, radar_pcl, distance_threshold=1.):
    """
    Function:
        Use calibrated stereo points to find the corresponding radar points

    Args:
        stereo_pcl          ->          calibrated stereo 2d point cloud 
        radar_pcl           ->          radar 2d point cloud
        distance_threshold  ->          threshold used for first filtering
    """
    stereo_mean, stereo_cov = GaussianModel(stereo_pcl)
    radar_pcl_xz = radar_pcl[:, :2]
    ##### first filtering #####
    all_ditances = EuclideanDist(radar_pcl_xz, np.expand_dims(stereo_mean, 0))
    obj_pcl_mask = all_ditances <= distance_threshold
    radar_obj_pcl = radar_pcl[obj_pcl_mask]
    if len(radar_obj_pcl) > 1:
        ##### second filtering #####
        radar_obj_dp_rg_index = np.delete(radar_obj_pcl[:, 3:6], 1, -1)
        dp_rg_mask = DbscanPrimeClusterMask(radar_obj_dp_rg_index, epsilon=10, minimum_samples=50)
        radar_obj_pcl = radar_obj_pcl[dp_rg_mask]
        radar_obj_dp_rg = radar_obj_dp_rg_index[dp_rg_mask]
    else:
        radar_obj_pcl = None
        radar_obj_dp_rg = None
    return radar_obj_pcl, radar_obj_dp_rg

def applyMask(image, mask, color, alpha=0.5):
    """
    Apply the given mask to the image.
    """
    for c in range(3):
        image[:, :, c] = np.where(mask == 1,
                                 image[:, :, c] *
                                (1 - alpha) + alpha * color[c] * 255,
                                 image[:, :, c])
    return image

def GetDpRgMask(radar_ral_dir, dp_rg_index, frame_i):
    """
    Function:
        Get doppler range mask

    Args:
        radar_ral_dir           ->          radar processing results directory
        dp_rg_mask              ->          doppler range mask
        frame_i                 ->          frame id
    """
    png_filename = radar_ral_dir + "doppler_range/" + "%.6d.png"%(frame_i)
    if os.path.exists(png_filename):
        dp_rg_img = cv2.imread(png_filename)
        dp_rg_obj_mask = np.zeros(shape=(dp_rg_img.shape[0], dp_rg_img.shape[1]))
        for i in range(len(dp_rg_index)):
            doppler_ind, range_ind = dp_rg_index[i]
            range_ind = dp_rg_img.shape[0] - range_ind
            dp_rg_obj_mask[int(range_ind), int(doppler_ind)] = 1
    else:
        dp_rg_obj_mask = None
    return dp_rg_obj_mask

def DpRgPutOnAllMasks(radar_ral_dir, masks, frame_i, color_list):
    """
    Function:
        Put mask onto the doppler range images

    Args:
        radar_ral_dir           ->          radar processing results directory
        dp_rg_mask              ->          doppler range mask
        frame_i                 ->          frame id
        color                   ->          color to be drawn
    """
    png_filename = radar_ral_dir + "doppler_range/" + "%.6d.png"%(frame_i)
    if os.path.exists(png_filename):
        dp_rg_img = cv2.imread(png_filename)[..., ::-1]
        masked_img = dp_rg_img
        for ind in range(masks.shape[-1]):
            mask_ind = masks[..., ind]
            color_ind = color_list[ind][0]
            masked_img = applyMask(masked_img, mask_ind, color_ind)
    else:
        masked_img = None
    return masked_img

###################### PLOTTING FUNCTIONS START #########################
def prepareFigure(num_axes):
    assert num_axes <= 4
    plt.ion()
    fig = plt.figure()
    if num_axes == 1:
        ax1 = fig.add_subplot(111)
        return fig, ax1
    if num_axes == 2:
        ax1 = fig.add_subplot(121)
        ax2 = fig.add_subplot(122)
        return fig, ax1, ax2
    if num_axes == 3:
        ax1 = fig.add_subplot(131)
        ax2 = fig.add_subplot(132)
        ax3 = fig.add_subplot(133)
        return fig, ax1, ax2, ax3
    if num_axes == 4:
        ax1 = fig.add_subplot(221)
        ax2 = fig.add_subplot(222)
        ax3 = fig.add_subplot(223)
        ax4 = fig.add_subplot(224)
        return fig, ax1, ax2, ax3, ax4

def clearAxes(ax_list):
    assert len(ax_list) >=1 
    plt.cla()
    for ax_i in ax_list:
        ax_i.clear()

def createColorList(class_names, class_list, all_colors):
    color_output_list = []
    for i in range(len(class_list)):
        current_class = class_list[i]
        color_i = np.array([all_colors[class_names.index(current_class)]])
        color_output_list.append(color_i)
    return color_output_list
 
def createLabelList(class_list, label_prefix):
    label_list = []
    for i in range(len(class_list)):
        current_class = class_list[i]
        label_list.append(label_prefix + "_" + current_class)
    return label_list

def pointScatter(bg_points, point_list, color_list, label_list, ax, xlimits, ylimits, title):
    assert len(point_list) == len(color_list)
    assert len(point_list) == len(label_list)
    if bg_points is not None:
        ax.scatter(bg_points[:, 0], bg_points[:, 1], s=0.2, c='blue', label="background")
    for i in range(len(point_list)):
        points_i = point_list[i]
        color_i = color_list[i]
        label_i = label_list[i]
        ax.scatter(points_i[:, 0], points_i[:,1], s=0.3, c=color_i, label=label_i)
    ax.set_xlim(xlimits)
    ax.set_ylim(ylimits)
    ax.legend()
    ax.set_title(title)

def imgPlot(img, ax, title):
    ax.imshow(img)
    ax.axis('off')
    ax.set_title(title)

def keepDrawing(fig, time_duration):
    fig.canvas.draw()
    plt.pause(time_duration)

def saveFigure(save_dir, name_prefix, frame_i):
    plt.savefig(save_dir + name_prefix + str(frame_i) + ".png")
###################### PLOTTING FUNCTIONS END #########################
   
def main(stereo_obj_pcl_dir, stereo_mrcnn_img_dir,  radar_ral_dir, radar_pcl_dir, registration_matrix_dir, 
        sample_dir, radar_annotate_dir, sequence, if_plot=True, if_save=True):
    """
    MAIN FUNCTION
    """
    ##### frame delay between two sensors (not sure whether it exists) #####
    frame_delay = 13

    if if_plot:
        fig, ax1, ax2, ax3, ax4 = prepareFigure(4)

    designated_classes = ['person', 'bicycle', 'car', 'motorcycle', 'bus', 'train',  
            'truck', 'traffic light', 'fire hydrant', 'stop sign', 'parking meter']
    all_colors = RandomColors(len(designated_classes))
    registr_mat = ReadRegistrationMatrix(registration_matrix_dir)
    for frame_i in tqdm(range(sequence[0], sequence[1])):
        stereo_obj_pcl = ReadStereoObjPcl(stereo_obj_pcl_dir, frame_i + frame_delay) 
        radar_pcl = ReadRadarPcl(radar_pcl_dir, frame_i)
        radar_pcl_2d = np.delete(radar_pcl, 1, -1)
        ##### initializing all lists and radar dictionary #####
        radar_dict = {}
        radar_classes = []
        radar_obj_points_all = []
        radar_obj_dp_rg_masks = []
        stereo_corresponding_obj = []
        if stereo_obj_pcl is None or radar_pcl is None:
            print("Frame %d has no object detected."%(frame_i))
            continue
        ##### to see if there are valid objects shown in the radar frame #####
        has_pcl = False
        for class_i in range(len(stereo_obj_pcl['class'])):
            ##### initializing list for the current class #####
            radar_obj_pcl_current_class = []
            radar_obj_dp_rg_mask_current_class = []
            stereo_cor_current_class = []

            current_class = stereo_obj_pcl['class'][class_i]
            stereo_pcl = stereo_obj_pcl['pcl'][class_i]
            if len(stereo_pcl) == 0:
                continue
            has_pcl = True
            stereo_pcl_dbscan = DbscanDenoise(stereo_pcl, epsilon=0.5, 
                                                minimum_samples=300)
            ##### loop over each object of the current class #####
            for obj_i in range(len(stereo_pcl_dbscan)):
                current_stereo_obj = stereo_pcl_dbscan[obj_i]
                stereo_obj_to_radar = CalibrateStereoToRadar(current_stereo_obj, \
                                                            registr_mat)
                radar_obj_pcl, radar_obj_dp_rg = FindRadarObjPoint(stereo_obj_to_radar, \
                                                    radar_pcl_2d, distance_threshold=1.5)
                if radar_obj_pcl is None:
                    continue
                radar_obj_pcl_current_class.append(radar_obj_pcl)
                radar_obj_dp_rg_mask_current_class.append(radar_obj_dp_rg)
                stereo_cor_current_class.append(np.delete(current_stereo_obj, 1, -1))
            ##### if things detected, save them #####
            if len(radar_obj_pcl_current_class) != 0 and len(radar_obj_dp_rg_mask_current_class) != 0:
                radar_obj_dp_rg_mask_current_class = np.concatenate(radar_obj_dp_rg_mask_current_class, 0)
                radar_current_mask = GetDpRgMask(radar_ral_dir, radar_obj_dp_rg_mask_current_class, frame_i)
                radar_classes.append(current_class)
                radar_obj_points_all.append(np.concatenate(radar_obj_pcl_current_class, 0))
                radar_obj_dp_rg_masks.append(np.expand_dims(radar_current_mask, -1))
                stereo_corresponding_obj.append(np.concatenate(stereo_cor_current_class, 0))
        if not has_pcl:
            print("Frame %d has no object detected."%(frame_i))
            continue
        if len(radar_classes) != 0:
            radar_obj_dp_rg_masks = np.concatenate(radar_obj_dp_rg_masks, -1)
            radar_dict['classes'] = radar_classes
            radar_dict['pcl'] = radar_obj_points_all
            radar_dict['dprgmask'] = radar_obj_dp_rg_masks
            if if_plot:
                clearAxes([ax1, ax2, ax3, ax4])
                color_list = createColorList(designated_classes, radar_classes, all_colors)
                label_list_radar = createLabelList(radar_classes, "radar")
                label_list_stereo = createLabelList(radar_classes, "stereo")
                masked_dp_rg_img = DpRgPutOnAllMasks(radar_ral_dir, radar_obj_dp_rg_masks, \
                                                    frame_i, color_list)
                pointScatter(None, stereo_corresponding_obj, color_list, label_list_stereo, ax2, \
                            xlimits=[-15, 15], ylimits=[0,25], title="stereo points (Mask-RCNN)")
                pointScatter(radar_pcl_2d, radar_obj_points_all, color_list, label_list_radar, ax3, \
                            xlimits=[-15, 15], ylimits=[0,25], title="radar points annotation")
                imgPlot(masked_dp_rg_img, ax4, "doppler-range segmentation")
                imgPlot(ReadStereoMrcnnImg(stereo_mrcnn_img_dir, frame_i + frame_delay), ax1, "stereo Mask-RCNN")
                saveFigure(sample_dir, "annotation_samples_", frame_i)
                keepDrawing(fig, 0.1)
            if if_save:
                WriteRadarObjPcl(radar_annotate_dir, radar_dict, frame_i)
        
if __name__ == "__main__":
    stereo_mrcnn_img_dir = "./stereo_object_detection_output/"
    stereo_obj_pcl_dir = "./stereo_object_pcl/"
    radar_ral_dir = "/DATA/2020-06-01-14-44-13/ral_outputs_2020-06-01-14-44-13/"
    radar_pcl_dir = "../radar_process/radar_ral_process/radar_pcl/"
    registration_matrix_dir = "../registration/registration_matrix/"
    sample_dir = "./annotation_samples/"
    radar_annotate_dir = "./radar_annotation/"
    sequence = [0, 500]
    main(stereo_obj_pcl_dir, stereo_mrcnn_img_dir, radar_ral_dir, radar_pcl_dir, registration_matrix_dir, 
        sample_dir, radar_annotate_dir, sequence, if_plot=True, if_save=True)
